#include "LeakDetect.h"
#ifdef DETECT_LEAKS
#include <QDebug>
__leak_map __leaks;
__leak_map::~__leak_map(){
	for(QMap<void*, MemoryAllocInfo>::iterator it = this->begin(); it!=this->end(); ++it){
		qDebug()<<"leak"<<it->size<<"bytes at"<<it.key()<<" in function"<<it->function
			<<":"<<it->file<<", line"<<it->line;
	}
	qDebug()<<"maximum allocation ="<<max_bytes<<"   allocation ="<<total_bytes;
}

void __leak_map::insert(void *ptr, const MemoryAllocInfo &value){
	QMap<void*, MemoryAllocInfo>::iterator it = this->find(ptr);

	if(it!=this->end()){
		qDebug()<<"invalid allocation"<<ptr;
		return;
	}
	(*this)[ptr] = value;
	total_bytes += value.size;
	if(max_bytes < total_bytes && !report){
		if(!report)report = 10000;
		max_bytes = total_bytes;
	}
	if(report){
		if(!--report)qDebug()<<"maximum allocation size ="<<max_bytes;
	}
}

void __leak_map::remove(void *ptr){
	if(!ptr)return;
	QMap<void*, MemoryAllocInfo>::iterator it = this->find(ptr);
	if(it==this->end()){
	//	qDebug()<<"invalid deletion"<<ptr;
		return;
	}
	total_bytes -= it->size;
	this->erase(it);
}
#endif
