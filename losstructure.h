#ifndef LOSSTRUCTURE_H
#define LOSSTRUCTURE_H
#include <qglobal.h>
#include <QVector>
#include <QFile>
#include <QMap>
#include "Parsers/streamBase.h"
#include "losfragment.h"

class LosStructure{
	bool namesMod;
	QVector<LosProgInfo> index;            /// index[i] = position of program of i-th logic symbol;
	QVector<LosProgramFragment*> programs; /// programs[i] = program of logic symbol i; programs[0]=0;
	QVector<QString> names, initialNames;    /// names[i] = name of logic symbol with number i+1
	QMap<QString,uint> nameSym;/// nameSym[names[i]] = i+1;
	QMap<QString, int> translation;
	QVector<QVector<QString> > tr_table;
	QVector<QString> tr_types;
//	QMap<QStringList, LosProgramNode*> ruleNodes;
public:
	bool loadTranslations(const QString &fn);
	bool saveTranslations(const QString &fn)const;
	bool setTranslations(const QMap<QString, QString>& tr, const QString &from, const QString& lang, bool add = false);
	void translateSymbols(const QString &lang);
	bool namesModified()const{return namesMod;}
	uint nameToSymbol(const QString &str)const{
		QMap<QString,uint>::ConstIterator it = nameSym.find(str);
		if(it!=nameSym.end())return *it;
		return 0;
	}
	bool renameSymbol(uint n, const QString &s);
	const QVector<QString>& symbolNames()const{return names;}
	bool programToString(int i, QString &res);
	const LosProgramFragment* program(uint i)const{return i>=(uint)programs.size() ? 0 : programs[i];}
	LosStructure();
	const char *loadIndex(const QString &fn, OutputContext c = OutputContext());
	const char *readPrograms(const QString &dir, OutputContext c = OutputContext());
//	void updateRuleIndex();
	bool readNames(const QString &dir, OutputContext c = OutputContext());
	bool saveNames(const QString &dir, OutputContext c = OutputContext());
	~LosStructure();
	bool programToHString(int i, QString &res);
	QVector<QPair<QString,QString> > getTranstationTable(const QString &from, const QString &to);
};

#endif // LOSSTRUCTURE_H
