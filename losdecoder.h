#ifndef LOSDECODER_H
#define LOSDECODER_H
#include <QVector>
#include "losprogramtree.h"

struct LosMetaSymbol{
	enum Type{
		Sym, Var, Node, Label
	} tp;
	union{
		LosProgramNode* node;
		uint svnum;
	};
	LosMetaSymbol(){node=0;}
	LosMetaSymbol(Type t, uint n){tp = t; svnum = n;}
	LosMetaSymbol(bool symbol, uint n){
		if(symbol)tp=Sym; else tp=Var;
		svnum = n;
	}
	LosMetaSymbol(LosProgramNode* n){tp=Node; node = n;}
	bool isLabel()const{return tp==Label;}
	bool isSymbol()const{return tp==Sym;}
	bool isVar()const{return tp==Var;}
	bool isNode()const{return tp==Node;}
	LosProgramNode *toNode(){
		switch(tp){
		case Sym: return new LosProgramNode(LosProgramNode::LogSymbol, svnum);
		case Var: return new LosProgramNode(LosProgramNode::Variable, svnum);
		case Node: return node;
		default:break;
		}
		return 0;
	}
};

class LosDecoder{
public:
	typedef bool (*decoder)(uint, QVector<LosMetaSymbol> &);
	static bool decode(uint num, QVector<LosMetaSymbol> &st);
	static bool addDecoder(uint i, decoder d);
private:
	static QVector<decoder> decoders;
};

struct DecoderRegistrator{
	DecoderRegistrator(uint i, LosDecoder::decoder d);
	DecoderRegistrator(uint *i, uint n, LosDecoder::decoder d);
};

#define LOS_REGISTER_DECODER(decoder, number) \
	static DecoderRegistrator __los_decoder_register_##decoder(number,decoder);
#define LOS_REGISTER_DECODERS(decoder, numbers) \
	static uint __los_decoder_array_##decoder = numbers; \
	static DecoderRegistrator __los_decoder_register_##decoder( \
	__los_decoder_array_##decoder, sizeof(__los_decoder_array_##decoder)/sizeof(uint), decoder);

#endif // LOSDECODER_H


