#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "losstructure.h"
#include "losprogramtree.h"
#include "losinfoblock.h"
#include "losinfoviewer.h"

namespace Ui {
class MainWindow;
}
class QListWidgetItem;
class QUrl;
struct FBItem{
	QString sym;
	int pos;
};
struct FBStack{
	QList<FBItem> st;
	int c, max_sz;
	FBStack(){c=0;max_sz = 1000;}
	void push(const QString &s, int pos){
		if(c<st.size())st.erase(st.begin()+c, st.end());//st.remove(c,st.size()-c);
		else if(st.size()>=max_sz){st.pop_front();}
		st.push_back(FBItem());
		st.last().sym = s; st.last().pos = pos;c = st.size();
	}
};
class LosHighlighter;
class QTableWidgetItem;
class QTextEdit;
class MainWindow : public QMainWindow{
	Q_OBJECT
	LosStructure s;
	QMap<QString, int> initialNames;
	QMap<QString, int> leftSet, rightSet;
	QMap<QString, QString> currTr;
	QMap<QStringList, Rule> rules;
	QSet<QString> transliterated;
	QList<QListWidgetItem*> found;
	QList<uint> progfound;
	Qt::MatchFlags search_flags;
	LosProgramTree progtree;
	QVector<LosProgramTree> trees;
	QMap<QStringList, QVector<LosProgramNode*>> ruleNodes;
	FBStack fb;
	LosInfoBlock help;
	LosInfoBlock priem;
	QVector<LosInfoBlock> infoBlocks;
	LosInfoBlock taskBook;
	LosHighlighter *hl;
	uint currSymbol;
	bool searching;
	bool restart_search;
	bool stop_search;
	void setupHighlighter();
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
public slots:
	void translitAll();
	void readDir();
	void showProgram(QListWidgetItem *i1=0, QListWidgetItem *i2=0, bool push=true);
	void showPos(QListWidgetItem *, QListWidgetItem *);
	void saveHtml();
	void print();
	void renameSymbol(QListWidgetItem *);
	void saveNames();
	void saveNameMap();
	void saveAllProgrammes();
	void saveAllProblems(QString dirname=QString());
	void updateAllProgrammes();
	void savePriem();
	void loadRules();
	void readSettings();
	void writeSettings();

	void updateFragments();
	void updateRuleIndex();
	void findRules();
	void onRuleURL(const QUrl &url);

	void onHRef(const QUrl &u);
	void onTextDblClk();
	void onHelp(QTextEdit *snd=0);
	void showCurrentInfo();
	void startSearch(QString str = QString(), Qt::MatchFlags f = Qt::MatchFlags());
	void stopSearch();

	bool find();
	void findFirst();
	void findFirstProg(int mf = -1, bool fromStart = true, bool backw = false);
	void findNext();
	void findPrevious();
	void clearSelection();

	void setSearchCaseSensitive(bool cs);
	void setSearchContains();
	void setSearchWhole();
	void setSearchRegExp();
	void setSearchPrefix();
	void setSearchSuffix();

	void forward();
	void back();
	void updateTranslationTable();
	void addTranslation();
	void checkTranslationEnabled();
	void retranslate();
	void resetSymbolList();
	void insertTranslationRow(const QString &l, const QString &r, int num = -1);
	void onTrTextChanged(QTableWidgetItem *I);
protected:
	void closeEvent(QCloseEvent *);
private:
	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
