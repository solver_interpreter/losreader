#-------------------------------------------------
#
# Project created by QtCreator 2014-04-06T18:49:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport
include(Parsers/parsers.pri)
TARGET = los_reader
TEMPLATE = app
INCLUDEPATH += .

SOURCES += main.cpp\
        mainwindow.cpp \
    losstructure.cpp \
    losprogramtree.cpp \
    losfragment.cpp \
    losdecoder.cpp \
    losregistry.cpp \
    searchlinewidget.cpp \
    losinfoblock.cpp \
    losinfoviewer.cpp \
    LeakDetect.cpp \
    loshighlighter.cpp \
    translit.cpp \
    LosProblemInfo.cpp

HEADERS  += mainwindow.h \
    losstructure.h \
    losprogramtree.h \
    unicode.h \
    losfragment.h \
    losdecoder.h \
    losregistry.h \
    searchlinewidget.h \
    losinfoblock.h \
    losinfoviewer.h \
    LeakDetect.h \
    loshighlighter.h \
    translit.h \
    LosProblemInfo.h

FORMS    += mainwindow.ui \
    searchlinewidget.ui \
    losinfoviewer.ui

RESOURCES += \
    icons.qrc

win32{
RC_FILE = win.rc
OTHER_FILES += win.rc
}

UI_DIR = ./ui
