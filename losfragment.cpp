#include "losfragment.h"
#include <QByteArray>
#include <QFile>
#include <QDebug>

static QChar conversionRusToUnicode[256] = {// Russian letters are from ASCII encoding
/****     0      1      2      3      4      5      6      7      8      9      A      B      C      D      E      F
/*00*/ 0x0000,0x0001,0x0002,0x0003,0x0004,0x0005,0x0006,0x0007,0x0008,0x2209,0x000A,0x000B,0x000C,0x000D,0x000E,0x2286,
/*10*/  ' '  ,0x2264,0x2200,0x0000,0x2203,0x0000,0x0000,0x2265,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
/*20*/  ' '  , '!'  , '"'  ,0x0000,0x0000,0x2257, '&'  ,0x0000, '('  , ')'  , ' '  , '+'  , ','  , '-'  , '.'  ,0x0000,
/*30*/  '0'  , '1'  , '2'  , '3'  , '4'  , '5'  , '6'  , '7'  , '8'  , '9'  , ':'  , ';'  , '<'  , '='  , '>'  , '?'  ,
/*40*/ 0x0000, 'A'  , 'B'  , 'C'  , 'D'  , 'E'  , 'F'  , 'G'  , 'H'  , 'I'  , 'J'  , 'K'  , 'L'  , 'M'  , 'N'  , 'O'  ,
/*50*/  'P'  , 'Q'  , 'R'  , 'S'  , 'T'  , 'U'  , 'V'  , 'W'  , 'X'  , 'Y'  , 'Z'  , '['  , ' '  , ']'  ,0x0000,0x0000,
/*60*/ 0x0000, 'a'  , 'b'  , 'c'  , 'd'  , 'e'  , 'f'  , 'g'  , 'h'  , 'i'  , 'j'  , 'k'  , 'l'  , 'm'  , 'n'  , 'o'  ,
/*70*/  'p'  , 'q'  , 'r'  , 's'  , 't'  , 'u'  , 'v'  , 'w'  , 'x'  , 'y'  , 'z'  , '{'  ,0x0000, '}'  ,0x0000, ' '  ,
/*80*/ 0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,
/*90*/ 0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,
/*A0*/ 0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,
/*B0*/  '%'  ,0x03C0,0x212E, '@'  , '*'  , '$'  ,0x221A,0x0000,0x0000,0x0000,0x0000, '~'  , '#'  ,0x0000,0x0000,0x0000,
/*C0*/ 0x0000,0x0000,0x2216,0x221E,0x0000,0x2223,0x2115,0x0000,0x2219,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
/*D0*/ 0x0000,0x2124,0x0000,0x0000,0x0000,0x21D0,0x2212,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x21D2,0x2228,0x211A,
/*E0*/ 0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F,
/*F0*/ 0x2220,0x2225,0x22A5,0x0000,0x0000,0x2261,0x0000,0x0000,0x0000,0x2026,0x211D,0x0000,0x0000,0x0000,0x0000,0x0000
};
bool convertRusToUnicode(const char *str, QString &res, uint maxlen){
	if(!str||!*str)return false;
	uint i, n;
	for(n=1; n<maxlen; n++)
		if(!str[n])break;
	res.resize(n);
	for(i=0; i<n; i++){
		res[i] = conversionRusToUnicode[uchar(str[i])];
	}
	return true;
}
QMap<QChar,char> invSet(QChar x[256]){
	QMap<QChar,char> res;
	for(int i=0; i<256; i++)
		res[x[i]]=i;
	res[0]=0;
	return res;
}

bool convertUnicodeToOld(const QString &str, char *res, uint maxlen){
	static QMap<QChar,char> inv = invSet(conversionRusToUnicode);
	if(str.isEmpty())return false;
	uint n;
	maxlen = qMin(maxlen, (uint)str.size());
	for(n=0; n<maxlen; n++){
		res[n] = inv.value(str[n],'?');
	}
	if(n<maxlen)res[n]=0;
	return true;
}

OutStream& operator <<(OutStream&s, LosProgInfo i){
	if(!i.valid)return s<<"no program";
	return s<<"file "<<i.num<<", position "<<i.pos;
}

const char *LosProgramFragment::read(QMap<uint, LosProgramFragment*> & mp, const QByteArray &f, LosProgInfo in, OutputContext c){
	static int depth = 0;
	const int max_depth = 1000;
	hasErrors = true;
	//if(!depth)positions.clear();
	if(depth>max_depth){c.errors()<<"error: too high program depth "<<depth<<"\n"; return "too high program depth";}
	LosProgramPrefix prefix;
	uint ptr = in.pos;
	if(!mp.contains(in.pos) || (in.pos+7>(uint)f.size())){
		c.errors()<<"error in reading "<<(this->parent ? this->parent->logSym:0)<<
					" (depth = "<<depth<<", "<<in<<"):\n     invalid fragment offset "<<in.pos<<"\n";
		return "invalid fragment offset";
	}
	memcpy(&prefix, f.data()+ptr, 7);
//	qDebug()<<in.d<<in.valid<<in.num<<in.pos;
	this->valid = false;
	if(prefix.length<7|| prefix.length>prefix.nextFragShift || ptr+prefix.nextFragShift>(uint)f.size()){
		c.errors()<<"error in reading symbol "<<(this->parent ? this->parent->logSym:0)<<
					" (depth = "<<depth<<"): invalid prefix\n    "<<
					"length = "<<prefix.length<<", next fragment at "<<prefix.nextFragShift<<"\n";
		return "invalid fragment prefix";
	}
	ptr+=7;
	this->isRoot = (prefix.root!=0);
	if(this->parent&&this->isRoot){
		c.errors()<<"error in reading symbol "<<(this->parent ? this->logSym:0)<<
					" (depth = "<<depth<<"): reference to root fragment of symbol "<<prefix.logSymNum<<"\n";
					//"length = "<<prefix.length<<", next fragment at "<<prefix.nextFragShift<<"\n";
		return "invalid fragment prefix";
	}
	if(!this->isRoot){
		this->pstart = prefix.parentFragPos;
		if(!this->parent){
			c.warnings()<<"warning ("<<in<<"): in root fragment prefix root flag is false\n";
			return "initial fragment isn't a root fragment";
		}
		logSym = this->parent->logSym;
	}else {this->pstart = -1; logSym = prefix.logSymNum;this->parent = 0;}
	this->start = in.pos; this->end = prefix.nextFragShift+in.pos;
	this->info = in;
	//this->parent = 0;
	uint l = prefix.length-7;
	BitStream bs; bs.init(l);
	memcpy(bs.data(), f.data()+ptr, l);
	ptr+=l;
	LosProgItem li;
	commands.clear();
	while(li.read(bs)){
		this->commands<<li;
	}
	quint32 sh = 0;
	SubFragShift sf;
	uint ns = (prefix.nextFragShift-7-l)/3, i;
	subFragShifts.resize(ns);
	qDeleteAll(subFrags);
	subFrags.resize(ns); subFrags.fill(0);
	for(i=0; i<ns; i++){
		memcpy(&sh, f.data()+ptr, 3); ptr+=3;
		if(sh&(1<<23)){sf.real = false; sf.logSym = sh&0xFFFF;}
		else {sf.real = true;  sf.shift = sh;}
		subFragShifts[i] = sf;
	}
	hasErrors = false;
	for(i=0; i<ns; i++){
		if(!subFragShifts[i].real)continue;
		LosProgInfo inf = in; inf.pos = subFragShifts[i].shift;//+= subFragShifts[i].shift;
		if(!mp.contains(inf.pos) || (in.pos+7>(uint)f.size())){
			c.errors()<<"error in "<<this->logSym<<
						" (depth = "<<depth<<", "<<in<<"): invalid subfragment offset "<<inf.pos<<"\n";
			hasErrors = true;
			continue;
		}
		subFrags[i] = new LosProgramFragment;
		subFrags[i]->parent = this;
		++depth;
		subFrags[i]->read(mp, f, inf, c);
		if(subFrags[i]->hasErrors)hasErrors = true;
		--depth;
	}
	this->valid = true;
	return 0;
//	this->info.valid = false;
}

const char* loadLosIndex(const QString &fn, QVector<LosProgInfo>&index, OutputContext c){
	QFile f(fn);
	if(!f.open(QFile::ReadOnly))return "cannot open file";
	quint64 sz = f.size();
	if(sz%4){
		c.errors()<<"error in index loading: index file size = "<<sz<<"\n";
		return "invalid file size, it must be divisible by 4";
	}
	index.resize(sz/4+1);
	index[0].d = 0;
	f.read((char*)(index.data()+1), sz);
	f.close();
	return 0;
}

const char* number7(int n){
	static char s[4];
	s[0] = '0'+char((n>>6)&7);
	s[1] = '0'+char((n>>3)&7); s[2] = '0'+char(n&7);
	s[3]=0;
	return s;
}
