#ifndef SEARCHLINEWIDGET_H
#define SEARCHLINEWIDGET_H

#include <QWidget>

namespace Ui {
class SearchLineWidget;
}
class QLineEdit;
class SearchLineWidget : public QWidget
{
	Q_OBJECT
	Qt::MatchFlags search_flags;
public:
	explicit SearchLineWidget(QWidget *parent = 0);
	~SearchLineWidget();
	QString text()const;
	QLineEdit *edit();
	Qt::MatchFlags flags()const{return search_flags;}
public slots:
	void setText(const QString &s);
	void setSearchCaseSensitive(bool cs);
	void setSearchContains();
	void setSearchWhole();
	void setSearchRegExp();
	void setSearchPrefix();
	void setSearchSuffix();
	void setSearchFlags(Qt::MatchFlags f);
signals:
	void searchStringChanged(const QString&);
	void entered();
	void findPressed();
	void nextPressed();
	void prevPressed();
	void flagsChanged(Qt::MatchFlags);
private:
	Ui::SearchLineWidget *ui;
};

#endif // SEARCHLINEWIDGET_H
