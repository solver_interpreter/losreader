#ifndef LOSPROGRAMTREE_H
#define LOSPROGRAMTREE_H
#include <qglobal.h>
#include <QList>
#include <QVector>
//#include <QDebug>
//#include <QSet>
#include <QDataStream>
//#include <QTypeInfo>
#include "losstructure.h"
#include "losinfoviewer.h"

struct NameInfo{
	enum OutputType{
		PlainText,
		ForCopy,
		HTML,
		SymbolicUnicode
	};
	const QVector<QString> *symbolNames;
	const QVector<QString> *operatorNames;
	const QMap<QStringList, Rule> *rules = 0;
	OutputType format;

	NameInfo(){
		format = PlainText;
		symbolNames = 0;
		operatorNames = 0;
	}
	bool forCopy()const { return format == ForCopy; }
	bool plainText()const { return format == PlainText; }
	bool textFormat()const { return format == ForCopy || format == PlainText; }
	QString & printName(QString &str, int num)const;
	QString &printVariable(QString &s, uint vn, const char* x="x") const;
	QString printSymbol(uint unicode, const char *def) const;
};

struct PrintState{
	uint left : 16;
	uint right : 16;
	uint indent : 16;
	uint subTerm : 1;
	uint arithm : 1;
	PrintState(uint p=0){
		left = right = p;
		indent = 0;
		subTerm = 0;
		arithm = 0;
	}
	PrintState(uint l, uint r, uint i=0, uint subterm=0, uint ar = 0){
		left = l; right=r;
		indent=i;
		subTerm=subterm;
		arithm = ar;
	}
};

struct LosProgramNode{
	enum Type{
		Invalid,
		Variable,
		LogSymbol,
		Operator,
		Fragment
	};
	Type type;
	virtual void createRuleIndex(const NameInfo &/*names*/, QMap<QStringList, QVector<LosProgramNode*>>& /*res*/) {}
/*	struct counter{
		uint n0;
		QSet<LosProgramNode*> set;
		counter(){n0=0;}
		void add(LosProgramNode* nd){set<<nd;}
		void del(LosProgramNode* nd){set.remove(nd);
									 if(set.isEmpty())qDebug()<<++n0<<" : zero nodes";}
		~counter(){
			NameInfo ni; ni.format = NameInfo::PlainText;
			qDebug()<<set.count()<<"nodes:";
			foreach(LosProgramNode* n, set){
				QString str;
				qDebug()<<n->print(str, ni, PrintState());
			}
		}
	};
	static counter cnt;*/
	//-------------
	uint val;
	LosProgramNode *parent = 0;
	virtual void updateParent() {}
	LosProgramNode(Type t=Invalid, uint v=0){type = t; val = v; /*cnt.add(this);*/}
	virtual ~LosProgramNode(){/*cnt.del(this);*/}

	bool isVariable()const{return type == Variable;}
	bool isSymbol()const{return type==LogSymbol;}
	bool isOperator()const{return type==Operator;}
//	QString name;
	virtual QString& print(QString &str, const NameInfo &names, PrintState p)const;
	virtual QString& printOp(QString &str, const NameInfo &names, PrintState p)const { return print(str, names, p); }
	virtual QString& printUpper(QString &str, const NameInfo &names, const LosProgramNode * /*last*/ = 0)const {
		if (parent)parent->printUpper(str, names, this);
		return print(str, names, PrintState(0, 0, 0, 0, 0));
	}
};
struct EmptyWordNode: public LosProgramNode{
	EmptyWordNode(int n) : LosProgramNode(LogSymbol, n){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};

struct OperatorNode: public LosProgramNode{
	bool isExpr;
//	bool neg;//, includeNot;
	QVector<LosProgramNode*> args;
	void updateParent() { for (auto node : args)if (node) { node->parent = this; node->updateParent(); } }
	OperatorNode(uint num=0){type = Operator; this->val = num;isExpr = false; }
	virtual ~OperatorNode(){qDeleteAll(args);}
	virtual bool applyNot(){return false;}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
	QString& printOp(QString &str, const NameInfo &names, PrintState p)const;
	void createRuleIndex(const NameInfo &names, QMap<QStringList, QVector<LosProgramNode*>>& /*res*/);
	QString &printUpper(QString &str, const NameInfo &names, const LosProgramNode*)const;
};
struct ReturnNode: public OperatorNode{
	ReturnNode(uint num=0):OperatorNode(num){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct SymbolicNode: public OperatorNode{
	enum Operation{
		Invalid=0xFF,
		Identity = 0,
		BinaryMask = 0x40,
		NotMask = 0x80,
		Minus = 1,
		PtrValue, // operator *
		 //number binary operations
		Add = BinaryMask,  //folowing operators have numbers n|BinaryMask, where 0<=n<BinaryMask
		Subtract,
		Multiply,
		AddS,
		SubtractS,
		MultiplyS,
		Divide,
		AssignListItem,
		Apply,
		SetMinus, Union, Intersection,   //set binary operations
		ListMinus, ListUnion, ListIntersection, //list operations
		Pointer,  // operator [| |]
//		Concatenation, // list concatenation
		ElementOf,
		Contains,

		Subset,
		Superset,
		ListSubset,

		Equal,
		Less,
		LessS,
		LessEqual,
		NotElementOf = ElementOf| NotMask,
		NotContain   = Contains | NotMask,
		NotSubset    = Subset   | NotMask,
		NotSuperset  = Superset | NotMask,
		NotEqual     = Equal    | NotMask,
		GreaterEqual = Less     | NotMask,
		Greater      = LessEqual| NotMask,
		GreaterEqualS= LessS    | NotMask
	};
	uint o;
	//bool binary()const{return }
	SymbolicNode():OperatorNode(0){o = Invalid;}
	SymbolicNode(Operation op, uint num):OperatorNode(num){o=op;}
	bool applyNot();
	int arity()const{return o&BinaryMask ? 2 : 1;}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct ProgramFragment: public LosProgramNode{
	QString header;
	QVector<OperatorNode*> cmd;
	void updateParent() { for (auto node : cmd)if (node) { node->parent = this; node->updateParent(); } }
	ProgramFragment(){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
	virtual ~ProgramFragment(){qDeleteAll(cmd);}
	void createRuleIndex(const NameInfo &names, QMap<QStringList, QVector<LosProgramNode*>>& /*res*/);
	QString &printUpper(QString &str, const NameInfo &names, const LosProgramNode*)const;
};
struct CallIndexNode : public OperatorNode{
	CallIndexNode(int n) : OperatorNode(n){}
	~CallIndexNode(){}
};
struct LabelNode : public OperatorNode{
	LabelNode(int n) : OperatorNode(n){}
	~LabelNode(){}
};
struct ResultNode:public OperatorNode{
	ResultNode(int num):OperatorNode(num){}
	~ResultNode(){}
	QString& print(QString &str, const NameInfo&names, PrintState p)const;
	QString& print0(QString &str, const NameInfo&names, PrintState p)const;
};

struct ProgramHeader{
	enum CallType{
		Program,
		Symbol,
		CallIndex,
		NoType,
		Other
	};

	struct Call{
		int arity;
		CallType tp;
		uint symbol;
		Call(){tp = NoType;arity = 0;}
		bool operator<(const Call &c)const{
			return tp<c.tp || (tp==c.tp && symbol<c.symbol);
		}
		void print(QString &res, const NameInfo& names)const;
	};
	mutable QMap<Call, LosProgramNode*> calls;
	ProgramHeader(const ProgramHeader&h){calls.swap(h.calls);}
	ProgramHeader &operator=(const ProgramHeader& h){calls.swap(h.calls);return *this;}

	ProgramHeader(){}
	void setProgram(LosProgramNode *n, OutputContext c);
	~ProgramHeader(){
		qDeleteAll(calls);
	}
};

struct LosListNode:public OperatorNode{
	LosListNode(){isExpr = true;}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct LosIndexNode:public OperatorNode{
	bool left;
	LosIndexNode(bool l = true){isExpr= true;left = l;}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};

struct QuantifierNode:public OperatorNode{
	bool neg;
	enum Quantifier{
		Forall,
		Exists
	} qn;
	uint xb, xe;
	QuantifierNode(){xb=xe=0;neg = false;}
	QuantifierNode(Quantifier q, LosProgramNode *A, uint x1, uint xn){qn = q;if(A)args<<A;xb=x1;xe=xn;}
	bool applyNot(){
		//if(qn==Exists){neg=!neg; return true;}
		return false;
	}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct CommutAssocNode:public OperatorNode{
	enum Operation{
		And,
		Or,
		Concatenation
	};
	Operation o;
	CommutAssocNode(Operation op){o=op;}
	~CommutAssocNode(){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct MakeTermNode:public OperatorNode{
	MakeTermNode(int num):OperatorNode(num){}
	~MakeTermNode(){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
	QString& printOp(QString &str, const NameInfo &names, PrintState p)const;
};
struct LosNotOperator: public OperatorNode{
	LosNotOperator(LosProgramNode* n=0){
		if(n)this->args<<n;
	}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct IfElseNode:public OperatorNode{
	bool isExpr;
	IfElseNode(bool is_expr=false){isExpr=is_expr;}
	IfElseNode(LosProgramNode *cond, LosProgramNode *ifTrue, LosProgramNode *ifFalse=0, bool is_expr=false);
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct BranchNode:public OperatorNode{
	enum Operation{
		Branch,
		Else
	};
	Operation o;
	uint branch;
	BranchNode(Operation op, int br, int num):OperatorNode(num){ branch = br; o=op; }
	bool hasRef()const{return this->args.size()==1 && this->args[0];}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
	QString &printUpper(QString &str, const NameInfo &names, const LosProgramNode*) const;
	void createRuleIndex(const NameInfo &names, QMap<QStringList, QVector<LosProgramNode*>>& /*res*/);
};
struct CollectorOperator:public OperatorNode{
	enum Operation{
		Invalid=0,
		FindAll,
		SetOf,
		SumOfAll
	} o;
	uint x1, xn;
	CollectorOperator(){x1=xn=0;o=Invalid;}
	CollectorOperator(Operation op, uint xx1, uint xxn,uint num):OperatorNode(num){x1=xx1; xn=xxn;o = op;}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct CallToOperator:public OperatorNode{
	CallToOperator(uint num):OperatorNode(num){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
struct ReplaceOperator:public OperatorNode{
	ReplaceOperator(uint num=0):OperatorNode(num){}
	QString& print(QString &str, const NameInfo &names, PrintState p)const;
};
/*struct SimpleOperatorNode:public OperatorNode{
	QVector<OperatorNode*> args;
};*/

class LosProgramTree{
	//QVector<LosProgramNode*> prog;
	ProgramFragment* fr;
	ProgramHeader prog;
	ProgramFragment *oldFragmentToTree(const LosProgramFragment *f);
public:
	void print(QString &res, NameInfo names)const;
	bool print(ProgramHeader::Call c, int n, QString &res, NameInfo names)const;
	bool updateHeaders(int n, NameInfo names) const;

	const ProgramFragment *fragmentTree(){return fr;}
	const ProgramHeader& program(){return prog;}
	LosProgramTree();
	~LosProgramTree(){delete fr;}
//	bool fragmentToTree(const LosProgramFragment *f);
	bool decodeFragment(const LosProgramFragment *f);
	bool decodeProgram(const LosProgramFragment *f, OutputContext c=OutputContext());
};

#endif // LOSPROGRAMTREE_H
