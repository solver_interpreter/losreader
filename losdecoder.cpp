#include "losdecoder.h"
#include <QDebug>
#include "losregistry.h"

bool stdDecoder(uint num, QVector<LosMetaSymbol> &st){
	const LosSymbolInternalInfo& info = LosSymbolRegistry::symbolInfo(num);
	if(!info.properties.defined || info.properties.special)return false;
	bool far = info.properties.isFloating;
	OperatorNode *res = 0;
	if(info.properties.role == LosSymbolProperty::Verification ||
			info.properties.role ==  LosSymbolProperty::Enumerative){
		uint ar;
		if(far){
			if(st.isEmpty())return false;
			ar = st.last().svnum - 260; st.pop_back();
			if(ar > (uint)st.size())return false;
		}else ar = info.properties.arity;
		res = new OperatorNode(num);
		res->args.resize(ar);
		LosProgramNode **an = res->args.data();
		LosMetaSymbol *ms = st.data()+st.size()-ar;
		for(uint i=0; i<ar; i++)
			an[i] = ms[i].toNode();
		res->isExpr = info.properties.isExpresion;
		st.resize(st.size()-ar+1);
		st.last() = res;
		return true;
	}else if(info.properties.role == LosSymbolProperty::Auxiliary) return true;
	return true;
}


bool LosDecoder::decode(uint num, QVector<LosMetaSymbol> &st){
	if(num >= (uint)decoders.size() || !decoders[num])return stdDecoder(num, st);
	return decoders[num](num, st);
}

bool LosDecoder::addDecoder(uint i, LosDecoder::decoder d){
	if(!d){
		if(i >= (uint)decoders.size())return false;
		if(!decoders[i])return false;
		decoders[i] = d;
		return true;
	}
	if(i >= (uint)decoders.size()){
		uint j, n = decoders.size();
		decoders.resize(i+1);
		for(j=n; j<i; j++)decoders[j]=0;
	}else if(decoders[i])return false;
	decoders[i] = d;
	return true;
}

DecoderRegistrator::DecoderRegistrator(uint i, LosDecoder::decoder d){
	if(!LosDecoder::addDecoder(i, d))
		qDebug()<<"Decoder for number"<<i<<"already defined";
}

DecoderRegistrator::DecoderRegistrator(uint *i, uint n, LosDecoder::decoder d){
	for(uint j=0; j<n; j++){
		if(!LosDecoder::addDecoder(i[j], d))
			qDebug()<<"Decoder for number"<<i<<"already defined";
	}
}
QVector<LosDecoder::decoder> LosDecoder::decoders;

bool notDecoder(uint num, QVector<LosMetaSymbol>& st){
	if(st.isEmpty())return false;
	OperatorNode *op = new LosNotOperator(st.last().toNode());
	op->val = num;
	st.last() = op;
	return true;
}
/*bool orDecoder(uint num, QVector<LosMetaSymbol>& st){
	static uint n_or = LosSymbolRegistry::symbolNumber("or");
	static uint n_or_fst = LosSymbolRegistry::symbolNumber("or_start");
	static uint n_or_continue = LosSymbolRegistry::symbolNumber("or_continue");
	if(st.isEmpty())return false;
	OperatorNode *op = new LosNotOperator(st.last().toNode());
	op->val = num;
	st.last() = op;
	return true;
}
*/
