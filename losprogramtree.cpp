#include "losprogramtree.h"
#include "unicode.h"
#include "losdecoder.h"
#include <QHash>

static const PrintState AssignPriority(150,101);
static const PrintState OrPriority(200);
static const PrintState AndPriority(250);
static const PrintState ImplPriority(150);
static const PrintState QuantifierPriority(50);
static const PrintState NotPriority(500);
static const PrintState PtrValuePriority(1100);
static const PrintState IfElsePriority(100,99);
static const PrintState ComparePriority(400);
static const PrintState AddPriority(450);
static const PrintState MulPriority(600);
static const PrintState InPriority(300);
static const PrintState SubsetPriority(300);
static const PrintState SetMinusPriority(450);
static const PrintState ConcatPriority(430);
static const PrintState ApplyPriority(420,410);
static const PrintState IndexPriority(1500);

//LosProgramNode::counter LosProgramNode::cnt;

LosProgramTree::LosProgramTree(){
	fr = 0;
}

QString &LosProgramNode::print(QString &str, const NameInfo &names, PrintState p) const{
	QString s;
	if(type == Variable)return names.printVariable(str, val);
	else if(type == LogSymbol){
		if(p.arithm || (val<=280 && val>=259)){
			str += QString::number(int(val-260));
			if(names.forCopy())str+= uint(val-260)<10 ? "d" : "s";
			return str;
		}
		else{
			return names.printName(str, val);
		}
	}else if(type == Operator){
		if(names.operatorNames && (uint)names.operatorNames->size()>val)
			return str+=(*names.operatorNames)[val];
		s.sprintf("F%d", val);
	}else return str+="[invalid node]";
	return str+=s;
}
const int SymKontrolPriema = 401;
QString &OperatorNode::printUpper(QString &str, const NameInfo &names, const LosProgramNode*)const {
	if (parent)return parent->printUpper(str, names, 0);
	return print(str, names, {});
}

QString &OperatorNode::print(QString &str, const NameInfo &names, PrintState p)const{
/*	bool br = neg && (p.right>NotPriority.right);
	if(neg){
		if(br)str+="(";
		str+=names.printSymbol(Unicode::NotSign, "not ");
	}*/
	if(names.operatorNames && (uint)names.operatorNames->size()>this->val)
		str+=(*names.operatorNames)[this->val];
	else str+="F"+QString::number(this->val);
	if(args.size()){
		str+="(";
		for(int i=0; i<args.size(); i++){
			if(i)str+=", ";
			args[i]->print(str, names, PrintState(0,0,p.indent+1, 1));
		}
		str+=")";
	}
	if (!p.subTerm && names.rules && names.operatorNames && this->val == SymKontrolPriema) {
		if (args.size() == 3 && args[0]->isSymbol() && (args[1]->isSymbol() || dynamic_cast<LosListNode*>(args[1]))) {
			QStringList l;
			QString t;
			args[0]->printOp(t, names, PrintState(0,0,0,1,0));
			l << t;
			t.clear();
			if (args[1]->isSymbol()) {
				args[1]->printOp(t, names, {});
				l << t;
				t.clear();
			} else {
				LosListNode *n = static_cast<LosListNode*>(args[1]);
				for (LosProgramNode* arg : n->args) {
					if (!arg->isSymbol())return str;
					arg->printOp(t, names, {});
					l << t;
					t.clear();
				}
			}
			args[2]->printOp(t, names, {});
			t.remove(' ');
			l << t;
			str += ';';
			if (names.rules->contains(l)) {
				QByteArray ba;
				{
					QTextStream s(&ba);
					(*names.rules)[l].write(s, names.operatorNames);
				}
				for (const QByteArray &b : ba.split('\n'))
					str += '\n' + QString(p.indent, '\t') + "//\t" + QString::fromLocal8Bit(b);
			} else {
				str += '\n' + QString(p.indent, '\t') + "//\tcannot find rule : ";
				for (auto s : l)
					str += s + ' ';
			}
		}
	}
//	if(br)str+=")";
	return str;
}
QString &OperatorNode::printOp(QString &str, const NameInfo &names, PrintState p)const {
	if (names.operatorNames && (uint)names.operatorNames->size()>this->val)
		str += (*names.operatorNames)[this->val];
	else str += "F" + QString::number(this->val);
	if (args.size()) {
		str += "(";
		for (int i = 0; i<args.size(); i++) {
			if (i)str += ",";
			args[i]->printOp(str, names, PrintState(0, 0, p.indent + 1, 1));
		}
		str += ")";
	}
	return str;
}

void OperatorNode::createRuleIndex(const NameInfo &names, QMap<QStringList, QVector<LosProgramNode*>>& res) {
	if (this->val == SymKontrolPriema) {
		if (args.size() == 3 && args[0]->isSymbol() && (args[1]->isSymbol() || dynamic_cast<LosListNode*>(args[1]))) {
			QStringList l;
			QString t;
			args[0]->printOp(t, names, PrintState(0, 0, 0, 1, 0));
			l << t;
			t.clear();
			if (args[1]->isSymbol()) {
				args[1]->printOp(t, names, {});
				l << t;
				t.clear();
			} else {
				LosListNode *n = static_cast<LosListNode*>(args[1]);
				for (LosProgramNode* arg : n->args) {
					if (!arg->isSymbol())return;
					arg->printOp(t, names, {});
					l << t;
					t.clear();
				}
			}
			args[2]->printOp(t, names, {});
			t.remove(' ');
			l << t;
			res[l].push_back(this);
		}
	}
}

QString &QuantifierNode::print(QString &str, const NameInfo &names, PrintState p) const{
	if(args.size()<=0 || xb>xe)return str+="[invalid quantifier operator]";
	bool pt = !(names.format == NameInfo::HTML || names.format == NameInfo::SymbolicUnicode);
	bool ex = this->qn == Exists;
/*	if(!p.subTerm && names.format==NameInfo::SymbolicUnicode && xb==xe && qn == Forall &&
			this->args.size()==2 &&
			dynamic_cast<const SymbolicNode*>(this->args[0])&&
			dynamic_cast<const SymbolicNode*>(this->args[0])->args.size()==2&&
			dynamic_cast<const SymbolicNode*>(this->args[0])->args[0]->type == LosProgramNode::Variable&&
			dynamic_cast<const SymbolicNode*>(this->args[0])->args[0]->val == xb){
		str+= "for "; this->args[0]->print(str, names, PrintState(0,0,p.indent+1, 1));
		str+= " do";
		const CommutAssocNode* a = dynamic_cast<const CommutAssocNode*>(this->args[1]);
		if(a && a->o==CommutAssocNode::And){
			str+="{";
			for(int i=0; i<a->args.size(); i++){
				str+="\n"+QString(p.indent+1,'\t');
				a->args[i]->print(str, names, PrintState(0,0,p.indent+1,0));
			}
			str+="\n"+QString(p.indent,'\t')+"}";
		}else{
			str+="\n"+QString(p.indent+1,'\t');
			this->args[1]->print(str, names, PrintState(0,0,p.indent+1,0));
		}
		return str;
	}*/
	switch(names.format){
	case NameInfo::HTML:
//		str += "&#"+QString::number(this->qn == Forall ? Unicode::Forall : Unicode::Exists)+" ";
//		break;
	case NameInfo::SymbolicUnicode:
		str+=names.printSymbol(this->qn == Forall ? Unicode::Forall : neg ? Unicode::DoesntExist : Unicode::Exists, "")+" ";
//		(str += QChar(this->qn == Forall ? Unicode::Forall : Unicode::Exists))+=" ";
		break;
	default:
		//if(neg)str += "!";
		str += (ex ? "exists(" : "forall("); break;
	}
	names.printVariable(str, xb);// if(names.format==NameInfo::PlainText)str+=" ";
	if(pt){
		for(uint i = xb+1; i<=xe; i++){
			str+=", ";
			names.printVariable(str, i);
		}
		if(ex)str+=" : ";
		else str+=' ';
	}else{
		switch(xe-xb+1){
		case 4: if(names.format==NameInfo::PlainText)str+=" "; names.printVariable(str, xe-2);
		case 3: if(names.format==NameInfo::PlainText)str+=" "; names.printVariable(str, xe-1);
		case 2: if(names.format==NameInfo::PlainText)str+=" "; names.printVariable(str, xe);
		case 1:
			break;
		default:
			str+="..."; names.printVariable(str,xe);
		}
	}
	//else str+=") ";
	if(this->args.size()==1){
		if(!pt)str+="(";
		this->args[0]->print(str, names, PrintState(0, 0, p.indent+1, 1));
		str+=")";
	}else if(this->args.size()>1){
	/*	const SymbolicNode * sn = dynamic_cast<const SymbolicNode*>(this->args[0]);
		if(names.format==NameInfo::SymbolicUnicode && xb==xe && qn == Forall &&
				this->args.size()==2 && sn && sn->o == SymbolicNode::ElementOf&&
				sn->args.size()==2&&sn->args[0]->type == LosProgramNode::Variable&&
				sn->args[0]->val == xb){
			str+=names.printSymbol(Unicode::ElementOf, " in ");
			sn->args[1]->print(str,names,PrintState(InPriority.right, p.right, p.indent+1, 1));
			str+="(";
		}else*/{
			if(!pt)str+="(";
			str+=QString::fromLocal8Bit("if ");
			this->args[0]->print(str, names,PrintState(0,AndPriority.left,p.indent+1,1));
			for(int i=1; i<this->args.size()-1; i++){
				str+=" "+names.printSymbol(Unicode::LogicAnd,"&")+" ";
				this->args[i]->print(str, names,PrintState(AndPriority.right+1,AndPriority.left,p.indent+1,1));
			}
			str += QString::fromLocal8Bit(" then ");
			//str+=" "+names.printSymbol(Unicode::RightDoubleArrow,"=>") +" ";
		}
	//	if(pt)str+=" -> ";
	//	else if(names.format==NameInfo::SymbolicUnicode){str+=" "; str+=QChar(Unicode::RightArrow); str+=" ";}
	//	else str+=" &#"+QString::number(Unicode::RightArrow)+" ";
		this->args.last()->print(str, names, PrintState(0/*ImplPriority.right+1*/,0,p.indent+1,1));
		str+=")";
	}
	//if(pt)str+="";
	return str;
}

QString &IfElseNode::print(QString &str, const NameInfo &names, PrintState p) const{
	if(this->args.size()!=3)return str+="[invalid ifelse operator]";
	if(p.subTerm){
		bool br = (p.left>=IfElsePriority.left || p.right>IfElsePriority.right);
		if(br){str+="("; p.left=p.right=0;}
		this->args[0]->print(str, names, PrintState(p.left, IfElsePriority.left, p.indent+1, 1));
		str+=" ? ";
		this->args[1]->print(str, names, PrintState(0, 0, p.indent+1, 1));
		str+=" : ";
		this->args[2]->print(str, names, PrintState(IfElsePriority.right, p.right, p.indent+1, 1));
		if(br)str+=")";
	}else{
		str+= "if(";
		this->args[0]->print(str, names, PrintState(0,0,p.indent+1,1));
		str+=")";
		const CommutAssocNode* a = dynamic_cast<const CommutAssocNode*>(this->args[1]);
		const char *els = "else ";
		if(a && a->o==CommutAssocNode::And){
			els = "} else ";
			str+="{";
			for(int i=0; i<a->args.size(); i++){
				str+="\n"+QString(p.indent+1,'\t');
				a->args[i]->print(str, names, PrintState(0,0,p.indent+1,0));
				if(!str.endsWith(';')&&!str.endsWith('}'))
					str+=";";
			}
		}else{
			str+="\n"+QString(p.indent+1,'\t');
			this->args[1]->print(str, names, PrintState(0,0,p.indent+1,0));
			if(!str.endsWith(';')&&!str.endsWith('}'))
				str+=";";
		}
		if(this->args.size()>2){
			str+="\n"+QString(p.indent,'\t')+els;
			a = dynamic_cast<const CommutAssocNode*>(this->args[2]);
			if(a && a->o == CommutAssocNode::And){
				str+=" {";
				for(int i=0; i<a->args.size(); i++){
					str+="\n"+QString(p.indent+1,'\t');
					a->args[i]->print(str, names, PrintState(0,0,p.indent+1,0));
					if(!str.endsWith(';')&&!str.endsWith('}'))
						str+=";";
				}
				str+="\n"+QString(p.indent,'\t')+"}";
			}else{
				this->args[2]->print(str, names, PrintState(0,0,p.indent));
				if(!str.endsWith(';')&&!str.endsWith('}'))
					str+=";";
			}
		}
		//str+="\n"+QString(p.indent,'\t');
	}
	return str;
}

QString &BranchNode::printUpper(QString &str, const NameInfo &names, const LosProgramNode*) const {
	if (parent)parent->printUpper(str, names, this);
	return str;
}
QString &BranchNode::print(QString &str, const NameInfo &names, PrintState p) const{
//	if(o == Branch)str += "branch ";
//	else str+="IfFalse ";
	//names.printName(str, this->val);
	if(this->o == Branch)str+="branch ";
	else str+="if_false ";
	if(this->args.size()!=1)return str+="[invalid fragment "+QString::number(branch)+"]";
	str+="{\n"+QString(p.indent+1,'\t');
//	if(names.format==NameInfo::HTML)str+="<p>";
	this->args[0]->print(str, names, PrintState(0,0,p.indent+1));
	if(str[str.length()-1]==QChar('\t'))str.chop(1);
//	if(names.format==NameInfo::HTML)str+="</p>";
	str+="}\n"+QString(p.indent,'\t');
	//if(names.format==NameInfo::HTML)str+="<br>";
	return str;
}
void BranchNode::createRuleIndex(const NameInfo &names, QMap<QStringList, QVector<LosProgramNode*>>& res) {
	if (this->args.size() != 1)return;
	this->args[0]->createRuleIndex(names, res);
}

QString &CollectorOperator::print(QString &str, const NameInfo &names, PrintState p) const{
	if(this->args.size()!=2)return str+="[invalid enumerating expression]";
	bool pt = names.format == NameInfo::PlainText, fc = names.format==NameInfo::ForCopy;
	bool cof = o==SumOfAll && this->args[1]->isSymbol() && this->args[1]->val == 261;
	char beg='(',end = ')';
	if(cof){
		if(fc)
			str += QString::fromLocal8Bit("oldcountof(");
		else str += QString::fromLocal8Bit("countof(");
	}else{
		if (o == FindAll)beg = '[', end=']';
		else if (o == SetOf)beg = '{', end = '}';
		else if (o == SumOfAll)str += fc ? "oldsumof" : "sumof";
		else if (names.operatorNames && (uint)names.operatorNames->size()>this->val &&
				!(*names.operatorNames)[this->val].isEmpty())
			str+=(*names.operatorNames)[this->val];
		else {
			if(o == FindAll)str+="findall";
			else if(o==SetOf)str+="setof";
			else if(o == SumOfAll)str+=fc ? "oldsumofall" : "sumfoall";
			else return str+="[invalid enumerating expression]";
		}
		str += beg;// "(";
		//if(this->x1!=this->xn || this->args[1]->type != Variable || this->args[1]->val!=x1){
			this->args[1]->print(str, names,PrintState(0,0,p.indent+1,1));

			str+=QString::fromLocal8Bit(" where ");
		//}
	}
	//str+="(";
//	this->args[1]->print(str, names,PrintState(0,0,p.indent+1,1));
//	str+=" | ";
//	if(this->x1!=this->xn || this->args[1]->type != Variable || this->args[1]->val!=x1){
	if(pt||fc){
		const SymbolicNode* sn = dynamic_cast<const SymbolicNode*>(this->args[0]);
		const CommutAssocNode *c = 0;
		if(!sn){
			if((c = dynamic_cast<const CommutAssocNode*>(this->args[0]))!=0){
				if(c->o == CommutAssocNode::And){
					sn = dynamic_cast<const SymbolicNode*>(c->args[0]);
				}else c = 0;
			}
		}
		int excl = -1;
		if(!cof && sn && (sn->o == SymbolicNode::ElementOf || sn->o == SymbolicNode::Equal) &&
				sn->args[0]->isVariable() && sn->args[0]->val >= x1 && sn->args[0]->val <= xn){
			excl = sn->args[0]->val;
			sn->print(str, names, PrintState(0,0,p.indent+1,1));
		}else names.printVariable(str, x1);
		//if(cof)excl=-1;
		for(uint i = excl>=0 ? x1 : x1+1; i<=xn; i++){
			if(i!=excl){
				str+=", ";
				names.printVariable(str, i);
			}
		}
		if(excl<0 || (c && c->args.size()>1)){
			str+=" : ";
			if(c){
				for(int i = excl<0 ? 0 : 1; i<c->args.size(); i++){
					c->args[i]->print(str, names, PrintState(0,0,p.indent+1,1));
					if(i<c->args.size()-1)str+=", ";
				}
			}else this->args[0]->print(str, names, PrintState(0,0,p.indent+1,1));
		}
	}else{
		names.printVariable(str, x1);// if(names.format==NameInfo::PlainText)str+=" ";
		switch(xn-x1+1){
		case 4: if(names.format==NameInfo::PlainText)str+=" "; names.printVariable(str, xn-2);
		case 3: if(names.format==NameInfo::PlainText)str+=" "; names.printVariable(str, xn-1);
		case 2: if(names.format==NameInfo::PlainText)str+=" "; names.printVariable(str, xn);
		case 1:
			break;
		default:
			str+="..."; names.printVariable(str,xn);
		}
		str+=" : "; //QString::fromLocal8Bit(" �����, ��� ");
		this->args[0]->print(str, names,PrintState(0,0,p.indent+1,1));
	}
	str+=end;
	return str;
}
#include <QStack>
QString &ProgramFragment::printUpper(QString &str, const NameInfo &names, const LosProgramNode*n) const {
	if (!parent && !header.isEmpty()) {
		str += "\n"+header + "\n_____________________________________________\n\n";
	} else if (parent) {
		parent->printUpper(str, names, this);
	}
	for (int i = 0; i < cmd.size(); i++) {
		if (n == cmd[i])break;
		if (i + 1 < cmd.size() && cmd[i + 1] == n) {
			if (const BranchNode *bn = dynamic_cast<const BranchNode*>(cmd[i + 1])) {
				if (bn->o == BranchNode::Else) {
					str += "NOT ";
				}
			}
		}
		if (dynamic_cast<const BranchNode*>(cmd[i]))continue;
		const ResultNode *rn = dynamic_cast<const ResultNode*>(cmd[i]);
		if (rn) {
			str += "// define result:\n";
		}
		if (rn && i < cmd.size() - 1 && cmd[i + 1]->val == 17) {
			rn->print0(str, names, PrintState(0, 0, 0));
			continue;
		}
		cmd[i]->print(str, names, PrintState(0, 0, 0));
		int j;
		for (j = str.size(); j--;)
			if (str[j] != 0 && str[j] != ' '&&str[j] != '\t'&&str[j] != '\n')break;
		if (str[j] != ';'&&str[j] != '}')str += ";";
		if (names.format == NameInfo::HTML)str += "<br>";
		if (str[str.length() - 1] != '\t'&&str[str.length() - 1] != '\n')
			str += "\n";
	}
	return str;
}
QString &ProgramFragment::print(QString &str, const NameInfo &names, PrintState p) const{
	const ProgramFragment *fr = this;
	QStack<const ProgramFragment*> bs; bs << this;
	while(!bs.empty()){
		fr = bs.top();
		bs.pop();
		for(int i=0; i < fr->cmd.size(); i++){
			const BranchNode *bn = dynamic_cast<const BranchNode*>(fr->cmd[i]);
			const ProgramFragment*ffr = bn && bn->args.size() ?
						dynamic_cast<const ProgramFragment*>(bn->args[0]) : 0;
			if(ffr && bn->o==BranchNode::Branch){
				str+="branch {";
				bs.push(ffr);
				p.indent++;
			}else{
				const ResultNode *rn = dynamic_cast<const ResultNode*>(fr->cmd[i]);
				if(rn){
					str+="// define result:\n"+QString(p.indent,'\t');
				}
				if(rn && i<fr->cmd.size()-1 && fr->cmd[i+1]->val == 17){
					rn->print0(str, names, PrintState(0,0,p.indent));
					continue;
				}
				fr->cmd[i]->print(str,names,PrintState(0,0,p.indent));
				int j;
				for(j=str.size();j--;)
					if(str[j]!=0&&str[j]!=' '&&str[j]!='\t'&&str[j]!='\n')break;
				if(str[j]!=';'&&str[j]!='}')str+=";";
			}
			if(names.format==NameInfo::HTML)str+="<br>";
			if(str[str.length()-1]!='\t'&&str[str.length()-1]!='\n')
				str+="\n"+QString(p.indent,'\t');
		}
		p.indent--;
		if(!bs.empty()){
			str[str.size()-1]='}';
			if(names.format==NameInfo::HTML)str+="<br>";
			if(str[str.length()-1]!='\t'&&str[str.length()-1]!='\n')
				str+="\n"+QString(p.indent,'\t');
		}
	}
	return str;
}
void ProgramFragment::createRuleIndex(const NameInfo &names, QMap<QStringList, QVector<LosProgramNode*>>& res) {
	const ProgramFragment *fr = this;
	for (int i = 0; i < fr->cmd.size(); i++)
		cmd[i]->createRuleIndex(names, res);
}
QString &LosNotOperator::print(QString &str, const NameInfo &names, PrintState p) const{
	if(!this->args.size())return str+="[invalid operator not]";
	bool br = (p.right>NotPriority.right);
	if(br){str+="("; p.left = p.right=0;}
	str+=names.printSymbol(Unicode::NotSign, "!");
	this->args[0]->print(str, names, PrintState(qMax(p.left, NotPriority.right), p.right, p.indent,1));
	if(br)str+=")";
	return str;
}

QString &NameInfo::printName(QString & str, int val)const{
	//if(val==281)return str+="{ }";
	if(forCopy() && uint(val-260)<10){
		str += char('0'+val-260);
		return str += 'd';
	}
	if(symbolNames && symbolNames->size() > val)
		return str+=(*symbolNames)[val];
	return str+="Symbol("+QString::number(val)+")";
}

QString &NameInfo::printVariable(QString &s, uint vn, const char *x)const{
	s+=x;
	switch(format){
	case HTML:
		s += "<sub>";
		s += QString::number(vn);
		s += "</sub>";
		break;
	case SymbolicUnicode:/*{
		QChar s0[15];int i=15;
		//s0[14]=0;
		do {
			s0[--i] = QChar((vn%10)+0x2080);
			vn/=10;
		}while(vn);
		s+=QString(s0+i,15-i);
	}break;*/
	default:
		s += QString::number(vn);
	}
	return s;
}

QString NameInfo::printSymbol(uint unicode, const char *def) const{
	switch(format){
	case HTML: return "&#"+QString::number(unicode)+";";
	case SymbolicUnicode: return QString(unicode);
	default: break;
	}
	return def;
}

QString &CommutAssocNode::print(QString &str, const NameInfo &names, PrintState p) const{
	if(this->args.size()<=0)return str+="[empty argument list]";
	if(this->args.size()==1)return this->args[0]->print(str, names, p);
	PrintState op; int i;
	QString oper = " ";
	switch(o){
	case Concatenation: op = ConcatPriority;
		oper+="::"; break;
	case Or:  op=OrPriority;
		oper+=names.printSymbol(Unicode::LogicOr, "|");
		break;
	case And: op=AndPriority;
		oper+=names.printSymbol(Unicode::LogicAnd, "&");
		break;
	}
	oper+=" ";
	bool br = op.left<p.left || op.right<p.right;
	if(br){str+="(";p.left=p.right=0;}
	this->args[0]->print(str, names, PrintState(p.left, op.left, p.indent+1, 1));
	for(i=1; i<this->args.size()-1; i++){
		str+=oper;
		this->args[i]->print(str, names, PrintState(op.right,op.left, p.indent+1, 1));
	}
	str+=oper;
	this->args[i]->print(str, names, PrintState(op.right,p.right, p.indent+1, 1));
	if(br)str+=")";
	return str;
}

QString detectNumber(const LosProgramNode*const*v, int n, bool integers){
	int i=0,j=0;
	const int minus_num = 210;
	const int comma_num = 404;
	if(!n)return QString();
	QString res;res.reserve(n);
	if(v[0]->isSymbol()&&v[0]->val == minus_num){i=1; res+='-';}
	if(i==n || !v[i]->isSymbol()||uint(v[i]->val-260)>9)return QString();
	for(; i<n; i++){
		if(!v[i]->isSymbol())return QString();
		if(uint(v[i]->val-260)<=9)res+=QChar(v[i]->val-260+'0');
		else if(j || v[i]->val != comma_num)return QString();
		else {res+=QChar('.');j=1;}
	}
	if(res.length()==1||(!integers &&!j))return QString();
	return res;
}
QString &LosListNode::print(QString &str, const NameInfo &names, PrintState p) const{
	QString n = detectNumber(this->args.data(), this->args.size(), p.arithm || names.forCopy());
	if(!n.isEmpty()){
		str+=n;
		if(names.forCopy())str+="d";
		return str;
	}
	str+="[";
	for(int i=0; i<this->args.size(); i++){
		if(i)str+=", ";
		this->args[i]->print(str, names, PrintState(0,0,p.indent+2,1));
	}
	str+="]";
	return str;
}

QString &ReturnNode::print(QString &str, const NameInfo &names, PrintState p) const
{
	if(this->args.size()!=1)return str+="[invalid return node]";
	str += "return ";
	p.subTerm=1;
	return this->args[0]->print(str,names, p);
}

QString &SymbolicNode::print(QString &str, const NameInfo &names, PrintState p) const
{
	if(arity()!=this->args.size() && o!=Apply){
		return str+="[invalid operation]";
	}
	const char *sym=0;
	uint unicode = 0;
	PrintState ps;
	uint arithm = 0;
	bool pt = names.format == NameInfo::PlainText;
	bool sp=true;
	if(names.forCopy()){
		switch(o){
		case GreaterEqual:
		case GreaterEqualS:
			str+="!";
		case Minus:
		case Add:
		case Subtract:
		case Multiply:
		case MultiplyS:
		case AddS:
		case SubtractS:
		case Divide:
		case Less:
		case LessS:
		case Contains:
		case NotContain:
		case Subset:
		case NotSubset:
		case Superset:
		case NotSuperset:
		case ListSubset:
		case SetMinus:
		case Union:
		case Intersection:
		case ListUnion:
		case ListIntersection:
			p.arithm = 1;
			return OperatorNode::print(str, names, p);
		}
	}else if(o==Apply)
		return OperatorNode::print(str, names, p);
	switch(o){
	case Identity:      return args[0]->print(str, names, p);
	case NotMask:       unicode = Unicode::NotSign; sym = "not "; ps = NotPriority; break;
	case Minus:         unicode = Unicode::Minus; sym = "-"; ps = NotPriority; sp = false; break;
	case PtrValue:      unicode = '*'; sym = "*"; ps = PtrValuePriority; sp = false; break;
	case Add:           unicode = '+'; sym = "+"; ps = AddPriority;  arithm = 1; break;
	case Subtract:      unicode = Unicode::Minus; sym = "-"; ps = AddPriority; arithm = 1; break;
	case MultiplyS:      unicode = Unicode::AsteriskOperator; sym = "*"; ps = MulPriority; arithm = 1; break;
	case AddS:          unicode = Unicode::DotPlus;  sym = "+"; ps = AddPriority; arithm = 1; break;
	case SubtractS:     unicode = Unicode::DotMinus; sym = "-"; ps = AddPriority; arithm = 1; break;
	case Multiply:     unicode = '*'; sym = "*"; ps = MulPriority; arithm = 1; break;
	case Divide:        unicode = '/'; sym = "/"; ps = MulPriority; arithm = 1; break;
	case AssignListItem:unicode = Unicode::LeftArrowWithTail; sym = "<-"; ps=AssignPriority; sp = true; break;

	case Apply:        unicode = '@'; sym = "@"; ps = ApplyPriority; break;

	case Equal:        unicode = '='; sym = "="; ps = ComparePriority; break;
	case Less:         unicode = '<'; sym = "<"; ps = ComparePriority; arithm = 1; break;
	case Greater:      unicode = '>'; sym = ">"; ps = ComparePriority; arithm = 1; break;
	case LessS:        unicode = Unicode::DotLess;        sym = "<"; ps = ComparePriority; arithm = 1; break;
	case LessEqual:    unicode = Unicode::LessOrEqual;    sym = "<="; ps = ComparePriority; arithm = 1; break;
	case GreaterEqual: unicode = Unicode::GreaterOrEqual; sym = ">="; ps = ComparePriority; arithm = 1; break;
	case GreaterEqualS:unicode = Unicode::DotGreaterEqual;sym = ">=";ps = ComparePriority; arithm = 1; break;
	case NotEqual:     unicode = Unicode::NotEqual;       sym = "!="; ps = ComparePriority; break;

	case ElementOf:    unicode = Unicode::ElementOf;     sym = " in ";         ps = InPriority; break;
	case NotElementOf: unicode = Unicode::NotElementOf;  sym = " not_in ";     ps = InPriority; break;
	case Contains:     if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::Contains;      sym = " contains ";   ps = InPriority; break;
	case NotContain:   if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::DoesntContain; sym = " notcontain "; ps = InPriority; break;

	case Subset:       if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::Subset;        sym = " subsetof ";      ps = SubsetPriority; break;
	case NotSubset:    if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::NotSubset;     sym = " notsubsetof ";   ps = SubsetPriority; break;
	case Superset:     if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::Superset;      sym = " supersetof ";    ps = SubsetPriority; break;
	case NotSuperset:  if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::NotSuperset;   sym = " notsupersetof "; ps = SubsetPriority; break;

	case ListSubset:   if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::DotSubset;     sym = " listsubsetof ";  ps = SubsetPriority; break;

	case SetMinus:     if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::SetMinus;      sym = 0;  ps = SetMinusPriority; break;
	case Union:        if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::Union;         sym = 0;  ps = AddPriority; break;
	case Intersection: if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::Intersection;  sym = 0;  ps = MulPriority; break;
//	case ListMinus:        unicode = Unicode::SetMinus;         sym = 0;  ps = SetMinusPriority; break;
	case ListUnion:    if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::DotUnion;         sym = 0;  ps = AddPriority; break;
	case ListIntersection: if(pt)return OperatorNode::print(str, names, p);
		unicode = Unicode::DotIntersection;  sym = 0;  ps = MulPriority; break;
//	case Concatenation:    unicode = 0; sym = ":::"; ps = ConcatPriority; break;
	case Pointer:
		if(arity()!=2)return OperatorNode::print(str, names, p);
		this->args[0]->print(str, names, PrintState(p.left, qMax(p.right, IndexPriority.left), p.indent, 1));
		str+=names.printSymbol(Unicode::DoubleLeftBracket, "[|");
		this->args[1]->print(str, names, PrintState(0, 0, p.indent+1, 1));
		return str+=names.printSymbol(Unicode::DoubleRightBracket, "|]");
	default: return str+="[invalid operation]";
	}
	if((names.format==NameInfo::PlainText||!unicode) && !sym)
		return OperatorNode::print(str, names, p);
	bool br=false;
	if(arity()==1){
		br = (p.right>ps.right);
		if(br){str+="(";p.left=p.right=0;}
		if(unicode)str+=names.printSymbol(unicode, sym);
		else str+=sym;
		if(sp)str+=" ";
		this->args[0]->print(str, names, PrintState(qMax(p.left,ps.right), p.right, p.indent, 1, arithm));
	}else{ // then arity = 2
		br = (p.left>=ps.left || p.right>ps.right);
		if(br){str+="("; p.left=p.right=0;}
		this->args[0]->print(str, names, PrintState(p.left, ps.left, p.indent+1, 1, arithm));
		if(sp)str+=" ";
		if(unicode)str+=names.printSymbol(unicode, sym);
		else str+=sym;
		if(sp)str+=" ";
		this->args[1]->print(str, names, PrintState(ps.right, p.right, p.indent+1, 1, arithm));
	}
	if(br)str+=")";
	return str;
}

bool SymbolicNode::applyNot(){
	switch(o){
//	case NotElementOf:
//	case ElementOf:
//	case NotContain:
//	case Contains:
//	case NotSubset:
//	case Subset:
//	case NotSuperset:
//	case Superset:
	case NotEqual:
	case Equal:
	case Less:
	case LessS:
	case LessEqual:
	case Greater:
	case GreaterEqual:
	case GreaterEqualS:
	case NotMask:
	case Identity:
		o^=NotMask; return true;
//	default:
	}
	return false;
}

struct DecoderStack{
	LosMetaSymbol *p;
	int top;
	DecoderStack(){p=0; top = 0;}
	DecoderStack(LosMetaSymbol *ptr){p = ptr-1; top = 0;}

	bool pop(uint n=1){top -= n; return top >= 0;}
	bool del(){
		if(top>0){
			if(p[top].isNode())
				delete p[top].node; top--;
			return true;
		}
		return false;
	}
	LosMetaSymbol& last(){return p[top];}
	bool getLastNum(uint &res){
		if(top<=0 || !p[top].isSymbol())return false;
		res = p[top].svnum - 260;
		return true;
	}
	bool popNum(uint &res){
		if(top<=0 || !p[top].isSymbol())return false;
		res = p[top--].svnum - 260;
		return true;
	}
	bool popSymbol(uint &res){
		if(top<=0 || !p[top].isSymbol())return false;
		res = p[top--].svnum;
		return true;
	}
	bool popVar(uint &res){
		if(top<=0 || !p[top].isVar())return false;
		res = p[top--].svnum;
		return true;
	}
	bool getNum(int n, uint &res){
		if(top<n || !p[top-n+1].isSymbol())return false;
		res = p[top-n+1].svnum - 260;
		return true;
	}
	bool getVar(int n, uint &res){
		if(top<n || !p[top-n+1].isVar())return false;
		res = p[top-n+1].svnum;
		return true;
	}
	LosMetaSymbol& operator*(){return p[top];}
	LosMetaSymbol& operator [](int i){return p[i+1];}
	LosMetaSymbol& operator ()(int i){return p[top-i];}
	bool empty()const{return top<=0;}
	bool isValid(){return p&&top>=0;}
	bool replace(uint n, LosMetaSymbol s){
		if((top-=(n-1))<=0)return false;
		p[top]=s;
		return true;
	}
	bool convolve(uint n, OperatorNode *res){
		if(!n){p[++top] = res; return true;}
		if((top-=(n-1))<=0||!res){delete res; return false;}
		res->args.resize(res->args.size()+n);
		LosProgramNode **an = &res->args.last()-n+1;
		LosMetaSymbol *ms = p+top;
		for(uint i=0; i<n; i++)
			an[i] = ms[i].toNode();
		p[top] = res;
		return true;
	}
	DecoderStack& operator<<(LosMetaSymbol s){p[++top]=s;return *this;}
	DecoderStack part(){
		return DecoderStack(p+top+1);
	}
	void clear(){
		for(int i=1; i<=top; i++)if(p[i].isNode())delete p[i].node;
		top=0;
	}
};

IfElseNode::IfElseNode(LosProgramNode *cond, LosProgramNode *ifTrue, LosProgramNode *ifFalse, bool is_expr){
	isExpr=is_expr;
	this->args<<cond<<ifTrue; if(ifFalse)this->args<<ifFalse;
}
bool oldDecodeFragment(const LosProgItem *b, const LosProgItem *e, DecoderStack &st);

bool writeFragmentsRec(OperatorNode* n, const QVector<ProgramFragment*> &f, bool *b){
	if(!n)return true;
	if(BranchNode *bn = dynamic_cast<BranchNode*>(n)){
		if(bn->branch>(uint)f.size() || bn->args.size() || !f[bn->branch-1] || b[bn->branch-1])return false;
		bn->args<<f[bn->branch-1];
		b[bn->branch-1] = true;
		return true;
	}
	bool res = true;
	for(int i=0; i<n->args.size(); i++){
		res = res && writeFragmentsRec(dynamic_cast<OperatorNode*>(n->args[i]),f,b);
	}
	return res;
}

ProgramFragment* LosProgramTree::oldFragmentToTree(const LosProgramFragment *f){
	if(!f)return 0;
	uint i, n = f->commands.size(), fn = f->subFrags.size();
	QVector<LosMetaSymbol> st0(n);
	DecoderStack st(st0.data());
	const LosProgItem *p = f->commands.data();
	if(!oldDecodeFragment(p, p+n, st))return 0;
	for(i=0; i<(uint)st.top; i++)
		if(!st[i].isNode()||!dynamic_cast<OperatorNode*>(st[i].node)){st.clear();return 0;}
	QVector<ProgramFragment*> frags(fn);
	QVector<bool> fragUsed(fn);
	for(i=0; i<fn; i++)
		frags[i] = oldFragmentToTree(f->subFrags[i]);
	ProgramFragment *res = new ProgramFragment;
	res->cmd.resize(st.top);
	for(i=0; i<(uint)st.top; i++){
		res->cmd[i] = dynamic_cast<OperatorNode*>(st[i].node);
		writeFragmentsRec(res->cmd[i], frags, fragUsed.data());
	}
	return res;
}

void LosProgramTree::print(QString &res, NameInfo names) const{
	if(!fr)res+="[null fragment]";
	else fr->print(res, names,0);
}

bool LosProgramTree::print(ProgramHeader::Call c, int n, QString &res, NameInfo names) const{
	QMap<ProgramHeader::Call, LosProgramNode*>::const_iterator it = prog.calls.find(c);
	//if(c.tp == ProgramHeader::Program ? )
	if(it==prog.calls.end()){
		res="[null fragment]";
		return false;
	}else{
	//	res = "program for symbol "+QString::number(n)+"\n";
		res = "\n//**********************************************//\n";
		bool func = true, solve = false;//, op = false;
		switch(c.tp){
		case ProgramHeader::CallIndex:
			if(c.symbol == 260){res += "function "; func = true;}
			else {res+="info "; func = false;}
			break;
		case ProgramHeader::Program:
			func = true;
			res+="operator ";
			break;
		case ProgramHeader::Symbol:
			func = true;
			switch(c.symbol){
			case 11:
				res+="solve ";
				solve = true;
				break;
			case 32:
				res+="operator ";
				break;
			}
			break;
		case ProgramHeader::NoType:
			res+="default ";
		//default:
		}
		if(names.symbolNames && names.symbolNames->size() > n)
			res+=(*names.symbolNames)[n];
		else names.printName(res,n);
		//names.printName(res, n);
		res+="(";
		if(solve && names.forCopy()){
			for(int i=1; i<=5; i++){
				if(i>1)res+=", ";
				names.printVariable(res, i, "x");
			}
		}else{
			if(it.key().arity>0 && it.key().arity<=20){
				for(int i=1; i<=it.key().arity; i++){
					if(i>1)res+=", ";
					names.printVariable(res, i, "x");
				}
			}
		}
		res+=")";
		if(!func){
			res+=" : ";
			names.printName(res, c.symbol);
			res+=" ";
			//c.print(res, names);
		}
		res += "{\n\t";
		PrintState ps(0); ps.indent = 1;
		it.value()->print(res, names, ps);
		while(res[res.length()-1]=='\0'||res[res.length()-1]=='\t')
			res.chop(1);
		res += "}\n";
	}
	return true;
}

bool LosProgramTree::updateHeaders(int n, NameInfo names) const {
	//QMap<ProgramHeader::Call, LosProgramNode*>::const_iterator it = prog.calls.find(c);
	//if(c.tp == ProgramHeader::Program ? )
	for (auto it = prog.calls.begin(); it != prog.calls.end(); ++it) {
		ProgramHeader::Call c = it.key();
		ProgramFragment *frag = dynamic_cast<ProgramFragment*>(it.value());
		if (!frag)continue;
		QString res;
		if (it == prog.calls.end()) {
			res = "[unknown fragment]";
			//return false;
		} else {
			//	res = "program for symbol "+QString::number(n)+"\n";
			res = "\n//**********************************************//\n";
			bool func = true, solve = false;//, op = false;
			switch (c.tp) {
			case ProgramHeader::CallIndex:
				if (c.symbol == 260) { res += "function "; func = true; } else { res += "info "; func = false; }
				break;
			case ProgramHeader::Program:
				func = true;
				res += "operator ";
				break;
			case ProgramHeader::Symbol:
				func = true;
				switch (c.symbol) {
				case 11:
					res += "solve ";
					solve = true;
					break;
				case 32:
					res += "operator ";
					break;
				}
				break;
			case ProgramHeader::NoType:
				res += "default ";
				//default:
			}
			if (names.symbolNames && names.symbolNames->size() > n)
				res += (*names.symbolNames)[n];
			else names.printName(res, n);
			//names.printName(res, n);
			res += "(";
			if (solve && names.forCopy()) {
				for (int i = 1; i <= 5; i++) {
					if (i > 1)res += ", ";
					names.printVariable(res, i, "x");
				}
			} else {
				if (it.key().arity > 0 && it.key().arity <= 20) {
					for (int i = 1; i <= it.key().arity; i++) {
						if (i > 1)res += ", ";
						names.printVariable(res, i, "x");
					}
				}
			}
			res += ")";
			if (!func) {
				res += " : ";
				names.printName(res, c.symbol);
				res += " ";
				//c.print(res, names);
			}
		}
		frag->header = res;
	}
	return true;
}


/*
bool LosProgramTree::fragmentToTree(const LosProgramFragment *f){
	uint i, n = f->commands.size();
	const LosProgItem *p = f->commands.data();
	QVector<LosMetaSymbol> st;
	for(i=0; i<n; i++){
		if(p[i].type == LosProgItem::Variable)st<<LosMetaSymbol(false, p[i].varNum);
		else if(p[i].type == LosProgItem::LogSymbol)st<<LosMetaSymbol(true, p[i].logSymNum);
		else if(p[i].type == LosProgItem::Procedure){

		}
	}
	return true;
}
*/
bool LosProgramTree::decodeFragment(const LosProgramFragment *f){
	delete fr;
	fr=oldFragmentToTree(f);
	return fr!=0;
}

bool LosProgramTree::decodeProgram(const LosProgramFragment *f, OutputContext c){
	decodeFragment(f);
	prog.setProgram(fr, c);
	if(!fr)return false;
	fr=0;
	return true;
}

// conversion from old format
bool readStdOperator(uint num, DecoderStack &st, uint ar){
	if(ar==(uint)(-1)){
		if(!st.popNum(ar))return false;
	}
	OperatorNode*res = new OperatorNode(num);
	return st.convolve(ar, res);
}

LosMetaSymbol Label(uint l){return LosMetaSymbol(LosMetaSymbol::Label, l);}
LosMetaSymbol Symbol(uint l){return LosMetaSymbol(true, l);}
void clearStack(QVector<LosMetaSymbol> &st){
	uint i, n=st.size();
	for(i=0; i<n; i++)if(st[i].isNode())delete st[i].node;
}

bool oldDecodeOperator(DecoderStack &st, const LosProgItem *&b, const LosProgItem *e);
bool oldDecodeFragment(const LosProgItem *b, const LosProgItem *e, DecoderStack &st){
	//uint n = b-e;
	for(; b<e; ){
		if(b->type == LosProgItem::Variable)st<<LosMetaSymbol(false, (b++)->varNum);
		else if(b->type == LosProgItem::LogSymbol)st<<LosMetaSymbol(true, (b++)->logSymNum);
		else if(b->type == LosProgItem::Procedure){
			if(!oldDecodeOperator(st, b, e)){
				st.clear();
				return false;
			}
		}
	}
	return true;
}

LosProgramNode *waitOperator(const LosProgItem *b, const LosProgItem *e, DecoderStack st){
	oldDecodeFragment(b,e,st);
	if(st.top==1)return st[0].toNode();
	st.clear();
	return 0;
}

bool oldDecodeOperator(DecoderStack &st, const LosProgItem *&b, const LosProgItem *e){
	uint num = b->procNum; ++b;
	uint i, t=0, sz = st.top;
	OperatorNode *res;
	switch(num){
	case 248:
	case 249:
	case 256:
	case 257:
//	case 259:  case 260:
	case 309: //?????????
		return st.del();
	case 245:   case 246:
	case 243:
	case 251:
	case 290: //first item in branch operator code
	case 263:
	case 264:
	case 266:
	case 258:
		//numbers of enumerating operator step procedures
	case 268: case 269: case 270: case 271:
	case 272: case 273: case 274: case 275:
	case 276: case 277: case 278: case 279:
	case 280: case 281: case 282: case 283:
	case 286: case 287: case 288: case 289:
	case 301: case 302: case 303: case 304:
	case 305: case 306: case 307: case 308:
	case 310: case 311:
		break;
	case 247:{ // or                                     (16.2.4)
		if(!sz)return false;
		t = st.last().svnum-260-2;
		if(b+t>=e || t<4)return false;
		const LosProgItem *s = b-2+t;
		if(s->type!=LosProgItem::LogSymbol)return false;
		t+=(s->logSymNum-260);
		s = b+t-2;
		if(s>e)return false;
		DecoderStack s0 = st.part();
		if(!oldDecodeFragment(b, s, s0))return false;
		b = s;
		res = new CommutAssocNode(CommutAssocNode::Or);
		res->args.resize(t = s0.top);
		for(i=0; i<t; i++)
			res->args[i] = s0[i].toNode();
		st.replace(1,res);
	}break;
	case 244:{ //and                                     (16.2.5)
		if(!st.getLastNum(t))return false;
		if(b+t-2 > e)return false;
		const LosProgItem *s = b+t-1;
		DecoderStack s0 = st.part();
		if(!oldDecodeFragment(b, s-2, s0))return false;
		res = new CommutAssocNode(CommutAssocNode::And);
		res->args.resize(t = s0.top);
		for(i=0; i<t; i++)
			res->args[i] = s0[i].toNode();
		b = s;
		st.replace(1,res);
	}break;
	case 296:  // variant                                (16.2.9)
	case 295:{ // alternativa                            (16.2.6)
		if(sz<2||!st.popNum(t)||b>=e || b+t-2>e)return false;
		//if(st.getLastNum(t))return false;
//		if(b+tl>e||tl<4)return false;
		const LosProgItem * m = b+t-2, * s, *b3,*e3;
		if(m>=e||m<=b+2)return false;
		uint tl; bool alt = ((m-1)->procNum==256);
		if(alt){ //alternativa
			if(b->type!=LosProgItem::LogSymbol)return false;
			tl = (b->logSymNum-260); s = b+tl+1;
			if(m+4>s||m<=b+4)return false;
			b+=2; b3 = m+2; e3=s-3;
		}else{   //variant
			if((m-2)->type!=LosProgItem::LogSymbol)return false;
			s = (m-2)+((m-2)->logSymNum-260);
			if(m>=s || s-1>e)return false;
			b3 = m; e3=s;
		}
		DecoderStack s0 = st.part();
		LosProgramNode *n1 = waitOperator(b, m-2, s0), *n0 = waitOperator(b3, e3, s0);
		if(!n0||!n1){delete n0; delete n1; return false;}
	//	if(s0.top!=2){s0.clear();return false;}
		IfElseNode *nd = new IfElseNode(st.last().toNode(),n1, n0, !alt);
		b = s;
		st.replace(1, nd);
	}break;
	case 252:
//		t=t;
	case 250:{ //forall / exists              (16.2.7, 16.2.8)
		if(!st.getLastNum(t) || b+t-2>e || t<4)return false;
		if(b[0].type!=LosProgItem::Variable || b[1].type!=LosProgItem::Variable)return false;
		uint x1 = b[0].varNum, xn = b[1].varNum;
		if(xn<x1)return false;
		const LosProgItem *s = b+t-1;
		DecoderStack s0 = st.part();
		uint pn = (s-2)->procNum;
		if(pn!=294 && pn!=254)return false;
		if(!oldDecodeFragment(b+2, s-2, s0))return false;
		if(s0.empty()){return false;}
		if(pn==254 && s0.top!=1)s0.convolve(s0.top, new CommutAssocNode(CommutAssocNode::And));
		res = new QuantifierNode(pn == 294 ? QuantifierNode::Forall : QuantifierNode::Exists, 0, x1, xn);
		res->args.resize(t = s0.top);
		for(i=0; i<t; i++)
			res->args[i] = s0[i].toNode();
		st.replace(1,res);
		b = s;
	}break;
	case 293:  // call to operator expression program (16.2.11)
	case 292:{ // call to operator program            (16.2.11)
		uint f=0;
		if(!st.popNum(t)||!st.popSymbol(f))return false;
		if(st.top<(int)t)return false;
		res = new CallToOperator(f);
		res->isExpr = (num==293);
		return st.convolve(t, res);
	}
	case 298:{// not                                  (16.2.3)
		if(!sz)return false;
		if(st.last().isNode() && 
			(res = dynamic_cast<OperatorNode*>(st.last().node))!=0 &&
				res->applyNot()){
//			res->neg = !res->neg;
			return true;
		}
		//res = new LosNotOperator(st.last().toNode());
		st.last() = new LosNotOperator(st.last().toNode());
	}break;
	case 291: // operator branch                      (16.2.13)
		if(!st.getLastNum(t))return false;
		st.replace(1, new BranchNode(BranchNode::Branch, t, 241));
		break;
	case 297: // operator else                        (16.2.13)
		if(b>=e||!b->isLogSymbol())return false;
		t = b->logSymNum-260; b++;
		st<<new BranchNode(BranchNode::Else, t, 242);
		break;
	case 259:  // operator expression setof
	case 260:  // operator expression findall
	case 267:{ // operator expression sumofall           (16.2.10)
		if(b+4>e||!st.popNum(t)||!b[0].isVariable()||!b[1].isVariable())return false;
		uint x1=b[0].varNum,xn=b[1].varNum;
		if(x1>xn)return false;
		const LosProgItem *s = b-1+t; //first command after current operator
		if(s-1>e)return false;
		b+=2;
		DecoderStack s0 = st.part();
		if(!oldDecodeFragment(b,s-2,s0)||s0.top<2){s0.clear(); return false;}
		LosProgramNode *nt = s0.last().toNode(); s0.pop();
		s0.convolve(s0.top, new CommutAssocNode(CommutAssocNode::And));
		res = new CollectorOperator(num == 259 ? CollectorOperator::SetOf:
									  num == 260 ? CollectorOperator::FindAll:
												   CollectorOperator::SumOfAll, x1, xn,
									  num == 259 ? 14:num == 260 ? 16:15);
		res->args<<s0[0].toNode()<<nt;
		st<<res;
		b=s;
		//st.replace(4, res);
	}break;
	case 56: // operator spravka
		if(!st.popNum(t)||(uint)st.top<t+2)return false;
		res = new OperatorNode(num);
		return st.convolve(t+2, res);
	case 186: // list (nabor)
		if(!st.popNum(t))return false;
		res = new LosListNode; res->val = num;
		return st.convolve(t, res);
	case 22: // call index
		if(!st.popNum(t))return false;
		if(t==1)res = new CallIndexNode(num);
		else res = new OperatorNode(num);// res->val = num;
		return st.convolve(t, res);
	case 1:   // label
		if(sz<1)return false;
		return st.convolve(1, new LabelNode(num));
	case 190: // return
		if(sz<1)return false;
		return st.convolve(1, new ReturnNode(num));
	case 187: // equals
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::Equal,num));
	case 160: // symbol add
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::AddS,num));
	case 161: // symbol subtract
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::SubtractS,num));
	case 162: // symbol multiply
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::MultiplyS,num));
	case 95:  // symbol less
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::LessS,num));
	case 210: // unary minus
		if(sz<1)return false;
		return st.convolve(1, new SymbolicNode(SymbolicNode::Minus,num));
	case 57:
		if(sz<1)return false;
		return st.convolve(1, new SymbolicNode(SymbolicNode::PtrValue, num));
	case 36:  // apply
		if(sz<3)return false;
/*		if(st.last().isNode()){
			delete st.last().toNode();
			st.pop();
		}*/
		return st.convolve(3, new SymbolicNode(SymbolicNode::Apply, num));
	case 153: // add
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::Add,num));
	case 154: // subtract
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::Subtract,num));
	case 155: // multiply
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::Multiply,num));
	case 211:  // symbol less
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::Less,num));
	case 198: // element of (enumerating operator)
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::ElementOf,num));
	case 118: // list intersection
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::ListIntersection,num));
	case 126: // list union
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::ListUnion,num));
	case 96:  // assignment to list item
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::AssignListItem,num));
	case 199: // list subset of
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::ListSubset,num));
	case 137:
		if(sz<2)return false;
		return st.convolve(2, new SymbolicNode(SymbolicNode::Pointer, num));
	case 53:  // list concatenation
		if(!st.popNum(t)||!t)return false;
		return st.convolve(t, new CommutAssocNode(CommutAssocNode::Concatenation));
	case 113:  // term construction
		if(!st.popNum(t))return false;
		return st.convolve(t, new MakeTermNode(num));
	case 191:  // result
		if(!st.popNum(t)||t<2)return false;
		return st.convolve(t, new ResultNode(num));
	case 89:  // replace
		if(sz<2)return false;
		return st.convolve(2, new ReplaceOperator(num));
	case 70:  // left position (index)
		if(sz<2)return false;
		return st.convolve(2, new LosIndexNode(true));
/*	case 71:  // right position (index)
		if(sz<2)return false;
		return st.convolve(2, new LosIndexNode(false));*/
	case 3:     case 12:    case 21:  /*case 22:*/  case 34:
	case 50:    case 51:  /*case 53:*/  case 62:    case 63:
	case 64:  /*case 113:*/ case 144:   case 159:   case 165:
	case 166:   case 167:   case 169:   case 172:   case 178:
/*	case 191:*/ case 195:   case 196:   case 197:   case 215:
	case 216:   case 222:   case 226:   case 227:   case 228:
		// operators with floating arity
		return readStdOperator(num, st, -1);
	case 134:   case 152: //enumerating operators with floating arity
		return readStdOperator(num, st, -1);
	case 11:    case 17:   case 18:   case 19:    case 20:
	case 26:    case 27:   case 29:   case 30:    case 32:
	case 33:    case 38:   case 48:   case 217:   case 221:
	case 225: // operators without arguments
		return readStdOperator(num, st, 0); /*
	case 1:*/     case 4:     case 13:    case 24:    case 25:
	case 31:    case 42:    case 43:    case 52:  /*case 57:*/
	case 59:    case 65:    case 73:    case 74:    case 76:
	case 77:    case 79:    case 80:    case 82:    case 83:
	case 85:    case 86:    case 92:    case 97:    case 98:
	case 100:   case 102:   case 104:   case 105:   case 111:
	case 115:   case 117:   case 121:   case 124:   case 125:
	case 131:   case 133:   case 135:   case 138:   case 143:
	case 145:   case 146:   case 150:   case 151:   case 158:
	case 168:   case 171:   case 173:   case 174:   case 175:
	case 176:   case 177:   case 179:   case 185: //case 190:
	case 200:   case 202:   case 204:   case 206:   case 207:
	case 208:   case 209: /*case 210:*/ case 213:   case 214:
	case 219:   case 224:   case 229: //unary operators
		return readStdOperator(num, st, 1);
	case 66: //unary enumerating operator
		return readStdOperator(num, st, 1);
	case 39:    case 40:    case 46:    case 54:    case 58:
	case 67:    case 68:    case 69:  /*case 70:*/  case 71:
	case 75:    case 78:    case 81:    case 84:  /*case 89:*/
	case 93:    case 94:  /*case 95:    case 96:*/  case 99:
	case 101:   case 103:   case 106:   case 107:   case 108:
	case 109: /*case 118:*/ case 119:   case 120:   case 122:/*
	case 126:*/ case 127:   case 129:   case 130:   case 132:
/*	case 137:*/ case 139:   case 140:   case 147: /*case 153:
	case 154:   case 155:*/ case 157: /*case 160:   case 161:
	case 162:*/ case 194: /*case 199:*/ case 205: //case 211:
		// binary operators
		return readStdOperator(num, st, 2);
	case 35:    case 55:    case 60:    case 61:    case 72:
	case 91:    case 141:   case 142:   case 149:   case 193:
	//case 198:
		// binary enumerating operators
		return readStdOperator(num, st, 2);
	case 23:    case 47:    case 87:    case 88:
	case 110:   case 112:   case 116:   case 123:   case 136:
	case 148:   case 156:   case 164:   case 201:   case 203:
	case 220: // ternary operators
		return readStdOperator(num, st, 3);
	case 41:    case 45:    case 49:    case 90:    case 128:
	case 180:   case 181:   case 182:   case 183:   case 218:
	case 223: // ternary enumerating operators
		return readStdOperator(num, st, 3);
	case 114:   case 163:   case 170:   case 212:
		// 4-ary operators
		return readStdOperator(num, st, 4);
	case 37:    case 44:   case 184:   case 188:   case 189:
	case 230:	// 4-ary enumerating operators
		return readStdOperator(num, st, 4);
	default:
		return false;
	}
	return true;
}

QString &CallToOperator::print(QString &str, const NameInfo &names, PrintState p) const{
	return OperatorNode::print(str,names, p);
	//return OperatorNode::print(str+="call ",names, p);
}

QString &ReplaceOperator::print(QString &str, const NameInfo &names, PrintState p) const{
	if(this->args.size()!=2||!this->args[0]->isSymbol())return str+="invalid replace operator";
	int xnum = this->args[0]->val-260;
	bool br = p.left >= AssignPriority.left||p.right>AssignPriority.right;
	if(br){p.left = p.right = 0;}
	PrintState ps(qMax(p.left,AssignPriority.right),p.right,p.indent+1, true);
	if(!names.forCopy()){
		if(const SymbolicNode* sn = dynamic_cast<const SymbolicNode*>(this->args[1])){
			if(sn->args.size()==2 && sn->args[0]->isVariable()&&sn->args[0]->val == xnum){
				switch(sn->o){
				case SymbolicNode::Add:
				case SymbolicNode::AddS:
					if(sn->args[1]->isSymbol()&&sn->args[1]->val == 261){
						names.printVariable(str, xnum);
						str+="++";
					}else {
						if(br){str+="(";}
						names.printVariable(str, xnum);
						str+=" += ";
						sn->args[1]->print(str, names, ps);
						if(br)str+= ")";
					}
					return str;
				case SymbolicNode::Subtract:
				case SymbolicNode::SubtractS:
					if(sn->args[1]->isSymbol()&&sn->args[1]->val == 261){
						names.printVariable(str, xnum);
						str+="--";
					}else {
						if(br){str+="(";}
						names.printVariable(str, xnum);
						str+=" -= ";
						sn->args[1]->print(str, names, ps);
						if(br)str+= ")";
					}
					return str;
				case SymbolicNode::Multiply:
				case SymbolicNode::MultiplyS:
					if(br){str+="(";}
					names.printVariable(str, xnum);
					str+=" *= ";
					sn->args[1]->print(str, names, ps);
					if(br)str+= ")";
					return str;
				default:break;
				}
			}
		}
	}
	if(br){str+="(";}
	names.printVariable(str, this->args[0]->val-260);
	str+=" "+names.printSymbol(Unicode::ColonEquals, ":=")+" ";
	this->args[1]->print(str, names, ps);
	if(br)str+=")";
	return str;
}

QString &LosIndexNode::print(QString &str, const NameInfo &names, PrintState p) const{
	if(this->args.size()!=2)return str+= "invalid index operator";
	this->args[0]->print(str, names, PrintState(p.left, qMax(p.right, IndexPriority.left), p.indent, 1));
	if(left)str+="[";
	else str+="[<";
	this->args[1]->print(str, names, PrintState(0, 0, p.indent+1, 1));
	if(left)str+="]";
	else str+="]";
	return str;
}

QString &EmptyWordNode::print(QString &str, const NameInfo&n, PrintState) const{
	if(n.forCopy())return str+="emptylist";
	return str+="[]";
}
const int XNodeNumber      = 171;
const int ProgramSymbolNum = 32;
const int CallIndexNum     = 22;
void ProgramHeader::setProgram(LosProgramNode *n, OutputContext out){
	qDeleteAll(calls);
	calls.clear();
	if(!n)return;
	for(int l = 0; n;l++){
		ProgramFragment *f = dynamic_cast<ProgramFragment*>(n);
		Call c;
		if(!f || f->cmd.size()<2){
			calls[c] = n;
			break;
		}
		int num = 1;
		BranchNode *br = dynamic_cast<BranchNode*>(f->cmd[1]);
		if(br)num++;
		if(f->cmd[0]->isOperator() && !dynamic_cast<OperatorNode*>(f->cmd[0])->args.size()){
			c.symbol = f->cmd[0]->val;
			c.tp = (c.symbol == ProgramSymbolNum ? Program : Symbol);
			delete f->cmd[0]; f->cmd[0] = 0;
		}else if(CallIndexNode* cn = dynamic_cast<CallIndexNode*>(f->cmd[0])){
			if(cn->args[0]->isSymbol()){
				c.tp = CallIndex;
				c.symbol = cn->args[0]->val;
				delete f->cmd[0]; f->cmd[0] = 0;
			}else br=0;
		}else br=0;
		if(num<f->cmd.size()){
			LabelNode * ln = dynamic_cast<LabelNode*>(f->cmd[num]);
			OperatorNode *x = ln ? dynamic_cast<OperatorNode*>(ln->args[0]) : 0;
			if(x && x->val == XNodeNumber && x->args[0]->isSymbol()){
				c.arity = (short)x->args[0]->val-(short)260-1;
				delete f->cmd[num]; f->cmd[num] = 0;
			}else num--;
		}else num--;
		if(calls.contains(c)){
			out.warnings()<<"branch at level "<<l<<" has been deleted";
			delete f;
			break;
		}
		if(br && br->hasRef()){
			n = br->args[0];
			br->args[0] = 0;
			delete br;
			f->cmd[1] = 0;
		}else n=0;
		int j;
		for(j=0; j<3; j++){
			if(f->cmd[j])break;
		}
		if(j<num && !f->cmd[num])f->cmd.remove(num,1);
		f->cmd.remove(0,j);
		calls[c] = f;
	}
}

void ProgramHeader::Call::print(QString &res, const NameInfo &names) const{
	switch(tp){
	case ProgramHeader::Program: names.printName(res, ProgramSymbolNum); break;
	case ProgramHeader::CallIndex :
		names.printName(res, CallIndexNum);
		res+="(";
		names.printName(res, symbol);
		res+=")";
		break;
	case ProgramHeader::Symbol: names.printName(res, symbol); break;
	default:
		  res+="default call type";
	}
}

QString &MakeTermNode::print(QString &str, const NameInfo &names, PrintState p) const{
	this->args[0]->print(str, names, PrintState(p.left, IndexPriority.right, p.indent+1, true));
	str+="[<";
	p.left = p.right = 0;
	p.indent++;
	for(int i=1; i<this->args.size(); i++){
		this->args[i]->print(str, names, p);
		if(i<this->args.size()-1)str+=", ";
	}
	str+=">]";
	return str;
}
QString &MakeTermNode::printOp(QString &str, const NameInfo &names, PrintState p) const {
	this->args[0]->printOp(str, names, PrintState(p.left, IndexPriority.right, p.indent + 1, true));
	str += "(";
	p.left = p.right = 0;
	p.indent++;
	for (int i = 1; i<this->args.size(); i++) {
		this->args[i]->printOp(str, names, p);
		if (i<this->args.size() - 1)str += ",";
	}
	str += ")";
	return str;
}
QString &ResultNode::print0(QString &str, const NameInfo &names, PrintState p) const{
	if(this->args.size()&1)return str+="[invalid result node]";
	p.left = p.right = ComparePriority.left;
	for(int i=0; i<this->args.size(); i+=2){
		this->args[i]->print(str, names, p);
		str += " = ";
		this->args[i+1]->print(str, names, PrintState(p.left, p.right, p.indent, 1));
		str += ";";
		str += '\n';
		str+=QString(p.indent, '\t');
	}
	return str;
}
QString &ResultNode::print(QString &str, const NameInfo &names, PrintState p) const{
	return print0(str, names, p)+="result";
}
