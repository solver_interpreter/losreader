#pragma once
#include <stdlib.h>
//#include "Array.h"
#include "Memory.h"

#ifdef _D_CHECK_INDEXES
#define INDEX_CHECK(cond) {if(!(cond))throw "Vector: index out of range";} 
#else
#define INDEX_CHECK(cond)
#endif

template<class T>
class Vector{
	T *_p;
	size_t _max_sz, _sz;
	size_t _check_size(size_t s, bool copy){
		if(s <= _max_sz)return _max_sz;
		T *np;
		if(!copy){clear(); free(_p);}
		s = Max(s, size_t(1.5*_max_sz)+1);
		np = (T*)malloc(sizeof(T)*s);
		if(!np){if(copy)return _sz = _max_sz = 0; else return _max_sz;}
		_max_sz = s;
		if(copy){
			_construct_copy(np, _p, _sz);
			_destruct(_p, _sz); free(_p);
		}
		_p = np;
		return _max_sz;
	}
public:
	T reg;
	typedef T value_type;
	typedef T* iterator, *reverse_iterator, *pointer;
	typedef const T* const_iterator, *const_reverse_iterator, *const_pointer;
	typedef T& reference;
	typedef const T& const_reference;

	Vector(){_p=0; _max_sz = _sz = 0;}
	Vector(size_t sz){
		_p = _construct(_p=0, sz);
		if(_p)_max_sz = _sz = sz;
		else _max_sz = _sz = 0;
	}
	template<class Y>
	Vector(size_t sz, const Y & val){
		_p = _construct((T*)0, val, sz);
		if(_p)_max_sz = _sz = sz;
		else _max_sz = _sz = 0;
	}
	template<class Y>
	Vector(size_t sz, const Y *val){
		_p = _construct((T*)0, sz);
		if(_p)_max_sz = _sz = sz;
		else _max_sz = _sz = 0;
		Memcpy(_p, val, _sz);
	}
	Vector(const Vector<T> &v){
		_p = _construct_copy(_p = 0, v._p, v._sz);
		if(_p)_sz = _max_sz = v._sz;
		else _sz = _max_sz = 0;
	}
	template<class Y>
	Vector(const Vector<Y> &v){
		_p = _construct_copy(0, v.data(), v.size());
		if(_p)_sz = _max_sz = v._sz;
		else _sz = _max_sz = 0;
	}
	template<class Cont>
	Vector<T>& operator=(const Cont &c){
		return assign(c.begin(), c.size());
	}
	template<class Cont>
	Vector<T>& operator+=(const Cont &c){
		return append(c.begin(), c.size());
	}
	Vector<T>& operator=(const Vector<T> &v){
		return assign(v._p, v._sz);
	}
	template<class It>
	Vector<T>& copyFrom(It it, size_t beg = 0, size_t num = -1){
		if(beg>=_sz)return *this;
		MemcpyI(_p+beg, it, num <= _sz-beg ? num : _sz-beg);
		return *this;
	}
	Vector<T>& fill(const T& val, size_t beg = 0, size_t num = -1){
		if(beg>=_sz)return *this;
		Memset(_p+beg, val, num <= _sz-beg ? num : _sz-beg);
		return *this;
	}
	template<class F>
	Vector<T>& apply(F f){
		for(size_t i=0; i<_sz; i++)
			_p[i] = f(_p[i]);
		return *this;
	}
	template<class F, class It>
	Vector<T>& mapFrom(It it, F f){
		for(size_t i=0; i<_sz; i++, ++it)
			_p[i] = f(*it);
		return *this;
	}

	template<class It>
	Vector<T>& assign(It it, size_t sz){
		if(_check_size(sz, true)<sz)return *this;
		size_t N = Min(_sz, sz);
		for(size_t i=0; i<N; i++, ++it)
			_p[i] = *it;
		if(sz > N)_construct_copy(_p+N, it, sz-N);
		else _destruct(_p+N, _sz-N);
		_sz = sz;
		return *this;
	}
	template<class It>
	Vector<T>& append(It it, size_t sz){
		size_t nsz = sz+_sz;
		if(_check_size(nsz, true)<nsz)return *this;
		_construct_copy(_p+_sz, it, sz);
		_sz = nsz;
		return *this;
	}
	template<class Cont>
	Vector<T>& assign(const Cont &c){return assign(c.begin(), c.size());}
	template<class Cont>
	Vector<T>& append(const Cont &c){return append(c.begin(), c.size());}
	void swap(Vector<T> &v){
		Exchange(_p, v._p);
		Exchange(_sz, v._sz);
		Exchange(_max_sz, v._max_sz);
	}
//======================================
	bool isEmpty()const{return  !_sz;}
	size_t size()const{return _sz;}
	Vector<T> &clear(bool del = true){
		_destruct(_p, _sz);
		_sz = _max_sz = 0;
		if(del)free(_p); _p = 0;
		return *this;
	}
	Vector<T> & reserve(size_t s){
		_check_size(s, true);
		return *this;
	}
	Vector<T> & remove(size_t pos, size_t n=1){
		if(pos>=_sz || !n)return *this;
		_sz = _sz-pos > n ? _sz-n : pos;
		for(;pos<_sz; pos++)
			_p[pos]=_p[pos+n];
		return *this;
	}
	Vector<T> & cut(){
		if(_sz == _max_sz)return *this;
//		T *np;
		_p = (T*)realloc(_p, sizeof(T)*_sz); ////???????????????
		_max_sz = _sz;
		return *this;
	}
	Vector<T> & resize(size_t s, bool copy = true){
		if(s<=_sz){_destruct(_p+s, _sz - s); _sz = s;}
		if(_check_size(s, copy)<s)return *this;//reserve(s);
		if(_max_sz>=s){
			_construct(_p+_sz, s-_sz);
		}
		_sz = s;
		return *this;
	}
	template<class Y>
	Vector<T> & resize(size_t s, const Y& val, bool copy = true){
		if(s<=_sz){_destruct(_p+s, _sz - s); _sz = s;}
		if(_check_size(s, copy)<s)return *this;//reserve(s);
		if(_max_sz>=s){
			_construct(_p+_sz, val, s-_sz);
		}
		_sz = s;
		return *this;
	}
	template<class Y>
	Vector<T> &push_back(const Y &y){
		if(_check_size(_sz+1, true) > _sz){
			::new(_p + _sz) T(y);
			_sz++;
		}else reg = y;
		return *this;
	}
	T& push_back(){
		if(_check_size(_sz+1, true) > _sz){
			::new(_p + _sz) T;
			return _p[_sz++];
		}return reg;
	}
	Vector<T>& pop_back(){
		if(_sz)_destruct(_p + (--_sz));
		return *this;
	}
	const T& back()const{
		INDEX_CHECK(0<_sz);
		return _p[_sz-1];
	}
	template<class Y>
	Vector<T> & operator<<(const Y &val){return push_back(val);}
	template<class Y>
	Vector<T> & operator >> (Y &val){INDEX_CHECK(0 < _sz); _sz--; val = _p[_sz]; _destruct(_p+_sz); return *this;}

	const T& operator[](size_t i)const{
		INDEX_CHECK(i<_sz);
		return _p[i];
	}
	T& operator[](size_t i){
		INDEX_CHECK(i<_sz);
		return _p[i];
	}
	const T& fromEnd(size_t i)const{
		INDEX_CHECK(i<_sz);
		return _p[_sz-i-1];
	}
	T& fromEnd(size_t i){
		INDEX_CHECK(i<_sz);
		return _p[_sz-i-1];
	}
	const T& operator()(size_t i)const{
		if(i<_sz)return _p[i];
		return _p[_sz+i];
	}
	T& operator()(size_t i){
		if(i<_sz)return _p[i];
		return _p[_sz+i];
	}
	const T& operator()(size_t i, const T & def)const{
		if(i<_sz)return _p[i];
		if(_sz+i<_sz)return _p[_sz+i];
		return def;
	}
	T& operator()(size_t i, T & def){
		if(i<_sz)return _p[i];
		if(_sz+i < _sz)return _p[_sz+i];
		return def;
	}
//====================
	pointer data(){return _p;}
	const_pointer data()const{return _p;}
	const_pointer constData()const{return _p;}

	iterator begin(){return _p;}
	const_iterator begin()const{return _p;}
	iterator end(){return _p+_sz;}
	const_iterator end()const{return _p+_sz;}

/*	reverse_iterator rbegin(){return _p+_sz-1;}
	const_reverse_iterator rbegin()const{return _p+_sz-1;}
	reverse_iterator rend(){return _p-1;}
	const_reverse_iterator rend()const{return _p-1;}*/
//====================
	Vector<T>& reverse(){
		for(size_t i=0, j = _sz-1; i<j; i++,j--)
			Exchange(_p[i], _p[j]);
		return *this;
	}
//====================
	~Vector(){
		clear(true);
		_max_sz = _sz = 0;
	}
};
template<class T>
void Exchange(Vector<T> &v1, Vector<T> &v2){v1.swap(v2);}
