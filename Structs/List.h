#pragma once
#include "Memory.h"

template<class T>
struct list_node{
	list_node *next, *prev;
	T data;
};

template<class T>
class ListAlloc{
	struct ListAllocNode{
		size_t sz;
		list_node<T>* p;
		ListAllocNode *next;
		ListAllocNode(size_t n, ListAllocNode *nxt=0){
			p = (list_node<T>*)calloc(n, sizeof(list_node<T>));
			for(size_t i=1; i<n; i++)
				p[i-1].next = p+i;
			if(n)p[n-1].next = 0;
			next = nxt;
			sz = n;
		}
		~ListAllocNode(){
			::free(p);
		}
	};

	double exp_coef;
	int start_size;
	ListAllocNode *head;
	list_node<T> *fr;
	int used;
	size_t _sz, _fr_sz;
	void _check(){
		if(!fr){
			if(head)head = new ListAllocNode(int(head->sz*exp_coef)+1, head);
			else head = new ListAllocNode(start_size);
			_fr_sz+=head->sz; _sz+=head->sz;
	//		printf("new ListAllocNode of size %d\n", head->sz);
			fr = head->p;
		}
	}
	bool _check_node(const list_node<T> *x)const{
		for(ListAllocNode * p = head; p; p = p->next)
			if(x>=p->p && x - p->p < p->sz)return true;
		return false;
	}
public:
	void reserve(size_t sz){
		if(_sz<sz){
			if(head)head = new ListAllocNode(int(sz - _sz)+1, head);
			else head = new ListAllocNode(Max(size_t(start_size), sz-_sz));
			head->p[head->sz-1].next = fr;
			fr = head->p;
			_fr_sz+=head->sz; _sz+=head->sz;
		}
	}
	void inc_used(){used++;}
	void dec_used(){
		D_CHECK(used>0, "ListAlloc : used<0");
		used--;
	}
	bool is_used(){return used>0;}
	ListAlloc(size_t start_sz=10, double k=1.5){
#ifdef _DEBUG
		if(k<1)throw "ListAlloc : k<1";
#endif
		_sz = _fr_sz = 0;
		start_size = start_sz;
		exp_coef = k;
		head = 0;
		used = 1;
		fr = 0;
	}
	list_node<T>* get_new(){
		_check();
		list_node<T> *p = fr;
		fr = fr->next;
		_fr_sz--;
		::new(&p->data) T();
		return p;
	}
	template<class Y>
	list_node<T>* get_new(const Y &y){
		_check();
		list_node<T> *p = fr;
		fr = fr->next;
		_fr_sz--;
		::new(&p->data) T(y);
		return p;
	}
	template<class Y, class R>
	list_node<T>* get_new_swap(Y &y, R swap(T&,Y&)){
		_check();
		list_node<T> *p = fr;
		fr = fr->next;
		_fr_sz--;
		::new(&p->data) T;
		swap(p->data, y);
		return p;
	}
	void free(list_node<T> *n){
#ifdef _DEBUG_ALLOCATION
		if(!_check_node(n))throw "free : node isn't from current allocator";
#endif
		_destruct(&n->data);
		n->next = fr;
		_fr_sz++;
		fr = n;
	}
	void check_clear(){
		if(_fr_sz == _sz){
			ListAllocNode *p = head;
			for(;p; p=head){
				head = p->next;
				delete p;
			}
			head = 0;
			fr = 0;
			_fr_sz = _sz = 0;
		}
	}
	int free_sequence(list_node<T> *first, list_node<T> *last);
	~ListAlloc();
};

template<class T>
int ListAlloc<T>::free_sequence(list_node<T> *first, list_node<T> *last){
	if(!first)return 0;
	list_node<T> *n;
	int res=1;
	for(n = first; ;n = n->next, res++){
#ifdef _DEBUG
		if(!n)throw "free_sequence : end must be after begin";
#endif
#ifdef _DEBUG_ALLOCATION
		if(!_check_node(n))throw "free : node isn't from current allocator";
#endif
		_destruct(&n->data);
		_fr_sz++;
		if(n==last)break;
	}
	last->next = fr;
	fr = first;
	return res;
}

template<class T>
ListAlloc<T>::~ListAlloc(){
	ListAllocNode *p = head;
	for(;p; p=head){
		head = p->next;
		delete p;
	}
	head = 0;
}

template<class T>
class List{
	typedef list_node<T> _node;
	typedef List<T> _myt;
	typedef ListAlloc<T> _alloc;
	size_t _sz;
	_node *_head, *_tail;
	_alloc *_Al;

	void _Construct(_alloc *Al){
		_head = _tail = 0;
		if(!Al)_Al = new _alloc;
		else (_Al = Al)->inc_used();
		_sz = 0;
	}
public:
	typedef size_t size_type;
	typedef T value_type;
	typedef size_type difference_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;

	class const_iterator{
	protected:
		const List<T> *_pl;
		const _node *_ptr;
		friend class List<T>;
		friend class const_reverse_iterator;
		const_iterator(const List<T> *l, const _node *p){_ptr=p; _pl = l;}
	public:
		const_iterator(){_ptr=0;_pl=0;}
		explicit const_iterator(const typename List<T>::const_reverse_iterator &it);
		bool is_valid(){return _ptr!=0;}
		const T& operator *()const{return _ptr->data;}
		const T* operator ->()const{return &_ptr->data;}
		const_iterator& operator++(){_ptr = _ptr->next; return *this;}
		const_iterator operator++(int){const_iterator t = *this; _ptr = _ptr->next; return t;}
		const_iterator prev(){return const_iterator(_pl, _ptr ? _ptr->prev : _pl->_tail);}
		const_iterator next(){return const_iterator(_pl, _ptr->next);}
		bool operator==(const const_iterator &it){return _ptr==it._ptr && _pl==it._pl;}
		bool operator!=(const const_iterator &it){return _ptr!=it._ptr || _pl!=it._pl;}
		const_iterator& operator--(){
			if(!_ptr)_ptr = _pl->_tail;
			else _ptr = _ptr->prev;
			return *this;
		}
		const_iterator operator--(int){
			const_iterator t = *this;
			if(!_ptr)_ptr = _pl->_tail;
			else _ptr = _ptr->prev;
			return t;
		}
	};
	class iterator: public const_iterator{
		friend class List<T>;
		friend class reverse_iterator;
		iterator(List<T> *pl, _node *ptr):const_iterator(pl, ptr){}
	public:
		iterator():const_iterator(){}
		explicit iterator(const typename List<T>::reverse_iterator &it);
		iterator prev(){return iterator((List<T>*)this->_pl, (_node*)(this->_ptr ? this->_ptr->prev : this->_pl->_tail));}
		iterator next(){return iterator((List<T>*)this->_pl, (_node*)this->_ptr->next);}
		T& operator *()const{return *(T*)&this->_ptr->data;}
		T* operator ->()const{return (T*)&this->_ptr->data;}
		iterator& operator++(){this->_ptr = this->_ptr->next; return *this;}
		iterator operator++(int){iterator t = *this; this->_ptr = this->_ptr->next; return t;}
		iterator& operator--(){
			--(*(const_iterator*)this);
			return *this;
		}
		iterator operator--(int){
			iterator t = *this;
			--(*(const_iterator*)this);
			return t;
		}
	};
	class const_reverse_iterator{
	protected:
		const List<T> *_pl;
		const _node *_ptr;
		friend class List<T>;
		friend class const_iterator;
		const_reverse_iterator(const List<T> *l, const _node *p){_ptr=p; _pl = l;}
	public:
		const_reverse_iterator(){_ptr=0;_pl=0;}
		explicit const_reverse_iterator(const const_iterator& i){
			_pl = i._pl; _ptr = i._ptr;
			if(!_ptr)_ptr = _pl->_tail;
			else _ptr = _ptr->prev;
		}
		operator const_iterator()const{
			const_iterator res(_pl, _ptr);
			if(!_ptr)res._ptr = _pl->_head;
			else res._ptr = _ptr->next;
			return res;
		}
		bool is_valid(){return _ptr!=0;}
		const_reverse_iterator next(){return const_reverse_iterator(_pl, _ptr->prev);}
		const_reverse_iterator prev(){return const_reverse_iterator(_pl, _ptr ? _ptr->next : _pl->_head);}
		const T& operator *()const{return _ptr->data;}
		const T* operator ->()const{return &_ptr->data;}
		const_reverse_iterator& operator++(){_ptr = _ptr->prev; return *this;}
		const_reverse_iterator operator++(int){const_reverse_iterator t = *this; _ptr = _ptr->prev; return t;}
		bool operator==(const const_reverse_iterator &it){return _ptr==it._ptr && _pl==it._pl;}
		bool operator!=(const const_reverse_iterator &it){return _ptr!=it._ptr || _pl!=it._pl;}
		const_reverse_iterator& operator--(){
			if(!_ptr)_ptr = _pl->_head;
			else _ptr = _ptr->next;
			return *this;
		}
		const_reverse_iterator operator--(int){
			const_reverse_iterator t = *this;
			if(!_ptr)_ptr = _pl->_head;
			else _ptr = _ptr->next;
			return t;
		}
	};
	class reverse_iterator: public const_reverse_iterator{
		friend class List<T>;
		friend class iterator;
		reverse_iterator(List<T> *pl, _node *ptr):const_reverse_iterator(pl, ptr){}
	public:
		explicit reverse_iterator(const iterator& i):const_reverse_iterator(i){}
		reverse_iterator():const_reverse_iterator(){}
		T& operator *()const{return *(T*)&this->_ptr->data;}
		T* operator ->()const{return (T*)&this->_ptr->data;}
		operator iterator()const{
			iterator res((List<T>*)this->_pl, (list_node<T>*)this->_ptr);
			if(!this->_ptr)res._ptr = this->_pl->_head;
			else res._ptr = this->_ptr->next;
			return res;
		}
		reverse_iterator next(){return reverse_iterator((List<T>*)this->_pl, (_node*)this->_ptr->prev);}
		reverse_iterator prev(){return reverse_iterator((List<T>*)this->_pl, (_node*)(this->_ptr ? this->_ptr->next : this->_pl->_head));}
		reverse_iterator& operator++(){this->_ptr = this->_ptr->prev; return *this;}
		reverse_iterator operator++(int){reverse_iterator t = *this; this->_ptr = this->_ptr->prev; return t;}
		reverse_iterator& operator--(){
			--(*(const_reverse_iterator*)this);
			return *this;
		}
		reverse_iterator operator--(int){
			reverse_iterator t = *this;
			--(*(const_reverse_iterator*)this);
			return t;
		}
	};

	List(_alloc *Al=0){
		_Construct(Al);
	}
	bool set_alloc(_alloc *Al){
		if(_sz||!Al)return false;
		if(_Al)_Al->dec_used();
		if(!_Al->is_used())delete _Al;
		_Al = Al;
		_Al->inc_used();
		return true;
	}
	void reserve(size_t sz){_Al->reserve(sz);}

	iterator begin(){return iterator(this, _head);}
	const_iterator begin()const{return const_iterator(this, _head);}
	T& front(){return _head->data;}
	const T& front()const{return _head->data;}
	reverse_iterator rbegin(){return reverse_iterator(this, _tail);}
	const_reverse_iterator rbegin()const{return const_reverse_iterator(this, _tail);}

	iterator end(){return iterator(this, 0);}
	const_iterator end()const{return const_iterator(this, 0);}
	T& back(){return _tail->data;}
	const T& back()const{return _tail->data;}
	reverse_iterator rend(){return reverse_iterator(this, 0);}
	const_reverse_iterator rend()const{return const_reverse_iterator(this, 0);}
private:
	_myt& _insert_normalize(_node *before, _node *n){
		if((n->next = before)){
			n->prev = before->prev;
			before->prev = n;
		}else{
			n->prev = _tail;
			_tail = n;
		}
		if(!n->prev)_head = n;
		else n->prev->next = n;
		_sz++;
		return *this;
	}
	_myt &_delete_normalize(_node *p){
		if(p->next)p->next->prev = p->prev;
		else _tail = p->prev;
		if(p->prev)p->prev->next = p->next;
		else _head = p->next;
		_Al->free(p);
		_sz--;
		return *this;
	}
public:
	template<class Y>
	_myt &insert(const_iterator before, const Y& val, size_t sz){
		if(!size)return *this;
		_node *nb, *ne, *p = (_node*)before._ptr;
		int i;
		for(i=1, nb=ne=_Al->get_new(val); i<sz; i++){
			_node *n = _Al->get_new(val);
			n->prev = ne;
			ne = ne->next = n;
		}
		if((ne->next = p)){
			nb->prev = p->prev;
			p->prev = ne;
		}else{
			nb->prev = _tail;
			_tail = ne;
		}
		if(!nb->prev)_head = nb;
		else nb->prev->next = nb;
		_sz+=sz;
		return *this;
	}
	template<class It>
	_myt &insert(const_iterator before, It beg, It end){
		if(beg==end)return *this;
		_node *nb, *ne, *p = (_node*)before._ptr;
		int i;
		for(i=1, nb=ne=_Al->get_new(*beg); ++beg!=end; i++){
			_node *n = _Al->get_new(*beg);
			n->prev = ne;
			ne = ne->next = n;
		}
		if((ne->next = p)){
			nb->prev = p->prev;
			p->prev = ne;
		}else{
			nb->prev = _tail;
			_tail = ne;
		}
		if(!nb->prev)_head = nb;
		else nb->prev->next = nb;
		_sz+=i;
		return *this;
	}
	template<class Y>
	_myt &insert(const_iterator before, const Y& val){
		return _insert_normalize((_node*)before._ptr, _Al->get_new(val));
	}
	_myt &insert(const_iterator before){
		return _insert_normalize((_node*)before._ptr, _Al->get_new());
	}
	template<class Y, class R>
	_myt &put(const_iterator before, const Y& val, R swap(T&,Y&)){
		return _insert_normalize((_node*)before._ptr, _Al->get_new_swap(val, swap));
	}
	template<class Y>
	_myt &insert(const_reverse_iterator before, const Y& val){
		return insert(const_iterator(this, (--before)._ptr), val);
	}
	template<class Y>
	_myt &insert(const_reverse_iterator before){
		return insert(const_iterator(this, (--before)._ptr));
	}
	template<class Y, class R>
	_myt &put(const_reverse_iterator before, Y& val, R swap(T&,Y&)){
		return put(const_iterator(this, (--before)._ptr), val, swap);
	}

	T& push_back(){
		_insert_normalize(0, _Al->get_new());
		return _tail->data;
	}
	template<class Y>
	_myt &push_back(const Y &val){
		return _insert_normalize(0, _Al->get_new(val));
	}
	template<class Y, class R>
	_myt &put_back(Y &val, R swap(T&,Y&)){
		return _insert_normalize(0, _Al->get_new_swap(val, swap));
	}
	_myt &pop_back(){
		return _delete_normalize(_tail);
	}

	_myt &push_front(){
		return _insert_normalize(_head, _Al->get_new());
	}
	template<class Y>
	_myt &push_front(const Y &val){
		return _insert_normalize(_head, _Al->get_new(val));
	}
	template<class Y, class R>
	_myt &put_front(Y &val, R swap(T&,Y&)){
		return _insert_normalize(_head, _Al->get_new_swap(val, swap));
	}
	_myt &pop_front(){
		return _delete_normalize(_head);
	}

	_myt &erase(const_iterator Where){
		return _delete_normalize((_node*)Where._ptr);
	}
	_myt &erase(const_iterator from, size_t count, bool forward){
		if(!count)return *this;
		_node *b = (_node*)from._ptr, *p = b, *l;
		if(forward)
			while(count--)p = p->next;
		else{
			if(!b)count--, b=_tail;
			while(count--)b = b->prev;
		}
		if(p)l=p->prev, p->prev = b->prev;
		else l=_tail, _tail = b->prev;
		if(b->prev)b->prev->next = p;
		else _head = p;
		_sz -= _Al->free_sequence(b, l);
		return *this;
	}
	_myt &erase(const_iterator from, const_iterator to){
		if(!from.is_valid())return *this;
		_node *b = (_node*)from._ptr, *p = (_node*)to._ptr, *l;
		if(p)l=p->prev, p->prev = b->prev;
		else l=_tail, _tail = b->prev;
		if(b->prev)b->prev->next = p;
		else _head = p;
		_sz -= _Al->free_sequence(b, l);
		return *this;
	}
	_myt &erase(const_reverse_iterator Where){
		return erase(const_iterator(this, (--Where)._ptr));
	}
	_myt &erase(const_reverse_iterator from, const_reverse_iterator to){
		return erase(const_iterator(this, (--to)._ptr), const_iterator(this, (--from)._ptr));
	}
	_myt &clear(){
		_Al->free_sequence(_head, _tail);
		_head = _tail = 0;
		_sz = 0;
		return *this;
	}
	bool isEmpty()const{return !_sz;}
	size_t size()const{return _sz;}
	template<class Y>
	List(const Y &val, int n, _alloc *Al=0){
		_Construct(Al);
		insert(begin(), val, n);
	}
	template<class It>
	List(It b, It e, _alloc *Al=0){
		_Construct(Al);
		insert(begin(), b, e);
	}
	List(const _myt &l){
		_Construct(l._Al);
		insert(begin(), l.begin(), l.end());
	}
	_myt& operator=(const _myt &l){
		_node *p=_head, *lp = l._head;
		for(;p&&lp; p = p->next, lp = lp->next)
			p->data = lp->data;
		if(!p)return insert(end(), const_iterator(&l, lp), l.end());
		return erase(const_iterator(this, p), end());
	}
	template<class Y>
	_myt& operator=(const List<Y> &l){
		iterator it = begin();
		typename List<Y>::const_iterator lit = l.begin();
		for(;it.is_valid()&&lit.is_valid(); ++it, ++lit)
			*it = *lit;
		if(it.is_valid())return erase(it, end());
		return insert(end(), lit, l.end());
	}
	template<class It>
	_myt &append(It b, It e){
		return insert(end(), b, e);
	}
	template<class Y>
	_myt &append(const List<Y>&l){
		return insert(end(), l.begin(), l.end());
	}
	_myt &concat(_myt & l){
		if(l.isEmpty())return *this;
		_node *nb = l._head, *ne = l._tail;
		nb->prev = _tail;
		_tail = ne;
		if(!nb->prev)_head = nb;
		else nb->prev->next = nb;
		_sz+=l._sz;
		l._sz=0;
		l._head = l._tail = 0;
		return *this;
	}
	_myt &swap(_myt &x){
		_alloc * a = _Al; _Al = x._Al; x._Al = a;
		_node *t = _head; _head = x._head; x._head = t;
		t = _tail; _tail = x._tail; x._tail = t;
		size_t sz = _sz; _sz = x._sz; x._sz = sz;
		return *this;
	}
/*	void get_post(_myt &res, const_iterator mid){
		res.clear();
		res.set_alloc(Al);
		if(mid._ptr == _head){swap(res); return;}
		else if(!mid._ptr)return;
		res._tail = _tail;
		res._head = mid._ptr;
		_tail = mid._ptr->prev;
		_tail->next = 0;
		res._head->prev = 0;
	}
	_myt get_post(const_iterator mid){
		_myt res(_Al);
		if(mid._ptr == _head){swap(res); return;}
		else if(!mid._ptr)return;
		res._tail = _tail;
		res._head = mid._ptr;
		_tail = mid._ptr->prev;
		_tail->next = 0;
		res._head->prev = 0;
		return res;
	}*/
	List(_myt &L, size_t npost){ //constructs list from tail(length = npost) of L
		_Al = L._Al;
		_Al->inc_used();
		_sz = npost;
		D_CHECK(npost<=L._sz, "List(List &L, size_t len) : len>L.size");
		if(!npost){_head=_tail=0; _sz=0; return;}
		if(npost==L._sz){
			_head = L._head; _sz = L._sz; _tail = L._tail;
			L._head=L._tail = 0; L._sz=0;
			return;
		}
		L._sz -= npost;
		list_node<T> *p;
		for(p = L._tail; --npost; p = p->prev);
		_tail = L._tail;
		_head = p;
		L._tail = p->prev;
		L._tail->next = 0;
		p->prev = 0;
	}
/*	List(_myt &L, const_reverse_iterator mid){
		_myt res(_Al);
		if(mid._ptr == _tail)return;
		else if(!mid._ptr){swap(res); return;}
		_tail = L._tail;
		_head = mid._ptr->next;
		L._tail = mid._ptr->prev;
		L._tail->next = 0;
		_head->prev = 0;
	}*/
	virtual ~List();
};

template<class T>
List<T>::~List(){
	clear();
	if(_Al){
		_Al->dec_used();
		if(!_Al->is_used())delete _Al;
	}
	_head = _tail = 0;
	_sz = 0;
}

template<class T>
List<T>::iterator::iterator(const typename List<T>::reverse_iterator &it){
	this->_pl = it._pl; this->_ptr = it._ptr;
	if(!this->_ptr)this->_ptr = this->_pl->_head;
	else this->_ptr = this->_ptr->next;
}

template<class T>
List<T>::const_iterator::const_iterator(const typename List<T>::const_reverse_iterator &it){
	this->_pl = it._pl; this->_ptr = it._ptr;
	if(!this->_ptr)this->_ptr = this->_pl->_head;
	else this->_ptr = this->_ptr->next;
}

template<class T>void swap(List<T>&x, List<T>&y){x.swap(y);}
