#pragma once
#include "Memory.h"
#include <stdarg.h>

#define vaArg(list, type) va_arg(list,type)

template<class T, int sz>
class StaticArray{
	typedef StaticArray<T,sz> _myt;
protected:
	T _p[sz];
public:
	typedef T * iterator, * reverse_iterator;
	typedef const T * const_iterator, * const_reverse_iterator;
	typedef T& reference;
	typedef const T& const_reference;

	iterator begin(){return _p;}
	iterator end(){return _p+sz;}
	const_iterator begin()const{return _p;}
	const_iterator end()const{return _p+sz;}
	reverse_iterator rbegin(){return _p+sz-1;}
	reverse_iterator rend(){return _p-1;}
	const_reverse_iterator rbegin()const{return _p+sz-1;}
	const_reverse_iterator rend()const{return _p-1;}

	template<class Y>
	StaticArray<T,sz>& fill(const Y& val){for(int i=0; i<sz; i++)_p[i]=val;return *this;}
	static int size(){return sz;}
//	operator T*(){return _p;}
//	operator const T*()const{return _p;}
	T* data(){return _p;}
	const T* data()const{return _p;}
	const T&operator[](int i)const{return _p[i];}
	T&operator[](int i){return _p[i];}
	StaticArray(){}
	template<class Y>
	StaticArray(const StaticArray<Y, sz> &arr){ Memcpy(_p, arr._p, sz); }
	template<class Y>
	StaticArray<T,sz>& operator=(const StaticArray<Y,sz> &a){
		Memcpy(_p, a._p, sz);
		return *this;
	}
	template<class Y>
	StaticArray<T,sz>& operator=(const Y *val){
		Memcpy(_p, val, sz);
		return *this;
	}
	bool operator<(const StaticArray<T,sz> &a)const{
		for(int i=0; i<sz; i++)
			if(_p[i]<a._p[i])true;
			else if(a._p[i]<_p[i])return false;
		return false;
	}
	template<class Y>
	bool operator==(const StaticArray<Y,sz> &A)const{
		for(int i=0; i<sz; i++)
			if(_p[i]!=A._p[i])return false;
		return true;
	}
	template<class Y>
	bool operator!=(const StaticArray<Y,sz> &A)const{return !(*this==A);}
	T sum()const{T res = _p[0]; for(int i=1; i<sz; i++)res+=_p[i]; return res;}
	T product()const{T res = _p[0]; for(int i=1; i<sz; i++)res*=_p[i]; return res;}
	T bitAnd()const{T res = _p[0]; for(int i=1; i<sz; i++)res&=_p[i]; return res;}
	T bitOr()const{T res = _p[0]; for(int i=1; i<sz; i++)res|=_p[i]; return res;}
	T bitXor()const{T res = _p[0]; for(int i=1; i<sz; i++)res^=_p[i]; return res;}
	template<class Y>
	StaticArray(const Y* val){Memcpy(_p, val, sz);}
#define _MARR_DECL_TEMPL_OPERATOR(op)\
	template<class Y> StaticArray<T,sz>& operator op(const StaticArray<Y,sz> &a){\
		for(int i=0; i<sz; i++){_p[i] op a._p[i];} return *this;}
#define _MARR_SCALAR_OPERATOR(op)\
	template<class Y> StaticArray<T,sz>& operator op(const Y &val){\
		for(int i=0; i<sz; i++){_p[i] op val;} return *this;}

	_MARR_DECL_TEMPL_OPERATOR(+=)
	_MARR_DECL_TEMPL_OPERATOR(-=)
	_MARR_DECL_TEMPL_OPERATOR(*=)
	_MARR_SCALAR_OPERATOR(*=)
	_MARR_DECL_TEMPL_OPERATOR(/=)
	_MARR_SCALAR_OPERATOR(/=)
	_MARR_DECL_TEMPL_OPERATOR(%=)
	_MARR_SCALAR_OPERATOR(%=)
	_MARR_DECL_TEMPL_OPERATOR(&=)
	_MARR_DECL_TEMPL_OPERATOR(|=)
	_MARR_DECL_TEMPL_OPERATOR(<<=)
	_MARR_SCALAR_OPERATOR(<<=)
	_MARR_DECL_TEMPL_OPERATOR(>>=)
	_MARR_SCALAR_OPERATOR(>>=)
#undef _MARR_DECL_TEMPL_OPERATOR
#undef _MARR_SCALAR_OPERATOR
	StaticArray<T,sz> operator-(){
		StaticArray<T,sz> res;
		for(int i=0; i<sz; i++)res._p[i]=-_p[i];
		return res;
	}
	StaticArray<T,sz> operator~(){
		StaticArray<T,sz> res;
		for(int i=0; i<sz; i++)res._p[i]=~_p[i];
		return res;
	}
	_myt& read(const T &x1, va_list& vl);
	explicit StaticArray(T x1,...);
	~StaticArray(){}
};

template<class T>
class VectorConstRef{
	const T *_p;
	unsigned int _sz;
	typedef VectorConstRef<T> _myt;
public:
	int size()const{return _sz;}
	const T* data()const{return _p;}
	const T&operator[](int i)const{return _p[i];}
	VectorConstRef(){_p=0; _sz=0;}
	VectorConstRef(const T *ptr, unsigned int sz){_p=ptr; _sz=sz;}

	bool operator<(const _myt &a)const{
		for(int i=0; i<_sz; i++)
			if(_p[i]<a._p[i])true;
			else if(a._p[i]<_p[i])return false;
		return false;
	}
	template<class Y>
	bool operator==(const VectorConstRef<Y> &A)const{
		for(int i=0; i<_sz; i++)
			if(_p[i]!=A._p[i])return false;
		return true;
	}
	template<class Y>
	bool operator!=(const VectorConstRef<Y> &A)const{return !(*this==A);}
	T sum()const{T res = _p[0]; for(int i=1; i<_sz; i++)res+=_p[i]; return res;}
	T product()const{T res = _p[0]; for(int i=1; i<_sz; i++)res*=_p[i]; return res;}
	T bitAnd()const{T res = _p[0]; for(int i=1; i<_sz; i++)res&=_p[i]; return res;}
	T bitOr()const{T res = _p[0]; for(int i=1; i<_sz; i++)res|=_p[i]; return res;}
	T bitXor()const{T res = _p[0]; for(int i=1; i<_sz; i++)res^=_p[i]; return res;}
};

template<class T>
class StaticArray<T,1>{
	typedef StaticArray<T,1> _myt;
protected:
	T _p;
public:
	typedef T * iterator, * reverse_iterator;
	typedef const T * const_iterator, * const_reverse_iterator;
	typedef T& reference;
	typedef const T& const_reference;

	iterator begin(){return &_p;}
	iterator end(){return &_p+1;}
	const_iterator begin()const{return &_p;}
	const_iterator end()const{return &_p+1;}
	reverse_iterator rbegin(){return &_p;}
	reverse_iterator rend(){return &_p-1;}
	const_reverse_iterator rbegin()const{return &_p;}
	const_reverse_iterator rend()const{return &_p-1;}

	template<class Y>
	StaticArray<T,1>& fill(const Y& val){_p=val; return *this;}
	static int size(){return 1;}
	T* data(){return &_p;}
	const T* data()const{return &_p;}
	const T&operator[](int)const{return _p;}
	T&operator[](int){return _p;}
	StaticArray(){}
	template<class Y>
	StaticArray(const StaticArray<Y, 1> &arr){_p = arr._p; }
	template<class Y> StaticArray<T,1>& operator=(const StaticArray<Y,1> &a){_p = a._p;return *this;}
	template<class Y> StaticArray<T,1>& operator=(const Y *val){_p = *val;return *this;}
	operator T&(){return _p;}
	operator const T&()const{return _p;}
	T sum()const{return _p;}
	T product()const{return _p;}
	T bitAnd()const{return _p;}
	T bitOr()const{return _p;}
	T bitXor()const{return _p;}
	template<class Y> StaticArray(const Y* val){_p= *val;}
	StaticArray(const T& val){_p= val;}
	_myt& read(const T &x1, va_list&){_p=x1;return *this;}
	~StaticArray(){}
};

#define _MARR_DECL_TEMPL_OPERATOR(op)\
template<class T, int sz> StaticArray<T,sz> operator op(const StaticArray<T,sz> &a, const StaticArray<T,sz> &b){\
	StaticArray<T,sz> res; for(int i=0; i<sz; i++){res[i] = a[i] op b[i];} return res;}
#define _MARR_DECL_TEMPL_SCALAR_OPERATOR(op)\
template<class T, class Y, int sz> StaticArray<T,sz> operator op(const StaticArray<T,sz> &a, const Y&b){\
	StaticArray<T,sz> res; for(int i=0; i<sz; i++){res[i] = a[i] op b;} return res;}
_MARR_DECL_TEMPL_OPERATOR(+)
_MARR_DECL_TEMPL_OPERATOR(-)
_MARR_DECL_TEMPL_OPERATOR(*)
_MARR_DECL_TEMPL_OPERATOR(/)
_MARR_DECL_TEMPL_OPERATOR(%)
_MARR_DECL_TEMPL_OPERATOR(&)
_MARR_DECL_TEMPL_OPERATOR(|)
_MARR_DECL_TEMPL_OPERATOR(<<)
_MARR_DECL_TEMPL_OPERATOR(>>)

_MARR_DECL_TEMPL_SCALAR_OPERATOR(*)
_MARR_DECL_TEMPL_SCALAR_OPERATOR(/)
#undef _MARR_DECL_TEMPL_OPERATOR
#undef _MARR_DECL_TEMPL_SCALAR_OPERATOR

template<class T, int sz>
T operator,(const StaticArray<T, sz> &a, const T* b){
	T res(0);
	for(int i=0;i<sz;i++)
		res += a[i] * b[i];
	return res;
}
template<class T, int sz>
T operator,(const T*b, const StaticArray<T, sz> &a){return (a,b);}
template<class T, int sz1, int sz2>
T operator,(const StaticArray<T, sz1> &a, const StaticArray<T,sz2> &b){
	if(sz1<sz2)return (a, b.data());
	return (b, a.data());
}

template<int sz>
class IndexMD:public StaticArray<int, sz>{
public:
	IndexMD():StaticArray<int,sz>(){}
	explicit IndexMD(int x1,...);
	IndexMD(const int *i):StaticArray<int,sz>(i){}
};

class Index2D:public IndexMD<2>{
public:
	Index2D():IndexMD<2>(){}
	Index2D(int x1,int x2){_p[0]=x1; _p[1]=x2;}
	Index2D(const int *i){_p[0]=i[0]; _p[1]=i[1];}
};

class Index3D:public IndexMD<3>{
public:
	Index3D():IndexMD<3>(){}
	Index3D(int x1, int x2, int x3){_p[0]=x1; _p[1]=x2; _p[2]=x3;}
	Index3D(const int *i){_p[0]=i[0]; _p[1]=i[1]; _p[2]=i[2];}
};

class Index4D:public IndexMD<4>{
public:
	int &i(){return this->_p[0];}
	int &j(){return this->_p[1];}
	int &k(){return this->_p[2];}
	int &l(){return this->_p[3];}
	int i()const{return this->_p[0];}
	int j()const{return this->_p[1];}
	int k()const{return this->_p[2];}
	int l()const{return this->_p[3];}
	Index4D(){}
	Index4D(int x1, int x2, int x3, int x4){_p[0]=x1; _p[1]=x2; _p[2]=x3; _p[3]=x4;}
	Index4D(const int *i):IndexMD<4>(i){}
};

template<class T, int dim>
class ConstPointerMD{
	typedef ConstPointerMD<T,dim> _myt;
protected:
	const T* _p;
	IndexMD<dim> _d;
public:
	ConstPointerMD(){D_CHECK(dim>0, "invalid array dimension"); _p=0; _d.fill(0);}
	ConstPointerMD(const IndexMD<dim> &d):_d(d){D_CHECK(dim>0, "invalid array dimension");_p=0;}
	ConstPointerMD(const T *p, const IndexMD<dim> &d):_d(d){D_CHECK(dim>0, "invalid array dimension");_p=p;}
	void setDelta(IndexMD<dim> &d){_d = d;}
	const IndexMD<dim>& delta()const{return _d;}
	_myt& operator +=(const IndexMD<dim> &d){_p+=(_d,d); return *this;}
	_myt& operator -=(const IndexMD<dim> &d){_p-=(_d,d); return *this;}
	_myt operator+(const IndexMD<dim> &d)const{ return _myt(_p+(d,_d), _d);}
	_myt operator-(const IndexMD<dim> &d)const{ return _myt(_p-(d,_d), _d);}
	IndexMD<dim> operator-(const _myt& x){
		int delta = _p-x._p;
		IndexMD<dim> res;
		for(int i=0; i<dim; i++){
			res[i] = delta/_d[i];
			delta %= _d[i];
		}
		D_CHECK(!delta, "invalid pointer operation");
		return res;
	}
	const T& operator[](const IndexMD<dim> &i)const{
		return _p[(_d,i)];
	}
	template<int dim1>
	ConstPointerMD<T, dim-dim1> operator[](const IndexMD<dim1> &i)const{
		return ConstPointerMD<T,dim-dim1>(_p+(i,_d), IndexMD<dim-dim1>(_d.data()+dim1));
	}
	ConstPointerMD<T, dim-1> operator[](int i)const{
		return ConstPointerMD<T,dim-1>(_p+i*_d[0], IndexMD<dim-1>(_d.data()+1));
	}
	const T& operator*()const{return *_p;}
	~ConstPointerMD(){}
};

template<class T, int dim>
class PointerMD : public ConstPointerMD<T,dim>{
	typedef ConstPointerMD<T,dim> _myt;
public:
	PointerMD():ConstPointerMD<T,dim>(){}
	PointerMD(const IndexMD<dim> &d):ConstPointerMD<T,dim>(0,d){}
	PointerMD(T *p, const IndexMD<dim> &d):ConstPointerMD<T,dim>(p,d){}
	_myt& operator = (const T *p){this->_p = p; this->d=1;}
	_myt& operator +=(const IndexMD<dim> &d){this->_p+=(this->_d,d); return *this;}
	_myt& operator -=(const IndexMD<dim> &d){this->_p-=(this->_d,d); return *this;}
	_myt operator+(const IndexMD<dim> &d)const{ return _myt((T*)this->_p+(d,this->_d), this->_d);}
	_myt operator-(const IndexMD<dim> &d)const{ return _myt((T*)this->_p-(d,this->_d), this->_d);}
	T& operator*()const{return *(T*)this->_p;}
	T& operator[](const IndexMD<dim> &i)const{
		return *(T*)(this->_p+(this->_d,i));
	}
	template<int dim1>
	PointerMD<T, dim-dim1> operator[](const IndexMD<dim1> &i)const{
		return PointerMD<T, dim-dim1>((T*)this->_p+(i,this->_d), IndexMD<dim-dim1>(this->_d.data()+dim1));
	}
	PointerMD<T, dim-1> operator[](int i)const{
		return PointerMD<T,dim-1>((T*)this->_p+i*this->_d[0], IndexMD<dim-1>(this->_d.data()+1));
	}
};

template<class T>
class ConstPointerMD<T, 1>{
	typedef ConstPointerMD<T,1> _myt;
protected:
	const T *_p;
	int _d;
public:
	ConstPointerMD(){_p=0; _d=0;}
	ConstPointerMD(int d){_p=0; _d=d;}
	ConstPointerMD(const T *p, int d){_p=p; _d=d;}
	void setDelta(int d){_d = d;}
	int delta()const{return _d;}
	const T *ptr()const{return _p;}
	_myt& operator = (const T *p){_p = p;}
	_myt& operator +=(int d){_p += _d*d; return *this;}
	_myt& operator -=(int d){_p -= _d*d; return *this;}
	_myt operator+(int d)const{ return _myt(_p + _d*d, _d);}
	_myt operator-(int d)const{ return _myt(_p - _d*d, _d);}
	int operator-(const _myt& x){
		return (_p - x._p)/_d;
	}
	const T& operator[](int i)const{
		return _p[_d*i];
	}
	const T& operator*()const{return *_p;}
};

template<class T>
class PointerMD<T, 1>:public ConstPointerMD<T,1>{
	typedef PointerMD<T,1> _myt;
public:
	PointerMD(){}
	PointerMD(int d):ConstPointerMD<T,1>(d){}
	PointerMD(T *p, int d):ConstPointerMD<T,1>(p,d){}
	T *ptr()const{return (T*)this->_p;}
	_myt& operator = (T *p){this->_p = p; this->d=1;}
	_myt& operator +=(int d){this->_p += this->_d*d; return *this;}
	_myt& operator -=(int d){this->_p -= this->_d*d; return *this;}
	_myt operator+(int d)const{ return _myt(this->_p + this->_d*d, this->_d);}
	_myt operator-(int d)const{ return _myt(this->_p - this->_d*d, this->_d);}
	T& operator[](int i)const{
		return *(T*)(this->_p+this->_d*i);
	}
	T& operator*()const{return *(T*)this->_p;}
};

template<class T>
class ConstPointer2D : public ConstPointerMD<T,2>{
public:
	ConstPointer2D(T * p, IndexMD<2> &i):PointerMD<T,2>(p,i){}
	ConstPointer2D(T * p, size_t d1, size_t d2){
		this->_p = p;
		this->_d[0] = d1; this->_d[1] = d2;
	}
	ConstPointer2D(const ConstPointerMD<T,2>&p):ConstPointerMD<T,2>(p){}
	Index2D operator-(const ConstPointer2D &p)const{
		int delta = this->_p-p._p;
		if(this->_d[1]>this->_d[0])
			return Index2D((delta%this->_d[1])/this->_d[0], delta/this->_d[1]);
		return Index2D(delta/this->_d[0], (delta%this->_d[0])/this->_d[1]);
	}
};
template<class T>
class Pointer2D : public PointerMD<T,2>{
public:
	Pointer2D(T * p, IndexMD<2> &i):PointerMD<T,2>(p,i){}
	Pointer2D(T * p, size_t d1, size_t d2){
		this->_p = p;
		this->_d[0] = d1; this->_d[1] = d2;
	}
	Pointer2D(const PointerMD<T,2>&p):PointerMD<T,2>(p){}
	operator ConstPointer2D<T>&(){return *(ConstPointer2D<T>*)this;}
};

template<class T>
class ConstPointer3D : public ConstPointerMD<T,3>{
public:
	ConstPointer3D(T * p, IndexMD<3> &i):PointerMD<T,3>(p,i){}
	ConstPointer3D(T * p, size_t d1, size_t d2, size_t d3){
		this->_p = p;
		this->_d[0] = d1; this->_d[1] = d2; this->_d[2] = d3;
	}
	ConstPointer3D(const ConstPointerMD<T,3>&p):ConstPointerMD<T,3>(p){}
	ConstPointer2D<T> operator[](int i){return ConstPointer2D<T>(this->_p+i*this->_d[0], this->_d[1], this->_d[2]);}
	Index3D operator-(const ConstPointer3D &p)const{
		int delta = this->_p - p._p;
		int d1 = delta%this->_d[0];
		return Index3D(delta/this->_d[0],d1/this->_d[1], (d1%this->_d[1])/this->_d[2]);
	}
};
template<class T>
class Pointer3D : public PointerMD<T,3>{
public:
	Pointer3D(T * p, IndexMD<2> &i):PointerMD<T,3>(p,i){}
	Pointer3D(T * p, size_t d1, size_t d2, size_t d3){
		this->_p = p;
		this->_d[0] = d1; this->_d[1] = d2; this->_d[2] = d3;
	}
	Pointer3D(const PointerMD<T,3>&p):PointerMD<T,3>(p){}
	Pointer2D<T> operator[](int i){return Pointer2D<T>((T*)this->_p+i*this->_d[0], this->_d[1], this->_d[2]);}
	operator ConstPointer3D<T>&(){return *(ConstPointer3D<T>*)this;}
};

template<class T, int dim>
class ArrayMD{
	typedef ArrayMD<T, dim> _myt;
protected:
	T *_p;
	IndexMD<dim> _sz;
public:
	const IndexMD<dim>& size()const{return _sz;}
	ArrayMD(){_sz.fill(0); _p=0;}
	ArrayMD(int sz1, ...);
	ArrayMD(const _myt &arr){
		_p = 0; _sz.fill(0);
		resize(arr._sz);
		Memcpy(_p, arr._p, _sz.product());
	}
	_myt& operator=(const _myt &arr){
		resize(arr._sz);
		Memcpy(_p, arr._p, _sz.product());
		return *this;
	}
	ArrayMD(const IndexMD<dim> &sz){
		_sz = sz;
		_p = _construct(_p=0, sz.product());
	}
	ArrayMD(const T &val, const IndexMD<dim> &sz){
		_sz = sz;
		_p = _construct(_p=0, val, sz.product());
	}
	void resize(const IndexMD<dim> &sz){
		if(_sz == sz)return;
		int p = _sz.product(), n = sz.product();
		_sz = sz;
		if(n!=p){
			delete _p;
			_p = _construct(_p=0, n);
		}
	}
	const T& operator[](const IndexMD<dim> &ind)const{
		int d = ind[0];
		for(int i=1; i<dim; i++)
			d = d*_sz[i]+ind[i];
		return _p[d];
	}
	T &operator [](const IndexMD<dim> &ind){
		return *(T*)&(*((const _myt*)(this)))[ind];
	}

	template<int dim1>
	ConstPointerMD<T, dim-dim1> operator[](const IndexMD<dim1> &ind)const{
		return ConstPointerMD<T,dim>(*this)[ind];
	}
	template<int dim1>
	PointerMD<T, dim-dim1> operator[](const IndexMD<dim1> &ind){
		return PointerMD<T,dim>(*this)[ind];
	}

	const T* operator[](const IndexMD<dim-1> &ind)const{
		int d = ind[0];
		for(int i=1; i<dim-1; i++)
			d = d*_sz[i]+ind[i];
		return _p+d*_sz[dim-1];
	}
	T *operator [](const IndexMD<dim-1> &ind){
		return (T*)(*((const _myt*)(this)))[ind];
	}

	const T& operator()(int i1, ...)const;
	T& operator()(int i1, ...);
	operator PointerMD<T,dim>(){
		IndexMD<dim> ind;
		ind[dim-1]=1;
		for(int i=dim; --i; )ind[i-1]=_sz[i]*ind[i];
		return PointerMD<T,dim>(_p, ind);
	}
	operator ConstPointerMD<T,dim>()const{return (PointerMD<T,dim>)(*(_myt*)this);}
	void clear(){_destruct(_p, _sz.product()); _sz.fill(0); free(_p); _p=0;}
	~ArrayMD(){clear();}
};

template<class T>
class Array2D:public ArrayMD<T,2>{
public:
	Array2D(){}
	Array2D(size_t sz1, size_t sz2):ArrayMD<T,2>(Index2D(sz1,sz2)){}
	Array2D(size_t sz1, size_t sz2, const T &val):ArrayMD<T,2>(val, Index2D(sz1, sz2)){}

	void resize(const IndexMD<2> &sz){ArrayMD<T,2>::resize(sz);}
	void resize(int sz1, int sz2){ArrayMD<T,2>::resize(Index2D(sz1,sz2));}

	const T* operator[](size_t i)const{return this->_p+i*this->_sz[1];}
	T* operator[](size_t i){return this->_p+i*this->_sz[1];}
	const T* row(size_t i)const{return (*this)[i];}
	T* row(size_t i){return (*this)[i];}

	const T& operator[](const Index2D &i)const{return this->_p[i[0]*this->_sz[1]+i[1]];}
	T& operator[](const Index2D&i){return this->_p[i[0]*this->_sz[1]+i[1]];}
	const T& operator()(size_t i, size_t j)const{return this->_p[this->_sz[1]*i+j];}
	T& operator()(size_t i, size_t j){return this->_p[this->_sz[1]*i+j];}

	operator Pointer2D<T>(){return Pointer2D<T>(this->_p, this->_sz[1], 1);}
	operator ConstPointer2D<T>()const{return ConstPointer2D<T>(this->_p, this->_sz[1], 1);}
};

template<class T>
class Array3D:public ArrayMD<T,3>{
public:
	Array3D(){}
	Array3D(Index3D &sz):ArrayMD<T,3>(sz){}
	Array3D(const T&val, Index3D &sz):ArrayMD<T,3>(val, sz){}
	Array3D(size_t sz1, size_t sz2, size_t sz3):ArrayMD<T,3>(Index3D(sz1,sz2,sz3)){}
	Array3D(size_t sz1, size_t sz2, size_t sz3, const T &val):ArrayMD<T,3>(val, Index3D(sz1, sz2, sz3)){}

	const T* operator()(size_t i, size_t j)const{return this->_p + this->_sz[2]*(this->_sz[1]*i+j);}
	T* operator()(size_t i, size_t j){return this->_p + this->_sz[2]*(this->_sz[1]*i+j);}
	const T& operator()(size_t i, size_t j, size_t k)const{return this->_p[this->_sz[2]*(this->_sz[1]*i+j)+k];}
	T& operator()(size_t i, size_t j, size_t k){return this->_p[this->_sz[2]*(this->_sz[1]*i+j)+k];}
};

template<class T>
class Array4D:public ArrayMD<T,4>{
public:
	Array4D(){}
	Array4D(Index4D &sz):ArrayMD<T,4>(sz){}
	Array4D(const T&val, Index4D &sz):ArrayMD<T,4>(val, sz){}
	Array4D(size_t sz1, size_t sz2, size_t sz3, size_t sz4):ArrayMD<T,4>(Index4D(sz1,sz2,sz3)){}
	Array4D(size_t sz1, size_t sz2, size_t sz3, size_t sz4, const T &val):ArrayMD<T,4>(val, Index4D(sz1, sz2, sz3, sz4)){}

	const T* operator()(size_t i, size_t j, size_t k)const{return this->_p + this->_sz[3]*(this->_sz[2]*(this->_sz[1]*i+j)+k);}
	T* operator()(size_t i, size_t j, size_t k){return this->_p + this->_sz[3]*(this->_sz[2]*(this->_sz[1]*i+j)+k);}
	const T& operator()(size_t i, size_t j, size_t k, size_t l)const{return this->_p[this->_sz[3]*(this->_sz[2]*(this->_sz[1]*i+j)+k)+l];}
	T& operator()(size_t i, size_t j, size_t k, size_t l){return this->_p[this->_sz[3]*(this->_sz[2]*(this->_sz[1]*i+j)+k)+l];}
};

template<class T, int sz>
StaticArray<T,sz> &StaticArray<T,sz>::read(const T &x1, va_list &vl){
	_p[0] = x1;
	for(int i=1; i<sz; i++)
		_p[i] = va_arg(vl, T);
	return *this;
}

template<class T, int sz>
StaticArray<T,sz>::StaticArray(T x1,...){
	va_list vl;
	va_start(vl, x1);
	read(x1, vl);
	va_end(vl);
}

template<int sz>
IndexMD<sz>::IndexMD(int x1,...){
	va_list vl;
	va_start(vl, x1);
	this->read(x1, vl);
	va_end(vl);
}

template<class T, int dim>
ArrayMD<T,dim>::ArrayMD(int sz1, ...){
	va_list vl;
	va_start(vl, sz1);
	_sz.read(sz1, vl);
	va_end(vl);
	_p = _construct(_p=0, _sz.product());
}

template<class T, int dim>
const T &ArrayMD<T,dim>::operator ()(int i1,...) const{
	IndexMD<dim> i;
	va_list vl;
	va_start(vl, i1);
	i.read(i1, vl);
	va_end(vl);
	return (*this)[i];
}

template<class T, int dim>
T &ArrayMD<T,dim>::operator ()(int i1,...){
	IndexMD<dim> i;
	va_list vl;
	va_start(vl, i1);
	i.read(i1, vl);
	va_end(vl);
	return (*this)[i];
}
