#pragma once
#include <stdlib.h>
#include <new>
#include "../common/commonfunctions.h"

template<class T>
void _destruct(T* x){
	x->~T(); return; x=x;
}
template<class T>
void _destruct(T**){}

#define DECL_DESTRUCT(T) template<> inline void _destruct(T*){} inline void _destruct(T*, size_t){}
DECL_DESTRUCT(bool)
DECL_DESTRUCT(char)
DECL_DESTRUCT(wchar_t)
DECL_DESTRUCT(int)
DECL_DESTRUCT(long)
DECL_DESTRUCT(long long)
DECL_DESTRUCT(unsigned char)
DECL_DESTRUCT(unsigned int)
DECL_DESTRUCT(unsigned long)
DECL_DESTRUCT(unsigned long long)
DECL_DESTRUCT(double)
DECL_DESTRUCT(float)
#undef DECL_DESTRUCT

template<class T, class Y>
T * _construct(T *dst, const Y &val, size_t sz){
	if(!sz)return dst;
	if(!dst)dst = (T*)calloc(sz, sizeof(T));
	if(!dst)return 0;
	for(size_t i=0; i<sz; i++)
		::new(dst+i) T(val);
	return dst;
}

template<class T>
T * _construct(T *dst, size_t sz){
	if(!sz)return dst;
	if(!dst)dst = (T*)calloc(sz, sizeof(T));
	if(!dst)return 0;
	for(size_t i=0; i<sz; i++)
		::new(dst+i) T;
	return dst;
}

template<class T, class It>
T * _construct_copy(T *dst, It src, size_t sz){
	if(!sz)return dst;
	if(!dst)dst = (T*)calloc(sz, sizeof(T));
	if(!dst)return 0;
	for(size_t i=0; i<sz; i++, ++src)
		::new(dst+i) T(*src);
	return dst;
}

template<class T> void _destruct(T *arr, size_t sz){
	for(size_t i=0; i<sz; i++)
		_destruct(arr+i);
}

template<class T> class AllocRef;

class memoryRegistry{
	void *_data;
	memoryRegistry(const memoryRegistry&);
	memoryRegistry& operator = (const memoryRegistry& reg);
public:
	enum memFlags{
		singleAlloc = 0x0,
		vectorAlloc = 0x1
	};
	struct memData{
		unsigned long long sz;
		size_t size()const{
			return size_t(sz>>8);
		}
		int flags()const{return int(sz&255);}
		memData(size_t s, int flags){sz = (s<<8)|(flags&255);}
	};
	memoryRegistry();
	~memoryRegistry();
/*	void registerMemory(const void *ptr, size_t size, bool isVector){
		return registerMemory(ptr, memData(size, isVector ? vectorAlloc : 0));
	}*/
	void registerMemory(const void *ptr, const memData &data);
	void deleteData(void *ptr);
	size_t avaliable(const void *ptr)const;
	memData memoryData(const void *ptr)const;
	size_t memorySize(const void *ptr)const;
	const void *blockBegin(const void *ptr);
	void *blockBegin(void *ptr);
	bool hasPointer(const void *ptr);
	template<class T> void destruct(T *ptr){
		if(!ptr)return 0;
		const memData &dat = memoryData(ptr);
		size_t sz = dat.size()/sizeof(T);
		D_CHECK(sz!=0, "destruct : it wasn't same type when created");
		if(dat.flags()&vectorAlloc)delete[] ptr;
		else {_destruct(ptr, sz); free(ptr);}
		deleteData(ptr);
	}
};
//static memoryRegistry __mem_reg;
inline void *operator new(size_t sz, memoryRegistry &mr){
	void *res = sz ? malloc(sz) : 0;
	if(res)mr.registerMemory(res, memoryRegistry::memData(sz, memoryRegistry::singleAlloc));
	return res;
}
inline void *operator new[](size_t sz, memoryRegistry &mr){
	void *res = sz ? malloc(sz) : 0;
	if(res)mr.registerMemory(res, memoryRegistry::memData(sz, memoryRegistry::vectorAlloc));
	return res;
}
inline void operator delete(void* ptr, memoryRegistry &mr){
	if(mr.hasPointer(ptr))free(ptr);
	mr.deleteData(ptr);
}

template<class T>
class Alloc{
	union AllocTp{
		char p[sizeof(T)];
		AllocTp *next;
	};
	struct ListAllocNode{
		size_t sz;
		AllocTp* p;
		ListAllocNode *next;
		ListAllocNode(size_t n, ListAllocNode *nxt=0){
			p = (AllocTp*)calloc(n, sizeof(AllocTp));
			for(size_t i=1; i<n; i++)
				p[i-1].next = p+i;
			if(n)p[n-1].next = 0;
			next = nxt;
			sz = n;
		}
		~ListAllocNode(){
			::free(p);
		}
	};

	double exp_coef;
	int start_size;
	ListAllocNode *head;
	AllocTp *fr;
	int used;
	bool _is_static;
	void _check(){
		if(!fr){
			if(head)head = new ListAllocNode(int(head->sz*exp_coef)+1, head);
			else head = new ListAllocNode(start_size);
			fr = head->p;
		}
	}
	bool _check_node(const T *x)const{
		for(ListAllocNode * p = head; p; p = p->next)
			if(x>=p->p && x - p->p < p->sz)return true;
		return false;
	}

//	friend Alloc<T>* NewAlloc(size_t start_sz, double k);
//	friend void CopyAlloc(Alloc<T> *&to, Alloc<T> *from);
//	friend void DeleteAlloc(Alloc<T> *p);

	//friend class AllocRef<T>;
	void destroy(){
		ListAllocNode *p = head;
		for(;p; p=head){
			head = p->next;
			delete p;
		}
		head = 0;
	}
public:
	void inc_used(){if(this)used++;}
	void dec_used(){if(this)used--;}
	int is_used(){return used;}
	bool is_static(){return _is_static;}
	Alloc(size_t start_sz=10, double k=1.5){
#ifdef _DEBUG
		if(k<1)throw "Alloc : k<1";
#endif
		start_size = start_sz;
		exp_coef = k;
		head = 0;
		used = 1;
		_is_static = true;
		fr = 0;
	}
	T* get_new(){
		_check();
		T *p = (T*)fr;
		fr = fr->next;
		::new(p) T();
		return p;
	}
	template<class Y>
	T* get_new(const Y &y){
		_check();
		T *p = (T*)fr;
		fr = fr->next;
		::new(p) T(y);
		return p;
	}
	void free(T *n){
#ifdef _DEBUG_ALLOCATION
		if(!_check_node(n))throw "Alloc::free : node isn't from current allocator";
#endif
		_destruct(n);
		((AllocTp*)n)->next = fr;
		fr = ((AllocTp*)n);
	}
	~Alloc(){
		destroy();
	}
};

template<class T> Alloc<T>* NewAlloc(size_t start_sz=10, double k=1.5){
	Alloc<T> *res = new Alloc<T>(start_sz, k);
//	res->_is_static = false;
	return res;
}

template<class T> void CopyAlloc(Alloc<T> *&to, Alloc<T> *from){
	(to = from)->inc_used();
}

template<class T> void DeleteAlloc(Alloc<T> *p){
	if(!p)return;
	p->dec_used();
	if(!p->is_used())delete p;
}

template<class T>
class AllocRef{
	Alloc<T> *_Al;
public:
	void swap(AllocRef<T> &ar){
		Alloc<T> *t = _Al; _Al = ar._Al; ar._Al = t;
	}
	AllocRef(size_t start_sz=10, double k=1.5){
		_Al = NewAlloc<T>(start_sz, k);
	}
	AllocRef(const AllocRef<T> &ar){
		CopyAlloc(_Al, (Alloc<T>*)ar._Al);
	}
	AllocRef& operator=(const AllocRef &ar){
		if(ar._Al==_Al)return *this;
		DeleteAlloc(_Al);
		CopyAlloc(_Al, (Alloc<T>*)ar._Al);
		return *this;
	}
	~AllocRef(){
		DeleteAlloc(_Al);
		_Al = 0;
	}
	T *get_new(){
		return _Al->get_new();
	}
	template<class Y>
	T *get_new(const Y& y){
		return _Al->get_new(y);
	}
	void free(T *p){
		return _Al->free(p);
	}
};
template<class T>
void Exchange(AllocRef<T> &x, AllocRef<T> &y){x.swap(y);}
