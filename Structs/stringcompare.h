#pragma once

#include <string>
#include "Tree.h"

struct _StringCompare{
	int compare(const std::string&s1, const std::string &s2)const{
		return strcmp(s1.c_str(), s2.c_str());
	}
	int compare(const std::string&s1, const char *s2)const{
		return strcmp(s1.c_str(), s2);
	}
	template<class C>
	int compare(const std::string &s1, const Pair<const C*, int>& p)const{
		const C *ptr = p.first;
		const char *str = s1.c_str();
		size_t l = Min(size_t(p.second), s1.length()), i;
		for(i = 0; i<l; i++)
			if(str[i]<ptr[i])return -1;
			else if(str[i]>ptr[i])return 1;
		if(s1.length()>l)return 1;
		if(p.second>(int)l)return -1;
		return 0;
	}
};

#define stringSet Set<std::string, _StringCompare>
#define stringNumberedSet NumberedSet<std::string, _StringCompare>
#define stringMap(T) Map<std::string, T, _StringCompare>
#define stringNumberedMap(T) NumberedMap<std::string, T, _StringCompare>
