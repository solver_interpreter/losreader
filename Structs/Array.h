#pragma once
#include <stdlib.h>
#include <stdarg.h>

#ifdef ENABLE_MEMORY_ERROR
#define MEMORY_ERROR(decl) decl
#else
#define MEMORY_ERROR(decl)
#endif

MEMORY_ERROR(
	int reset_memory_error(int Err=0);
	int get_memory_error();
	)
/*#ifdef _DEBUG
#ifndef _ASSERT_EXPR
#define _ASSERT_EXPR(expr, except) if(!(expr))throw except;
#endif
#else
#ifndef _ASSERT_EXPR
#define _ASSERT_EXPR(expr, except) ;
#endif
#endif*/
#ifndef D_CHECK
#ifdef _DEBUG
#define D_CHECK(expr, except) if(!(expr))throw except;
#else
#define D_CHECK(expr, except) ;
#endif
#endif

#ifndef MAX_MIN_ABS_SWAP
#define MAX_MIN_ABS_SWAP
template<class T> T& pMin(T &a, T &b){return a>b ? b : a;}
template<class T> T& pMax(T &a, T &b){return a<b ? b : a;}
template<class T> const T& pcMin(const T &a, const T &b){return a>b ? b : a;}
template<class T> const T& pcMax(const T &a, const T &b){return a<b ? b : a;}
template<class T> T Min(T a, T b){return a>b ? b : a;}
template<class T> T Max(T a, T b){return a<b ? b : a;}
template<class T> T Sqr(T x){return x*x;}
template<class T> T Cube(T x){return x*x*x;}
template<class T> T Abs(const T &a){return a<0 ? -a : a;}
template<class T> void Exchange(T&a, T&b){T t=a; a=b; b=t;}
#endif

template<class T>
struct Array{
	T * p;
	int size;
	Array(int sz = 0);
	Array<T>& swap(Array<T> &a){
		Exchange(p, a.p);
		Exchange(size, a.size);
		return *this;
	}
	Array<T>& init(int sz, bool copy=false);
	Array(const Array<T> & A){
		p=0; size=0;
		init(A.size);
		copy_from(A);
	}
	Array<T>& expand(int sz, bool save=false){
		if(sz<=size)return *this;
		if(save){
			T *po = p;
			p=0;
			int osz = size;
			init(sz);
			if(p)copy_from(po, osz);
			delete[] po;
		}else init(sz);
		return *this;
	}
	Array<T>& expand_coef(int sz, double coef=1.5){
		D_CHECK(coef>=1, "Array::expand_coef : coef<1");
		if(sz<=size)return *this;
		T *po = p;
		p=0;
		int osz = size;
		init(Max(int(size*coef)+1, sz));
		if(p)copy_from(po, osz);
		delete[] po;
		return *this;
	}
	Array<T>& copy_from(const T * src){
		for(int i=0; i<size; i++)
			p[i]=src[i];
		return *this;
	}
	Array<T>& copy_from(const T * src, int sz, int start = 0){
		for(int i=0; i<sz; i++)
			p[start+i]=src[i];
		return *this;
	}
	T* copy_into(T * dest, int sz, int start = 0)const{
		for(int i=0; i<sz; i++)
			dest[i] = p[start+i];
		return dest;
	}
	Array<T>& operator=(const Array<T>& arr){
		return init(arr.size).copy_from(arr.p);
	}
	Array<T>& set_val(const T& val, int start = 0, int sz=-1){
		if(sz<0)sz = size;
		else sz += start;
		for(; start<sz; start++)
			p[start] = val;
		return *this;
	}
	operator T*(){return p;}
	operator const T*()const{return p;}
	~Array(){init(0);}
};

template<class T>
Array<T>::Array(int sz){
	p=0; size = 0;
	init(sz);
}

template<class T>
inline bool operator<(const Array<T>& a, const Array<T>& b){
	int i, n = Min(a.size, b.size);
	for(i=0; i<n; i++)
		if(a[i]<b[i])return true;
		else if(b[i]<a[i])return false;
	return a.size<b.size;
}

template<class T>
Array<T>& Array<T>::init(int sz, bool copy){
	if(sz==size)return *this;
	T *q=p;
	if(!copy)
		delete[] p;
	p=0;
	if(sz)
/*#ifndef USE_NEW
		if(!(p = (T*)calloc(sz,sizeof(T)))){
			MEMORY_ERROR(reset_memory_error(sz);)
			size = 0;
		}
#else*/
		try{
			if(!(p = new T[sz])){
				MEMORY_ERROR(reset_memory_error(sz);)
				sz = 0;
			}
		}catch(...){
			MEMORY_ERROR(reset_memory_error(sz);)
			sz = 0;
		}
	if(copy){
		for(int i=0, e = Min(size, sz); i<e; i++)
			p[i] = q[i];
		delete[] q;
	}
	size = sz;
//#endif
	return *this;
}

template<class T> Array<T> MakeArray(T _0, ...){
	va_list vl;
	int len;
	T curr=_0;
	va_start(vl, _0);
	for(len=0; curr; len++)
		curr = va_arg(vl, T);
	va_end(vl);
	Array<T> res(len);
	va_start(vl, _0);
	curr=_0;
	for(len=0; curr; len++){
		res[len] = curr;
		curr = va_arg(vl, T);
	}
	va_end(vl);
	return res;
}

template<class T>
class RArray: protected Array<T>{
	typedef Array<T> _base;
	T *p0;
public:
	typedef T value_type;
	typedef T* iterator, *reverse_iterator, *pointer, &reference;
	typedef const T* const_iterator, *const_reverse_iterator, *const_pointer, &const_reference;

	RArray(long sz=0): _base(sz){p0 = this->p;}
	RArray(long beg, size_t sz):_base(sz){
		p0 = this->p - beg;
	}
	RArray<T>& set_begin(long beg){
		if(this->p)p0 = this->p - beg;
		else p0=0;
		return *this;
	}
	size_t size()const{return _base::size;}
	long Begin()const{return this->p - p0;}
	long End()const{return Begin()+_base::size;}
	iterator begin(){return this->p;}
	const_iterator begin()const{return this->p;}
	iterator end(){return begin()+_base::size;}
	const_iterator end()const{return begin()+_base::size;}
	const T *data()const{return p0;}
	T *data(){return p0;}
	bool operator!()const{return !this->p;}
	RArray<T>& expand(size_t sz, bool save=false){
		if(sz<=_base::size)return *this;
		long delta = Begin();
		_base::init(sz, save);
		return set_begin(delta);
	}
	RArray<T>& expand_coef(size_t sz, double k = 1.5){
		if(sz<=(size_t)_base::size)return *this;
		long delta = Begin();
		_base::init(size_t(_base::size*k) + 1, true);
		return set_begin(delta);
	}
	RArray<T>& init(long begin, size_t sz, bool copy = false){
		_base::init(sz, copy);
		return set_begin(begin);
	}
	RArray<T>& resize(size_t sz, bool copy = false){
		long delta = Begin();
		_base::init(sz, copy);
		return set_begin(begin);
	}
	template<class I>
	T& operator[](I index){
	//	_ASSERT_EXPR((int)index>=(int)(this->p-p0)&&(int)index<(int)(this->p-p0)+Array<T>::size, "RArray : index out of range");
		return p0[index];
	}
	template<class I>
	const T& operator[](I index)const{
	//	_ASSERT_EXPR((int)index>=(int)(this->p-p0)&&(int)index<(int)(this->p-p0)+Array<T>::size, "RArray : index out of range");
		return p0[index];
	}
};
