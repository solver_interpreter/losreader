#pragma once
#include "Memory.h"
#include "Array.h"

template<class X, class Y> struct Pair{
	X first;
	Y second;
	Pair():first(),second(){}
	template<class X1, class Y1>
	Pair(const X1 &_x, const Y1&_y):first(_x),second(_y){}
	Pair(const Pair<X,Y> &p):first(p.first), second(p.second){}
	template<class X1, class Y1>
	Pair(const Pair<X1,Y1> &p):first(p.first), second(p.second){}
	bool operator<(const Pair &p)const{
		return first<p.first || (!(p.first<first) && second<p.second);
	}
	bool operator>(const Pair &p)const{
		return p.first<p.first || (!(first<p.first) && p.second<second);
	}
	bool operator<=(const Pair &p)const{
		return !(p.first<first) && ((first<p.first) && !(p.second<second));
	}
	bool operator>=(const Pair &p)const{
		return !(first<p.first) && ((p.first<first) && !(second<p.second));
	}
	bool operator==(const Pair &p)const{
		return !((first<p.first)||(p.first<p.first)||(second<p.second)||(p.second<second));
	}
	bool operator!=(const Pair &p)const{
		return (first<p.first)||(p.first<p.first)||(second<p.second)||(p.second<second);
	}
	Pair<X,Y>& operator=(const Pair<X,Y> &p){
		first=p.first; second=p.second;
		return *this;
	}
	~Pair(){}
};

template<class X, class Y>
struct _Pair{
	X first;
	Y second;
	_Pair():first(),second(){}
	template<class X1, class Y1>
	_Pair(const X1 &_x, const Y1&_y):first(_x),second(_y){}
	template<class X1>
	_Pair(const X1 &_x):first(_x),second(){}
	_Pair(const _Pair<X,Y> &p):first(p.first), second(p.second){}
	template<class X1, class Y1>
	_Pair(const _Pair<X1,Y1> &p):first(p.first), second(p.second){}
	~_Pair(){}
};

template<class X, class Y>
Pair<X,Y> MakePair(const X&x, const Y&y){return Pair<X,Y>(x,y);}

template<class X, class Y>
_Pair<X,Y> _MakePair(const X&x, const Y&y){return _Pair<X,Y>(x,y);}

struct _Tree_node{
	_Tree_node *l, *r, *p;
	char c, is_nil;
};

template<class node, bool _Multy, class Pr >//node must be inherited from _Tree_node
class Tree_base{// ordered base of red-black tree
	AllocRef<node> _Al;
	Pr _pred;
	_Tree_node *_nil;
	size_t _sz;
/*	bool _comp(const _Tree_node *a, const _Tree_node *b)const{
		return _pred(*(const node*)a, *(const node*)b);
	}
public:*/
	typedef Tree_base<node, _Multy, Pr> _Myt;

protected:

	enum _Redbl{_Red=0, _Black=1};

	static _Tree_node* _Next(_Tree_node *x){
		if(!x->r->is_nil)
			return _subtree_min(x->r);
		_Tree_node *y;
		for(y=x->p; !y->is_nil && x==y->r; y=y->p)x=y;
		return y;
	}
	static _Tree_node* _Prev(_Tree_node *x){
		if(!x->l->is_nil)
			return _subtree_max(x->r);
		_Tree_node *y;
		for(y=x->p; !y->is_nil && x==y->l; y=y->p)x=y;
		return y;
	}
	static const _Tree_node* _Next(const _Tree_node *x){return _Next((_Tree_node*)x);}
	static const _Tree_node* _Prev(const _Tree_node *x){return _Prev((_Tree_node*)x);}
	static _Tree_node* _Next(_Tree_node *x, const _Tree_node *nil){
		_Tree_node *y;
		if(x->r!=nil)
			for(y=x->r; y->l!=nil; y=y->l);
		else for(y=x->p; !y->is_nil && x==y->r; y=y->p)x=y;
		return y;
	}
	static _Tree_node* _Prev(_Tree_node *x, const _Tree_node *nil){
		_Tree_node *y;
		if(x->l!=nil)
			for(y=x->l; y->r!=nil; y=y->r);
		else for(y=x->p; y!=nil && x==y->l; y=y->p) x=y;
		return y;
	}
	struct _ins_place{
		_Tree_node *w;
		char add_l;
		char is_eq;
		_ins_place(_Tree_node *_w, bool l, bool eq=false){
			w=_w;
			add_l = l ? 1 : 0;
			is_eq = eq ? 1 : 0;
		}
	};
	class cit_base{
	protected:
		friend class Tree_base<node, _Multy, Pr>;
		const _Tree_node *_ptr;
		cit_base(const _Tree_node *p){_ptr = p;}
		const node *_val()const{return (const node*)_ptr;}
	public:
		cit_base(){_ptr=0;}
		bool hasData()const{return _ptr!=0 && !_ptr->is_nil;}
		bool is_valid()const{return _ptr!=0;}
		bool is_nil()const{return _ptr->is_nil!=0;}
		cit_base& operator ++(){
			D_CHECK(!is_nil(), "trying increase end it_base")
			_ptr = _Next(_ptr); return *this;
		}
		cit_base operator ++(int){cit_base r(*this); ++(*this); return r;}
		cit_base& operator --(){
			_ptr = _ptr->is_nil ? _ptr->r : _Prev(_ptr);
			D_CHECK(!_ptr->is_nil, "trying decrease begin it_base");
			return *this;
		}
		cit_base operator --(int){cit_base r(*this); --(*this); return r;}
		bool operator ==(const cit_base&cb)const{return _ptr == cb._ptr;}
		bool operator !=(const cit_base&cb)const{return _ptr != cb._ptr;}
	};
	class it_base:public cit_base{
	protected:
		friend class Tree_base<node, _Multy, Pr>;
		it_base(_Tree_node *p):cit_base(p){}
		node *_val()const{return (node*)this->_ptr;}
	public:
		it_base():cit_base(){}
		it_base& operator ++(){++(*(cit_base*)this); return *this;}
		it_base operator ++(int){it_base r(*this); ++(*(cit_base*)this); return r;}
		it_base& operator --(){--(*(cit_base*)this); return *this;}
		it_base operator --(int){it_base r(*this); --(*(cit_base*)this); return r;}
	};
	class rcit_base{
	protected:
		friend class Tree_base<node, _Multy, Pr>;
		const _Tree_node *_ptr;
		rcit_base(const _Tree_node *p){_ptr = p;}
		const node *_val()const{return (const node*)_ptr;}
	public:
		rcit_base(){_ptr=0;}
		bool hasData()const{return _ptr!=0 && !_ptr->is_nil;}
		bool is_valid()const{return _ptr!=0;}
		bool is_nil()const{return _ptr->is_nil;}
		rcit_base& operator ++(){D_CHECK(!is_nil(), "trying increase rend it_base") _ptr = _Prev(_ptr); return *this;}
		rcit_base operator ++(int){rcit_base r(*this); ++(*this); return r;}
		rcit_base& operator --(){
			_ptr = _ptr->is_nil ? _ptr->l : _Next(_ptr);
			D_CHECK(!_ptr->is_nil, "trying decrease rbegin it_base");
			return *this;
		}
		rcit_base operator --(int){rcit_base r(*this); --(*this); return r;}
		bool operator ==(const rcit_base&cb)const{return _ptr == cb._ptr;}
		bool operator !=(const rcit_base&cb)const{return _ptr != cb._ptr;}
	};
	class rit_base:public rcit_base{
	protected:
		friend class Tree_base<node, _Multy, Pr>;
		rit_base(_Tree_node *p):rcit_base(p){}
		node *_val()const{return (node*)this->_ptr;}
	public:
		rit_base():rcit_base(){}
		rit_base& operator ++(){++(*(rcit_base*)this); return *this;}
		rit_base operator ++(int){rit_base r(*this); ++(*(rcit_base*)this); return r;}
		rit_base& operator --(){--(*(rcit_base*)this); return *this;}
		rit_base operator --(int){rit_base r(*this); --(*(rcit_base*)this); return r;}
	};
public:
		// CLASS cit_base
	Tree_base(const Pr& p, const AllocRef<node> &Al = AllocRef<node>(10,1.5)):_pred(p){	// construct empty tree
		_Al = Al;
		_create();
	}

	Tree_base(const AllocRef<node> &Al){	// construct empty tree
		_Al = Al;
		_create();
	}

	Tree_base(size_t stsz = 10, double k=1.5):_Al(stsz,k){	// construct empty tree
		_create();
	}

	Tree_base(const _Myt& tb) :_Al(tb._Al), _pred(tb._pred) {	// construct tree by copying _Right
		_create();
		try{
			_copy(tb);
		}catch(...){
			_destroy();
			throw;
		}
	}

	virtual ~Tree_base();

	_Myt& operator = (const _Myt& tb){	// replace contents from _Right
		if (this != &tb){	// worth doing
			_clear(true);
			this->_pred = tb._pred;
			_copy(tb);
		}
		return *this;
	}

	it_base begin(){return it_base(_nil->l);}
	cit_base begin() const{return cit_base(_nil->l);}
	it_base end(){return it_base(_nil);}
	cit_base end() const{return cit_base(_nil);}

	rit_base rbegin(){return rit_base(_nil->l);}
	rcit_base rbegin() const{return rcit_base(_nil->l);}
	rit_base rend(){return rit_base(_nil);}
	rcit_base rend() const{return rcit_base(_nil);}

	size_t size() const{return _sz;}
	bool empty() const{	return !_sz;}
	const AllocRef<node>& get_allocator() const{return _Al;}
	template<class Y> //int Pr::compare(const node&, constY&)const
	_ins_place find_ins_place(const Y &y){	// try to insert node with value v
		_Tree_node * c = _nil->p;
		_Tree_node * w = _nil;
		if(_Multy){
			bool add_l=true;// add to left of head if tree empty
			while(!c->is_nil){	// look for leaf to insert before (add_l) or after
				w = c;
				add_l = _pred.compare(*(node*)c, y)>0;
				c=add_l ? c->l : c->r;
			}
			return _ins_place(w, add_l);
		}else{	// insert only if unique
			int cr=-1;// add to left of head if tree empty
			while(!c->is_nil){	// look for leaf to insert before (add_l) or after
				w = c;
				cr = _pred.compare(*(node*)c, y);
				if(!cr)return _ins_place(c, false, true);
				c = cr>0 ? c->l : c->r;
			}
			return _ins_place(w, cr>0);
		}
	}
	template<class Y> //int Pr::compare(const node&, constY&)const
	Pair<it_base, bool> insert(const Y &y){	// try to insert node with value v
		_ins_place pl = find_ins_place(y);
		if(!pl.is_eq)return MakePair(_ins_fixup(pl, y), true);
		return MakePair(it_base(pl.w), false);
	}
	Pair<it_base,bool> insert(node *n){	// try to insert node with value v
		_ins_place pl = find_ins_place(*n);
		if(!pl.is_eq)return MakePair(_ins_fixup(pl, n), true);
		return MakePair(it_base(pl.w), false);
//		return MakePair(it_base(_ins_fixup(pl, n)), !pl.is_eq);
	}
	template<class V>
	it_base insert(cit_base wi, const V& v){// try to insert node with value v using w as a hint
		_Tree_node * w = wi._val();
		_Tree_node *nxt;

		if(!_sz)
			return _ins_fixup(true, _nil, v);	// insert into empty tree
		if(this->_Multi){	// insert even if duplicate
			if (w == _nil->l){	// insert at beginning if before first element
				if (_pred.compare(*(node*)w, v)>=0)
					return _ins_fixup(true, w, v);
			}else if (w == _nil){	// insert at end if after last element
				if (_pred.compare(*(node*)(_nil->r), v)<=0)
					return (_ins_fixup(false, _nil->r, v));
			}else{
				int cwv = _pred.compare(*(node*)w, v);
				nxt = _Prev(w,_nil);
				if (cwv>=0 && _pred.compare(*(node*)(nxt=_Prev(w,_nil)), v)<=0){	// insert before w
					if(nxt->r==_nil)
						return _ins_fixup(false, nxt, v);
					return _ins_fixup(true, w, v);
				}
				nxt = _Next(w,_nil);
				if (cwv<=0 && ((nxt = _Next(w,_nil)) == end() || _pred.compare(*(node*)nxt,v)>=0)){	// insert after w
					if (w->r==_nil)
						return _ins_fixup(false, w, v);
					return _ins_fixup(true, nxt, v);
				}
			}
		}else{	// insert only if unique
			if (w == _nil->l){	// insert at beginning if before first element
				if (_pred.compare(*(node*)w,v)>0)
					return (_ins_fixup(true, w, v));
			}else if (w == end()){	// insert at end if after last element
				if (_pred.compare(*(node*)(_nil->r), v)<0)
					return (_ins_fixup(false, _nil->r, v));
			}else{
				int cwv = _pred.compare(*(node*)w, v);
				nxt = _Prev(w,_nil);
				if (cwv>0 && _pred.compare(*(node*)(nxt=_Prev(w,_nil)), v)<0){	// insert before w
					if (nxt->r==_nil)
						return _ins_fixup(false, nxt, v);
					return _ins_fixup(true, w, v);
				}
				else if (cwv<0 && ((nxt = _Next(w,_nil)) == _nil || _pred.compare(*(node*)nxt,v)>0)){	// insert after w
					if (w->r==_nil)
						return _ins_fixup(false, w, v);
					return _ins_fixup(true, nxt, v);
				}
			}
		}
		return insert(v).first;	// try usual insert if all else fails
	}

	template<class _Iter>
	void insert(_Iter fst, _Iter lst){	// insert [fst, lst) one at a time
		for (; fst != lst; ++fst)
			insert(*fst);
	}
protected:
	static it_base _create_iter(node *n){
		return it_base(n);
	}
	static rit_base _create_riter(node *n){
		return rit_base(n);
	}
/*	Pair<it_base, bool> reInsert(cit_base w, _ins_place pl){
		if(pl.is_eq){
			erase(w, true);
			return MakePair(this->end(), false);
		}else erase(w,false);
		return MakePair(_ins_fixup(pl, (_Tree_node*)w._ptr), true);
	}*/
	Pair<it_base, bool> reInsert(cit_base w, bool replace_if_exist){ //must be defined const T& node::operator*()const, where T is value type
		this->erase(w, false);
		_ins_place pl = find_ins_place(*w._val());
		if(pl.is_eq){
			if(!replace_if_exist){_Al.free((node*)w._ptr); return MakePair(it_base(pl.w),false);}
			else{
				_Tree_node *n = (_Tree_node*)w._ptr, *o = pl.w;
				if(o->p != _nil){
					if(o->p->l == o)o->p->l = n;
					else o->p->r = n;
				}else _nil->p = n;
				if(o->l != _nil)o->l->p = n;
				else _nil->l = n;
				if(o->r != _nil)o->r->p = n;
				else _nil->r = n;
				*n = *o;
				_Al.free((node*)o);
				return MakePair(it_base(n), false);
			}
		}
		return MakePair(_ins_fixup(pl, (_Tree_node*)w._ptr),true);
	}
	it_base erase(cit_base iw, bool del){// erase element at w
		_Tree_node * w = (_Tree_node*)iw._val(), *er_n = w;	// node to erase
		w=_Next(w,_nil);	// save successor it_base for return

		_Tree_node* fn;	// the node to recolor as needed
		_Tree_node* fnp;	// parent of fn (which may be _nil)
		_Tree_node* pn = er_n;

		if (pn->l==_nil)fn = pn->r;	// must stitch up right subtree
		else if (pn->r==_nil)fn = pn->l;	// must stitch up left subtree
		else{	// two subtrees, must lift successor node to replace erased
			pn = w;	// pn is successor node
			fn = pn->r;	// fn is its only subtree
		}
		if(pn == er_n){	// at most one subtree, relink it
			fnp = er_n->p;
			if (fn!=_nil)
				fn->p = fnp;	// link up

			if (_nil->p == er_n)
				_nil->p = fn;	// link down from root
			else if (fnp->l == er_n)fnp->l = fn;	// link down to left
			else fnp->r = fn;	// link down to right

			if (_nil->l == er_n)
				_nil->l = fn==_nil
					? fnp	// smallest is parent of erased node
					: _subtree_min(fn);	// smallest in relinked subtree

			if (_nil->r == er_n)
				_nil->r = fn==_nil
					? fnp	// largest is parent of erased node
					: _subtree_max(fn);	// largest in relinked subtree
		}else{	// erased has two subtrees, pn is successor to erased
			er_n->l->p = pn;	// link left up
			pn->l = er_n->l;	// link successor down

			if (pn == er_n->r)
				fnp = pn;	// successor is next to erased
			else{	// successor further down, link in place of erased
				fnp = pn->p;	// parent is successor's
				if(!fn->is_nil)
					fn->p = fnp;	// link fix up
				fnp->l = fn;	// link fix down
				pn->r = er_n->r;	// link successor down
				er_n->r->p = pn;	// link right up
			}

			if (_nil->p == er_n)
				_nil->p = pn;	// link down from root
			else if (er_n->p->l == er_n)
				er_n->p->l = pn;	// link down to left
			else
				er_n->p->r = pn;	// link down to right

			pn->p = er_n->p;	// link successor up
			Exchange(pn->c, er_n->c);	// recolor it
		}
		if (er_n->c == _Black){	// erasing black link, must recolor/rebalance tree
			for (; fn != _nil->p && fn->c == _Black; fnp = fn->p)
				if (fn == fnp->l){	// fixup left subtree
					pn = fnp->r;
					if (pn->c == _Red){	// rotate red up from right subtree
						pn->c = _Black;
						fnp->c = _Red;
						_left_rot(fnp);
						pn = fnp->r;
					}
					if (pn->is_nil)
						fn = fnp;	// shouldn't happen
					else if (pn->l->c == _Black && pn->r->c == _Black){	// redden right subtree with black children
						pn->c = _Red;
						fn = fnp;
					}else{	// must rearrange right subtree
						if (pn->r->c == _Black){	// rotate red up from left sub-subtree
							pn->l->c = _Black;
							pn->c = _Red;
							_right_rot(pn);
							pn = fnp->r;
						}

						pn->c = fnp->c;
						fnp->c = _Black;
						pn->r->c = _Black;
						_left_rot(fnp);
						break;	// tree now recolored/rebalanced
					}
				}else{	// fixup right subtree
					pn = fnp->l;
					if (pn->c == _Red){	// rotate red up from left subtree
						pn->c = _Black;
						fnp->c = _Red;
						_right_rot(fnp);
						pn = fnp->l;
					}
					if (pn==_nil)
						fn = fnp;	// shouldn't happen
					else if (pn->r->c == _Black	&& pn->l->c == _Black){	// redden left subtree with black children
						pn->c = _Red;
						fn = fnp;
					}else{	// must rearrange left subtree
						if (pn->l->c == _Black){	// rotate red up from right sub-subtree
							pn->r->c = _Black;
							pn->c = _Red;
							_left_rot(pn);
							pn = fnp->l;
						}
						pn->c = fnp->c;
						fnp->c = _Black;
						pn->l->c = _Black;
						_right_rot(fnp);
						break;	// tree now recolored/rebalanced
					}
				}

			fn->c = _Black;	// ensure stopping node is black
		}

		if(del)_Al.free((node*)er_n);
		D_CHECK(_sz>0, "size isn't positive in not empty tree");
		--_sz;

		return (node*)w;	// return successor it_base
	}

	it_base erase(cit_base fst, cit_base lst, bool del){	// erase [fst, lst)
		if (fst._ptr == _nil->l && lst._ptr == _nil) _clear(del);
		while(fst != lst) fst = erase(fst, del);
		return it_base((_Tree_node*)lst._val());
	}

	template<class V>
	size_t erase_T(const V& v, bool del){	// erase and count all that match v
		Pair<cit_base, cit_base> w = _interval(v);
		cit_base fst = w.first, lst = w.second;
		size_t n;
		if (fst == _nil->l && lst == _nil){	// erase all
			n = _sz;
			_clear(del);
		}else
			for(n=0; fst != lst; n++)
				fst = erase(fst, del);
		return n;
	}
	template<class It>
	void erase_values(It fst, It lst, bool del){	// erase all that match array of keys [fst, lst)
		for(;fst != lst;){
			It x = fst;
			++fst;
			erase(*x, del);
		}
	}
public:

	void clear(){_clear(true);}

	template<class V>
	it_base find(const V& v){	// find an element in nonmutable sequence that matches v
		it_base w = _lower(v);
		return (w.is_nil()||_pred.compare(*w._val(),v)>0) ? end() : w;
	}

	template<class V>
	cit_base find(const V& v) const{	// find an element in nonmutable sequence that matches v
		cit_base w = _lower(v);
		return (w.is_nil()||_pred.compare(*w._val(),v)>0) ? end() : w;
	}

	template<class V>
	size_t count(const V& v) const{	// count all elements that match v
		Pair<cit_base, cit_base> r = _interval(v);
		cit_base fst = r.first, lst = r.second;
		size_t n;
		for(n=0; fst!=lst; n++) ++fst;
		return n;
	}
	void swap(_Myt& t){	// exchange contents with t
		if(this == &t)return;
		Exchange(_Al, t._Al);
		Exchange(_pred, t._pred);
		Exchange(_nil, t._nil);
		Exchange(_sz, t._sz);
	}
/*	void print(FILE *fp)const{
		if(empty())fprintf(fp, "<empty tree>");
		else _print(fp, _nil->p);
	}*/

protected:
/*	static void _print(FILE *fp, const _Tree_node *rt, int rec = 0){
		if(rt->is_nil)return;
		for(int i=0; i<=rec; i++)fprintf(fp, "  ");
		::print(fp, *(const node*)rt);
		fprintf(fp, "(%s)\n", rt->c == _Black ? "Black" : "Red");
		_print(fp, rt->l, rec+1);
		_print(fp, rt->r, rec+1);
	}*/
	void _clear(bool del){	// erase all
		if(del)_del_subtree(_nil->p);
		_nil->l = _nil->r = _nil->p = _nil;
		_sz = 0;
	}
	void _copy(const _Myt& t){	// copy entire tree from _Right
		_nil->p = _copy(t._nil->p, _nil);
		_sz = t.size();
		if (_nil->p == _nil){	// nonempty tree, look for new smallest and largest
			_nil->l = _subtree_min(_nil->p);
			_nil->r = _subtree_max(_nil->p);
		}
		else
			_nil->l = _nil->r = _nil;	// empty tree
		}

	_Tree_node* _copy(const _Tree_node* rt, _Tree_node* w);	// copy entire subtree, recursively

	template<class V>
	Pair<it_base, it_base> _interval(const V& v)
		{	// find leftmost node not less than _Keyval
		_Tree_node* pn = _nil->p;
		_Tree_node* l = _nil;	// end() if search fails
		_Tree_node* h = _nil;	// end() if search fails

		while (pn!=_nil){
			int cpv = _pred.compare(*(node*)pn, v);
			if(cpv<0)pn = pn->r;	// descend right subtree
			else{	// pn not less than _Keyval, remember it
				if(h==_nil && cpv>0)
					h = pn;	// pn greater, remember it
				l = pn;
				pn = pn->l;	// descend left subtree
			}
		}

		pn = h->is_nil ? _nil->p : h->l;	// continue scan for upper bound
		while (pn!=_nil){
			int cpv = _pred.compare(*(node*)pn, v);
			if (cpv>0){	// pn greater than _Keyval, remember it
				h = pn;
				pn = pn->l;	// descend left subtree
			}else pn = pn->r;	// descend right subtree
		}

		return MakePair(it_base(l),it_base(h));
	}

	template<class V>
	Pair<cit_base, cit_base> _c_interval(const V& val)const{	// find leftmost node not less than _Keyval
		Pair<node*, node*> r = ((_Myt*)this)->_interval(val);
		return MakePair(r.first, r.second);
	}

	void _del_subtree(_Tree_node* rt);	// free entire subtree, recursively

	void _create(){
		_nil = _Al.get_new();
		_nil->is_nil = true;
		_nil->l = _nil->r = _nil->p = _nil;
		_sz = 0;
	}

	template<class V>
	it_base _ins_fixup(bool add_l, _Tree_node* w, V &v){
		_Tree_node *n = _Al.get_new(v);
		return _ins_fixup(_ins_place(add_l, w), n);
	}
	template<class V>
	it_base _ins_fixup(_ins_place pl, const V &v){
		if(pl.is_eq)return pl.w;
		_Tree_node *n = _Al.get_new(v);
		return _ins_fixup(pl, n);
	}

	it_base _ins_fixup(_ins_place pl, _Tree_node *n){// add node with value next to w, to left if _Addnode
		_Tree_node *w=pl.w;
		n->c = _Red;
		n->l = n->r = _nil;
		n->p = w;
		++_sz;
		if (w == _nil){	// first node in tree, just set head values
			_nil->p = n;
			_nil->l = n, _nil->r = n;
		}else if (pl.add_l){	// add to left of w
			w->l = n;
			if (w == _nil->l)
				_nil->l = n;
		}else{	// add to right of w
			w->r = n;
			if (w == _nil->r)
				_nil->r = n;
		}
		for (_Tree_node* pn = n; pn->p->c == _Red; )
			if (pn->p == pn->p->p->l){	// fixup red-red in left subtree
				w = pn->p->p->r;
				if (w->c == _Red){	// parent has two red children, blacken both
					pn->p->c = _Black;
					w->c = _Black;
					pn->p->p->c = _Red;
					pn = pn->p->p;
				}else{	// parent has red and black children
					if (pn == pn->p->r){	// rotate right child to left
						pn = pn->p;
						_left_rot(pn);
					}
					pn->p->c = _Black;	// propagate red up
					pn->p->p->c = _Red;
					_right_rot(pn->p->p);
				}
			}else{	// fixup red-red in right subtree
				w = pn->p->p->l;
				if (w->c == _Red){	// parent has two red children, blacken both
					pn->p->c = _Black;
					w->c = _Black;
					pn->p->p->c = _Red;
					pn = pn->p->p;
				}else{	// parent has red and black children
					if (pn == pn->p->l){	// rotate left child to right
						pn = pn->p;
						_right_rot(pn);
					}
					pn->p->c = _Black;	// propagate red up
					pn->p->p->c = _Red;
					_left_rot(pn->p->p);
				}
			}

		_nil->p->c = _Black;	// root is always black
		return n;
	}

	static _Tree_node* _subtree_max(_Tree_node* p){	// return rightmost node in subtree at pn
		while (!p->r->is_nil)
			p = p->r;
		return p;
	}
	static _Tree_node* _subtree_min(_Tree_node* p){	// return leftmost node in subtree at pn
		while (!p->l->is_nil)
			p = p->l;
		return p;
	}

	void _left_rot(_Tree_node* w){	// promote right node to root of subtree
		_Tree_node* pn = w->r;
		w->r = pn->l;

		if (!pn->l->is_nil)
			pn->l->p = w;
		pn->p = w->p;

		if (w == _nil->p)
			_nil->p = pn;
		else if (w == w->p->l)w->p->l = pn;
		else w->p->r = pn;

		pn->l = w;
		w->p = pn;
	}
	void _right_rot(_Tree_node* w){	// promote left node to root of subtree
		_Tree_node* pn = w->l;
		w->l = pn->r;

		if (!pn->r->is_nil)
			pn->r->p = w;
		pn->p = w->p;

		if (w == _nil->p)
			_nil->p = pn;
		else if (w == w->p->r)
			w->p->r = pn;
		else
			w->p->l = pn;

		pn->r = w;
		w->p = pn;
	}
	template<class V>
	it_base _upper(const V& v){	// find leftmost node greater than _Keyval
		_Tree_node *pn = _nil->p, *w = _nil;	// end() if search fails

		while (!pn->is_nil)
			if (_pred.compare(*(node*)pn, v)>0){// pn greater than _Keyval, remember it
				w = pn;
				pn = pn->l;	// descend left subtree
			}else pn = pn->r;	// descend right subtree

		return w;	// return best remembered candidate
	}
	template<class V>
	it_base _lower(const V& v){	// find leftmost node not less than _Keyval
		_Tree_node* pn = _nil->p;
		_Tree_node* w = _nil;	// end() if search fails

		while(pn!=_nil)
			if(_pred.compare(*(node*)pn, v)<0)pn = pn->r;	// descend right subtree
			else{	// pn not less than _Keyval, remember it
				w = pn;
				pn = pn->l;	// descend left subtree
			}
		return w;	// return best remembered candidate
	}
	template<class V>
	it_base _max_not_more(const V& v){	// find leftmost node not less than _Keyval
		_Tree_node* pn = _nil->p;
		_Tree_node* w = _nil;	// end() if search fails

		while(pn!=_nil){
			int cmp = _pred.compare(*(node*)pn, v);
			if(cmp==0)return pn;
			if(cmp<0){
				w = pn;
				pn = pn->r;
			}// descend right subtree
			else pn = pn->l;	// descend left subtree
		}
		return w;	// return best remembered candidate
	}

	template<class V>
	cit_base _lower(const V& v) const{return ((_Myt*)this)->_lower(v);}
	template<class V>
	cit_base _upper(const V& v) const{return ((_Myt*)this)->_upper(v);}
	template<class V>
	cit_base _max_not_more(const V& v) const{return ((_Myt*)this)->_max_not_more(v);}

	template <class Y>
	_Tree_node* _get_node(_Tree_node* l, _Tree_node* p, _Tree_node* r, const Y& y, char col){// allocate a node with pointers, value, and color
		_Tree_node* w = _Al.get_new(y);
		w->l = l;
		w->p = p;
		w->r = r;
		w->c = col;
		return w;
	}
	void _destroy(){	// free all storage
		_clear(true);
		//_Al.free((node*)_nil);
		_sz = 0;
		_Al.free((node*)_nil);
		_nil=0;
	}
};

template<class node, bool M, class Pr>
Tree_base<node, M, Pr>::~Tree_base(){
	_destroy();
}

template<class node, bool M, class Pr>
void Tree_base<node, M, Pr>::_del_subtree(_Tree_node* rt){	// free entire subtree, recursively
	for (_Tree_node* pn = rt; pn!=_nil; rt = pn){	// free subtrees, then node
		_del_subtree(pn->r);
		pn = pn->l;
		_Al.free((node*)rt);
	}
}
template<class node, bool M, class Pr>
_Tree_node* Tree_base<node, M, Pr>::_copy(const _Tree_node* rt, _Tree_node* w){	// copy entire subtree, recursively
	_Tree_node* res = _nil;	// point at _nil node
	if (!rt->is_nil){	// copy a node, then any subtrees
		_Tree_node* pn = _get_node(_nil, w, _nil, *(const node*)rt, rt->c);
		if(res->is_nil)
			res = pn;	// memorize new root
		try{
			pn->l = _copy(rt->l, pn);
			pn->r = _copy(rt->r, pn);
		}catch(...){
			_del_subtree(res);	// subtree copy failed, bail out
			throw ;
		}
	}
	return res;	// return newly constructed tree
}

template<class T>
struct _T_node : public _Tree_node{
	T val;
	_T_node():val(){}
	template<class V>
	_T_node(const V &v):val(v){}
	_T_node(const T &v):val(v){}
	const T& operator*()const{return val;}
	operator const T&()const{return val;}
	~_T_node(){}
};

template<class X>
struct _T_cmp{
	template<class Y>
	int compare(const X&x, const Y&y)const{
		return x<y ? -1 : y<x ? 1 : 0;
	}
};

template<class T, bool _Multy = false, class Cmp = _T_cmp<T> >
class Tree : public Tree_base<_T_node<T>, _Multy, Cmp>{
protected:
	typedef _T_node<T> _node;
	typedef Tree_base<_T_node<T>, _Multy, Cmp> _base;
	typedef typename _base::it_base it_base;
	typedef typename _base::cit_base cit_base;
	typedef typename _base::rit_base rit_base;
	typedef typename _base::rcit_base rcit_base;
public:
	typedef size_t size_type;
	typedef T value_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;

	class const_iterator : public _base::cit_base{
	public:
		const_iterator():cit_base(){}
		const_iterator(const cit_base& c):cit_base(c){}
		const T& operator*()const{return this->_val()->val;}
		const T* operator->()const{return &this->_val()->val;}
		const_iterator& operator++(){++(*(cit_base*)this); return *this;}
		const_iterator operator++(int){return (*(cit_base*)this)++;}
		const_iterator& operator--(){--(*(cit_base*)this); return *this;}
		const_iterator operator--(int){return (*(cit_base*)this)--;}
	};
	class iterator : public _base::it_base{
	public:
		iterator():it_base(){}
		iterator(const it_base& c):it_base(c){}
		T& operator*()const{return this->_val()->val;}
		T* operator->()const{return &this->_val()->val;}
		iterator& operator++(){++(*(it_base*)this); return *this;}
		iterator operator++(int){return (*(it_base*)this)++;}
		iterator& operator--(){--(*(it_base*)this); return *this;}
		iterator operator--(int){return (*(it_base*)this)--;}
	};
	class const_reverse_iterator : public _base::rcit_base{
	public:
		const_reverse_iterator():rcit_base(){}
		const_reverse_iterator(const rcit_base& c):rcit_base(c){}
		const T& operator*()const{return this->_val()->val;}
		const T* operator->()const{return &this->_val()->val;}
		const_reverse_iterator& operator++(){++(*(rcit_base*)this); return *this;}
		const_reverse_iterator operator++(int){return (*(rcit_base*)this)++;}
		const_reverse_iterator& operator--(){--(*(rcit_base*)this); return *this;}
		const_reverse_iterator operator--(int){return (*(rcit_base*)this)--;}
	};
	class reverse_iterator : public _base::rit_base{
	public:
		reverse_iterator():rit_base(){}
		reverse_iterator(const rit_base& c):rit_base(c){}
		T& operator*()const{return this->_val()->val;}
		T* operator->()const{return &this->_val()->val;}
		reverse_iterator& operator++(){++(*(rit_base*)this); return *this;}
		reverse_iterator operator++(int){return (*(rit_base*)this)++;}
		reverse_iterator& operator--(){--(*(rit_base*)this); return *this;}
		reverse_iterator operator--(int){return (*(rit_base*)this)--;}
	};

	typedef Pair<iterator, iterator> it_pair;
	typedef Pair<const_iterator, const_iterator> cit_pair;

	//===================================
	typedef AllocRef<_T_node<T> > allocator_type;
	explicit Tree(const Cmp &cmp, const allocator_type& Al = allocator_type());
	Tree(const allocator_type& Al);
	Tree();
	Tree(const Tree &tr);
	~Tree(){}

	iterator begin(){return _base::begin();}
	const_iterator begin() const{return _base::begin();}
	it_base end(){return _base::end();}
	const_iterator end() const{return _base::end();}

	reverse_iterator rbegin(){return _base::rbegin();}
	const_reverse_iterator rbegin() const{return _base::rbegin();}
	reverse_iterator rend(){return _base::end();}
	const_reverse_iterator rend() const{return _base::end();}

	template<class V>
	Pair<iterator, bool> insert_T(const V& val){return _base::insert(val);}
	Pair<iterator, bool> insert(const T& val){return _base::insert(val);}

	template<class V>
	size_t erase_T(const V& val){return _base::erase_T(val, true);}
	size_t erase(const T& val){return _base::erase(val, true);}
	iterator erase(const_iterator fst, const_iterator lst){return _base::erase(fst, lst, true);}
	template<class It>
	void erase_values(It fst, It lst){return _base::erase_values(fst, lst, true);}

	template<class V>
	iterator find_T(const V& val){return _base::find(val);}
	template<class V>
	const_iterator find_T(const V& val)const{return _base::find(val);}

	iterator find(const T& val){return _base::find(val);}
	const_iterator find(const T& val)const{return _base::find(val);}

	template<class V>
	iterator lower_bound_T(const V& val){return this->_lower(val);}
	template<class V>
	const_iterator lower_bound_T(const V& val)const{return this->_lower(val);}

	iterator lower_bound(const T& val){return this->_lower(val);}
	const_iterator lower_bound(const T& val)const{return this->_lower(val);}

	template<class V>
	iterator max_not_more_T(const V& val){return this->_max_not_more(val);}
	template<class V>
	const_iterator max_not_more_T(const V& val)const{return this->_max_not_more(val);}

	iterator max_not_more(const T& val){return this->_max_not_more(val);}
	const_iterator max_not_more(const T& val)const{return this->_max_not_more(val);}

	template<class V>
	iterator upper_bound_T(const V& val){return this->_upper(val);}
	template<class V>
	const_iterator upper_bound_T(const V& val)const{return this->_upper(val);}

	iterator upper_bound(const T& val){return this->_upper(val);}
	const_iterator upper_bound(const T& val)const{return this->_upper(val);}

	template<class V>
	Pair<iterator,iterator> interval_T(const V& val){return this->_interval(val);}
	template<class V>
	Pair<const_iterator,const_iterator> interval_T(const V& val)const{return this->_interval(val);}

	Pair<iterator,iterator> interval(const T& val){return this->_interval(val);}
	Pair<const_iterator,const_iterator> interval(const T& val)const{return this->_interval(val);}
protected:
	static iterator _create_iter(_node *n){
		return iterator(_base::_create_iter(n));
	}
	static reverse_iterator _create_riter(_node *n){
		return reverse_iterator(_base::_create_riter(n));
	}
};

template<class T, bool M, class Cmp>
Tree<T,M,Cmp>::Tree(const Cmp &cmp, const allocator_type& Al):_base(cmp, Al){}
template<class T, bool M, class Cmp>
Tree<T,M,Cmp>::Tree(const allocator_type& Al):_base(Al){}
template<class T, bool M, class Cmp>
Tree<T,M,Cmp>::Tree():_base(){}
template<class T, bool M, class Cmp>
Tree<T,M,Cmp>::Tree(const Tree &tr):_base(tr){}

template<class T, class Y1, class TCmp>
struct _PairFirstCmp : public TCmp{
	_PairFirstCmp(const TCmp& c):TCmp(c){}
	_PairFirstCmp():TCmp(){}
	~_PairFirstCmp(){}
	template<class N>
	int compare(const _Pair<T,Y1> &p1, const _T_node<N> &p2)const{
		return compare(p1.first, p2.val);
	}
	template<class X2, class Y2>
	int compare(const _Pair<T,Y1> &p1, const _Pair<X2,Y2> &p2)const{
		return TCmp::compare(p1.first, p2.first);
	}
	template<class X2>
	int compare(const _Pair<T,Y1> &p1, const X2 &p2)const{
		return TCmp::compare(p1.first, p2);
	}
};

template<class K, class V, class TCmp = _T_cmp<K> >
class Map: public Tree<_Pair<K,V>, false, _PairFirstCmp<K,V,TCmp> >{
protected:
	typedef Tree<_Pair<K,V>, false, _PairFirstCmp<K,V,TCmp> > _tree;
public:
	typedef K key_type;
	typedef V value_type;
	typedef typename _tree::cit_base cit_base;
	typedef typename _tree::it_base it_base;
	typedef typename _tree::rit_base rit_base;
	typedef typename _tree::rcit_base rcit_base;
	typedef typename _tree::allocator_type allocator_type;
	typedef typename _tree::const_iterator base_const_iterator;
	typedef typename _tree::const_reverse_iterator base_const_reverse_iterator;
	typedef typename _tree::iterator base_iterator;
	typedef typename _tree::reverse_iterator base_reverse_iterator;

	class iterator : public base_iterator{
	protected:
		_Pair<K,V>* _val()const{return &base_iterator::_val()->val;}
	public:
		iterator(it_base c) : base_iterator(c){}
		iterator(){}
		const K& key()const{return this->_val()->first;}
		V& value()const{return this->_val()->second;}
		V* operator->()const{return this->_ptr ? &this->_val()->second : 0;}
		const _Pair<K,V>& data()const{return *this->_val();}
		V& operator*()const{return this->_val()->second;}
		iterator& operator++(){++(*(it_base*)this); return *this;}
		iterator operator++(int){return (*(it_base*)this)++;}
		iterator& operator--(){--(*(it_base*)this); return *this;}
		iterator operator--(int){return (*(it_base*)this)--;}
		iterator next()const{iterator it = *this; return ++it;}
		iterator prev()const{iterator it = *this; return --it;}
	};
	class const_iterator : public base_const_iterator{
	protected:
		const _Pair<K,V>* _val()const{return &base_const_iterator::_val()->val;}
	public:
		const_iterator(iterator c) : base_const_iterator(c){}
		const_iterator(cit_base c) : base_const_iterator(c){}
		const_iterator(){}
		const K& key()const{return this->_val()->first;}
		const V& value()const{return this->_val()->second;}
		const V& operator*()const{return this->_val()->second;}
		const V* operator->()const{return this->_ptr ? &this->_val()->second : 0;}
		const _Pair<K,V>& data()const{return *this->_val();}
		const_iterator& operator++(){++(*(cit_base*)this); return *this;}
		const_iterator operator++(int){return (*(cit_base*)this)++;}
		const_iterator& operator--(){--(*(cit_base*)this); return *this;}
		const_iterator operator--(int){return (*(cit_base*)this)--;}
		const_iterator next()const{const_iterator it = *this; return ++it;}
		const_iterator prev()const{const_iterator it = *this; return --it;}
	};
	class reverse_iterator : public base_reverse_iterator{
	protected:
		_Pair<K,V>* _val()const{return &base_reverse_iterator::_val()->val;}
	public:
		reverse_iterator(){}
		reverse_iterator(rit_base c) : base_reverse_iterator(c){}
		const K& key()const{return this->_val()->first;}
		V& value()const{return this->_val()->second;}
		V& operator*()const{return this->_val()->second;}
		V* operator->()const{return this->_ptr ? &this->_val()->second : 0;}
		const _Pair<K,V>& data()const{return *this->_val();}
		reverse_iterator& operator++(){++(*(rit_base*)this); return *this;}
		reverse_iterator operator++(int){return (*(rit_base*)this)++;}
		reverse_iterator& operator--(){--(*(rit_base*)this); return *this;}
		reverse_iterator operator--(int){return (*(rit_base*)this)--;}
		reverse_iterator next()const {reverse_iterator it = *this; return ++it;}
		reverse_iterator prev()const {reverse_iterator it = *this; return --it;}
	};
	class const_reverse_iterator : public base_const_reverse_iterator{
	protected:
		const _Pair<K,V>* _val()const{return &base_const_reverse_iterator::_val()->val;}
	public:
		const_reverse_iterator(){}
		const_reverse_iterator(reverse_iterator c) : base_const_reverse_iterator(c){}
		const_reverse_iterator(rcit_base c) : base_const_reverse_iterator(c){}
		const K& key()const{return this->_val()->first;}
		const V& value()const{return this->_val()->second;}
		const V& operator*()const{return this->_val()->second;}
		const V* operator->()const{return this->_ptr ? &this->_val()->second : 0;}
		const _Pair<K,V>& data()const{return *this->_val();}
		const_reverse_iterator& operator++(){++(*(rcit_base*)this); return *this;}
		const_reverse_iterator operator++(int){return (*(rcit_base*)this)++;}
		const_reverse_iterator& operator--(){--(*(rcit_base*)this); return *this;}
		const_reverse_iterator operator--(int){return (*(rcit_base*)this)--;}
		const_reverse_iterator next()const {const_reverse_iterator it = *this; return ++it;}
		const_reverse_iterator prev()const {const_reverse_iterator it = *this; return --it;}
	};
	iterator begin(){return _tree::begin();}
	const_iterator begin() const{return _tree::begin();}
	iterator end(){return _tree::end();}
	const_iterator end() const{return _tree::end();}

	reverse_iterator rbegin(){return _tree::rbegin();}
	const_reverse_iterator rbegin() const{return _tree::rbegin();}
	reverse_iterator rend(){return _tree::end();}
	const_reverse_iterator rend() const{return _tree::end();}

	explicit Map(const TCmp & cmp, const allocator_type& Al = allocator_type());
	explicit Map(const allocator_type& Al);
	Map();
	template<class K1>
	V &operator()(const K1& key){return _tree::insert_T(key).first->second;}
	V &operator[](const K& key){return (*this)(key);}
	template<class Map1>
	bool operator ==(const Map1 & mp)const{
		if(this->size()!=mp.size())return false;
		const_iterator it = this->begin();
		typename Map1::const_iterator mit = mp.begin();
		for(;it!=this->end(); ++it, ++mit)
			if(it->first!=mit->first || it->second!=mit->second)
				return false;
		return true;
	}
	template<class Map1>
	bool operator !=(const Map1 & mp)const{return !((*this)==mp);}

	template<class K1>
	V *find_ptr_T(const K1& key){
		iterator it = this->find_T(key);
		return it == this->end()? 0 : &it.value();
	}
	Pair<iterator, bool> insert(const K& key, const V& val){
		Pair<iterator, bool> res = _tree::insert_T(key);
		if(res.second) res.first.value() = val;
		return res;
	}
	template<class K1, class V1>
	Pair<iterator, bool> insert_T(const K1& key, const V1& val){
		Pair<iterator, bool> res = _tree::insert_T(key);
		if(res.second)res.first.value() = val;
		return res;
	}
	template<class K1>
	Pair<iterator, bool> insert_T(const K1& key){return _tree::insert_T(key);}
	Pair<iterator, bool> insert(const K& key){return _tree::insert_T(key);}
	template<class K1>
	Pair<iterator, bool> changeKey_T(iterator it, const K1& key, bool insNew=false){
		if(it.is_nil()){
			if(insNew)return insert_T(key);
			return MakePair(this->end(), false);
		}
		iterator pl = this->find_T(key);
		if(pl!=this->end())return MakePair(pl, false);
		it->first = key;
		return reInsert(it, false);
	}
	template<class K1, class K0>
	Pair<iterator, bool> changeKey_TT(const K0& old, const K1& key, bool insNew=false){
		return changeKey_T(find_T(old), key, insNew);
	}
	Pair<iterator, bool> changeKey(const K& old, const K& key, bool insNew=false){
		return changeKey_TT(old, key, insNew);
	}
	Pair<iterator, bool> changeKey(iterator it, const K& key, bool insNew=false){
		return changeKey_T(it, key, insNew);
	}

	template<class K1>
	const V *find_ptr_T(const K1& key)const{
		const_iterator it = this->find_T(key);
		return it == this->end()? 0 : &it.value();
	}
	V *find_ptr(const K& key){return find_ptr_T(key);}
	const V *find_ptr(const K& key)const{return find_ptr_T(key);}
	template<class It>
	void erase_values(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			this->erase_T(K(*i));
		}
	}
	template<class It>
	void erase_values_T(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			this->erase_T(*i);
		}
	}

	virtual ~Map();
};
template<class K, class V, class Cmp>
Map<K,V,Cmp>::~Map(){}
template<class K, class V, class Cmp>
Map<K,V,Cmp>::Map(const Cmp & cmp, const allocator_type& Al) : _tree(cmp, Al){}
template<class K, class V, class Cmp>
Map<K,V,Cmp>::Map(const allocator_type& Al) : _tree(Al){}
template<class K, class V, class Cmp>
Map<K,V,Cmp>::Map() : _tree(){}

#define NULL_INDEX 0;

template<class K, class V, class TCmp = _T_cmp<K> >
class NumberedMap: public Map<K, Pair<size_t,V>, TCmp>{
	typedef Map<K, Pair<size_t,V>, TCmp> _map;
	typedef typename _map::_tree _tree;
	typedef typename _tree::_base _base;
	typedef NumberedMap<K,V,TCmp> _Myt;
public:
	typedef K key_type;
	typedef V value_type;
	typedef typename _tree::cit_base cit_base;
	typedef typename _tree::it_base it_base;
	typedef typename _tree::rit_base rit_base;
	typedef typename _tree::rcit_base rcit_base;
	typedef typename _map::allocator_type allocator_type;
	typedef typename _map::const_iterator map_const_iterator;
	typedef typename _map::const_reverse_iterator map_const_reverse_iterator;
	typedef typename _map::iterator map_iterator;
	typedef typename _map::reverse_iterator map_reverse_iterator;
public:
	class iterator : public map_iterator{
	public:
		iterator(it_base c) : map_iterator(c){}
		iterator(){}
		V& value()const{return this->_val()->second.second;}
		V& operator*()const{return this->_val()->second.second;}
		size_t number()const{if(this->_ptr)return this->_val()->second.first; return 0;}
		V* operator->()const{return this->_ptr ? &this->_val()->second.second : 0;}
		iterator& operator++(){++(*(it_base*)this); return *this;}
		iterator operator++(int){return (*(it_base*)this)++;}
		iterator& operator--(){--(*(it_base*)this); return *this;}
		iterator operator--(int){return (*(it_base*)this)--;}
		iterator next()const{const_iterator it = *this; return ++it;}
		iterator prev()const{const_iterator it = *this; return --it;}
	};
	class const_iterator : public map_const_iterator{
	public:
		const_iterator(iterator c) : map_const_iterator(c){}
		const_iterator(cit_base c) : map_const_iterator(c){}
		const_iterator(){}
		const V& value()const{return this->_val()->second.second;}
		const V& operator*()const{return this->_val()->second.second;}
		const V* operator->()const{return this->_ptr ? &this->_val()->second.second : 0;}
		size_t number()const{return this->_ptr ? this->_val()->second.first : 0;}
		const_iterator& operator++(){++(*(cit_base*)this); return *this;}
		const_iterator operator++(int){return (*(cit_base*)this)++;}
		const_iterator& operator--(){--(*(cit_base*)this); return *this;}
		const_iterator operator--(int){return (*(cit_base*)this)--;}
		const_iterator next()const{const_iterator it = *this; return ++it;}
		const_iterator prev()const{const_iterator it = *this; return --it;}
	};
	class reverse_iterator : public map_reverse_iterator{
	public:
		reverse_iterator(){}
		reverse_iterator(rit_base c) : map_reverse_iterator(c){}
		V& value()const{return this->_val()->second.second;}
		V& operator*()const{return this->_val()->second.second;}
		V* operator->()const{return this->_ptr ? &this->_val()->second.second : 0;}
		size_t number()const{return this->_ptr ? this->_val()->second.first : 0;}
		reverse_iterator& operator++(){++(*(rit_base*)this); return *this;}
		reverse_iterator operator++(int){return (*(rit_base*)this)++;}
		reverse_iterator& operator--(){--(*(rit_base*)this); return *this;}
		reverse_iterator operator--(int){return (*(rit_base*)this)--;}
		reverse_iterator next()const {reverse_iterator it = *this; return ++it;}
		reverse_iterator prev()const {reverse_iterator it = *this; return --it;}
	};
	class const_reverse_iterator : public map_const_reverse_iterator{
	public:
		const_reverse_iterator(){}
		const_reverse_iterator(reverse_iterator c) : map_const_reverse_iterator(c){}
		const_reverse_iterator(rcit_base c) : map_const_reverse_iterator(c){}
		const V& value()const{return this->_val()->second.second;}
		const V& operator*()const{return this->_val()->second.second;}
		const V* operator->()const{return this->_ptr ? &this->_val()->second.second : 0;}
		size_t number()const{return this->_ptr ? this->_val()->second.first : 0;}
		const_reverse_iterator& operator++(){++(*(rcit_base*)this); return *this;}
		const_reverse_iterator operator++(int){return (*(rcit_base*)this)++;}
		const_reverse_iterator& operator--(){--(*(rcit_base*)this); return *this;}
		const_reverse_iterator operator--(int){return (*(rcit_base*)this)--;}
		const_reverse_iterator next()const {const_reverse_iterator it = *this; return ++it;}
		const_reverse_iterator prev()const {const_reverse_iterator it = *this; return --it;}
	};
private:
	typedef RArray<iterator> _array;
	_array _arr;
public:
	iterator begin(){return _base::begin();}
	const_iterator begin() const{return _base::begin();}
	iterator end(){return _base::end();}
	const_iterator end() const{return _base::end();}

	reverse_iterator rbegin(){return _base::rbegin();}
	const_reverse_iterator rbegin() const{return _base::rbegin();}
	reverse_iterator rend(){return _base::end();}
	const_reverse_iterator rend() const{return _base::end();}

	explicit NumberedMap(const TCmp & cmp, const allocator_type& Al = allocator_type());
	explicit NumberedMap(const allocator_type& Al);
	NumberedMap();
	template<class K1>
	V &operator()(const K1& key){
		return insert_T(key).first->second.second;
	}
	V &operator[](const K& key){ 
		return *insert_T(key).first;
	}
	template<class K1>
	size_t number_of_T(const K1& key)const{
		const Pair<size_t,V> * res = _map::find_ptr_T(key);
		if(!res)return NULL_INDEX;
		return res->first;
	}
	size_t number_of(const K& key)const{
		return number_of_T(key);
	}
	V &by_index(size_t index){
		D_CHECK(index&&index<=_arr.size(), "iter: index out of range");
		iterator it = _arr[index];
		D_CHECK(it!=this->end(), "iter: index out of range");
		return *it;
	}
	const V &by_index(size_t index)const{
		D_CHECK(index&&index<=_arr.size(), "iter: index out of range");
		iterator it = _arr[index];
		D_CHECK(it!=this->end(), "iter: index out of range");
		return it->second.second;
	}

	V* ptr_by_index(size_t index){
		if(!(index&&index<=_arr.size()))return 0;
		iterator it = _arr[index];
		if(it==this->end())return 0;
		return &it->second.second;
	}
	const V * ptr_by_index(size_t index)const{
		if(!(index&&index<=_arr.size()))return 0;
		iterator it = _arr[index];
		if(it==this->end())return 0;
		return &it->second.second;
	}

	const_iterator iter(size_t index)const{
		D_CHECK(index&&index<=_arr.size(), "iter: index out of range");
		return _arr[index];
	}
	iterator iter(size_t index){
		D_CHECK(index&&index<=_arr.size(), "iter: index out of range");
		return _arr[index];
	}

	template<class K1>
	Pair<iterator, bool> insert_T(const K1& key, const V& val){
		Pair<map_iterator, bool> res = _map::insert_T(key);
		if(res.second){
			_arr.expand_coef(this->size(), 1.5)[this->size()] = res.first;
			res.first->second = val;
			res.first->first = this->size();
		}
		
		return res;
	}
	Pair<iterator, bool> insert(const K& key, const V& val){
		return insert_T(key, val);
	}
	template<class K1>
	Pair<iterator, bool> insert_T(const K1& key){
		Pair<map_iterator, bool> res = _map::insert_T(key);
		if(res.second){
			_arr.expand_coef(this->size(), 1.5)[this->size()] = res.first;
			res.first->first = this->size();
		}
		
		return res;
	}
	Pair<iterator, bool> insert(const K& key){return insert_T(key);}

	template<class K1>
	Pair<size_t, V> *find_pair_T(const K1& key){
		return _map::find_ptr_T(key);
	}
	template<class K1>
	const Pair<size_t, V> *find_pair_T(const K1& key)const{
		return _map::find_ptr_T(key);
	}
	Pair<size_t, V> *find_pair(const K& key){
		return _map::find_ptr(key);
	}
	const Pair<size_t, V> *find_pair(const K& key)const{
		return _map::find_ptr(key);
	}
	template<class K1>
	V *find_ptr_T(const K1& key){
		iterator it = this->find_T(key);
		return it == this->end()? 0 : &it->second.second;
	}
	template<class K1>
	const V *find_ptr_T(const K1& key)const{
		const_iterator it = this->find_T(key);
		return it == this->end()? 0 : &it->second.second;
	}
	V *find_ptr(const K& key){return find_ptr_T(key);}
	const V *find_ptr(const K& key)const{return find_ptr_T(key);}

	template<class K1>
	size_t erase_T(const K1& val){
		iterator it = find_T(val);
		if(it==this->end())return 0;
		_arr[it->second.first] = this->end();
		_base::erase((typename _base::cit_base)it, true);
		return 1;
	}
	size_t erase(const K& val){return erase_T(val);}
	iterator erase(const_iterator Where){
		if(Where == this->end())return this->end();
		_arr[Where->second.first] = this->end();
		return _base::erase(Where, true);
	}
	iterator erase(const_iterator fst, const_iterator lst){
		for(const_iterator it = fst; it!=lst; ++it)
			_arr[it->second.first] = this->end();
		return _base::erase(fst, lst, true);
	}
	template<class It>
	void erase_values(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			erase(*i);
		}
	}
	template<class It>
	void erase_values_T(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			erase_T(*i);
		}
	}
	void swap(_Myt &t){
		_base::swap(t);
		_arr.swap(t._arr);
	}
	virtual ~NumberedMap();
};
template<class K, class V, class Cmp>
NumberedMap<K,V,Cmp>::~NumberedMap(){}
template<class K, class V, class Cmp>
NumberedMap<K,V,Cmp>::NumberedMap(const Cmp & cmp, const allocator_type& Al) : _map(Al), _arr(1,0){}
template<class K, class V, class Cmp>
NumberedMap<K,V,Cmp>::NumberedMap(const allocator_type& Al) : _map(Al), _arr(1,0){}
template<class K, class V, class Cmp>
NumberedMap<K,V,Cmp>::NumberedMap() : _map(), _arr(1,0){}

template<class K, class TCmp = _T_cmp<K> >
class Set: public Tree<K, false, TCmp>{
	typedef Tree<K, false, TCmp> _tree;
	typedef Set<K,TCmp> _Myt;
public:
	typedef typename _tree::allocator_type allocator_type;
	typedef typename _tree::const_iterator const_iterator;
	typedef typename _tree::const_reverse_iterator const_reverse_iterator;
	typedef typename _tree::iterator iterator;
	typedef typename _tree::reverse_iterator reverse_iterator;
	explicit Set(const TCmp & cmp, const allocator_type& Al = allocator_type());
	explicit Set(const allocator_type& Al);
	Set();
	template<class K1>
	Set<K,TCmp>& include_T(const K1& key){
		insert_T(key);
		return *this;
	}
	template<class K1>
	Set<K,TCmp>& exclude_T(const K1& key){
		erase_T(key);
		return *this;
	}
	Set<K,TCmp>& include(const K& key){return include_T(key);}
	Set<K,TCmp>& exclude(const K& key){return exclude_T(key);}
	Set<K,TCmp>& operator <<(const K& key){return include_T(key);}
	Set<K,TCmp>& operator >>(const K& key){return exclude_T(key);}

	template<class K1>
	const K& operator()(const K1& key){return *_tree::insert_T(key).first;}
	const K& operator[](const K& key){return *_tree::insert(key).first;}
	Pair<iterator, bool> insert(const K& key){
		return _tree::insert_T(key);
	}
	template<class K1>
	Pair<iterator, bool> insert_T(const K1& key){
		return _tree::insert_T(key);
	}
	template<class K1>
	const K& find_ref_T(const K1& key)const{
		return *_tree::find_T(key);
	}
	bool in(const K& key)const{
		return _tree::find_T(key)!=this->end();
	}
	template<class K1>
	bool in_T(const K1& key)const{
		return _tree::find_T(key)!=this->end();
	}
	const K& find_ref(const K& key)const{
		return *_tree::find(key);
	}
	template<class It>
	void erase_values(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			erase(*i);
		}
	}
	template<class It>
	void erase_values_T(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			erase_T(*i);
		}
	}
	template<class Cont>
	Set<K,TCmp>&operator-=(const Cont &c){
		this->erase_values(c.begin(), c.end());
		return *this;
	}
	template<class Cont>
	Set<K,TCmp>&operator+=(const Cont &c){
		for(typename Cont::const_iterator it = c.begin(); it!=c.end(); ++it)
			insert(*it);
		return *this;
	}
	template<class Cont>
	Set<K,TCmp>&operator^=(const Cont &c){
		for(typename Cont::const_iterator it = c.begin(); it!=c.end(); ++it){
			Pair<iterator, bool> r = insert(*it);
			if(!r.second)erase(r.first);
		}
		return *this;
	}
	template<class Cont>
	Set<K,TCmp>&operator&=(const Cont &c){
		for(const_iterator it = this->begin(); it!=this->end();){
			typename Cont::const_iterator i = c.find(*it);
			if(i==c.end())it = erase(it);
			else ++it;
		}
		return *this;
	}
	virtual ~Set();
};
template<class K, class Cmp>
Set<K,Cmp>::~Set(){}
template<class K, class Cmp>
Set<K,Cmp>::Set(const Cmp & cmp, const allocator_type& Al) : _tree(Al){}
template<class K, class Cmp>
Set<K,Cmp>::Set(const allocator_type& Al) : _tree(Al){}
template<class K, class Cmp>
Set<K,Cmp>::Set() : _tree(){}

template<class K, class TCmp = _T_cmp<K> >
class NumberedSet: public Map<K, size_t, TCmp>{
	typedef Map<K, size_t, TCmp> _map;
	typedef typename _map::_tree _tree;
	typedef typename _tree::_base _base;
	typedef NumberedSet<K,TCmp> _Myt;
public:
	typedef typename _map::allocator_type allocator_type;
	typedef typename _map::iterator iterator;
	typedef typename _map::const_iterator const_iterator;
	typedef typename _map::const_reverse_iterator const_reverse_iterator;
	typedef typename _map::reverse_iterator reverse_iterator;
private:
	typedef RArray<iterator> _array;
	_array _arr;
public:

	explicit NumberedSet(const TCmp & cmp, const allocator_type& Al = allocator_type());
	explicit NumberedSet(const allocator_type& Al);
	NumberedSet();
	template<class K1>
	NumberedSet<K,TCmp>& include_T(const K1& key){
		insert_T(key);
		return *this;
	}
	template<class K1>
	NumberedSet<K,TCmp>& exclude_T(const K1& key){
		erase_T(key);
		return *this;
	}
	NumberedSet<K,TCmp>& include(const K& key){return include_T(key);}
	NumberedSet<K,TCmp>& exclude(const K& key){return exclude_T(key);}
	NumberedSet<K,TCmp>& operator <<(const K& key){return include_T(key);}
	NumberedSet<K,TCmp>& operator >>(const K& key){return exclude_T(key);}
	template<class K1>
	size_t operator()(const K1& key){
		return insert_T(key).first.value();
	}
	size_t operator[](const K& key){ 
		return *insert_T(key).first;
	}
	template<class K1>
	size_t number_of_T(const K1& key)const{
		const size_t * res = _map::find_ptr_T(key);
		if(!res)return NULL_INDEX;
		return *res;
	}
	size_t number_of(const K& key)const{
		return number_of_T(key);
	}
	const K& by_index(size_t index)const{
		D_CHECK(index&&index<=size(), "iter: index out of range");
		iterator it = _arr[index];
		D_CHECK(it!=this->end(), "iter: index out of range");
		return it.key();
	}
	const K* ptr_by_index(size_t index)const{
	//	D_CHECK(index&&index<=size(), "iter: index out of range");
		if(index && index<=_arr.size()){
			iterator it = _arr[index];
			if(it==this->end())return 0;
			return &it.key();
		}return 0;
	}
	bool has_index(size_t index)const{
		return index && index<=_arr.size() && _arr[index]!=this->end();
	}
	iterator iter(size_t index)const{
		D_CHECK(index && index<=_arr.size(), "iter: index out of range");
		return _arr[index];
	}

	template<class K1>
	Pair<iterator, bool> insert_T(const K1& key){
		Pair<typename _map::iterator, bool> res = _map::insert_T(key);
		if(res.second){
			_arr.expand_coef(this->size(), 1.5)[this->size()] = res.first;
			*res.first = this->size();
		}
		return res;
	}
	Pair<iterator, bool> insert(const K& key){return insert_T(key);}

	template<class K1>
	size_t erase_T(const K1& val){
		iterator it = find_T(val);
		if(it==this->end())return 0;
		_arr[it->second] = this->end();
		_base::erase(it, true);
		return 1;
	}
	size_t erase(const K& val){return erase_T(val);}
	iterator erase(const_iterator Where){
		if(Where==this->end())return Where;
		_arr[Where->second] = this->end();
		return _base::erase(Where, true);
	}
	iterator erase(const_iterator fst, const_iterator lst){
		for(const_iterator it = fst; it!=lst; ++it)
			_arr[it->second.first] = this->end();
		return _base::erase(fst, lst, true);
	}
	template<class K1>
	const K& find_ref_T(const K1& key)const{
		return _tree::find_T(key)->first;
	}
	const K& find_ref(const K& key)const{
		return _tree::find(key)->first;
	}

	template<class It>
	void erase_values(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			erase(*i);
		}
	}
	template<class It>
	void erase_values_T(It fst, It lst){
		for(It it = fst; it!=lst;){
			It i = it;
			++it;
			erase_T(*i);
		}
	}
	void swap(_Myt &t){
		_base::swap(t);
		_arr.swap(t._arr);
	}
	virtual ~NumberedSet();
};
template<class K, class Cmp>
NumberedSet<K,Cmp>::~NumberedSet(){}
template<class K, class Cmp>
NumberedSet<K,Cmp>::NumberedSet(const Cmp & cmp, const allocator_type& Al) : _map(cmp, Al),_arr(1,0){}
template<class K, class Cmp>
NumberedSet<K,Cmp>::NumberedSet(const allocator_type& Al) : _map(Al),_arr(1,0){}
template<class K, class Cmp>
NumberedSet<K,Cmp>::NumberedSet() : _map(),_arr(1,0){}
