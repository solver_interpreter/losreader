#include "loshighlighter.h"
#include <QTextEdit>
#include <QDebug>
#include <QEvent>
#include <QToolTip>
#include <QTextLayout>
//#include

LosHighlighter::LosHighlighter(QTextEdit *parent) :
	QSyntaxHighlighter(parent){
	fixed_formats["text"]       = &def_cf;
	fixed_formats["number"]     = &number;
	fixed_formats["numbers"]    = &numbers;
	fixed_formats["numberd"]    = &numberd;
	fixed_formats["index"]      = &index;
	fixed_formats["call index"] = &call_ind;
	fixed_formats["par"]        = &par;
	fixed_formats["comment"]    = &comment;
	foreach(QTextCharFormat *f, fixed_formats)
		f->setFont(parent->font());
	def_cf.setFontFixedPitch(true);
//	index.setFontPointSize(index.fontPointSize()*0.6);
//	index.setVerticalAlignment(QTextCharFormat::AlignSubScript);
	comment.setForeground(QColor(20,150,0));
	//comment.setFontItalic(true);
	info_comment.setForeground(QColor(0,50,50));
	//info_comment.setFontFixedPitch();
	number.setForeground(QColor(100,0,50));
	numbers.setForeground(QColor(0,0,100));
	numberd.setForeground(QColor(0,50,0));
	spec[QString::fromLocal8Bit("обращение")] = 1;
	spec[QString::fromLocal8Bit("справка")] = spec["info"] = 2;
	call_ind.setForeground(QColor(100,100,100));
	if(parent)new LosHlFilter(parent);
}

LosHighlighter::~LosHighlighter(){
}

void LosHighlighter::setWordType(const QString &word, int t){
	if(t<0)kw.remove(word);
	else{
		kw[word] = t;
	}
}

void LosHighlighter::setCharFormat(int type, const QTextCharFormat &f){
	formats[type] = f;
}

void LosHighlighter::setToolTip(const QString &s, const QString &t){
	if(t.isEmpty())
		tooltips.remove(s);
	else tooltips[s].simple=t;
}

void LosHighlighter::setToolTips(const QString &s, const QString &simple, const QMap<QString, QString> &calls, const QString &ind){
	_tooltipinfo& i = tooltips[s];
	i.simple = simple;
	i.calls = calls;
	i.callIndex = ind;
	if(i.simple.isEmpty()){
		if(!i.calls.isEmpty())i.simple = i.calls.begin().value();
		else i.simple = ind;
	}else{
		if(i.callIndex.isEmpty())i.callIndex = i.simple;
		if(!i.calls.contains(QString()))
			i.calls[QString()] = i.simple;
	}
}

bool LosHighlighter::setNamedCharFormat(const char *name, const QTextCharFormat &f){
	QTextCharFormat *pf = fixed_formats.value(name, 0);
	if(!pf)return false;
	*pf = f;
	return true;
}

QTextCharFormat &LosHighlighter::charFormat(int type){
	int n = formats.size();
	QTextCharFormat &res = formats[type];
	if(n!=formats.size())res.setFont(this->parent()->property("font").value<QFont>());
	return res;
}
QTextCharFormat *LosHighlighter::namedCharFormat(const QString &nm){
	return fixed_formats.value(nm,0);
}

int category(QChar c){
	if(c.isLetter()||c=='_')return QChar::Number_Letter;
	if(c.isDigit())return QChar::Number_DecimalDigit;
	if(c.isSpace())return QChar::Separator_Space;
	switch(c.category()){
	case QChar::Symbol_Math:
		if(c.unicode()>256)return -1;
		else return c.category();
	default:
		switch(c.unicode()){
		case '(':
		case '{':
		case '[':
		case ']':
		case '}':
		case ')':
		case ',':
			return -1;
		case '/': case '%':
		case '*':return QChar::Symbol_Math;
		}
	}
	return 0;
}

void LosHighlighter::highlightBlock(const QString &text){
	int i, l = text.size(), j, ni;
	int st = 0, st1, pst=0;
	int sp = 0;
	const int spec_mask = (1<<20) -1;
	for(i=j=0; i<=l; i++){
		st1 = i<l ? category(text[i]) : 0;
		if(i&&((text[i-1]=='[' && text[i]=='<')||(text[i-1]=='>' && text[i]==']'))){
			st = -1;
			continue;
		}
		if(st1!=st || st1<0){
			ni = i;
			if(i>j){
				QString s(text.data()+j, i-j);
				if(st == QChar::Number_DecimalDigit){
					if(pst == QChar::Number_Letter)
						this->setFormat(j,i-j, index);
					else if(sp & (spec_mask+1)){
						QHash<QString, _tooltipinfo>::const_iterator it = tooltips.find(s);
						QTextCharFormat fmt = call_ind;
						if(it!=tooltips.end())
							fmt.setToolTip(it->callIndex);
						this->setFormat(j, i-j, fmt);
					}else {
						if(st1&&(text[i]=='d'||text[i]=='D')){
							st1 = st;
							this->setFormat(j, i-j, numberd);
							QTextCharFormat indexd(index);
							indexd.setForeground(numberd.foreground());
							this->setFormat(i, 1, indexd);
							i++;
							st1 = i<l ? category(text[i]) : 0;
							ni = i;
						}else if(st1&&(text[i]=='s'||text[i]=='S')){
							this->setFormat(j, i-j, numbers);
							QTextCharFormat indexs(index);
							indexs.setForeground(numbers.foreground());
							this->setFormat(i, 1, indexs);
							i++;
							st1 = i<l ? category(text[i]) : 0;
							ni = i;
						}else this->setFormat(j,i-j, number);
					}
				}
				else{
					QTextCharFormat fmt = formats.value(kw.value(s, -1), def_cf);
					if(st == QChar::Number_Letter){
						QHash<QString, _tooltipinfo>::const_iterator it = tooltips.find(s);
						if(sp & (spec_mask+1)){
							fmt = call_ind;
							if(it!=tooltips.end())fmt.setToolTip(it->callIndex);
							sp = 0;
						}else{
							if(it!=tooltips.end()){
								int k, l;
								for(k=i; k<text.length(); k++)
									if(!text[k].isSpace())break;
								if(k<text.length()&&text[k] == '('){
									for(k++; k<text.length(); k++)
										if(!text[k].isSpace())break;
									QString arg;
									for(l=k; l<text.length(); l++){
										if(!text[l].isLetterOrNumber())break;
									}
									if(l>k)arg = text.mid(k,l-k);
									QMap<QString,QString>::ConstIterator f = it->calls.find(arg);
									if(f!=it->calls.end()){
										if(l>k){
											QTextCharFormat fint = call_ind;
											fint.setToolTip(f.value());
											this->setFormat(k, l-k, fint), ni=l;
										}
									} else {
										f = it->calls.begin();
									}
									if(f!=it->calls.end())fmt.setToolTip(f.value());
									else fmt.setToolTip(it->simple);
								}else fmt.setToolTip(it->simple);
							}
							sp = spec.value(s);
						}
					}else if(st!=QChar::Separator_Space){
						/*if(text[i] == '[' && text[i+1]=='<'){
							i++;

						}else*/
						if(s=="(" && sp && !(sp&(spec_mask+1)))
							sp|=(spec_mask+1);
						else sp = 0;
					}
					this->setFormat(j, i-j, fmt);
				}
				j = i = ni;
			}
			pst = st; st = st1;
		}//else{
		if(i < l-1 && text[i]=='/' && text[i+1]=='/'){
			if(i+2<l && text[i+2]=='/')this->setFormat(i,l-i, info_comment);
			else this->setFormat(i, l-i, comment);
			break;
		}
		//}
	}
//	this->setFormat();
}


bool LosHlFilter::eventFilter(QObject *, QEvent *e){
	if(e->type() == QEvent::MouseMove){
		QMouseEvent *me = (QMouseEvent*)e;
		tpos = me->globalPos();
		if(me->buttons() || me->modifiers()){
			hideT();
			return false;
		}
		QTextCursor c1 = br->cursorForPosition(me->pos());
		if(c1==c)return false;
		int c1pos = c1.positionInBlock();
		QTextLayout *tl = c1.block().layout();
		QList<QTextLayout::FormatRange> lr = tl->additionalFormats();
		bool found = false;
		for(QList<QTextLayout::FormatRange>::Iterator it = lr.begin(); it!=lr.end(); ++it){
			if(it->start<=c1pos && it->start + it->length > c1pos){
				found = true;
				if(it->format.toolTip().isEmpty()){
					hideT();
//					qDebug()<<"hide tooltip";
				}else {
					showT(me->globalPos(), it->format.toolTip());
//					qDebug()<<"show tooltip"<<it->format.toolTip();
				}
				break;
			}
		}
		if(!found){
			hideT();
//				qDebug()<<"hide tooltip";
		}
		c = c1;
	}else if(e->type() == QEvent::Leave){
		hideT();
		c = QTextCursor();
	}
	return false;
}

LosHlFilter::LosHlFilter(QTextEdit *b):QObject(b){
	br = b;
	ttdelay = 500;
	b->setMouseTracking(true);
	connect(&tm, SIGNAL(timeout()), this, SLOT(onTimer()));
	tm.setSingleShot(true);
	if(br){
		br->installEventFilter(this);
		if(br->viewport())br->viewport()->installEventFilter(this);
	}
}

void LosHlFilter::hideT(){
	tm.stop();
	QToolTip::hideText();
}

void LosHlFilter::onTimer(){
	QToolTip::showText(tpos, tt, br);
}
