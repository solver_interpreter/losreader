#include "LosProblemInfo.h"
#include <QMap>

const int zadachaS = 549;
const int timeS = 170;
const int answerS = 190;
const int oglavlenieS = 840;
const int L0 = 260;
const int naborS = 186;
const int velichinaS = 418;
const int goalsS = 102;
const int premiseS = 182;
const int conditionS = 183;
const int commentsS = 92;

QVector<int> getPath(const LosInfoNode *p){
	QVector<int> res;
	const LosLogicTerminal* t = dynamic_cast<const LosLogicTerminal*>(p);
	if(!t || t->terms.size()<1)return res;
	const QVector<LosTerm::Item> &d = t->terms[0].data;
	int l = d.size();
	if(l<=3 || !d[0].isSymbol(naborS))return res;
	res.resize(l-3);
	for(int i=2; i<l-1; i++){
		if(!d[i].isSymbol())return QVector<int>();
		res[i-2] = d[i].val-L0;
	}
	return res;
}

long long getTime(const LosLogicTerminal *t){
	long long res=0;
	if(!t || t->terms.size()<1)return -1;
	const QVector<LosTerm::Item> &d = t->terms[0].data;
	int l = d.size();
	if(l<=6 || !d[0].isSymbol(timeS) || !d[2].isSymbol(velichinaS))
		return -1;
	for(int i=4; i<l-2; i++){
		if(d[i].isSymbol())return -1;
		res = res*10+(d[i].val-L0);
	}
	return res;
}
template<class T>
const T* getNode(const QMap<LosTerm, const LosInfoList::Label*>& mp, int sym, int index){
	const LosInfoList::Label* lb = mp.value(LosTerm(sym, true), 0);
	if(lb && lb->refs.size()>index)return dynamic_cast<const T*>(lb->refs[index]);
	return 0;
}

QString& operator<<(QString& str, const QString& s0){
	return str+=s0;
}
QString& operator<<(QString& str, const char* s0){
	str.append(s0);
	return str;
}
QString& operator<<(QString& str, char s0){
	return str+=s0;
}

void printProblem(QString &s, const LosInfoList* p, const SymbolNames&sn, const QString &tab, int indent=1){
	QString tab0, tab1;
	for (int i = 0; i<indent; i++)tab0 += tab;
	if (!p) {
		s << tab0 << "problem : <invalid problem>;\n";
		return;
	}
	QMap<LosTerm, const LosInfoList::Label*> items;
	foreach(const LosInfoList::Label& lb, p->labels)
		items[lb.term] = &lb;
	const LosLogicTerminal *goalsNode = getNode<LosLogicTerminal>(items, goalsS, 0);
	const LosLogicTerminal *comments = getNode<LosLogicTerminal>(items, commentsS, 0);
	const LosInfoList::Label* premices = items.value(LosTerm(premiseS, true), 0);
	const LosInfoList::Label* conditions = items.value(LosTerm(conditionS, true), 0);
	if(!goalsNode||!premices||!goalsNode->terms.size()||!premices->refs.size()){
		s << tab0 << "problem : <invalid problem>;\n";
		return;
	}
	s << tab0 << "problem :\n";
	tab0 += tab;
	tab1 = tab0 + tab;
	QString ts;
	goalsNode->terms[0].toString(ts, sn);
	s << tab0 << "type : " << ts << ";\n" << tab0 << "premises :\n";
	int prnum = premices->refs.size();
	for(int i=0; i<prnum; i++){
		const LosLogicTerminal* t = dynamic_cast<const LosLogicTerminal*>(premices->refs[i]);
		if(!t)continue;
		for(int j=0; j<t->terms.size(); j++){
			if (!t->terms[j].data.size())continue;
			t->terms[j].toString(ts, sn, ", ");
			s << tab1 << ts << ";\n";
		}
	}
	s<<"\n";
	if(conditions && conditions->refs.size()){
		s << tab0 << "conditions :\n";
		int cnum = conditions->refs.size();
		for(int i=0; i<cnum; i++){
			const LosLogicTerminal* t = dynamic_cast<const LosLogicTerminal*>(conditions->refs[i]);
			if(!t)continue;
			for(int j=0; j<t->terms.size(); j++){
				if (!t->terms[j].data.size())continue;
				t->terms[j].toString(ts, sn, ", ");
				s << tab1 << ts << ";\n";
			}
		}
		s<<"\n";
	}
	if(goalsNode->terms.size()>1){
		s << tab0 << "goals :\n";
		for(int j=1; j<goalsNode->terms.size(); j++){
			if (!goalsNode->terms[j].data.size())continue;
			goalsNode->terms[j].toString(ts, sn, ", ");
			s << tab1 << ts << ";\n";
		}
	}
	if (comments && comments->terms.size() > 0) {
		s << tab0 << "comments :\n";
		for (int j = 0; j < comments->terms.size(); j++) {
			if (!comments->terms[j].data.size())continue;
			comments->terms[j].toString(ts, sn, ", ");
			s << tab1 << ts << ";\n";
		}
	}

}

QString convertProblemNode(LosInfoNode *p, const SymbolNames & sn, QVector<int> &path){
	if(p->is_terminal())return "<invalid terminal problem node>";
	LosInfoList *l = dynamic_cast<LosInfoList*>(p);
	QMap<LosTerm, const LosInfoList::Label*> items;
	foreach(const LosInfoList::Label& lb, l->labels)
		items[lb.term] = &lb;
	const LosInfoList *problemNode = getNode<LosInfoList>(items, zadachaS, 0);
	const LosLogicTerminal *timeNode = getNode<LosLogicTerminal>(items, timeS, 0);
	const LosLogicTerminal *answerNode = getNode<LosLogicTerminal>(items, answerS, 0);
	const LosLogicTerminal *pathNode = getNode<LosLogicTerminal>(items, oglavlenieS, 0);
	path = getPath(pathNode);
	QString res;
	QString tab = "    ";
	res += "problem {\n";
	if (path.size()>1) {
		res << tab << "number : " << QString::number(path.last()) << ";\n";
		path.pop_back();
	}
	printProblem(res, problemNode, sn, tab, 1);
	res << tab << "//------------------------\n";
	if(timeNode){
		long long tm = getTime(timeNode);
		if(tm>=0){
			res+=tab+"time : "+QString::number(tm)+";\n";
		}
	}
	if(answerNode && answerNode->terms.size()){
		QString t;
		answerNode->terms[0].toString(t, sn, ", ");
		res << tab << "answer : " << t << ";\n";
	}
	return res+="}\n///////////////////////////////////////////////////////////////////////////////////////\n\n";
}
