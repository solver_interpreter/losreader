#ifndef LOSINFOVIEWER_H
#define LOSINFOVIEWER_H

#include <QWidget>
#include "losinfoblock.h"
#include <common/classref.h>

namespace Ui {
class LosInfoViewer;
}

class StringFilter:public ReferencedBase{
public:
	virtual void apply(QString &){}
};
DECLARE_CLASS_REFS(StringFilter)

struct Rule {
	int sym;
	LosTerm theorem;
	LosTerm header;
	QString path;
	QVector<LosTerm> filters;
	QVector<LosTerm> specifiers;
	QVector<LosTerm> normalizers;
	QString comment;
	void write(QTextStream &s, const QVector<QString> *names)const;
	void writeHTML(int num, QString &res, const QVector<QString> *names, const QString &link = "")const;
};

struct RuleFilter {
	QVector<TermCond> h, f, s, n,t;
	bool hasC = false;
	QString read(const QMap<QString,int>& nm, const QString &str);
	bool empty() const { return h.empty() && f.empty() && s.empty() && n.empty() && t.empty()&&!hasC; }
	bool operator()(const Rule &r)const;
};

bool operator<(const QStringList &l1, const QStringList &l2);
class LosInfoViewer : public QWidget
{
	Q_OBJECT
	LosInfoNode *n;
	class SItem{
	public:
		LosInfoNode *n;
		int pos;
		SItem(const SItem &s){
			n=s.n; pos = s.pos;
			if(n)n->_used++;
		}
		SItem& operator=(const SItem&s){
			if(s.n)s.n->_used++;
			n->del();
			n = s.n;  pos = s.pos;
			return *this;
		}
		SItem(LosInfoNode *nn, int p){
			n = nn;
			pos = p;
			if(nn)nn->_used++;
		}
		SItem(){}
		~SItem(){ n->del();	}
	};
	struct FBStack{
		QVector<SItem> st;
		int c;
		FBStack(){c=0;}
		void push(LosInfoNode *s, int pos){
			if(!s)return;
			if(c<st.size())st.remove(c,st.size()-c);
			st.push_back(SItem(s, pos));
			c = st.size();
		}
		bool atEnd()const{return c>=st.size()-1;}
		bool atStart()const{return c<=0;}
		const SItem &curr()const{return st[c];}
	};
	FBStack fb;
	void node_updated(int pos = 0);
	QVector<QString> *names;
	StringFilterRef filter;
public:
	explicit LosInfoViewer(QWidget *parent = 0);
	QVector<QString>* getNames(){ return names; }
	StringFilterRef getFilter(){ return filter; }
	~LosInfoViewer();
	void writePriem(LosInfoNode *n, QTextStream &stream);
	void writePriem(int sym, LosInfoNode *nn, QMap<QString, QByteArray> &out, QString path = "");
	void writePriem(int sym, LosInfoNode*nn, QMap<QStringList, Rule> &out, QStringList path = {});
	public slots:
	void setStringFilter(const StringFilterRef &f){filter = f; if(n&&n->is_terminal())node_updated();}
	void setNames(QVector<QString>* nm){names = nm; if(n)node_updated();}
	void setNode(LosInfoNode *n);
	void onAnchor(const QUrl&);
	void forward();
	void backward();
	void writePriem(QTextStream &stream);
private:
	Ui::LosInfoViewer *ui;
	QVector<QString> filters, pointers, norms, headers;
	QString theorem, priem_path;
	LosTerm theoremT;
	QVector<LosTerm> headerT;
	int head = 0;
};

#endif // LOSINFOVIEWER_H
