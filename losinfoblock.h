#ifndef LOSINFOBLOCK_H
#define LOSINFOBLOCK_H
#include "losfragment.h"
#include <QVariant>

struct LosInfoNode{
	int _used;
	void del(){if(this && !--_used)delete this;}
	LosInfoNode(){_used = 0;}
	virtual ~LosInfoNode(){}
	virtual bool is_terminal()const = 0;
	virtual bool is_logic()const { return false; }
	static LosInfoNode * read(ByteStream &bs, QMap<int, LosInfoNode*> &n);
	static LosInfoNode * read_list(int used_refs, int p0, ByteStream &bs, QMap<int, LosInfoNode *> &n);
	static LosInfoNode * read_text_term(ByteStream &bs);
	static LosInfoNode * read_log_term(ByteStream &bs);
};

typedef QVector<QString> SymbolNames;

struct LosTerm{
	struct Item{
		enum Type{
			Var = 0,
			Sym = 1,
			LeftP = 2,
			RightP = 3,
			Invalid = 4
		};
		union{
			struct{
				Type tp : 3;
				int val : 29;
			};
			int cmp : 32;
		};
		int compare(Item i)const{return cmp<i.cmp ? -1 : cmp==i.cmp ? 0 : 1;}
		Item(Type t = Invalid){tp = t;}
		Item(Type t, int v){tp = t; val = v;}
		bool isSymbol(int sn)const{return tp == Sym && val==sn;}
		bool isSymbol()const{return tp==Sym;}
	};
	QVector<Item> data;
	void toString(QString &res, const QVector<QString> &sn, const char *sep = " ")const{
		res.clear();
		QString d = "";
		for(int i=0; i<data.size(); i++){
			const Item &it = data[i];
			switch(it.tp){
			case Item::Var: res += d+"x"+QString::number(it.val); d = sep;
				break;
			case Item::Sym:
				res+=d;
				if(it.val<sn.size() && !sn[it.val].isEmpty())res += sn[it.val];
				else res+="<symbol "+QString::number(it.val)+">";
				d = sep;
				break;
			case Item::LeftP: res+='('; d=""; break;
			case Item::RightP: res+=')'; d = sep; break;
			}
		}
	}
	int compare(const LosTerm& t)const{
		int l = qMin(data.size(), t.data.size());
		for(int i=0; i<l; i++){
			int c = data[i].compare(t.data[i]);
			if(c<0)return -1;
			if(c>0)return 1;
		}
		return t.data.size()>l ? -1 : t.data.size()==l ? 0 : 1;
	}
	bool operator<(const LosTerm& t)const{ // lexicographic comparation
		int l = qMin(data.size(), t.data.size());
		for(int i=0; i<l; i++){
			int c = data[i].compare(t.data[i]);
			if(c<0)return true;
			if(c>0)return false;
		}
		return t.data.size()>l;
	}
	bool operator==(const LosTerm& t)const{
		return !compare(t);
	}

	bool isSymbol(int sn)const{
		return data.size()==1 && data[0].tp == Item::Sym && data[0].val == sn;
	}
	bool isSymbol()const{
		return data.size()==1 && data[0].tp == Item::Sym;
	}
	bool isVariable(int v)const{
		return data.size()==1 && data[0].tp == Item::Var && data[0].val == v;
	}
	LosTerm(){}
	LosTerm(int num, bool symbol){
		data.push_back(Item(symbol ? Item::Sym : Item::Var, num));
	}
	QVector<LosTerm> subterms(int *head=0)const{
		QVector<LosTerm> res;
		if(!data.size())return res;

		if(head){
			if(data[0].isSymbol())
				*head = data[0].val;
			else *head = -data[0].val;
		}
		if(data.size()<=3)return res;
		for(int i=2; i<data.size()-1; i++){
			if(data[i].isSymbol() && data[i+1].tp == Item::LeftP){
				LosTerm t;
				t.data<<data[i]<<data[i+1];
				int balance = 1;
				for(i+=2; balance; i++){
					if(data[i].tp==Item::LeftP)balance++;
					else if(data[i].tp==Item::RightP)balance--;
					t.data<<data[i];
				}
				i--;
				res.push_back(t);
			} else res.push_back(LosTerm(data[i].val, data[i].isSymbol()));
		}
		return res;
	}
	bool match(const LosTerm&t1)const {
		QMap<int, LosTerm> m;
		return match_m(t1, m);
	}
	bool match_m(const LosTerm&t, QMap<int, LosTerm> &m)const;
};

struct TermCond {
	LosTerm pattern;
	bool exact;
	bool root;
	bool subterm;
	bool operator()(const LosTerm &t)const;
};

struct LosInfoTerminal:public LosInfoNode{
	bool is_terminal()const{return true;}
	virtual bool is_text()const = 0;
	virtual bool toHtml(QString &res, const QVector<QString> &sn)const = 0;
	virtual bool toString(QString &res, const QVector<QString> &sn, const char *sep = " ")const = 0;
};

struct LosLogicTerminal : public LosInfoTerminal{
	bool is_text()const{return false;}
	bool is_logic()const { return true; }
	QVector<LosTerm> terms;
	bool toHtml(QString &res, const QVector<QString> &sn) const;
	bool toString(QString &res, const QVector<QString> &sn, const char *sep = " ") const;
};

struct LosDataTerminal : public LosInfoTerminal{
	bool is_text()const{return true;}
	QVariant data;
	bool toHtml(QString &res, const QVector<QString> &sn) const;
	bool toString(QString &res, const QVector<QString> &sn, const char *sep = " ") const;
};

struct LosInfoList : public LosInfoNode{
	LosInfoList(){}
	bool is_terminal()const{return false;}
	struct Label{
		LosTerm term;
		QVector<LosInfoNode*> refs;
		Label(){}
		Label(const Label&l):term(l.term),refs(l.refs){
			foreach(LosInfoNode* n, refs)
				if(n)n->_used++;
		}
		Label& operator=(const Label &l){
			foreach(LosInfoNode* n, l.refs)
				if(n)n->_used++;
			foreach(LosInfoNode* n, refs)
				n->del();
			term=l.term;
			refs = l.refs;
			return *this;
		}
		bool operator<(const Label &l)const{
			return term<l.term;
		}
		~Label(){
			foreach(LosInfoNode* n, refs)
				n->del();
		}
	//private:
	//	Label(const Label&);
	//	Label& operator=(const Label&);
	};
//	QMap<Label, int> labels;
	QVector<Label> labels;
//	QVector< > refs;
	~LosInfoList(){}
};

class LosInfoBlock{
	QVector<LosProgInfo> catalog;
	QVector<LosInfoNode*> nodes;
public:
	LosInfoBlock();
	bool read(const QString &path, OutputContext c = OutputContext());
	~LosInfoBlock(){
		foreach(LosInfoNode *n, nodes)n->del();
	}
	void clear(){
		foreach(LosInfoNode* n, nodes)n->del();
		nodes.clear();
		catalog.clear();
	}
	LosInfoNode *node(int n){
		return nodes.value(n, 0);
	}
	QVector<LosInfoNode*>& getNodes(){return nodes;}
};

#endif // LOSINFOBLOCK_H
