#ifndef LOSPROGRAM_H
#define LOSPROGRAM_H
#include <qglobal.h>
//#include <QMap>
#include <QDebug>
#include <QVector>
#include "Parsers/streamBase.h"
#include "LeakDetect.h"

union LosProgInfo{
	quint32 d;
	struct{
		uint pos:22;
		uint num:9;
		uint valid:1;
	};
	LosProgInfo(quint32 x=0){d=x;}
};
bool convertRusToUnicode(const char *str, QString &res, uint maxlen=-1);
bool convertUnicodeToOld(const QString &str, char *res, uint maxlen=-1);
const char* loadLosIndex(const QString &fn, QVector<LosProgInfo>&index, OutputContext c);
const char* number7(int n);

class BitStream{
	quint32 * d;
	uint byte, bit, len, bitlen, dsz;
	bool read_err;
public:
	BitStream(){len=0; d=0;byte=0;bit=0;}
	~BitStream(){
		delete[]d;
	}
	void start(){bit=byte=0;read_err = false;}
	void init(int l){
		delete[]d;
		d = new quint32[dsz=((len=l)+3)/4];
		start();bitlen=len*8;
	}
	char *data(){return (char*)d;}
	quint32 read(uint bits){
		quint32 mask = ((1<<bits)-1);
		quint32 res = (d[byte]>>bit);
		bit+=bits;
		if(bit>=32){
			if(byte+1>=dsz){
				if(bit>32){read_err = true; bit=33;}
				return res;
			}
			++byte;
			bit-=32;
			res |= d[byte]<<(bits-bit);
		}
		return res & mask;
	}
	bool atEnd()const{return byte*32+bit>=bitlen;}
	bool wasError()const{return byte*32+bit>bitlen;}

};
struct ByteStream{
	uchar * d;
	uint ptr, len;
	bool read_err;
//public:
	ByteStream(){len=0; d=0; ptr=0;read_err = false;}
	void start(uint ptr0){ptr=ptr0;read_err = false;}
	void init(void *p, int l){
		d = (uchar*)p;
		len=l;
		read_err = false;
	}
	char *data(){return (char*)d;}
	uchar readbyte(){
		if(ptr>=len){read_err = true; return 0;}
		return d[ptr++];
	}
	quint16 read2bytes(){
		if(ptr+2>len){read_err = true; return 0;}
		quint16 res = quint16(d[ptr]) | (quint16(d[ptr+1])<<8);
		ptr+=2;
		return res;
	}
	quint32 read3bytes(){
		if(ptr+3>len){read_err = true; return 0;}
		quint32 res = quint32(d[ptr]) | (quint32(d[ptr+1])<<8) | (quint32(d[ptr+2])<<16);
		ptr+=3;
		return res;
	}
	quint32 read4bytes(){
		if(ptr+4>len){read_err = true; return 0;}
		quint32 res = quint32(d[ptr]) | (quint32(d[ptr+1])<<8) |
				(quint32(d[ptr+2])<<16) | (quint32(d[ptr+3])<<24);
		ptr+=4;
		return res;
	}
};

struct LosProgramPrefix{
	//struct{ //A1
		uint length : 14;
		uint root : 1;
		uint invalid: 1;
	//}:16;
//	struct{ //A2
		uint nextFragShift : 14;
		uint unusedA2:2;
	//}:16;
	union{ //A3
		uint logSymNum : 16;
		uint parentFragPos : 22;
	};//:24;
};

struct LosProgItem{
	enum Type{
		Variable,
		LogSymbol,
		Procedure
	} type;
	union{
		uint varNum;
		uint procNum;
		uint logSymNum;
	};
	bool isVariable()const{return type==Variable;}
	bool isLogSymbol()const{return type==LogSymbol;}
	bool isProcedure()const{return type==Procedure;}
	bool read(BitStream &s){
		if(!s.read(1)){      //logic symbol procedure call
			type = LogSymbol;
			logSymNum = s.read(19);
		}else if(s.read(1)){ //interpreter procedure call
			type = Procedure;
			procNum = s.read(9);
		}else{
			type = Variable;
			varNum = s.read(9);
		}
		return !s.wasError();
	}
};

struct SubFragShift{
	union{
		uint shift : 31;
		uint logSym :31;
	};//:16;
	uint real:1;

	SubFragShift(){real=0; logSym = 0;}
};

struct LosProgramFragment{
	LosProgInfo info;
	uint start, end, pstart;
	uint logSym;
	bool hasErrors;
//	QSet<uint> positions;

	LosProgramFragment *parent;
	bool valid;
	bool isRoot;
	QVector<LosProgItem> commands;
	QVector<SubFragShift> subFragShifts;
	QVector<LosProgramFragment*> subFrags;
	const char* read(QMap<uint, LosProgramFragment*> &, const QByteArray &s, LosProgInfo i, OutputContext c = OutputContext());
	LosProgramFragment(){parent = 0;}
	~LosProgramFragment(){qDeleteAll(subFrags);}
	bool isLeaf()const{return subFragShifts.isEmpty();}
};
#endif // LOSPROGRAM_H
