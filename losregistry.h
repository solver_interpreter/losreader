#ifndef LOSREGISTRY_H
#define LOSREGISTRY_H
#include <QVector>
#include <QMap>

struct LosSymbolProperty{
	enum Role{
		Auxiliary = 0,
		Verification = 1,
		Enumerative = 2,
		Mixed = 3
	};
	uint arity:16;      //arity of symbol or the least arity if it is floating;
	uint isFloating:1;  //true if symbol has floating arity;
	uint special:1;     //true if symbol is nonstandart
	uint isExpresion:1; //true if symbol realizes operator expression;
	Role role:4;        //type of symbol;
//	uint builtIn:1;     //true if interpreter realizes this operator;
	uint defined:1;     //true if properties defined
	const char *read(const char* str); //
	LosSymbolProperty(){defined = 0;}
};

struct LosSymbolInternalInfo{
	const char *internalName;
	uint number;
	uint type;
	LosSymbolProperty properties;
	union{
		uint enumerationStep; //step operator, required for enumerative operators only
	};

	LosSymbolInternalInfo(uint num=0, const char *name=0){
		number = num;
		internalName = name;
		type = 0;
	}
};

struct LosSymbolTypeInfo{
	const char *name;
	uint number;
	QList<uint> symbols;
	LosSymbolTypeInfo(){name = 0; number = 0;}
	LosSymbolTypeInfo(const char *nm, uint num){name = nm; number = num;}
};

class LosSymbolRegistry{
public:
	static uint addSymbol(const char* name, uint num = 0, const char *type = 0, const char *property = 0);
	static bool setSymbolType(const char *name, const char *type);
	static uint registerType(const char* tname);
	//===========================
	static uint symbolNumber(const char *name);
	static uint typeNumber(const char *type);
	static const LosSymbolInternalInfo& symbolInfo(int num);
//	static bool setSymbolNumber(const char *name, const char *type);

	LosSymbolRegistry(const char *name, uint num = 0, const char *type = 0, const char *property = 0);
	LosSymbolRegistry(const char *symbol, uint num, const char *type, const char *step_name, uint stnum, const char *property = 0);
	LosSymbolRegistry(const char *name, const char *step_name);
private:
	struct _string{
		const char *s;
		bool operator<(_string b)const{
			return strcmp(s,b.s)<0;
		}
		_string(const char *str=""){s = str;}
	};
	static QMap<_string, uint> types;
	static QVector<LosSymbolTypeInfo> typeInfo;

	static QMap<_string, uint> numbers;
	static QVector<LosSymbolInternalInfo> floatingInfo;
	static QVector<LosSymbolInternalInfo> names;
};

#define LOS_DECLARE_SYMBOL(symbol, num, type) static LosSymbolRegistry __los_symbol_##symbol(#symbol,num, #type);
#define LOS_DECLARE_SYMBOL_EXT(symbol, num, type, properties) static LosSymbolRegistry __los_symbol_##symbol(#symbol,num, #type,#properties);
#define LOS_DECLARE_SYMBOL_NUM(symbol, num) static LosSymbolRegistry __los_symbol_##symbol(#symbol,num);
#define LOS_DECLARE_SYMBOL_NUM_EXT(symbol, num, properties) static LosSymbolRegistry __los_symbol_##symbol(#symbol,num,0,#properties);
#define LOS_DECLARE_SYMBOL_TYPE(symbol, type) static LosSymbolRegistry __los_symbol_##symbol(#symbol,0,#type);
#define LOS_DECLARE_SYMBOL_TYPE_EXT(symbol, type, properties) static LosSymbolRegistry __los_symbol_##symbol(#symbol,0,#type,#properties);
#define LOS_DECLARE_ENUMERATIVE_OPERATOR(symbol, num, type, step_proc, stproc_num) \
	static LosSymbolRegistry __los_symbol_##symbol(#symbol, num, #type, #step_proc, stproc_num, 0);
#define LOS_DECLARE_ENUMERATIVE_OPERATOR_EXT(symbol, num, type, stproc_num, properties) \
	static LosSymbolRegistry __los_symbol_##symbol(#symbol, num, #type, #symbol "_step", stproc_num, #properties);
#define LOS_REGISTER_ENUMERATIVE_STEP(symbol, step_proc) \
	static LosSymbolRegistry __los_symbol_##symbol(#symbol, #step_proc);

#endif // LOSREGISTRY_H
