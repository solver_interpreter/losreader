#include "losregistry.h"
#include "Parsers/simple_parse.h"
#include <QDebug>
uint LosSymbolRegistry::addSymbol(const char *name, uint num, const char *type, const char *property){
	if(numbers.contains(name)){
		qDebug()<<"symbol with name"<<name<<"already registered";
		return 0;
	}else if(num && num<(uint)names.size() && names[num].internalName){
		qDebug()<<"symbol with number"<<num<<"already registered";
		return 0;
	}
	LosSymbolInternalInfo *info=0;
	if(num){
		numbers[name] = num;
		if((uint)names.size()<=num)names.resize(num+1);
		info = &(names[num]);
	}else{
		numbers[name] = num = -1-floatingInfo.size();
		floatingInfo.push_back(LosSymbolInternalInfo(num, name));
		info = &floatingInfo.back();
	}
	info->number = num;
	info->internalName = name;
	if((info->type = typeNumber(type))>0)
		typeInfo[info->type-1].symbols<<num;
	if(property){
		if(const char *res = info->properties.read(property)){
			qDebug()<<"error in addSymbol("<<name<<","<<num<<")"<<":"<<res;
			return 0;
		}
	}
	return num;
}

bool LosSymbolRegistry::setSymbolType(const char *name, const char *type){
	uint num = numbers.value(name,0);
	if(!num)return false;
	uint t = typeNumber(type), ot = names[num].type;
	if(t==ot)return true;
	if(ot)typeInfo[ot-1].symbols.removeOne(num);
	if(t)typeInfo[t-1].symbols.append(num);
	names[num].type = t;
	return true;
}

uint LosSymbolRegistry::registerType(const char *type){
	if(!type||!*type)return false;
	if(types.contains(type))return false;
	uint tp = types[type] = typeInfo.size()+1;
	typeInfo.push_back(LosSymbolTypeInfo(type, tp));
	return tp;
}

uint LosSymbolRegistry::symbolNumber(const char *name){
	return numbers.value(name, 0);
}

uint LosSymbolRegistry::typeNumber(const char *type){
	if(!type||!*type)return 0;
	uint tp = types.value(type,0);
	if(!tp){
		tp = types[type] = typeInfo.size()+1;
		typeInfo.push_back(LosSymbolTypeInfo(type, tp));
	}
	return tp;
}

const LosSymbolInternalInfo &LosSymbolRegistry::symbolInfo(int num){
	static LosSymbolInternalInfo z;
	if(!num || num>=names.size() || -1-num>=floatingInfo.size())return z;
	if(num>0)return names[num];
	return floatingInfo[-1-num];
}

LosSymbolRegistry::LosSymbolRegistry(const char *name, uint num, const char *type, const char *property){
	addSymbol(name,num,type,property);
}

LosSymbolRegistry::LosSymbolRegistry(const char *symbol, uint num, const char *type, const char *step_name, uint stnum, const char *property){
	uint n = addSymbol(symbol,num,type,property);
	uint sn = addSymbol(step_name, stnum);
	if(!n||!sn)return;
	names[n].enumerationStep = sn;
	if(names[n].type != LosSymbolProperty::Auxiliary && names[n].type != LosSymbolProperty::Enumerative){
		qDebug()<<"incorrect type specified for symbol"<<symbol;
		return;
	}else names[n].type = LosSymbolProperty::Enumerative;
}

LosSymbolRegistry::LosSymbolRegistry(const char *name, const char *step_name){
	uint n = numbers.value(name, 0);
	uint sn = numbers.value(step_name, 0);
	if(!n)qDebug()<<"registering enumerative step: symbol"<<name<<"hasn't been defined";
	if(!sn)qDebug()<<"registering enumerative step: symbol"<<step_name<<"hasn't been defined";
	if(n&&sn){
		if(names[n].type != LosSymbolProperty::Auxiliary && names[n].type != LosSymbolProperty::Enumerative){
			qDebug()<<"registering enumerative step: type specified for"<<name<<"isn't enumerative";
			return;
		}else names[n].type = LosSymbolProperty::Enumerative;
		names[n].enumerationStep = sn;
	}
}

QMap<LosSymbolRegistry::_string, uint> LosSymbolRegistry::types;
QVector<LosSymbolTypeInfo> LosSymbolRegistry::typeInfo;

QMap<LosSymbolRegistry::_string, uint> LosSymbolRegistry::numbers;
QVector<LosSymbolInternalInfo> LosSymbolRegistry::names;
QVector<LosSymbolInternalInfo> LosSymbolRegistry::floatingInfo;

const char* LosSymbolProperty::read(const char *str){
	defined = 0;
	isExpresion = 0;
	role = Auxiliary;
	strParseStream s(str);
	if(!str||!*(s<<' '))return "property string is empty";
	static char b[100];
//	bool expr = false;
	bool wasArity=false, wasExpr = false, wasType = false;
	while(*(s<<' ')){
		switch(*s){
		case ';':
		case ',':
			++s; continue;
		case 'a':
		case 'A':
			if(++s << "rity"){
				if(wasArity)return "arity already defined";
				wasArity = true;
				if(s<<" >= "){
					uint i;
					if(!(s>>i))return "cannot read arity";
					if(i >= 1<<16)return "arity is too much";
					arity = i;
					isFloating = 1;
				}else if(s.popState()<<" = "){
					if(digit(*s)){
						uint i;
						if(!(s>>i))return "cannot read arity";
						arity = i;
						if(i != arity)return "arity is too much";
						isFloating = special = 0;
					}else if(s<<" any"){ arity = 0; isFloating = 1; special = 0;}
					else if(s.popState()<<" special"){special = 1; arity = 0; isFloating = 0;}
					else return "invalid arity value";
				}else return "'=' or '>=' after 'arity' expected";
			}else return "invalid property field";
			break;
		case 'r':
		case 'R':
			if((++s).read("ole = %s", b)){
				if(wasType)return "role already defined";
				wasType = true;
				switch(*b){
				case 'V':case 'v':
					if(!strcmp(b+1, "erification"))return "invalid role specification";
				case 'E':case 'e':
					if(!strcmp(b+1, "numerative"))return "invalid role specification";
				case 'M':case 'm':
					if(!strcmp(b+1, "ixed"))return "invalid role specification";
				default:
					return "invalid role specification";
				}
			}else return "invalid field declaration";
		case 'E':case 'e':
			if((++s).read("xpression = %B", b)){
				if(wasExpr)return "isExpression already defined";
				wasExpr = true;
				isExpresion = b?1:0;
			}else return "invalid field declaration";
		default: return "invalid property field";
		}
	}

	if(wasArity){defined=1;return 0;}
	return 0;
}
