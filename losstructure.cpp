#include "losstructure.h"
#include <QFile>
#include <QMap>
#include <QDebug>
#include <QDataStream>
#include <memory.h>
bool findAllFragments(const QByteArray &f, QMap<uint, LosProgramFragment*> &frags, OutputContext c);

LosStructure::LosStructure(){
	namesMod = false;
}
const char* LosStructure::loadIndex(const QString &fn, OutputContext c){
	QFile f(fn);
	if(!f.open(QFile::ReadOnly))return "cannot open file";
	quint64 sz = f.size();
	if(sz%4){
		c.errors()<<"error in index loading: index file size = "<<sz<<"\n";
		return "invalid file size, it must be divisible by 4";
	}
	index.resize(sz/4+1);
	index[0].d = 0;
	f.read((char*)(index.data()+1), sz);
	f.close();
	return 0;
}

const char *LosStructure::readPrograms(const QString &dir, OutputContext c){
	QString fn = dir;
	if(!fn.endsWith('/'))fn+='/';
	if(const char *ret = loadIndex(fn+"K.los",c))return ret;
	QMap<uint, QVector<uint> > mp;
	uint i, n = index.size();
	for(i=0; i<n; i++){
		if(!index[i].valid)continue;
		mp[index[i].num]<<i;
	}
	qDeleteAll(programs);
	programs.resize(index.size());
	programs.fill(0);
	for(QMap<uint, QVector<uint> >::Iterator it = mp.begin(); it!=mp.end(); ++it){
		QFile f(fn+"L"+number7(it.key())+".los");
		if(!f.open(QFile::ReadOnly))c.errors()<<"error: cannot open file L"<<number7(it.key())<<".los\n";
		QByteArray ba = f.readAll();
		f.close();
		QMap<uint,LosProgramFragment*> mp;
		if(!findAllFragments(ba, mp, c)){
			c.errors()<<"error in file L"<<number7(it.key())<<".los\n";
			continue;
		}
		//qDebug()<<it.key()<<":"<<mp;
		foreach(uint j, it.value()){
			programs[j]=new LosProgramFragment;
			const char *ret = programs[j]->read(mp, ba, index[j], c);
			if(ret)c.log()<<"cannot load program for symbol "<<(j+1)<<"\n";
			else c.log()<<"program for symbol "<<(j+1)<<" loaded"<<(programs[j]->hasErrors ? " with errors\n":"\n");
		}
	}
	return 0;
}
template<class cont>
typename cont::value_type maximum(cont &c){
	if(!c.size())return cont::value_type();
	typename cont::value_type res = *c.begin();
	for(cont::iterator it = c.begin(); it!=c.end(); ++it)
		res = qMax(res, *it);
	return res;
}
bool LosStructure::readNames(const QString &dir, OutputContext c){
	QString fn = dir;
	if(!fn.endsWith('/'))fn+='/';
	uint j=0;
	names.clear();
	names.resize(index.size());
	//names.clear();
	//for(uint i=1;;i++){
	do {
		QFile f(fn + "v.los");
		if (!f.exists()) {
			c.error("cannot open file v.los");
			return false;
		}
		f.open(QFile::ReadOnly);
		quint32 len;
		if (4 != f.read((char*)&len, 4)) {
			c.error("cannot read symbol numbers from dictionary file");
			return false;
		}
		QVector<quint32> sym(len - 1);
		if (len * 4 - 4 != f.read((char*)sym.data(), len * 4 - 4)) {
			c.error("cannot read symbol numbers from dictionary file"); return false;
		}
		len -= 2;
		uint mx = maximum(sym);
		if (!mx) { c.warning("empty name index"); break; }
		if (mx >= (uint)names.size()) { c.error("symbol number out of range"); return false; }
		QVector<char> data(24 * len);
		if (24 * len != f.read((char*)data.data(), 24 * len)) {
			c.error("cannot read symbol names from dictionary file"); return false;
		}
		f.close();
		for (j = 0; j < len && data[j * 24]; j++) {
			if (!convertRusToUnicode(data.data() + j * 24, names[j+1], 24))
				c.errors() << "error : empty name of symbol " << j << "\n";
			else nameSym[names[j+1]] = j+1;
			/*			if(!convertRusToUnicode(data.data()+j*24, names[sym[j]],24))
							c.errors()<<"error : empty name of symbol "<<sym[j]<<"\n";
							else nameSym[names[sym[j]]] = sym[j];*/
		}
		//if(j<10000)break;
		//}
	} while (0);
	namesMod = false;
	initialNames = names;
	return true;
}

bool LosStructure::saveNames(const QString &dir, OutputContext c){
	QString fn = dir;
	if(!fn.endsWith('/'))fn+='/';
	uint j=0;
	//names.clear();
	QMap<QByteArray, quint16> symnums;
	for(QMap<QString,uint>::const_iterator it = nameSym.begin(); it!=nameSym.end(); ++it){
		QByteArray ba(24,0);
		if(!convertUnicodeToOld(it.key(), ba.data(), 24)){
			c.errors()<<"cannot convert name "<<it.key().toLocal8Bit().data()<<" from unicode\n";
			continue;
		}
		symnums[ba] = quint16(it.value());
	}
	QMap<QByteArray,quint16>::const_iterator it = symnums.begin();
	for(uint i=0;it!=symnums.end();i++){
		QFile f(fn+"V"+QString::number(i+1)+".los");
		f.open(QFile::WriteOnly);
		QVector<quint16> sym(10000);
		for(j=0; j<10000 && it!=symnums.end();++it, j++)
			sym[j] = it.value();

		if(20000!=f.write((char*)sym.data(),20000)){
			c.error("cannot write symbol numbers to dictionary file"); return false;}
		QVector<char> data(240000);data.fill(0);
		uint n = i*10000+1, sz = names.size();
		for(j=0; j<10000 && n+j<sz; j++){
			convertUnicodeToOld(names[n+j], data.data()+j*24, 24);
		}
		if(240000!=f.write((char*)data.data(),240000)){
			c.error("cannot write symbol names to dictionary file"); return false;}
		f.close();
	}
	namesMod = false;
	return true;
}

LosStructure::~LosStructure(){
	index.clear();
	qDeleteAll(programs);
	programs.clear();
}

bool findAllFragments(const QByteArray &f, QMap<uint, LosProgramFragment*> &frags, OutputContext c){
	int ptr = 0;
	LosProgramPrefix pr;
	frags.clear();
	for(;ptr<f.size()-7;){
		memcpy(&pr, f.data()+ptr, 7);
		if(pr.length<7|| pr.length>pr.nextFragShift){
			c.errors()<<"error: invalid prefix\n    "<<
						"length = "<<pr.length<<", next fragment at "<<pr.nextFragShift<<"\n";
			return false;
		}
		frags[ptr] = 0;
		ptr+=pr.nextFragShift;
	}
	return true;
}

QString decFunc(const QByteArray &s){
	return QString::fromLocal8Bit(s.data(), s.size());
}
#include <fstream>
bool LosStructure::loadTranslations(const QString &fn){
	//QFile f(fn);
	//if(!f.open(QFile::ReadOnly))return false;
	//f.setDecodingFunction(decFunc);
	//QByteArray ff = f.readAll();
	std::string str;
	std::fstream ds;
	ds.open(fn.toStdString().c_str(), std::ios::in);
	if(ds.fail())return false;
	//QDataStream ds(ff);
	int tn;
	tr_table.clear();
	translation.clear();
	tr_types.clear();
	for(tn=0;;tn++){
		ds>>str;
		if(ds.fail())return false;
		if(str == ";")break;
		tr_types<<QString::fromLocal8Bit(str.c_str());
	}
	while(!ds.eof()){
		QVector<QString> v;
		for(int i = 0; i<tn; i++){
			ds>>str;
			if(ds.fail()){
				if(!i && ds.eof())return true;
				else return false;
			}
			QString s = QString::fromLocal8Bit(str.c_str());
			v<<s;
			translation[s] = tr_table.size();
		}
		tr_table<<v;
	}
	return true;
}

bool LosStructure::saveTranslations(const QString &fn) const{
	if(tr_types.size()<=1)return false;
	QFile f0(fn);
	f0.open(QFile::WriteOnly);
	if(!f0.isOpen())return false;
	QDataStream f(&f0);
	foreach(const QString &str, tr_types){
		f0.write(str.toLocal8Bit());
		f0.write(QByteArray(qMax(1,25-str.length()), ' '));
	}
	f0.write(" ;\n\n");
	foreach(const QVector<QString>& row, tr_table){
		int cnt=0;
		foreach(const QString &str, row){
			if(!str.isEmpty() && str!="-")cnt++;
		}
		if(cnt>1){
			foreach(const QString &s0, row){
				f0.write(s0.toLocal8Bit());
				f0.write(QByteArray(qMax(1,25-s0.length()), ' '));
			}
			f0.write("\n");
		}
	}
	f0.close();
	return true;
}

bool LosStructure::setTranslations(const QMap<QString, QString> &tr, const QString&from, const QString &lang, bool add){
//	qDebug()<<"setTranslations";
	int l = tr_types.indexOf(lang), f = tr_types.indexOf(from);
	QSet<QString> k = tr.keys().toSet();
	if(f==-1||f==l)return false;
	if(l==-1){
		l = tr_types.size();
		for(int i = 0; i<tr_table.size(); i++){
			QVector<QString> & v= tr_table[i];
			v.resize(l+1);
			v[l] = tr.value(v[f], QString());
		}
	} else {
		for(int i = 0; i<tr_table.size(); i++){
			QVector<QString> & v= tr_table[i];
			k.remove(v[f]);
			if(!add){
				translation.remove(v[l]);
				v[l] = tr.value(v[f], QString());
			}else{
				v[l] = tr.value(v[f], v[l]);
			}
			if(!v[l].isEmpty())translation[v[l]] = i;
		}
		int len = tr_types.size(), j = tr_table.size();
		tr_table.resize(tr_table.size() + k.size());
		foreach(const QString &key, k) {
			QVector<QString> & v = tr_table[j];
			v.resize(len);
			v[f] = key;
			v[l] = tr[key];
			translation[v[f]] = translation[v[l]] = j;
			j++;
		}
	}
	return true;
}

void LosStructure::translateSymbols(const QString &lang){
//	qDebug()<<"translateSymbols";
	int l = tr_types.indexOf(lang);
	if(l<0)return;
	names = initialNames;
	nameSym.clear();
	for (int i = 0; i < names.size(); i++) {
		if (!names[i].isEmpty())
			nameSym[names[i]] = i;
	}
	for (int i = 0; i < names.size(); i++) {
		int n = translation.value(names[i], -1);
		if(n>=0 && n<tr_table.size() && !tr_table[n][l].isEmpty() && tr_table[n][l] != "-")
			renameSymbol(i, tr_table[n][l]);
	}
}

QVector<QPair<QString,QString> > LosStructure::getTranstationTable(const QString &from, const QString &to){
	int l = tr_types.indexOf(from), r = tr_types.indexOf(to), i;
	QVector<QPair<QString,QString> > res;
	for(i = 0; i<tr_table.size(); i++){
		res<<qMakePair(tr_table[i][l], tr_table[i][r]);
	}
	return res;
}

bool LosStructure::renameSymbol(uint n, const QString &s){
	if(n>=(uint)names.size())return false;
	uint i = nameSym.value(s, 0);
	if(i==n)return true;
	if(i)return false;
	nameSym.remove(names[n]);
	nameSym[s] = n;
	names[n] = s;
	namesMod = true;
	return true;
}

bool LosStructure::programToString(int i, QString &res){
	if(i<0||i>=index.size()||names[i].isEmpty())return false;
	if(!programs[i]){res = "no program associated with symbol "+names[i]; return true;}
	const QVector<LosProgItem> &f = programs[i]->commands;
	res.clear();
	for(i=0; i<f.size(); i++){
		switch(f[i].type){
		case LosProgItem::LogSymbol :
			if(names[f[i].logSymNum].isEmpty())res+=" LogSymbol("+QString::number(f[i].logSymNum)+")";
			else res+=" "+names[f[i].logSymNum];
			break;
		case LosProgItem::Procedure :
			res+=" F"+QString::number(f[i].procNum);
			break;
		case LosProgItem::Variable :
			res+=" x"+QString::number(f[i].varNum);
			break;
		default:
			res+=" <invalid command>";
		}
	}
	return true;
}
bool LosStructure::programToHString(int i, QString &res){
	if(i<0||i>=index.size()||names[i].isEmpty())return false;
	if(!programs[i]){res = "no program associated with symbol "+names[i]; return true;}
	const QVector<LosProgItem> &f = programs[i]->commands;
	res.clear();
	res+="<head> <style type=\"text/css\">"
			"a{text-decoration:none; color:#008;}"
			"a:active{color:red;}"
			"</style></head><p>";
	for(i=0; i<f.size(); i++){
		switch(f[i].type){
		case LosProgItem::LogSymbol :
			if(names[f[i].logSymNum].isEmpty()) res+=" LogSymbol("+QString::number(f[i].logSymNum)+")";
			else {
				res+=" <a href=\""+names[f[i].logSymNum]+"\">"+//"\" style = \"text-decoration:none; color:black;\">"+
						names[f[i].logSymNum]+
						"</a>";
			}
			break;
		case LosProgItem::Procedure :
			res+=" F"+QString::number(f[i].procNum);
			break;
		case LosProgItem::Variable :
			res+=" <font size=\"+1\">x<sub>"+QString::number(f[i].varNum)+"</sub></font>";
			break;
		default:
			res+=" <invalid command>";
		}
	}
	res+= "</p>";
	return true;
}
