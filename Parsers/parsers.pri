DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD
DEFINES += _CRT_SECURE_NO_WARNINGS
HEADERS += $$PWD/simple_parse.h \
		$$PWD/streamBase.h \
	common/commonfunctions.h
SOURCES += $$PWD/simple_parse.cpp \
		$$PWD/streamBase.cpp
