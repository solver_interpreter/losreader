#pragma once
#include <string.h>
#include <stdio.h>
extern char error_string[];
extern const char * error_pos;

extern const char *(*space)(const char*);
extern const char *(*space_ln)(const char*, int*);

#define _SP_ERROR(e, pos){strcpy(error_string, e); error_pos = pos; return 0;}

inline bool letter(char c){return (c>='a'&&c<='z')||(c>='A'&&c<='Z')||(c=='_');}
inline bool digit(char c){return (c>='0'&&c<='9');}
inline bool new_line(char c){return c=='\n';}
inline bool is_space(char c){return c==' '||c=='\t'||c=='\n'||c==13;}
inline int hexdigit(char c){
	if(c>='0'&&c<='9')return c-'0';
	if(c>='a'&&c<='f')return c+10-'a';
	if(c>='A'&&c<='F')return c+10-'A';
	return -1;
}

const char * space_com(const char * s);
const char * space_com_ln(const char * s, int *ln);
const char * space_custom(const char * s, const char * comment_begin, const char * nbe=0, const char *comment_end="\n", const char * nee = 0);
const char * read_word(const char * s, char * res, unsigned int max_sz=0);
const char * read_id(const char * s, char * res, unsigned int max_sz=0);

const char * read_bool(const char * s, bool &b);
template<class I> inline const char * read_uint(const char * s, I & res);
template<class I> inline const char * read_int(const char * s, I & res);
template<class R> inline const char * read_ureal(const char * s, R & res, char zpt = '.');
template<class R> inline const char * read_real(const char * s, R & res, char zpt = '.');

const char * wait_word(const char * s, const char * res);
const char * wait_element(const char * s, const char * res);
const char * wait_sequence(const char * s, const char * res, const char * e = 0, int *ln = 0);
const char * wait_char(const char * s, char c);

const char * space_cpp(const char * s);
inline const char * _space(const char * s);
const char * space_cpp_ln(const char * s, int *ln);
inline const char * _space_ln(const char * s, int *ln);

inline void set_cpp_comments(){space = &space_cpp; space_ln = &space_cpp_ln;}
inline void set_no_comments(){space = &_space; space_ln = &_space_ln;}
void set_comments(const char *b, const char *e, const char *line_com);

int remove_comments(char *s);

//   d    : int, dword
//   w    : short, word
//   b    : char, byte
//   q    : long long, qword
//   u... : unsigned ...
//   f    : float
//   r    : double
//   i    : word
//   s    : name
//   c    : char(symbol)
//   B    : bool

#define SIMPLE_PARSE_STREAM strParseStream

struct strParseStream{
	const char *prev;
	const char *str;
	char err[100];
	const char *err_pos;
	bool printErr;
	int ln, p_ln;
	const char *saveState(){if(str){prev = str; p_ln = ln;}return str;}
	//bool operator!()const{return !str;}
	operator bool()const{return str!=0;}
	strParseStream(const strParseStream& s){*this = s;}
	strParseStream& operator=(const strParseStream& s){
		str = s.str; prev = s.prev;
		if(!str){strncpy(err, s.err, 100); err_pos = s.err_pos;}
		ln = s.ln; p_ln = s.p_ln;
		printErr = s.printErr;
		return *this;
	}
	strParseStream& popState(){str = prev; ln = p_ln; prev=0; return *this;}
	strParseStream(const char *r=0){
		str = r; prev = 0;
		err[0] = err[99] = 0;
		err_pos = 0;
		ln = p_ln = 0;
		printErr = true;
	}
	strParseStream& operator << (const char *s){
		if(str)str = wait_sequence(saveState(), s, 0, &ln);
		return *this;
	}
	strParseStream& operator >> (char *s){
		if(str)str = read_word(saveState(), s);
		return *this;
	}
	strParseStream& operator >> (int &i){
		if(str)str = read_int(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (long &i){
		if(str)str = read_int(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (long long &i){
		if(str)str = read_int(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (short &i){
		if(str)str = read_int(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (unsigned int &i){
		if(str)str = read_uint(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (unsigned long &i){
		if(str)str = read_uint(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (unsigned long long &i){
		if(str)str = read_uint(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (unsigned short &i){
		if(str)str = read_uint(saveState(), i);
		return *this;
	}
	strParseStream& operator >> (long double &r){
		if(str)str = read_real(saveState(), r);
		return *this;
	}
	strParseStream& operator >> (double &r){
		if(str)str = read_real(saveState(), r);
		return *this;
	}
	strParseStream& operator >> (float &r){
		if(str)str = read_uint(saveState(), r);
		return *this;
	}
	strParseStream& operator << (char c){
		if(str)str = wait_char(saveState(),c);
		return *this;
	}
	strParseStream& operator >> (bool &b){if(str)str = read_bool(saveState(), b); return *this;}
	strParseStream getLine(char *res, size_t maxSz);
	strParseStream& skipLine();
	strParseStream& read(const char *fmt, ...);
	strParseStream& operator++(){if(str)str++; return *this;}
	strParseStream& operator+=(int i){if(str)str+=i; return *this;}
	strParseStream& error(const char* e){
		if(!prev)err_pos = str; else err_pos = prev;
		if(e)strncpy(err, e, 99);else *err=0;
		str = 0;
		return *this;
	}
	char operator[](int i){return str[i];}
	char operator*()const{return *str;}
};

//====================================================

template<class T>
strParseStream& readArray(strParseStream& str, T *arr, int sz, const char *sep = " , "){
	if(!(str<<' '>>arr[0]))return str;
	for(int i=1; i<sz; i++)
		if(!(str<<sep>>arr[i]))return str;
	return str;
}

template<class Cont> strParseStream& readListToCont(strParseStream& str, Cont &cont,
													const char *b = " {", const char *sep = " , ", const char *e = "}"){
	typename Cont::value_type T;
	if(!(str << b))return str.error("list begin expected");
	if(*e && (str <<' '<<e))return str;
	else str.popState();
	while(str<<' '>>T<<' '){
		cont.push_back(T);
		if(str << sep)continue;
		if(str.popState() << e)return str;
		else {sprintf(str.err, "'%s' or '%s' expected", sep, e); return str;}
	}
	return str.error("cannot read next list element");
}

//====================================================
const char * _space(const char * s){
	if(!s)return 0;
	while(is_space(*s))s++;
	return s;
}
const char * _space_ln(const char * s, int *ln){
	if(!s)return 0;
	while(is_space(*s))
		if(*(s++)=='\n')
			(*ln)++;
	return s;
}

inline const char * begins_with(const char *s, const char *b){
	if(!s || !b)return 0;
	while(*s && *(s++)==*(b++));
	if(*b)return 0;
	return s;
}

inline const char * read_word_sp(const char * s, char * res){
	return read_word(space(s), res);
}

template<class I>
inline const char * read_uint(const char * s, I & res){
	if(!s)return 0;
	I a;
	if(!digit(*s))_SP_ERROR("digit expected", s)
	for(a = I(*s-'0'); digit(*++s);)a = I(a*10+I(*s-'0'));
	res = a;
	return s;
}

template<class I>
inline const char * read_int(const char * s, I & res){
	if(!s)return 0;
//	s = space(s);
	int sgn = 0;
	if(*s == '-'){s++; sgn=1;}
	else if(*s == '+')s++;
	s = read_uint(s, res);
	if(s&&sgn)res = -res;
	return s;
}

template<class R>
inline void power(unsigned int p, R x, R & res){
	res = p&1 ? x : R(1);
	while(p>>=1){
		x*=x;
		if(p&1)res*=x;
	}
}

template<class R>
inline const char * read_ureal(const char * s, R & res, char zpt){
	const char * r;
	if(!s)return 0;
	R w = 0, e = 1;
	if(*s != '.'){
		if(!(r = read_uint(s, w)))_SP_ERROR("real number expected", s)
		s = r;
	}
	if(*s == zpt)
		while(digit(*(++s)))
			w += R(*s-'0')*(e*=R(0.1));
	if(*s=='e'||*s=='E'){
		int p;
		if(!(r = read_int(s+1, p)))_SP_ERROR("integer expected after '<number>e'", s)
		s = r;
		if(p>0)power(p, R(10), e);
		else power(-p, R(0.1), e);
		res = w*e;
	}else res = w;
	return s;
}

template<class R>
inline const char * read_real(const char * s, R & res, char zpt){
	if(!s)return 0;
//	s = space(s);
	int sgn = 0;
	if(*s=='-'){s++; sgn=1;}
	else if(*s=='+')s++;
	s = read_ureal(s, res, zpt);
	if(s&&sgn)res = -res;
	return s;
}
