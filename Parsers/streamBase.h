#pragma once

namespace stream_classes{
struct out_real{
	double d;
	unsigned short dat[3];
	out_real(double i, short prec, short min_sz=0, short max_sz=0){
		d=i;
		dat[0]=prec;
		dat[1]=min_sz;
		dat[2]=max_sz;
	}
};

template<class I>
struct out_int{
	I d, min_l;
	out_int(const I& i, int l){
		d=i;min_l=l;
	}
};

struct precision{
	int prec;
	precision(int p){prec = p;}
};
struct minLength{
	int l;
	minLength(int _l){l=_l;}
};
struct maxLength{
	int l;
	maxLength(int _l){l=_l;}
};
}

class OutStream{
protected:
	int min_print_len;
	int prec, def_prec;
	int max_print_len;
	bool _blocked;
protected:
	virtual void writeStr(const char *str) = 0;
	virtual void writeChar(char c){char s[2]; s[0]=c; s[1]=0; return writeStr(s);}
public:
	void setDefaultPrecision(int dp){def_prec = dp;}
	void setPrecision(int p){prec = p;}
	void setMaxPrintLength(int l){max_print_len = l;}
	void setMinPrintLength(int l){min_print_len = l;}

	OutStream(){_blocked = false; min_print_len = max_print_len = prec = 0; def_prec = 6;}
	template<class I> OutStream& write_integer(I i, int min_sz=0){
		if(isBlocked())return *this;
		if(i<0)return ((*this)<<'-').write_uint(-i, min_sz);
		return write_uint(i, min_sz);
	}
	template<class I> OutStream& write_uint(I i, int min_sz=0){
		if(isBlocked())return *this;
		if(!min_sz)min_sz = min_print_len; min_print_len=0;
		if(min_sz>100)min_sz = 100;
		char s[101], *r=s+100;
		r[0]=0;
		do{
			min_sz--;
			*--r = char(i%10)+'0';
			i/=10;
		}while(i);
		while(min_sz-- > 0)*--r = ' ';
		return (*this)<<r;
	}
	OutStream& write_real(double i, int min_sz=0, int prec=0, int max_sz=0);
	OutStream& operator<<(const char *str){writeStr(str);return *this;}
	OutStream& operator<<(int i){return write_integer(i);}
	OutStream& operator<<(unsigned int i){return write_uint(i);}
	OutStream& operator<<(long i){return write_integer(i);}
	OutStream& operator<<(unsigned long i){return write_uint(i);}
	OutStream& operator<<(long long i){return write_integer(i);}
	OutStream& operator<<(unsigned long long i){return write_uint(i);}
	OutStream& operator<<(double x){return write_real(x);}
	OutStream& operator<<(char c){writeChar(c);return *this;}
	OutStream& operator<<(stream_classes::out_real &r){return write_real(r.d, r.dat[1],r.dat[0],r.dat[2]);}
	template<class I>
	OutStream& operator<<(stream_classes::out_int<I> &r){return write_int(r.d, r.min_l);}

	OutStream& operator<<(stream_classes::precision p){setPrecision(p.prec); return *this;}
	OutStream& operator<<(stream_classes::minLength l){setMinPrintLength(l.l); return *this;}
	OutStream& operator<<(stream_classes::maxLength l){setMaxPrintLength(l.l); return *this;}

	bool isBlocked()const{return _blocked;}
};


class NoOutput : public OutStream{
protected:
	void writeStr(const char *){}
	void writeChar(char){}
public:
	NoOutput(){_blocked = true;}
};

extern NoOutput noOutput;

class OutputContext{
protected:
	OutStream *_err;
	OutStream *_warn;
	OutStream *_log;
public:
	OutputContext(){_err = _warn = _log = &noOutput;}
	OutputContext(OutStream &err, OutStream &warn, OutStream &__log){_err = &err; _warn = &warn; _log = &__log;}
	void setStreams(OutStream &err, OutStream &warn, OutStream &__log){_err = &err; _warn = &warn; _log = &__log;}
	void setErrorStream(OutStream &e){_err = &e;}
	void setWarningStream(OutStream &e){_warn = &e;}
	void setLogStream(OutStream &e){_log = &e;}
	OutStream & errors(){return (*_err);}
	OutStream & warnings(){return (*_warn);}
	OutStream & log(){return *_log;}
	void error(const char *str){(*_err)<<"error : "<<str<<"\n";}
	void warning(const char *str){(*_warn)<<"warning : "<<str<<"\n";}
	void logMessage(const char *str){(*_log)<<str<<"\n";}
};
