#include "streamBase.h"
#include <stdio.h>

NoOutput noOutput;

OutStream& OutStream::write_real(double i, int min_sz, int prec, int /*max_sz*/){
	if(isBlocked())return *this;
	char fmt[100], b[100];
	if(prec)sprintf(fmt, "%%%d.%dg", min_sz, prec);
	else sprintf(fmt, "%%%dg", min_sz);
	sprintf(b, fmt, i);
	return (*this)<<b;
}
