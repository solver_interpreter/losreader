#include "simple_parse.h"
#include "../common/commonfunctions.h"
#include <stdarg.h>

char error_string[1000];
const char * error_pos = 0;

const char *(*space)(const char*) = _space;
const char *(*space_ln)(const char*, int *ln) = _space_ln;

static const char *_b_com = 0, *_e_com = 0, *_l_com = 0;
static int _b_len = 0, _e_len = 0, _l_com_len = 0;
//static int line_num = 0;

const char *strchr_ln(const char *s, char f, int *ln){
	int n = *ln;
	for(;*s && *s!=f; s++)
		if(*s=='\n')n++;
	*ln = n;
	return s;
}

const char *strstr_ln(const char *s, const char *x, int *ln){
	char f = x[0];
	int i;
	for(;*(s = strchr_ln(s, f, ln));s++){
		for(i=1; x[i]; i++)
			if(s[i]!=x[i])break;
		if(!(x[i] && *s))return s;
		//if(!*s)return s;
	}
	return s;
}

void set_comments(const char *b, const char *e, const char *line_com){
	if(b && e && *b && *e){
		_b_len = strlen(_b_com = b);
		_e_len = strlen(_e_com = e);
	}else _b_com = _e_com = 0, _b_len = _e_len = 0;
	_l_com = line_com;
	if(_l_com)_l_com_len = strlen(_l_com);
	else _l_com_len = 0;
	space = &space_com;
	space_ln = &space_com_ln;
}

const char * space_cpp(const char * s){
	if(!s)return 0;
	const char *r;
	while(*s){
		s = _space(s);
		if(*s != '/' || (s[1] != '*' && s[1]!='/'))break;
		if(s[1]=='/'){
			if(!(r = strchr(s+2, '\n')))return s+strlen(s);
			s = r+1; continue;
		}else{ // then s[1] == '*'
			s+=2;
			for(s+=2; *s; s++)
				if(*s == '*' && s[1] == '/')break;
			if(*s)s+=2;
			else break;
		}
	}
	return s;
}
const char * space_cpp_ln(const char * s, int *ln){
	if(!s)return 0;
	const char *r;
	while(*s){
		s = _space_ln(s,ln);
		if(*s != '/' || (s[1] != '*' && s[1]!='/'))break;
		if(s[1]=='/'){
			if(!(r = strchr(s+2, '\n')))return s+strlen(s);
			s = r+1; (*ln)++; continue;
		}else{ // then s[1] == '*'
			s+=2;
			for(s+=2; *s; s++){
				if(*s == '*' && s[1] == '/')break;
				if(*s=='\n')(*ln)++;
			}
			if(*s)s+=2;
			else break;
		}
	}
	return s;
}

const char * space_com(const char * s){
	if(!s)return 0;
	const char *r;
	while(*s){
		s = _space(s);
		if((r = begins_with(s, _b_com))){
			s = strstr(r, _e_com);
			if(!s)return r+strlen(r);
			s += _e_len;
		}else if(_l_com_len && (r = begins_with(s, _l_com))){
			s = strchr(r, '\n');
			if(!s)return r+strlen(r);
			s++;
		}
	}
	return s;
}

const char * space_com_ln(const char * s, int *ln){
	if(!s)return 0;
	const char *r;
	while(*s){
		s = _space_ln(s,ln);
		if((r = begins_with(s, _b_com))){
			s = strstr_ln(r, _e_com, ln);
			if(!*s)return s;
			s += _e_len;
		}else if(_l_com_len && (r = begins_with(s, _l_com))){
			s = strchr(r, '\n');
			if(!*s)return s;
			s++; (*ln)++;
		}
	}
	return s;
}

const char * space_custom(const char * s, const char * comment_begin, const char * nbe, const char *comment_end, const char * nee){
	if(!s)return 0;
	int el = strlen(comment_end);
	while(*s){
		s = _space(s);
		int i;
		for(i=0; comment_begin[i]; i++)
			if(comment_begin[i] != s[i])break;
		if(comment_begin[i]||(nbe && strchr(nbe, s[i])))break;
		s+=i;
		const char *r=s;
		do{
			s = r;
			r = strstr(s, comment_end);
		}while(r && nee && strchr(nee, s[el]));
		if(!r)s+=strlen(s);
		else s=r;
	}
	return s;
}

int remove_comments(char *s){
	int ln = 0;
	const char *r = space(s), *s0=s, *x;
	for(;*r;r=x){
		*(s++) = *r;
		x = space_ln(r+1, &ln);
		if(ln){ln=0; *(s++) = '\n';}
		else if(x>r+1)*(s++) = ' ';
	}
	*s=0;
	return s-s0;
}

const char * read_word(const char * s, char * res, unsigned int max_sz){
	if(!s)return 0;
	if(!letter(*s))_SP_ERROR("word expected", s)
	if(res){
		for(*(res++)=*(s++); (--max_sz) && (letter(*s)||digit(*s)); s++, res++)
			*res=*s;
		*res = 0;
	}else for(; letter(*s)||digit(*s); s++);
	return s;
}

const char * read_id(const char * s, char * res, unsigned int max_sz){
	if(!s)return 0;
	s = space(s);
	if(*s!='"')return read_word(s, res, max_sz);
	do{
		for(s++; (--max_sz) && *s!='"'; s++, res++){
			switch(*s){
				case 0: _SP_ERROR("closing \" expected", s)
				case '\\': if(!*(++s))return 0;
						   else if(*s == 'n')*res = '\n';
						   else if(*s == 'r')*res = '\r';
						   else if(*s == 't')*res = '\t';
						   else if(*s == '0')*res = 0;
						   else break;
						   continue;
			}
			*res = *s;
		}
		s = space(s+1);
	}while(*s == '"');
	*res = 0;
	return s;
}

const char * wait_word(const char * s, const char * res){
	if(!s)return 0;
	s = space(s);
	while(*res)
		if(*(s++)!=*(res++)){
			sprintf(error_string, "word %s expected", res);
			error_pos = s;
			return 0;
		}
	if(letter(*s)||digit(*s))return 0;
	return s;
}

const char * wait_element(const char * s, const char * res){
	if(!s)return 0;
	s = space(s);
	while(*res)
		if(*(s++)!=*(res++))return 0;
	return s;
}

const char * wait_sequence(const char * s, const char * res, const char * e, int *ln){
	if(!s)return 0;
	if(ln)s = space_ln(s,ln);
	else s = space(s);
	while(*res){
		while(*res && !is_space(*res))
			if(*(s++) != *(res++))return 0;
		if(*res == '\t'){
			if(!is_space(*s))return 0;
			if(ln && new_line(*s))*ln++;
		}
		if(ln)s = space_ln(s,ln);
		else s = space(s);
		res = _space(res);
	}
	if(s && e && !strchr(e, *s))return 0;
	return s;
}

const char * wait_char(const char * s, char c){
	if(!s)return 0;
	s = space(s);
	if(c==' ')return s;
	if(*s!=c){
		if(c)sprintf(error_string,"symbol '%c' expected", c);
		else sprintf(error_string,"end of line expected");
		error_pos = s;
		return 0;
	}
	return s+1;
}

const char * read_bool(const char * s, bool &b){
	const char *ret=0;
	if(*s == 't' || *s=='T'){
		if((ret = wait_word(s+1, "rue")))b=true;
	}else if(*s == 'f' || *s=='F')
		if((ret = wait_word(s+1, "alse")))b=false;
	return ret;
}

//   d    : int, dword
//   w    : short, word
//   b    : char, byte
//   q    : long long, qword
//   u... : unsigned ...
//   f    : float
//   r    : double
//   i    : word
//   s    : name
//   c    : char(symbol)
//   B    : bool
//   A    : array

strParseStream strParseStream::getLine(char *res, size_t maxSz){
	if(!str)return strParseStream();
	prev = str;
	if(!*str){err_pos = str; str = 0; strcpy(err, "end of input"); return strParseStream();}
	const char *r = str ? strchr(str, '\n'):0;
	if(!r){
		int l = strlen(str);
		str += l;
		return strParseStream(strncpy(res, str, maxSz));
	}
	strncpy(res, str, Min(maxSz, size_t(r-str)));
	str = r+1; ln++;
	return res;
}

strParseStream& strParseStream::skipLine(){
	if(!str)return *this;
	prev = str;
	if(!*str){err_pos = str; str = 0; strcpy(err, "end of input"); return*this;}
	const char *r = str ? strchr(str, '\n') : 0;
	if(!r)str+=strlen(str);
	else str = r+1, ln++;
	return *this;
}

strParseStream& strParseStream::read(const char *fmt, ...){
	if(!str||!fmt)return *this;
	prev = str;
	const char *s1;
	va_list L;
    va_start(L, fmt);
	for(prev = str; *fmt; fmt++){
		switch(*fmt){
			case '\t':
				s1 = space_ln(str, &ln);
				if(s1>str){str = s1; continue;}
				err_pos = s1; str=0; break;
			case ' ':
				str = space_ln(str, &ln); continue;
			case '%':{
				++fmt;
				unsigned int max_len=-1;
				bool uns = false;
				if(digit(*fmt))fmt = read_uint(fmt, max_len);
				if(*fmt == 'u'){uns = true, ++fmt;}
				void *arg = 0;
				if(letter(*fmt))arg = va_arg(L, void*);
				err_pos = str;
				switch(*fmt){
				case 'q':
					if(!uns)str = read_int(str, *(long long*)arg);
					else str = read_uint(str, *(unsigned long long*)arg);
					if(str)continue;
					break;
				case 'd':
					if(!uns)str = read_int(str, *(int*)arg);
					else str = read_uint(str, *(unsigned int*)arg);
					if(str)continue;
					break;
				case 'w':
					if(!uns)str = read_int(str, *(short*)arg);
					else str = read_uint(str, *(unsigned short*)arg);
					if(str)continue;
					break;
				case 'b':
					if(!uns)str = read_int(str, *(char*)arg);
					else str = read_uint(str, *(unsigned char*)arg);
					if(str)continue;
					break;
				case 'B':
					if((str = read_bool(str, *(bool*)arg)))continue;
					break;
				case 'f':
					if(!uns)str = read_real(str, *(float*)arg);
					else str = read_ureal(str, *(float*)arg);
					if(str)continue;
					break;
				case 'r':
					if(!uns)str = read_real(str, *(double*)arg);
					else str = read_ureal(str, *(double*)arg);
					if(str)continue;
					break;
				case 'i':
					if((str = read_word(str, (char*)arg, max_len)))continue;
					break;
				case 's':
					if((str = read_id(str, (char*)arg, max_len)))continue;
					break;
				case ' ':
					if(str){
						const char *s0 = str;
						while(*str == ' ' || *str == '\t')
							str++;
						if(size_t(str-s0)<max_len && (int)max_len!=-1){
							err_pos = str;
							str = 0;
							strcpy(err, "space expected");
							break;
						}
						continue;
					}break;
			//	case 'A':
			//		break;
				default:
					str = 0;
					break;
				}
				break;
			}
			default:
				if(*str == *fmt){str++; continue;}
				err_pos = str;
				str=0;
				break;
		}
		break;
	}
	return *this;
}
