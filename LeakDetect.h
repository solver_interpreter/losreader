//#define DETECT_LEAKS
#ifndef LEAKDETECT_H
#define LEAKDETECT_H
#ifdef DETECT_LEAKS
#include <xlocale>
#include <xlocnum>
#include <xiosbase>
#include <QMap>
#include "Structs/Memory.h"
struct MemoryAllocInfo{
	const char *file, *function;
	int line;
	size_t size;
	bool is_vect;
	MemoryAllocInfo(size_t sz = 0, const char *fn="", const char *func="", int ln=-1, bool vec = false){
		file = fn; function = func; line = ln;
		size = sz;
		is_vect = vec;
	}
};

struct __leak_map: public QMap<void*, MemoryAllocInfo>{
	size_t total_bytes, max_bytes, report;
	__leak_map(){
		total_bytes = max_bytes = 0;
		report = 0;
	}
	void insert(void *ptr, const MemoryAllocInfo &value);
	void remove(void *ptr);

	~__leak_map();
};
extern __leak_map __leaks;
inline void *operator new(size_t sz, __leak_map &m, const char* file, const char *func, int line){
	void *res = sz ? malloc(sz) : 0;
	if(res)m.insert(res, MemoryAllocInfo(sz, file, func, line, false));
	return res;
}
inline void *operator new[](size_t sz, __leak_map &m, const char* file, const char *func, int line){
	void *res = sz ? malloc(sz) : 0;
	if(res)m.insert(res, MemoryAllocInfo(sz, file, func, line, true));
	return res;
}
inline void *operator new(size_t sz, __leak_map &, const char*, const char *, int, void *v){
	return ::operator new(sz,v);
}
inline void *operator new[](size_t sz, __leak_map &, const char*, const char *, int , void *v){
	return ::operator new[](sz,v);
}
inline void operator delete(void *vv, __leak_map &, const char*, const char *, int, void *v){
	return ::operator delete(vv,v);
}
inline void operator delete[](void *vv, __leak_map &, const char*, const char *, int, void *v){
	return ::operator delete[](vv,v);
}
inline void operator delete(void* ptr, __leak_map &m, const char*, const char *, int){
	m.remove(ptr);
	free(ptr);
}
inline void operator delete[](void* ptr, __leak_map &m, const char*, const char*, int){
	m.remove(ptr);
	free(ptr);
}
inline void operator delete(void* ptr){
	__leaks.remove(ptr);
	free(ptr);
}
inline void operator delete[](void* ptr){
	__leaks.remove(ptr);
	free(ptr);
}

#define new new(__leaks, __FILE__, __FUNCTION__, __LINE__)
#endif // DETECT_LEAKS
#endif // LEAKDETECT_H
