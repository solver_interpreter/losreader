#include "losinfoviewer.h"
#include "ui_losinfoviewer.h"
#include <QFile>
#include <QDir>

LosTerm parseTerm(int pos, const QString &str, const QMap<QString, int> &nm, QString &err) {
	QMap<QString, int> m;
	int dep = 0;
	bool wasSym;
	LosTerm res;
	for (int i = pos; i < str.size(); i++) {
		if (str[i].isSpace())continue;
		if (str[i] == '(') {
			if (!wasSym) {
				err.sprintf("unexpected '(' at position %d", i + 1); return{};
			}
			dep++;
			res.data.push_back(LosTerm::Item(LosTerm::Item::LeftP));
			wasSym = false;
		} else if (str[i] == ')') {
			if (dep <= 0) { err.sprintf("unexpected ')'"); return{}; }
			dep--;
			res.data.push_back(LosTerm::Item(LosTerm::Item::RightP));
			wasSym = false;
		} else {
			int k = i;
			while (k<str.size()&&!str[k].isSpace() && str[k] != '('&&str[k] != ')')k++;
			bool var = (str[i] == '\\');
			QString sn = var ? str.mid(i + 1, k - i - 1) : str.mid(i, k - i);
			LosTerm::Item item;
			if (var) {
				item.val = m.value(sn, m.size() + 1); m[sn] = item.val;
				item.tp = LosTerm::Item::Sym;
				wasSym = false;
			} else {
				if (sn == "_")item.val = -1;
				else if (!nm.contains(sn)) {
					err = QString("unknown symbol name `%1`").arg(sn); return{};
				} else item.val = nm[sn];
				item.tp = LosTerm::Item::Sym;
				wasSym = true;
			}
			i = k - 1;
			res.data.push_back(item);
		}
	}
	if (dep) {
		err = "')' expected in `" + str + "`"; return{};
	}
	return res;
}

QString RuleFilter::read(const QMap<QString,int> &nm, const QString &str) {
	QStringList l = str.split(';',QString::SkipEmptyParts);
	QVector<TermCond> *pv = 0;
	h.clear(); t.clear(); s.clear(); f.clear(); n.clear();
	QString rus = QString::fromUtf8("зфутнк");
	for (auto s : l) {
		if (s.size()==1&&s[0] == 'c' || s[0] == rus[5]) { hasC = true; continue; }
		if (s.size() < 3)return "invalid filter `" + s + "`";
		//QByteArray bb = s.mid(0, 2).toLocal8Bit();
		if (s[0] == 'h' || s[0] == rus[0])pv = &h;
		else if (s[0] == 'f' || s[0] == rus[1])pv = &f;
		else if (s[0] == 's' || s[0] == rus[2])pv = &this->s;
		else if (s[0] == 't' || s[0] == rus[3])pv = &t;
		else if (s[0] == 'n' || s[0] == rus[4])pv = &n;
		else return QString("unknown element name `%1` in `%2`").arg(s[0]).arg(s);

		TermCond tc;
		if (s[1] == '=') { tc.exact = tc.root = true; }
		else if (s[1] == '?') { tc.exact = false; tc.root = true; }
		else if (s[1] == ':') { tc.exact = tc.root = false; tc.subterm = false; }
		else if (s[1] == '\'') { tc.exact = tc.root = false; tc.subterm = true; }
		else return QString("unknown relation in `%1`").arg(s);

		QString err;
		tc.pattern = parseTerm(2, s, nm, err);
		if (!err.isEmpty())return err;
		pv->push_back(tc);
	}
	return"";
}
bool RuleFilter::operator()(const Rule &r)const {
	if (hasC && r.comment.isEmpty())
		return false;
	for (const TermCond &c : t)
		if (!c(r.theorem))return false;
	for (const TermCond &c : h)
		if (!c(r.header))return false;
	for (const TermCond &c : f) {
		bool found = false;
		for (const LosTerm &t : r.filters)
			if (c(t)) {
				found = true; break;
			}
		if (!found)return false;
	}
	for (const TermCond &c : s) {
		bool found = false;
		for (const LosTerm &t : r.specifiers)
			if (c(t)) {
				found = true; break;
			}
		if (!found)return false;
	}
	for (const TermCond &c : n) {
		bool found = false;
		for (const LosTerm &t : r.normalizers)
			if (c(t)) {
				found = true; break;
			}
		if (!found)return false;
	}
	return true;
}
LosInfoViewer::LosInfoViewer(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::LosInfoViewer){
	ui->setupUi(this);
	n=0;
	names=0;
	connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(onAnchor(QUrl)));
	connect(ui->forward,SIGNAL(clicked()),this,SLOT(forward()));
	connect(ui->backward,SIGNAL(clicked()),this,SLOT(backward()));
	ui->forward->setEnabled(false);
	ui->backward->setEnabled(false);
	filter = new StringFilter;
}

LosInfoViewer::~LosInfoViewer(){
	delete ui;
}

void LosInfoViewer::setNode(LosInfoNode *nn){
	if(nn==n)return;
	fb.push(n, ui->textBrowser->textCursor().position());
	n = nn;
	ui->forward->setEnabled(false);
	ui->backward->setEnabled(!fb.atStart());
	node_updated(0);
}

void LosInfoViewer::onAnchor(const QUrl &u){
	QByteArray ba = u.toString().toLocal8Bit();
	int i, j;
	if(sscanf(ba.data(), "%d%d", &i, &j)!=2)return;
	LosInfoList *l = dynamic_cast<LosInfoList*>(n);
	if(!l||i>=l->labels.size()||j>=l->labels[i].refs.size())return;
	setNode(l->labels[i].refs[j]);
}

void LosInfoViewer::forward(){
	if(fb.atEnd())return;
	fb.c++; n = fb.curr().n;
	node_updated(fb.curr().pos);
	ui->backward->setEnabled(true);
	ui->forward->setEnabled(!fb.atEnd());
}

void LosInfoViewer::backward(){
	if(fb.atStart())return;
	if(n&&fb.atEnd()){fb.push(n, ui->textBrowser->textCursor().position()); fb.c-=2;}
	else fb.c--;
	n = fb.curr().n;
	node_updated(fb.curr().pos);
	ui->forward->setEnabled(true);
	ui->backward->setEnabled(!fb.atStart());
}

void LosInfoViewer::writePriem(LosInfoNode*nn, QTextStream &stream)
{
	//QFile file("text.txt");
	//file.open(QIODevice::WriteOnly|QIODevice::Text);

	if(!nn){
		//ui->textBrowser->setHtml("<font size=\"+10\"> No information found </font>");
		return;
	}
	if(nn->is_terminal()){
		//QString str;
		//if(names)dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, *names);
		//else dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, QVector<QString>());
		//if(filter.isValid())filter->apply(str);
		//fprintf(theorem,"%s", str);

		//stream << str << endl;

		//ui->textBrowser->setHtml(str);
	}else{
		QString str;
		QString buf_str;
		LosInfoList *l = dynamic_cast<LosInfoList*>(nn);
		for(int i=0; i < l->labels.size(); i++){
			LosInfoList::Label &lb = l->labels[i];
			lb.term.toString(str, names ? *names : QVector<QString>());
			if(str == "терм"){
				str = "";
				nn = lb.refs[0];//setNode(lb.refs[0]);
				if(names)dynamic_cast<LosInfoTerminal*>(nn)->toString(str, *names);
				else dynamic_cast<LosInfoTerminal*>(nn)->toString(str, QVector<QString>());
				str[str.count()-1] = '\n';
				theorem = str;
				//stream<<str;
			}
			if(str == "оглавление")
			{
				str = "";
				//setNode(lb.refs[0]);
				nn = lb.refs[0];
				if(nn->is_terminal())
				{
					if(names)dynamic_cast<LosInfoTerminal*>(nn)->toString(str, *names);
					else dynamic_cast<LosInfoTerminal*>(nn)->toString(str, QVector<QString>());
					str[str.count()-1] = '\n';
					priem_path = str;
					priem_path.replace(";","->");
					str.replace(";","/");
					str.remove("\n");
					str.prepend("./Priem/");
					str.resize(str.size() - str.section('/', -2, -1).size() - 1);
					QDir().mkpath(str);
					str = str + "/" + priem_path.section("->",-2,-2) + ".txt";
					str.remove("\n");

					QFile file(str);
					file.open(QIODevice::Append|QIODevice::Text);
					stream.setDevice(&file);

					stream <<"Priem " <<priem_path <<"{" <<endl;

					buf_str = "Theorem: ";
					stream <<buf_str <<theorem;
					//stream <<endl;

					buf_str = "Header: ";
					stream <<buf_str <<headers[head];
					stream <<endl;
					head++;

					buf_str = "Filters: {";
					stream <<buf_str;
					for(int i = 0; i<filters.count(); i++)
					{
						stream <<filters[i] <<endl <<"\t ";
					}
					stream <<"}" <<endl;

					buf_str = "Specifiers: {";
					stream <<buf_str;
					for(int i = 0; i<pointers.count(); i++)
					{
						stream <<pointers[i] <<endl <<"\t    " ;
					}
					stream <<"}" <<endl;

					buf_str = "Normalizers: {";
					stream <<buf_str;
					for(int i = 0; i<norms.count(); i++)
					{
						stream <<norms[i] <<endl <<"\t     " ;
					}
					stream <<"}" <<endl <<"}" <<endl <<endl;

					filters.clear();
					norms.clear();
					//headers.clear();
					pointers.clear();
					file.close();
				}
			}
			if(str == "команды"){

				str = "";
				//setNode(lb.refs[0]);
				nn = lb.refs[0];
				LosInfoList *l2 = dynamic_cast<LosInfoList*>(nn);
				//lb = l->labels[0];
				for(int i = 0; i < l2->labels.count(); i++)
				{
					str = "";
					l2->labels[i].term.toString(str, names ? *names : QVector<QString>());
					headers.append(str);
				}
				//                if(l2->labels.count())
				//                    l2->labels[0].term.toString(str, names ? *names : QVector<QString>());
				//str = "Header: " + str;
				//str[str.count()-1] = '\n';
				//stream  <<str <<endl;
			}
			if(str == "команда"){
				//QVector<QString> filters,pointers,norms;
				//setNode(lb.refs[0]);
				nn = lb.refs[0];
				str = "";
				if(names)dynamic_cast<LosInfoTerminal*>(nn)->toString(str, *names);
				else dynamic_cast<LosInfoTerminal*>(nn)->toString(str, QVector<QString>());

				for(int i = 0; i<str.count(); i++)
				{
					if(str[i] != ';')
					{
						buf_str.append(str[i]);
					}
					else
					{
						buf_str.append(str[i]);
						QStringRef opr = buf_str.leftRef(7);
						QString new_str = "";
						if(opr == "условие")
						{
							int scope = 0;
							//filters.append(buf_str);
							new_str = "";
							for(int i = 8; i<buf_str.count(); i++)
							{
								if(buf_str[i] == '(')
									scope++;
								if(buf_str[i] == ')')
									scope--;
								new_str += buf_str[i];
								if(filters.count() == 0 && scope == 1)
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									filters.append(new_str+';');
									new_str = "";
									scope = 0;
								}
								else if(scope == 0 && new_str[new_str.count()-1] == ')')
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									filters.append(new_str+';');
									scope = 0;
									new_str = "";
								}
								else if(scope == 0 && buf_str[i] == ' ' && new_str != " ")
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									filters.append(new_str+';');
									scope = 0;
									new_str = "";
								}
								else if(scope == -1 && new_str[0] != ')')
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									new_str.resize(new_str.count()-1);
									filters.append(new_str+';');
									scope = 0;
									new_str = "";
								}
							}
						}
						else
						{
							opr = buf_str.leftRef(11);
							if(opr == "быстрпреобр")
								norms.append(buf_str);
							else
							{
								pointers.append(buf_str);
							}
						}
						buf_str = "";
					}
				}

				//                buf_str = filters[0];
				//                filters.clear();
				//                for(int i = 7; i < buf_str.count(); i++)
				//                {

				//                }
			}
			else{
				//setNode(l->labels[i].refs[0]);
				writePriem(l->labels[i].refs[0], stream);
			}
		}
	}
}

QString digmap(const QString &str) {
	static QMap<QString, QString> m = { { "one", "1" }, { "two", "2" }, { "three", "3" }, { "four", "4" }, { "five", "5" }, { "six", "6" }, { "seven", "7" }, { "eight", "8" }, { "nine", "9" }, { "zero", "0" }, { "", "" } };
	if (m.contains(str))return m[str];
	return ", " + str;
}

void LosInfoViewer::writePriem(int sym, LosInfoNode*nn, QMap<QString,QByteArray> &out, QString path){
	//QFile file("text.txt");
	//file.open(QIODevice::WriteOnly|QIODevice::Text);
	const int symAnd = 5, symUslovie = 183, symBistrpreobr = 370;
	if(!nn){
		//ui->textBrowser->setHtml("<font size=\"+10\"> No information found </font>");
		return;
	}
	if(nn->is_terminal()){
		//QString str;
		//if(names)dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, *names);
		//else dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, QVector<QString>());
		//if(filter.isValid())filter->apply(str);
		//fprintf(theorem,"%s", str);

		//stream << str << endl;

		//ui->textBrowser->setHtml(str);
	} else {
		QString str, str0;
		QString buf_str;
		LosInfoList *l = dynamic_cast<LosInfoList*>(nn);
		LosInfoNode *ogl = 0;
		for(int i=0; i < l->labels.size(); i++){
			LosInfoList::Label &lb = l->labels[i];
			lb.term.toString(str, names ? *names : QVector<QString>(), ", ");
			str0 = str;
			if(str == "терм"||str=="term"){
				str0=str = "";
				nn = lb.refs[0];//setNode(lb.refs[0]);
				if(names)dynamic_cast<LosInfoTerminal*>(nn)->toString(str, *names,", ");
				else dynamic_cast<LosInfoTerminal*>(nn)->toString(str, QVector<QString>(),", ");
				//str[str.count()-1] = '\n';
				theorem = str;
				//stream<<str;
			}
			if(str == "оглавление" || str == "oglavlenie")
			{
				str0 = str = "";
				//setNode(lb.refs[0]);
				ogl = lb.refs[0];
			}
			if(str == "команды" || str == "komandi"){

				str0 = str = "";
				//setNode(lb.refs[0]);
				nn = lb.refs[0];
				LosInfoList *l2 = dynamic_cast<LosInfoList*>(nn);
				//lb = l->labels[0];
				for(int i = 0; i < l2->labels.count(); i++)
				{
					str = "";
					l2->labels[i].term.toString(str, names ? *names : QVector<QString>(), ", ");
					headers.append(str);
				}
				//                if(l2->labels.count())
				//                    l2->labels[0].term.toString(str, names ? *names : QVector<QString>());
				//str = "Header: " + str;
				//str[str.count()-1] = '\n';
				//stream  <<str <<endl;
			}
			if(str == "команда" || str == "komanda"){
				//QVector<QString> filters,pointers,norms;
				//setNode(lb.refs[0]);
				nn = lb.refs[0];
				LosLogicTerminal* t = dynamic_cast<LosLogicTerminal*>(nn);
				if(!t)continue;
				str = "";
				//t->terms;
				if(names)t->toString(str, *names,", ");
				else t->toString(str, QVector<QString>(),", ");

				foreach(const LosTerm &term, t->terms){//int i = 0; i<str.count(); i++)
					/*
					if(str[i] != ';')
					{
						buf_str.append(str[i]);
					}
					else
					{*/
					//buf_str.append(str[i]);
					//QStringRef opr = buf_str.leftRef(7);
					if(term.data.empty())continue;
					QString new_str = "";
					if(term.data[0].isSymbol(symUslovie)){
						QVector<LosTerm> subterms = term.subterms(), plain;
						foreach(const LosTerm&subterm, subterms){
							if(subterm.data[0].isSymbol(symAnd))
								plain+=subterm.subterms();
							else plain.push_back(subterm);
						}
						foreach(const LosTerm&subterm, plain){
							//if(opr == "условие" || opr == "uslovie")
							//{
//							int scope = 0;
							//filters.append(buf_str);
							new_str = "";
							if(names)
								subterm.toString(new_str, *names, ",");
							else
								subterm.toString(new_str, QVector<QString>(), ",");
							filters.push_back(new_str);
							/*for(int i = 8; i<buf_str.count(); i++)
							{
								if(buf_str[i] == '(')
									scope++;
								if(buf_str[i] == ')')
									scope--;
								new_str += buf_str[i];
								if(filters.count() == 0 && scope == 1)
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									filters.append(new_str);
									new_str = "";
									scope = 0;
								}
								else if(scope == 0 && new_str[new_str.count()-1] == ')')
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									filters.append(new_str);
									scope = 0;
									new_str = "";
								}
								else if(scope == 0 && buf_str[i] == ' ' && new_str != " ")
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									filters.append(new_str);
									scope = 0;
									new_str = "";
								}
								else if(scope == -1 && new_str[0] != ')')
								{
									if(new_str[0] == ' ')
										new_str.remove(0,1);
									if(new_str[new_str.count()-1] == ' ')
										new_str.remove(new_str.count()-1,1);
									new_str.resize(new_str.count()-1);
									filters.append(new_str);
									scope = 0;
									new_str = "";
								}
							}*/
						}
					} else {
						//opr = buf_str.leftRef(11);
						if(names)term.toString(buf_str, *names, ",");
						else term.toString(buf_str, QVector<QString>(), ",");

						if(term.data[0].isSymbol(symBistrpreobr))
							norms.append(buf_str);
						else {
							pointers.append(buf_str);
						}
					}
					buf_str = "";
					//}
				}

				//                buf_str = filters[0];
				//                filters.clear();
				//                for(int i = 7; i < buf_str.count(); i++)
				//                {

				//                }
			}
			else{
				//setNode(l->labels[i].refs[0]);
				writePriem(sym, l->labels[i].refs[0], out, path + digmap(str0));
			}
		}
		if(ogl && ogl->is_terminal())
		{
			nn = ogl;
			str.clear();
			if(names)dynamic_cast<LosInfoTerminal*>(nn)->toString(str, *names);
			else dynamic_cast<LosInfoTerminal*>(nn)->toString(str, QVector<QString>());
			str[str.count()-1] = '\n';
			priem_path = str;
			priem_path.replace(";",",");
			str.replace(";","/");
			str.remove("\n");
			priem_path.remove("\n");
			str.prepend("./Priem/");
			str.resize(str.size() - str.section('/', -2, -1).size() - 1);
			//QDir().mkpath(str);
			str = str + "/" + priem_path.section(",",-2,-2) + ".txt";
			str.remove("\n");
			QString tab = "    ";
			QByteArray ba;
			QTextStream stream(&ba);

			stream << "Priem {" << endl; // <<priem_path <<"{" <<endl;
			if(names && names->size()>sym &&sym>=0){
				stream << tab << "Symbol: " << (*names)[sym] << ";" << endl;
				if (path.size())stream << tab << "Place: {" << (*names)[sym]<< "," << path << "};" << endl;
			}
			stream << tab << "Path: {" << priem_path << "};" << endl;
			stream << tab << "Theorem: " << theorem;
			stream << endl;

			stream << tab << "Header: " << headers[head] << ";";
			stream << endl;
			head++;

			stream << tab << "Filters:" << endl;
			for(int i = 0; i<filters.count(); i++)
			{
				stream << tab << tab << filters[i] << ";" << endl;
			}

			stream << tab << "Specifiers:" << endl;
			for(int i = 0; i<pointers.count(); i++)
			{
				stream << tab << tab << pointers[i] << ";" << endl;
			}

			stream <<tab<<"Normalizers:"<<endl;
			//if(norms.count())buf_str+='\n';
			for(int i = 0; i<norms.count(); i++)
			{
				stream <<"        " << norms[i] << ";" << endl;
			}
			stream <<"}" <<endl <<endl;

			filters.clear();
			norms.clear();
			//headers.clear();
			pointers.clear();
			stream.flush();
			out[str] += ba;
		}
	}
}
enum RulePart {
	RP_term,
	RP_ogl,
	RP_titr,
	RP_komandi,
	RP_komanda,
	RP_other
};
#define SymTerm       573
#define SymOglavlenie 840
#define SymKomandi    941
#define SymKomanda    981
#define SymTitr       249

//static QMap<QString, RulePart> ruleParts = { { "терм", RP_term }, { "term", RP_term }, { "оглавление", RP_ogl }, { "oglavlenie", RP_ogl }, { "команды", RP_komandi }, {"komandi", RP_komandi} };

bool operator<(const QStringList &l1, const QStringList &l2) {
	if (l1.size() != l2.size())return l1.size() < l2.size();
	for (int i = 0; i < l1.size(); i++) {
		int cmp = l1[i].compare(l2[i]);
		if (cmp)return cmp < 0;
	}
	return false;
}

void LosInfoViewer::writePriem(int sym, LosInfoNode*nn, QMap<QStringList, Rule> &out, QStringList path) {
	//QFile file("text.txt");
	//file.open(QIODevice::WriteOnly|QIODevice::Text);
	const int symAnd = 5, symUslovie = 183, symBistrpreobr = 370;
	if (!nn) {
		//ui->textBrowser->setHtml("<font size=\"+10\"> No information found </font>");
		return;
	}
	if (nn->is_terminal()) {
		//QString str;
		//if(names)dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, *names);
		//else dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, QVector<QString>());
		//if(filter.isValid())filter->apply(str);
		//fprintf(theorem,"%s", str);

		//stream << str << endl;

		//ui->textBrowser->setHtml(str);
	} else {
		Rule R;
		QString str, str0;
		QString buf_str;
		LosInfoList *l = dynamic_cast<LosInfoList*>(nn);
		LosInfoNode *ogl = 0;
		for (int i = 0; i < l->labels.size(); i++) {
			LosInfoList::Label &lb = l->labels[i];		
			str0 = "";// str;
			if (lb.term.isSymbol()) {
				switch (lb.term.data[0].val) {
				case SymTerm: {
					str0 = str = "";
					nn = lb.refs[0];//setNode(lb.refs[0]);
					if (names)dynamic_cast<LosInfoTerminal*>(nn)->toString(str, *names, ", ");
					else dynamic_cast<LosInfoTerminal*>(nn)->toString(str, QVector<QString>(), ", ");
					//str[str.count()-1] = '\n';
					if (!nn->is_logic() || static_cast<LosLogicTerminal*>(nn)->terms.isEmpty())theoremT = LosTerm(260, true);
					else theoremT = static_cast<LosLogicTerminal*>(nn)->terms[0];
					//stream<<str;
					continue;
				}
				case SymOglavlenie: {
					str0 = str = "";
					//setNode(lb.refs[0]);
					ogl = lb.refs[0];
					continue;
				}
				case SymKomandi: {
					str0 = str = "";
					//setNode(lb.refs[0]);
					nn = lb.refs[0];
					LosInfoList *l2 = dynamic_cast<LosInfoList*>(nn);
					//lb = l->labels[0];
					headerT.clear(); head = 0;
					for (int i = 0; i < l2->labels.count(); i++) {
						//str = "";
						//l2->labels[i].term.toString(str, names ? *names : QVector<QString>(), ",");
						if (l2->labels[i].term.data.size() > 1) {
							l2 = l2;
						}
						headerT.append(l2->labels[i].term);
					}
				}break;
				case SymTitr:{
					if (lb.refs.size() >= 1 && lb.refs[0]->is_terminal())
						static_cast<LosDataTerminal*>(lb.refs[0])->toString(R.comment, names ? *names : QVector<QString>(), ", ");
					continue;
				}
				case SymKomanda: {
					//QVector<QString> filters,pointers,norms;
					//setNode(lb.refs[0]);
					nn = lb.refs[0];
					LosLogicTerminal* t = dynamic_cast<LosLogicTerminal*>(nn);
					if (!t)continue;
					str = "";
					//t->terms;
					if (names)t->toString(str, *names, ", ");
					else t->toString(str, QVector<QString>(), ", ");

					foreach(const LosTerm &term, t->terms) {//int i = 0; i<str.count(); i++)
						if (term.data.empty())continue;
						QString new_str = "";
						if (term.data[0].isSymbol(symUslovie)) {
							QVector<LosTerm> subterms = term.subterms(), plain;
							foreach(const LosTerm&subterm, subterms) {
								if (subterm.data[0].isSymbol(symAnd))
									plain+=subterm.subterms();
								else plain.push_back(subterm);
							}
							R.filters+=plain;
							/*foreach(const LosTerm&subterm, plain) {
								new_str = "";
								if (names)
									subterm.toString(new_str, *names, ",");
								else
									subterm.toString(new_str, QVector<QString>(), ",");
								filters.push_back(new_str);
							}*/
						} else {
							//if (names)term.toString(buf_str, *names, ",");
							//else term.toString(buf_str, QVector<QString>(), ",");

							if (term.data[0].isSymbol(symBistrpreobr))
								R.normalizers.push_back(term);
							else {
								R.specifiers.push_back(term);
								//pointers.append(buf_str);
							}
						}
						//buf_str = "";
					}
				}break;
				default:
					lb.term.toString(str0, names ? *names : QVector<QString>(), ",");
				}
			} else {
				lb.term.toString(str0, names ? *names : QVector<QString>(), ",");
			}
			bool add = !str0.isEmpty() && (path.size()>1 || str0!="0");
			if (add)path << str0;
			writePriem(sym, l->labels[i].refs[0], out, path);
			if (add)path.pop_back();
		}
		if (ogl && ogl->is_terminal()) {
			R.sym = sym;
			R.theorem = theoremT;
			if (headerT.size() <= head)return;
			R.header = headerT[head++];
			nn = ogl;
			str.clear();
			if (names)static_cast<LosInfoTerminal*>(ogl)->toString(str, names ? *names : QVector<QString>());
			str[str.count() - 1] = '\n';
			priem_path = str;
			priem_path.replace(";", ",");
			priem_path.remove("\n");
			R.path = priem_path;

			out[path] = R;
			R.filters.clear();
		}
	}
}

void Rule::write(QTextStream &stream, const QVector<QString> *names)const {
	stream.setCodec("system");
	if (names && names->size()>sym &&sym >= 0) {
		stream << "Symbol: " << (*names)[sym] << ";" << endl;
	}
	stream << "Path: {" << path << "};" << endl;
	QString str;
	theorem.toString(str, names ? *names : QVector<QString>(),",");
	stream << "Theorem: " << str << endl;
	str.clear();
	header.toString(str, names ? *names : QVector<QString>(), ",");
	stream << "Header: " << str << ";" << endl;

	stream <<  "Filters:" << endl;
	const char *tab = "  ";
	if (!filters.empty()) {
		for (int i = 0; i < filters.count(); i++) {
			str.clear();
			filters[i].toString(str, names ? *names : QVector<QString>(), ",");
			stream << tab << str << ";" << endl;
		}
	}

	if (!specifiers.empty()) {
		stream << "Specifiers:";
		stream << endl;
		for (int i = 0; i < specifiers.count(); i++) {
			str.clear();
			specifiers[i].toString(str, names ? *names : QVector<QString>(), ",");
			stream << tab << str << ";" << endl;
		}
	}
	if (!normalizers.isEmpty()) {
		stream << "Normalizers:" << endl;
		//if(norms.count())buf_str+='\n';
		for (int i = 0; i < normalizers.count(); i++) {
			str.clear();
			normalizers[i].toString(str, names ? *names : QVector<QString>(), ",");
			stream << tab << str << ";" << endl;
		}
	}
}
void Rule::writeHTML(int num, QString &res, const QVector<QString> *names, const QString &link)const {
	//stream.setCodec("system");
	//if (names && names->size()>sym &&sym >= 0) {
	//	stream << "<B> Symbol: </B>" << (*names)[sym] << ";" << endl;
	//}
	//stream << "Path: {" << path << "};" << endl;
	QString str;
	if (names) {
		res+= QString::fromUtf8("<h3> Приём %1 ").arg(num);
		if (!link.isEmpty()) {
			res+= QString::fromUtf8("(<a href=\"%1\" style=color:blue >показать скомпилированный фрагмент на ЛОСе</a>)").arg(link);
		}
		res+= "</h3>";
	}
	if (!comment.isEmpty()) {
		res += "<blockquote><p>" + comment + "</p></blockquote>\n";
	}
	theorem.toString(str, names ? *names : QVector<QString>(), ",");
	res += QString::fromUtf8("<B>Теорема: </B>") + str + "<br>\n";
	str.clear();
	header.toString(str, names ? *names : QVector<QString>(), ",");
	res += "<B>Заголовок: </B>" + str + "<br>\n";

	if (!filters.empty()) {
		res += QString::fromUtf8("<B>Фильтры:</B>\n<ul>");
		for (int i = 0; i < filters.count(); i++) {
			str.clear();
			filters[i].toString(str, names ? *names : QVector<QString>(), ",");
			res+= "<li>" +str + "</li>\n";
		}
		res += "</ul>";
	}

	if (!specifiers.empty()) {
		res += QString::fromUtf8("<B>Указатели:</B>");
		res += "<ul>";
		for (int i = 0; i < specifiers.count(); i++) {
			str.clear();
			specifiers[i].toString(str, names ? *names : QVector<QString>(), ",");
			res += "<li>" + str + "</li>\n";
		}
		res += "</ul>\n";
	}
	if (!normalizers.isEmpty()) {
		res += QString::fromUtf8("<B>Нормализаторы:</B>\n<ul>");
		//if(norms.count())buf_str+='\n';
		for (int i = 0; i < normalizers.count(); i++) {
			str.clear();
			normalizers[i].toString(str, names ? *names : QVector<QString>(), ",");
			if (!str.isEmpty())
				res += "<li>" + str + "</li>\n";
		}
		res += "</ul>";
	}
	res += "<hr /><br>\n";
}

void LosInfoViewer::writePriem(QTextStream &stream){
	return writePriem(n, stream);
}

void LosInfoViewer::node_updated(int pos){
	if(!n){
		ui->textBrowser->setHtml("<font size=\"+10\"> No information found </font>");
	}else if(n->is_terminal()){
		QString str;
		if(names)dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, *names);
		else dynamic_cast<LosInfoTerminal*>(n)->toHtml(str, QVector<QString>());
		if(filter.isValid())filter->apply(str);
		ui->textBrowser->setHtml(str);
	}else{
		QString str, s0;
		LosInfoList *l = dynamic_cast<LosInfoList*>(n);
		s0+="<ul>";
		for(int i=0; i < l->labels.size(); i++){
			const LosInfoList::Label &lb = l->labels[i];
			lb.term.toString(str, names ? *names : QVector<QString>());
			s0+="<li>\n";
			if(lb.refs.size()==1){
				s0+="<a href=\""+QString::number(i)+" 0\">" + str+"</a>\n";
			}else{
				s0+=str+":<br>\n";
				for(int j=0; j<lb.refs.size(); j++){
					s0+="<a href=\""+QString::number(i)+" "+QString::number(j)+"\">"+QString::number(j)+"</a> \n";
				}
				s0+="\n";
			}
			s0+="</li>\n";
		}
		s0+="</ul>";
		s0.remove(QChar(0));
		ui->textBrowser->setHtml(s0);
	}
	QTextCursor c = ui->textBrowser->textCursor();
	c.setPosition(pos);
	ui->textBrowser->setTextCursor(c);
}
