#include "translit.h"
#include <QHash>
QString transliterate(const QString &str){
	static QHash<char, QString> mp;
	static int i=0;
	if(!i){
		mp['�'] = 'a';
		mp['�'] = 'b';
		mp['�'] = 'v';
		mp['�'] = 'g';
		mp['�'] = 'd';
		mp['�'] = 'e';
		mp['�'] = "yo";
		mp['�'] = "zh";
		mp['�'] = "z";
		mp['�'] = "i";
		mp['�'] = "j";
		mp['�'] = 'k';
		mp['�'] = 'l';
		mp['�'] = 'm';
		mp['�'] = 'n';
		mp['�'] = 'o';
		mp['�'] = 'p';
		mp['�'] = 'r';
		mp['�'] = 's';
		mp['�'] = 't';
		mp['�'] = 'u';
		mp['�'] = 'f';
		mp['�'] = "kh";
		mp['�'] = "c";
		mp['�'] = "ch";
		mp['�'] = "sh";
		mp['�'] = "sch";
		mp['�'] = "j";
		mp['�'] = "i";
		mp['�'] = "";
		mp['�'] = "e";
		mp['�'] = "yu";
		mp['�'] = "ya";

		mp['�'] = 'A';
		mp['�'] = 'B';
		mp['�'] = 'V';
		mp['�'] = 'G';
		mp['�'] = 'D';
		mp['�'] = 'E';
		mp['�'] = "Yo";
		mp['�'] = "Zh";
		mp['�'] = "Z";
		mp['�'] = "I";
		mp['�'] = "J";
		mp['�'] = 'K';
		mp['�'] = 'L';
		mp['�'] = 'M';
		mp['�'] = 'N';
		mp['�'] = 'O';
		mp['�'] = 'P';
		mp['�'] = 'R';
		mp['�'] = 'S';
		mp['�'] = 'T';
		mp['�'] = 'U';
		mp['�'] = 'F';
		mp['�'] = "Kh";
		mp['�'] = "C";
		mp['�'] = "Ch";
		mp['�'] = "Sh";
		mp['�'] = "Sch";
		mp['�'] = "J";
		mp['�'] = "I";
		mp['�'] = "";
		mp['�'] = "E";
		mp['�'] = "Yu";
		mp['�'] = "Ya";
		i = 1;
	}
	QByteArray bs = str.toLocal8Bit();
	QString s1;
	for(int i=0; i<str.size(); i++){
		s1 += mp.value(bs[i], str[i]);
	}
	return s1;
}
