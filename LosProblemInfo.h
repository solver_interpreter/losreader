#ifndef LOSPROBLEMINFO_H
#define LOSPROBLEMINFO_H
#include "losinfoblock.h"
QString convertProblemNode(LosInfoNode *p, const SymbolNames & nm, QVector<int> &path);

#endif // LOSPROBLEMINFO_H
