#pragma once
namespace _TempSpace{
class _RefBase;
}

class ReferencedBase{
	friend class _TempSpace::_RefBase;
	int _used;
	void _inc_used(){if(this)++_used;}
	void _dec_used(){
		if(this && !--_used)
			delete this;
	}
protected:
	ReferencedBase(){_used = 0;}
//public:
	virtual ~ReferencedBase(){}
};

namespace _TempSpace{
class _RefBase{
protected:
	_RefBase(){}
	_RefBase(ReferencedBase *x){x->_inc_used();}
	void _inc(ReferencedBase *x){x->_inc_used();}
	void _dec(ReferencedBase *x){x->_dec_used();}
};
}
template<class T>
class ClassConstRef : protected _TempSpace::_RefBase{
	typedef _TempSpace::_RefBase _base;
protected:
	T *ref;
public:
	ClassConstRef(T*x=0):_base(x){ref=x;}
	ClassConstRef(const ClassConstRef<T>& r):_base((T*)r.ref){ref = (T*)r.ref;}
	ClassConstRef<T>& operator=(const ClassConstRef<T>& r){
		if(ref!=r.ref){
			this->_dec(ref);
			this->_inc(ref = (T*)r.ref);
		}
		return *this;
	}
	const T *operator->()const{return ref;}
	const T* pointer()const{return ref;}
	const T& operator*()const{return *ref;}
	const T& reference()const{return *ref;}
	bool operator ==(const ClassConstRef<T>& r)const{return ref == r.ref;}
	bool operator !=(const ClassConstRef<T>& r)const{return ref != r.ref;}
	bool isValid()const{return ref!=0;}
	bool operator!(){return !ref;}
	~ClassConstRef(){this->_dec(ref);}
};

template<class T>
class ClassRef:public ClassConstRef<T>{
public:
	ClassRef(T*x=0):ClassConstRef<T>(x){}
	ClassRef(const ClassRef<T>& r):ClassConstRef<T>(r){}
	ClassRef<T>& operator=(const ClassRef<T>& r){
		ClassConstRef<T>::operator =(r);
		return *this;
	}
	T* operator->()const{return (T*)this->ref;}
	T* pointer()const{return (T*)this->ref;}
	T& operator*()const{return *(T*)this->ref;}
	T& reference()const{return *(T*)this->ref;}
	~ClassRef(){}
};

#define DECLARE_CLASS_REFS(class_name)\
	typedef ClassConstRef<class_name> class_name##ConstRef; \
	typedef ClassRef<class_name> class_name##Ref;

#define DECLARE_FRIEND_REFS(class_name)\
	friend class ClassConstRef<class_name>; \
	friend class ClassRef<class_name>;
