#pragma once

#include "commonfunctions.h"

struct defaultLessThen{
	defaultLessThen(){}
	template<class T>
	bool operator()(const T&x, const T&y)const{return x<y;}
};

template<class lessThan = defaultLessThen>
struct defaultCompare{
	lessThan lt;
	defaultCompare(){}
	defaultCompare(const lessThan & _lt):lt(_lt){}
	template<class T>
	int compare(const T&x, const T&y){
		return lt(x,y) ? -1 : lt(y,x) ? 1 : 0;
	}
};

template<>
struct defaultCompare<defaultLessThen>{
	defaultCompare(){}
	defaultCompare(const defaultLessThen&){}
	template<class T>
	int compare(const T&x, const T&y)const{
		return x < y ? -1 : y < x ? 1 : 0;
	}
};

template<class It, class LT>
void quickSort(It begin, It end, const LT& lt){//qSort from qalgorithms.h to use in other projects
	for(;;){
		int span = int(end - begin);
		if (span < 2)
			break;

		--end;
		It low = begin, high = end - 1;
		It pivot = begin + span / 2;

		if (lt(*end, *begin))
			Exchange(*end, *begin);
		if (span == 2)
			break;

		if (lt(*pivot, *begin))
			Exchange(*pivot, *begin);
		if (lt(*end, *pivot))
			Exchange(*end, *pivot);
		if (span == 3)
			return;

		Exchange(*pivot, *end);

		while (low < high){
			while (low < high && lt(*low, *end))
				++low;

			while (high > low && lt(*end, *high))
				--high;

			if (low < high) {
				Exchange(*low, *high);
				++low;
				--high;
			} else break;
		}

		if (lt(*low, *end))
			++low;

		Exchange(*end, *low);
		quickSort(begin, low, lt);

		begin = low + 1;
		++end;
	}
}

template<class It>
void quickSort(It begin, It end){
	quickSort(begin, end, defaultLessThen());
}

template<class Cont, class Cmp>
void quickSortCont(Cont &c, const Cmp& cmp){
	quickSort(c.begin(), c.end(), cmp);
}
template<class Cont>
void quickSortCont(Cont &c){
	quickSort(c.begin(), c.end(), defaultLessThen());
}

template<class T, class It, class Cmp>
int binFindInterval(const T& x, It beg, It end, Cmp cmp){//result in [0; end-beg]
	It m, b0 = beg;
	int n = end - beg;
	if(n<0)return -n-binFindInterval(x, end+1, beg+1, cmp);
	while(n>0){
		m = beg+n/2;
		int c = cmp.compare(x, *m);
		if(c<0)end = m;
		else if(c>0)beg = m+1;
		else return m-b0;
		n = end-beg;
	}
	return end-b0;
}

template<class T, class It>
int binFindInterval(const T& x, It beg, It end){
	return binFindInterval(x, beg, end, defaultCompare<>());
}
template<class T, class Cont, class Cmp>
int binFindInterval(const T& x, Cont &c, const Cmp & cmp){
	return binFindInterval(x, c.begin(), c.end(), cmp);
}
template<class T, class Cont>
int binFindInterval(const T& x, Cont &c){
	return binFindInterval(x, c.begin(), c.end(), defaultCompare<>());
}
