#pragma once
#include <stdlib.h>

#ifndef D_CHECK
#ifdef _DEBUG
#define D_CHECK(expr, except) {if(!(expr))throw except;}
#else
#define D_CHECK(expr, except) {}
#endif
#endif

#ifndef IF_DEBUG_DO
#ifdef _DEBUG
#define IF_DEBUG_DO(commands) {commands}
#else
#define IF_DEBUG_DO(commands) {}
#endif
#endif

#ifdef D_PRINT
#ifdef _DEBUG_PRINT
#include <stdio.h>
#define D_PRINT(commands) {commands}
#else
#define D_PRINT(commands) {}
#endif
#endif

#ifndef STATIC_ARR_SIZE
#define STATIC_ARR_SIZE(x) (sizeof(x)/sizeof(*(x)))
#endif

#ifndef MAX_MIN_ABS_SWAP
#define MAX_MIN_ABS_SWAP
template<class T> T& pMin(T &a, T &b){return a>b ? b : a;}
template<class T> T& pMax(T &a, T &b){return a<b ? b : a;}
template<class T> const T& pcMin(const T &a, const T &b){return a>b ? b : a;}
template<class T> const T& pcMax(const T &a, const T &b){return a<b ? b : a;}
template<class T> T Min(T a, T b){return a>b ? b : a;}
template<class T> T Max(T a, T b){return a<b ? b : a;}
template<class T> T Sqr(T x){return x*x;}
template<class T> T Cube(T x){return x*x*x;}
template<class T> T Power(T x, int N){
	if(N<0)return T(1)/Power(x, -N);
	T res = 1;
	while(N){
		if(N&1)res*=x;
		x*=x;
		N>>=1;
	}
	return res;
}
template<class T> T Abs(const T &a){return a<0 ? -a : a;}
template<class T> void Exchange(T&a, T&b){T t=a; a=b; b=t;}

#endif

#define REAL_INFINITY 1e301
#define IS_INFINITY(x) (Abs(x) > 1e300)
#define IS_POSITIVE_INFINITY(x) ((x) > 1e300)
#define IS_NEGATIVE_INFINITY(x) ((x) < -1e300)
template<class T> void Move(T&a, T&b){a = b;}

#define _CONT_DECL_OP_FUNC(name, op) \
template<class iter1, class iter2> \
void name(iter1 i1, iter2 i2, int sz){ \
	for(int i=0; i<sz; i++, ++i1, ++i2) \
		*i1 op *i2; \
}

_CONT_DECL_OP_FUNC(contAdd, +=)
_CONT_DECL_OP_FUNC(contSub, -=)
_CONT_DECL_OP_FUNC(contMul, *=)
_CONT_DECL_OP_FUNC(contDiv, /=)
_CONT_DECL_OP_FUNC(contShl, <<=)
_CONT_DECL_OP_FUNC(contShr, >>=)
_CONT_DECL_OP_FUNC(contAnd, &=)
_CONT_DECL_OP_FUNC(contOr, |=)
_CONT_DECL_OP_FUNC(contXor, ^=)
_CONT_DECL_OP_FUNC(contMod, %=)

#define _CONT_DECL_VECTOR_OPERATOR_IN(cont, op)\
template<class T, class cont2> \
cont<T> & operator op(cont<T>& c, const cont2 &b){ \
	typename cont<T>::iterator it = c.begin();\
	typename cont2::const_iterator i1 = b.begin();\
	for(;it!=c.end()&&i1!=b.end();++it, ++i1)\
		 *it op *i1;\
	return c;}

#define _CONT_DECL_VECTOR_OPERATOR_UN(cont, op)\
template<class T, class cont2> \
cont<T> operator op(const cont<T>& c){ \
	cont<T> res(c);\\
	typename cont<T>::iterator it = res.begin();\
	for(;it!=res.end();++it)*it = op *it;\
	return res;}

#define _CONT_DECL_VECTOR_OPERATOR(cont, op)\
template<class T, class cont2> \
cont<typeof(T() op *cont2::begin())> operator op(const cont<T>& c, const cont2 &b){ \
	cont<T> res(c);\
	res op##= b; return res;}

#define DECL_CONT_OPERATORS_IN(cont) \
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, +=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, -=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, *=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, /=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, %=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, |=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, &=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, ^=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, <<=)\
	_CONT_DECL_VECTOR_OPERATOR_IN(cont, >>=)

#define DECL_CONT_BINARY_OPERATORS(cont) \
	_CONT_DECL_VECTOR_OPERATOR(cont, +)\
	_CONT_DECL_VECTOR_OPERATOR(cont, -)\
	_CONT_DECL_VECTOR_OPERATOR(cont, *)\
	_CONT_DECL_VECTOR_OPERATOR(cont, /)\
	_CONT_DECL_VECTOR_OPERATOR(cont, %)\
	_CONT_DECL_VECTOR_OPERATOR(cont, |)\
	_CONT_DECL_VECTOR_OPERATOR(cont, &)\
	_CONT_DECL_VECTOR_OPERATOR(cont, ^)\
	_CONT_DECL_VECTOR_OPERATOR(cont, <<)\
	_CONT_DECL_VECTOR_OPERATOR(cont, >>)

#define DECL_CONT_UNARY_OPERATORS(cont)\
	_CONT_DECL_VECTOR_OPERATOR_UN(cont, -)\
	_CONT_DECL_VECTOR_OPERATOR_UN(cont, +)\
	_CONT_DECL_VECTOR_OPERATOR_UN(cont, ~)

#define _DECL_TEMPL_SCALAR_PRODUCT(cont) \
	template<class T, class cont2> T operator()(const cont<T> &a, const cont2 &b){\
	T res(0);\
	typename cont<T>::iterator it = c.begin();\
	typename cont2::const_iterator i1 = b.begin();\
	for(;it!=c.end()&&i1!=b.end();++it, ++i1)\
		 res += (*it) * (*i1);\
	return res;}

#define _DECL_SCALAR_PRODUCT(cont, T) \
	template<class cont2> T operator()(const cont<T> &a, const cont2 &b){\
	T res(0);\
	typename cont<T>::iterator it = c.begin();\
	typename cont2::const_iterator i1 = b.begin();\
	for(;it!=c.end()&&i1!=b.end();++it, ++i1)\
		 res += (*it) * (*i1);\
	return res;}

#define _DECL_INLINE_SCALAR_PRODUCT(cont, T) \
	inline T operator,(const cont &a, const cont &b){\
	T res(0);\
	typename cont::const_iterator it = c.begin(), i1 = b.begin();\
	for(;it!=c.end()&&i1!=b.end();++it, ++i1)\
		 res += (*it) * (*i1);\
	return res;}
/*
template<class cont>
struct _forEachIterator{
	const cont *c;
	typedef typename cont::const_iterator const_iterator;
	const_iterator it;
	_forEachIterator(const cont& C){c=&C;it = c->begin();}
	template<class Y>
	bool get(Y &res){
		if(it==c->end())return false;
		res = *it;
		++it;
	}
};*/

//#define ForEachInContainer(x, cont) for(typename cont::const_iterator it = cont.begin(); it != cont.end(); ++it)

namespace mathConsts{
static const double Pi = 3.14159265358979323846;
static const double Pi_2 = 1.57079632679489661923;
static const double Pi_4 = 0.78539816339744830962;
static const double E = 2.7182818284590452354;
static const double log2E = 1.4426950408889634074;
static const double log10E = 0.43429448190325182765;
static const double ln2	= 0.69314718055994530942;
static const double ln10 = 2.30258509299404568402;
static const double _1_Pi = 0.31830988618379067154;
static const double _2_Pi = 0.63661977236758134308;
static const double _2_sqrtPi = 1.12837916709551257390;
static const double _sqrt2 = 1.41421356237309504880;
static const double _sqrt1_2 = 0.70710678118654752440;
}
/*
template<class T1>
class MaxType{
	template<class T2>
	typedef void type;
};
template<>
class MaxType<>{
	typedef void type;
};
*/

template<class T,class T1,class T2>
T RangeV(const T &x, const T1 &min, const T2 &max){
	if(x<T(min))return T(min);
	else if(x>T(max))return T(max);
	return x;
}

template<class T>
T Range(const T& x, const T& min, const T& max){
	if(x<min)return min;
	else if(x>max)return max;
	return x;
}

template<class It1, class It2>
void MemcpyI(It1 dst, It2 src, size_t sz){
	for(size_t i=0; i<sz; i++, ++src, ++dst)
		*dst = *src;
}

template<class It1, class It2>
void MemmoveI(It1 dst, It2 src, size_t sz){
	for(size_t i=0; i<sz; i++, ++src, ++dst)
		Move(*dst, *src);
}

template<class It1, class It2>
void MemswapI(It1 dst, It2 src, size_t sz){
	for(size_t i=0; i<sz; i++, ++src, ++dst)
		Exchange(*dst, *src);
}
template<class It, class T>
void MemsetI(It dst, const T &val, size_t sz){
	for(size_t i=0; i<sz; i++, ++dst)
		*dst = (T)val;
	return dst;
}
template<class It1, class Func>
void ApplyF1(Func f, It1 it, size_t sz){
	for(size_t i=0; i<sz; i++, ++it)
		f(*it);
}
template<class It1, class It2, class Func>
void ApplyF2(Func f, It1 it1, It2 it2, size_t sz){
	for(size_t i=0; i<sz; i++, ++it1, ++it2)
		f(*it1, *it2);
}
template<class It1, class Func>
void ApplyMap(Func f, It1 it, size_t sz){
	for(size_t i=0; i<sz; i++, ++it)
		*it = f(*it);
}

template<class T, class Y>
T *Memcpy(T *dst, const Y *src, size_t sz){
	if(!dst)return 0;
	if((void*)dst!=(void*)src)
		for(size_t i=0; i<sz; i++)
			dst[i] = (T)(src[i]);
	return dst;
}

template<class T>
T *Memmove(T *dst, T *src, size_t sz){
	if(!dst)return 0;
	if((void*)dst!=(void*)src)
		for(size_t i=0; i<sz; i++)
			Move(dst[i], src[i]);
	return dst;
}

template<class T>
T *Memswap(T *dst, T *src, size_t sz){
	if(!dst)return 0;
	if((void*)dst!=(void*)src)
		for(size_t i=0; i<sz; i++)
			Exchange(dst[i], src[i]);
	return dst;
}

template<class T>
T *Memset(T *dst, const T &val, size_t sz){
	if(!dst)return 0;
	for(size_t i=0; i<sz; i++)
		dst[i] = val;
	return dst;
}

template<class T>
T *reverseArray(T *ptr, size_t sz){
	if(ptr && sz)
		for(size_t i=0, j=sz-1; i<j; i++,j--)
			Exchange(ptr[i], ptr[j]);
	return ptr;
}

template<class T>
T *rotateArray(T *ptr, unsigned int sz, int rot){
	if(!ptr || !sz || !rot)return ptr;
	rot = sz*(rot/sz)-rot;
	if(rot<0)rot+=sz;
	if(!rot)return ptr;
	T t;
	size_t sum=0, i, j, k;
	for(i=0; sum < sz; i++){
		Move(t, ptr[i]);
		for(k=i, j = i+rot; j!=i; k=j, j = (j+rot) % sz, sum++)
			Move(ptr[k],ptr[j]);
		Move(ptr[k], t);
	}
	return ptr;
}

template<class T, class Y>
T *copyRotated(T *dest, const Y *src, size_t sz, size_t from){
	if(!dest)return 0;
	if(!sz)return dest;
	if((void*)dest==(void*)src)return rotateArray(dest, (int)sz, (int)from);
	Memcpy(dest, src+from, sz-from);
	return Memcpy(dest+sz-from, src, from);
}

template<class T, class Sz>
Sz firstMinIndex(const T* ptr, Sz sz){
	if(!sz)return -1;
	int imin = 0;
	for(Sz i=1; i < sz; i++)
		if(ptr[i]<ptr[imin])
			imin = i;
	return imin;
}
template<class T, class Sz>
Sz lastMinIndex(const T* ptr, Sz sz){
	if(!sz)return -1;
	int imin = 0;
	for(Sz i=1; i < sz; i++)
		if(ptr[i]<=ptr[imin])
			imin = i;
	return imin;
}
template<class T, class Sz>
Sz firstMaxIndex(const T* ptr, Sz sz){
	if(!sz)return -1;
	int imin = 0;
	for(Sz i=1; i < sz; i++)
		if(ptr[imin]<ptr[i])
			imin = i;
	return imin;
}
template<class T, class Sz>
Sz lastMaxIndex(const T* ptr, Sz sz){
	if(!sz)return -1;
	int imin = 0;
	for(Sz i=1; i < sz; i++)
		if(ptr[imin]<=ptr[i])
			imin = i;
	return imin;
}

template<class T, class Y>
int lexCompare(const T *a, const Y *b, size_t sz){
	for(size_t i=0; i<sz; i++){
		if(a[i]<b[i])return -1;
		else if(b[i]<a[i])return 1;
	}
	return 0;
}
template<class T, class Y>
int lexCompare(const T *a, size_t sz1, const Y *b, size_t sz2){
	int sz = Min(sz1, sz2);
	for(size_t i=0; i<sz; i++){
		if(a[i]<b[i])return -1;
		else if(b[i]<a[i])return 1;
	}
	if(sz1<sz2)return -1;
	if(sz1>sz2)return 1;
	return 0;
}

template<class T, class Y, class Comparator>
int lexCompare(const T *a, const Y *b, size_t sz, const Comparator &cmp){
	for(size_t i=0; i<sz; i++){
		int res = cmp.compare(a[i],b[i]);
		if(res)return res;
	}
	return 0;
}
