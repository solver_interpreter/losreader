#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QCloseEvent>
#include <QMessageBox>
#include <QLabel>
#include <QCompleter>
#include "Parsers/streamBase.h"
#include <QDebug>
#include <QHash>
#include <QTextBlock>
#include <QTextDocumentFragment>
#include <QProgressBar>
#include <QDateTime>
#include <QToolTip>
#include <QTextLayout>
#include <QTimer>
#include <QPrinter>
#include <QFile>
#include <QSettings>
#include "loshighlighter.h"
#include "unicode.h"
#include "translit.h"
#include "LosProblemInfo.h"

struct SymbolReplace{
	static QHash<QString, int> bssym, sym;
	SymbolReplace(){
		bssym["le"] = Unicode::LessOrEqual;
		bssym["ge"] = Unicode::GreaterOrEqual;
		bssym["ne"] = Unicode::NotEqual;
		bssym["splus"] = Unicode::DotPlus;
		bssym["sminus"] = Unicode::DotMinus;
		bssym["smul"] = Unicode::AsteriskOperator;
		bssym["setminus"] = Unicode::SetMinus;
		bssym["union"] = Unicode::Union;
		bssym["u"] = Unicode::Union;
		bssym["lunion"] = Unicode::DotUnion;
		bssym["in"] = Unicode::ElementOf;
		bssym["notin"] = Unicode::NotElementOf;
		bssym["subset"] = Unicode::Subset;
		bssym["sublist"] = Unicode::DotSubset;
		bssym["intersection"] = Unicode::Intersection;
		bssym["lintersection"] = Unicode::DotIntersection;
		bssym["or"] = Unicode::LogicOr;
		bssym["and"] = Unicode::LogicAnd;
		bssym["not"] = Unicode::NotSign;
		bssym["forall"] = Unicode::Forall;
		bssym["exists"] = Unicode::Exists;
		bssym["notexist"] = Unicode::DoesntExist;
	}
};
QHash<QString, int> SymbolReplace::bssym;
QHash<QString, int> SymbolReplace::sym;
static SymbolReplace sr;

class FileOut:public OutStream{
	QFile f;
	void writeStr(const char *str){
		if(!f.isOpen())f.open(QFile::WriteOnly);
		f.write(str);
	}
	void writeChar(char c){
		if(!f.isOpen())f.open(QFile::WriteOnly);
		f.write(&c,1);
	}
public:
	FileOut(const QString &fn){
		f.setFileName(fn);
	}
};

bool isOpenP(QChar c){
	return c=='('||c=='{'||c=='[';
}
bool isCloseP(QChar c){
	return c==')'||c=='}'||c==']';
}

int indent_count(const QString &s, int l = -1){
	if(l<0)l=s.length();
	int i;
	for(i=0; i<l; i++)
		if(s[i]!='\t')break;
	for(int j=i; j<l; j++){
//		if(isOpenP(s[i]))i++;
//		else if(isCloseP(s[i]))i--;
//		else if
		switch(s[j].unicode()){
		case '(': case '[': case '{': i++; break;
		case ')': case ']': case '}': i--; break;
		case '/': if(j<l-1 && s[j+1].unicode()=='/')return qMax(i,0);
		}
	}
	return qMax(i,0);
}

class dclfilter:public QObject{
//	Q_OBJECT
	MainWindow *w;
	QTextEdit *br;
	QTextCursor c;
	QPoint tpos;
	QString tt;
	QTimer tm;
	int ttdelay;
	bool helpOnly = false;
/*private slots:
	void onTimer(){
		QToolTip::showText(tpos, tt, br);
	}*/

protected:
/*	void showT(QPoint pos, const QString &t){
		tt = t;
		tpos = pos;
		if(!t.isEmpty())
			tm.start(ttdelay);
	}
	void hideT(){
		tm.stop();
		QToolTip::hideText();
	}*/

	bool eventFilter(QObject *, QEvent *e){
		if(e->type()==QEvent::KeyPress){
			if(QKeyEvent* m = (QKeyEvent*)e){
//				qDebug()<<(void*)m->key();
				if (m->modifiers() == Qt::NoModifier && m->key() == Qt::Key_F1) {
					w->onHelp(br);
				} else if (!helpOnly) {
					if (m->modifiers() == Qt::NoModifier && m->key() == Qt::Key_F2) {
						w->onTextDblClk();
					} else {
						switch (m->key()) {
						case Qt::Key_Space:{
							QTextCursor c = br->textCursor();//.block();
							//					int pos = c.positionInBlock();
							c.select(QTextCursor::WordUnderCursor);
							QString str = c.selectedText();
							int p1 = str.lastIndexOf('\\');
							if (p1 < 0)return false;
							str.remove(0, p1 + 1);
							if (int s = sr.bssym.value(str, 0)) {
								//QTextDocumentFragment sel = c.selection();
								c = br->textCursor();
								c.clearSelection();
								for (int j = 0; j <= str.length(); j++)
									c.deletePreviousChar();
								c.insertText(QString(QChar(s)));
								return true;
							}
							break;
						}
						case Qt::Key_Enter:
						case Qt::Key_Return:{
							//						qDebug()<<"enter";
							int i, pb, i0;
							QTextCursor c = br->textCursor();
							if (c.isNull())return false;
							c.beginEditBlock();
							if (c.hasSelection())
								c.removeSelectedText();
							pb = c.positionInBlock();
							QString b = c.block().text();
							i = indent_count(b, pb);
							for (i0 = pb; i0 < b.length(); i0++)
								if (!b[i0].isSpace())break;
							const char *inst = "\n";
							int k;
							for (k = 0; k < b.length(); k++)
								if (!b[k].isSpace())break;
							if (k < b.length() && isCloseP(b[k])) inst = "\t\n", i++;
							if (i0<b.length() && isCloseP(b[i0])) {
								i--;
							}
							if (i0>i)
								c.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor, i0 - pb);
							//						c.removeSelectedText();
							//						qDebug()<<i<<qMax(brn+i, 0);
							c.insertText(inst + QString(qMax(i, 0), '\t'));
							//c.setPosition(c.position()+qMax(brn+i, 0)+1);
							c.endEditBlock();
							//	m->accept();
							return true;
						}
						case Qt::Key_BraceRight:
						case Qt::Key_BracketRight:
						case Qt::Key_ParenRight:{
							//					qDebug()<<"par";
							QTextCursor c = br->textCursor();
							if (c.isNull())return false;
							int p = c.positionInBlock(), j;
							QString s = c.block().text();
							for (j = 0; j < p; j++)
								if (!s[j].isSpace())return false;
							//				qDebug()<<"1";
							int bn = c.block().blockNumber();
							if (!bn)return false;
							int i = indent_count(c.block().previous().text());
							//						qDeb
							c.beginEditBlock();
							c.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, j);
							c.removeSelectedText();
							c.insertText(QString(i - 1, '\t') + char(m->key()));
							c.endEditBlock();
							return true;
						}

						}
					}
				}
			}
		}/*else if(e->type() == QEvent::MouseMove){
			QMouseEvent *me = (QMouseEvent*)e;
			if(me->buttons() || me->modifiers())return false;
			QTextCursor c1 = br->cursorForPosition(me->pos());
			if(c1==c)return false;
			int c1pos = c1.positionInBlock();
			QTextLayout *tl = c1.block().layout();
			QList<QTextLayout::FormatRange> lr = tl->additionalFormats();
			bool found = false;
			for(QList<QTextLayout::FormatRange>::Iterator it = lr.begin(); it!=lr.end(); ++it){
				if(it->start<=c1pos && it->start + it->length > c1pos){
					found = true;
					if(it->format.toolTip().isEmpty()){
						hideT();
	//					qDebug()<<"hide tooltip";
					}else {
						showT(me->globalPos(), it->format.toolTip());
	//					qDebug()<<"show tooltip"<<it->format.toolTip();
					}
					break;
				}
			}
			if(!found){
				hideT();//QToolTip::hideText();
//				qDebug()<<"hide tooltip";
			}
			c = c1;
		}else if(e->type() == QEvent::Leave){
			QToolTip::hideText();
			c = QTextCursor();
		}*/
		return false;
	}
public:
	dclfilter(MainWindow *w, QTextEdit *b, bool ho=false):QObject(w){
		this->w=w;br = b;
		helpOnly = ho;
		ttdelay = 500;
		//b->setMouseTracking(true);
		//connect(&tm, SIGNAL(timeout()), this, SLOT(onTimer()));
		//tm.setSingleShot(true);
		if(br){
			br->installEventFilter(this);
			if(br->viewport())br->viewport()->installEventFilter(this);
		}
	}

};
#include <QMdiArea>
class fbfilter:public QObject{
	MainWindow *w;
	QTextEdit *br;
protected:
	bool eventFilter(QObject *, QEvent *e){
	//	qDebug()<<e->type();
		if(e->type()==QEvent::MouseButtonPress){
			if(QMouseEvent* m = (QMouseEvent*)e){
				if(m->button()==Qt::XButton1){
					//o->event(e);
					w->back();
					e->accept();
					return true;
				}else if(m->button()==Qt::XButton2){
					e->accept();
					w->forward();
					return true;
				}
			}
		}
		return false;
	}
public:
	fbfilter(MainWindow *w, QTextEdit *b):QObject(w){
		this->w=w;br = b;
		if(br){
			w->installEventFilter(this);
			if(br->viewport())br->viewport()->installEventFilter(this);
		}
	}

};

void filterHelp(QString &res){
	res.replace('>', "&#62;");
	res.replace('<', "&#60;");
	res.replace('\n', "<br>");
	int n = res.length();
	res.replace(QRegExp("                                                                         [ \n]*"), "<li>");
	res.replace("<br><br>", "<li>");
	//res.replace(QRegExp("<li>(<li>)+"), "<li>");
	if(n!=res.length()){
		if(res.startsWith("<li>"))
			res.prepend("<ul>");
		else res.prepend("<ul><li>");
		res.append("</ul>");
	}
	res.replace(".", ". "); res.replace(",", ", ");
	res.replace(";", "; "); res.replace("-", QString(" &#8212; "));//+QChar('-')+' ');
	res.replace("...","&#8230;");
	res.replace(". . ."," &#8230; ");
	for(int i=1; i>=0 && i<res.length();){
		i = res.indexOf(QRegExp("<li>[ ]*\\d+\\."), i);
		if(i<0)break;
		//i+=8;
		int j, l, k = res.length();
		for(j = i+4;j>0;){
			j = res.indexOf("<li>", j+4);
			if(j<0)break;
			for(l = j+4; l<res.length(); l++){
				if(!res[l].isSpace())break;
			}
			if(l == res.length()||
					!res[l].isDigit()){k = j; break;}
			for(l++; l<res.length(); l++)
				if(!res[l].isDigit())break;
			if(l == res.length()||
					res[l]!='.'){k = j; break;}
		}
		res.insert(k, "</ol>");
		res.insert(i, "<ol> ");
		i = k+8;
	}
	res.replace(QRegExp("<li>[ ]*\\d+\\."), "<li>");
	int i, j;
	for(i=j=0; i<res.length(); i++, j++){
		if(res[i].isSpace()){
			while(res[i+1].isSpace())i++;
		}
		res[j]=res[i];
	}
	res.resize(j);
}

class HelpFilter:public StringFilter{
	void apply(QString &res){filterHelp(res);}
};
static const int SymFiks = 882;

bool convertHelpNode(LosInfoNode *& n, const QString& name){
	QString res, s;
	if(!n)return false;
	if(n->is_terminal())return true;
	LosInfoList *l = dynamic_cast<LosInfoList*>(n);
	if (!l || l->labels.size() != 1 || l->labels[0].refs.size() != 1)return false;
	l = dynamic_cast<LosInfoList*>(l->labels[0].refs[0]);
	if (!l)return false;
	do {
		std::vector<LosInfoList*> lists;
		for (LosInfoList::Label &lb : l->labels) {
			for (int i = 0; i < lb.refs.size(); i++) {
				if (LosInfoList* ll = dynamic_cast<LosInfoList*>(lb.refs[i])) {
					lists.push_back(ll);
					continue;
				}
				if (LosLogicTerminal* t = dynamic_cast<LosLogicTerminal*>(lb.refs[i])) {
					if (t->terms.size() == 1 && t->terms[0].data[0].isSymbol() && t->terms[0].data[0].val == SymFiks)
						if(!res.endsWith("\n\n"))res += "\n\n";
					continue;
				}
				LosDataTerminal *t = dynamic_cast<LosDataTerminal*>(lb.refs[i]);
				if (!t)continue;
				t->toHtml(s, QVector<QString>());
				s.remove(QChar(0));
				if (s.endsWith('-'))s.chop(1);
				(res += s);// += "\n\n";
			}
		}
		if (lists.size()>1)return false;
		if(lists.size())l = lists[0];
		else l = 0;
	} while (l);
	n->del();
	filterHelp(res);
	if(name.length())res.prepend("<h3>"+name+"</h3><hr>");
	LosDataTerminal *nn = new LosDataTerminal;
	nn->_used++;
	nn->data = res;
	n = nn;
	return true;
}

void MainWindow::readSettings() {
	QSettings s;
	QByteArray wg = s.value("windowGeometry").toByteArray();
	if (!wg.isEmpty())this->restoreGeometry(wg);
	QByteArray ws = s.value("windowState").toByteArray();
	if (!ws.isEmpty())this->restoreState(ws);
	ui->showInfo->setChecked(s.value("showHelp", true).toBool());
}

void MainWindow::writeSettings() {
	QSettings s;
	s.setValue("windowGeometry", this->saveGeometry());// .toByteArray();
	s.setValue("windowState", this->saveState());
	s.setValue("showHelp", ui->showInfo->isChecked());
}

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow){
	currSymbol = 0;
	restart_search = false;
	stop_search = true;
	ui->setupUi(this);
	ui->statusBar->addWidget(new QLabel());
//	QFont fnt = ui->decoded->font();
//	fnt.setPointSize(10);
//	ui->findedit->setFont(fnt);
//	ui->progsearch->edit()->setFont(fnt);
//	ui->statusBar->addPermanentWidget();
	new dclfilter(this, ui->decoded);
	new dclfilter(this, ui->ruleBrowser, true);
	new fbfilter(this, ui->decoded);
	connect(ui->actionLoad_directory, SIGNAL(triggered()),this,SLOT(readDir()));
	connect(ui->actionSave_names, SIGNAL(triggered()),this,SLOT(saveNames()));
	connect(ui->actionSaveHtml, SIGNAL(triggered()),this,SLOT(saveHtml()));
	connect(ui->actionPrint, SIGNAL(triggered()),this,SLOT(print()));
	connect(ui->SavePriem, SIGNAL(pressed()), this, SLOT(savePriem()));
	connect(ui->loadRules, SIGNAL(pressed()), this, SLOT(loadRules()));

	FileOut err("errors.log"), warn("warnings.log"), _log("output.log");
	if(const char *str = s.readPrograms("./LOS", OutputContext(err,warn,_log))){
		ui->statusBar->showMessage(str);
	}
	if(!s.readNames("./INF", OutputContext(err,warn,_log))){
		ui->statusBar->showMessage("cannot read names");
	}
	const QVector<QString> &vsn = s.symbolNames();
	for(int k = 0; k<vsn.size(); k++){
		if(!vsn[k].isEmpty())
			initialNames[vsn[k]] = k;
	}
	if(s.loadTranslations("eng.tr")){
		s.translateSymbols("eng");
	}
	updateTranslationTable();
	resetSymbolList();
/*	QStringList sl = s.symbolNames().toList();
	sl.removeAll(QString());
	ui->listWidget->addItems(sl);
	for(int i=sl.size(); i--;){
		QListWidgetItem *item = ui->listWidget->item(i);
		if(item){
			item->setFlags(Qt::ItemIsEditable|Qt::ItemIsSelectable|Qt::ItemIsEnabled);
			item->setData(Qt::UserRole, s.nameToSymbol(item->text()));
		}
	}*/

	QCompleter *tr_completer = new QCompleter(initialNames.keys(), ui->leftEdit);
	ui->leftEdit->setCompleter(tr_completer);

	//	QActionGroup ga;
	search_flags = Qt::MatchExactly;
	QAction *a = new QAction("case sensitive",ui->findopt);
	QActionGroup *ag = new QActionGroup(this);
	ui->findopt->addAction(a);	a->setCheckable(true);
	connect(a, SIGNAL(triggered(bool)), this, SLOT(setSearchCaseSensitive(bool)));

	ui->findopt->addAction(a = new QAction("",ui->findopt));     a->setSeparator(true);
	ui->findopt->addAction(a = ag->addAction("all occurences")); a->setCheckable(true);
	connect(a,SIGNAL(triggered()),this, SLOT(setSearchContains()));
	ui->findopt->addAction(a = ag->addAction("whole words"));    a->setCheckable(true);
	connect(a,SIGNAL(triggered()),this, SLOT(setSearchWhole()));
	a->setChecked(true);
	ui->findopt->addAction(a = ag->addAction("regular expressions")); a->setCheckable(true);
	connect(a,SIGNAL(triggered()),this, SLOT(setSearchRegExp()));
	ui->findopt->addAction(a = ag->addAction("starts with")); a->setCheckable(true);
	connect(a,SIGNAL(triggered()),this, SLOT(setSearchPrefix()));
	ui->findopt->addAction(a = ag->addAction("ends with")); a->setCheckable(true);
	connect(a,SIGNAL(triggered()),this, SLOT(setSearchSuffix()));
	

//	ui->textBrowser
	connect(ui->findedit, SIGNAL(textEdited(QString)), this, SLOT(clearSelection()));
	connect(ui->find, SIGNAL(clicked()),this,SLOT(findFirst()));
	connect(ui->findedit, SIGNAL(returnPressed()),this,SLOT(findFirst()));
	connect(ui->findNext, SIGNAL(clicked()),this,SLOT(findNext()));
	connect(ui->findPrev, SIGNAL(clicked()),this,SLOT(findPrevious()));

	connect(ui->findProgs, SIGNAL(clicked()),this,SLOT(startSearch()));

	connect(ui->progsearch, SIGNAL(searchStringChanged(QString)), this, SLOT(clearSelection()));
	connect(ui->progsearch, SIGNAL(findPressed()),this,SLOT(findFirst()));
	connect(ui->progsearch, SIGNAL(entered()),this,SLOT(findFirst()));
	connect(ui->progsearch, SIGNAL(nextPressed()),this,SLOT(findNext()));
	connect(ui->progsearch, SIGNAL(prevPressed()),this,SLOT(findPrevious()));

	connect(ui->textBrowser, SIGNAL(anchorClicked(QUrl)), this, SLOT(onHRef(QUrl)));
	connect(ui->listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(showProgram(QListWidgetItem*,QListWidgetItem*)));
	connect(ui->listWidget, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(renameSymbol(QListWidgetItem*)));
	connect(ui->findRes, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(showPos(QListWidgetItem*,QListWidgetItem*)));
	connect(ui->showInfo,SIGNAL(clicked()), this, SLOT(showCurrentInfo()));

	connect(ui->forSave, SIGNAL(clicked()), this, SLOT(showProgram()));
	connect(ui->leftEdit,SIGNAL(textChanged(QString)),this,SLOT(checkTranslationEnabled()));
	connect(ui->rightEdit,SIGNAL(textChanged(QString)),this,SLOT(checkTranslationEnabled()));
	connect(ui->addTranslation,SIGNAL(pressed()),this,SLOT(addTranslation()));
	connect(ui->rightEdit,SIGNAL(returnPressed()),this,SLOT(addTranslation()));
	connect(ui->retranslate,SIGNAL(pressed()),this,SLOT(retranslate()));
	connect(ui->translation,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(onTrTextChanged(QTableWidgetItem*)));

	connect(ui->translit, SIGNAL(pressed()),this,SLOT(translitAll()));
	connect(ui->actionSave_all_programmes, SIGNAL(triggered()), this, SLOT(saveAllProgrammes()));
	connect(ui->actionUpdate_all_programmes, SIGNAL(triggered()), this, SLOT(updateAllProgrammes()));
	connect(ui->actionSave_name_map, SIGNAL(triggered()), this, SLOT(saveNameMap()));

	connect(ui->comboBox,SIGNAL(currentIndexChanged(int)), this, SLOT(showCurrentInfo()));
	connect(ui->saveProblems, SIGNAL(triggered()), this, SLOT(saveAllProblems()));

	connect(ui->findRules, SIGNAL(clicked(bool)), this, SLOT(findRules()));
	connect(ui->lineRule, SIGNAL(returnPressed()), this, SLOT(findRules()));
	connect(ui->ruleBrowser, SIGNAL(anchorClicked(QUrl)), this, SLOT(onRuleURL(QUrl)));

	//taskBook.read("./TCH/E", OutputContext(err, warn, _log));
	help.read("./INF/H", OutputContext(err, warn, _log));
	//help.read("./GEN/P", OutputContext(err, warn, _log));
	//priem.read("./GEN/P", OutputContext(err, warn, _log));
	//help.node(currSymbol);
	QVector<LosInfoNode*>& lin = help.getNodes();
	for(int i=0; i<lin.size(); i++){
		convertHelpNode(lin[i], s.symbolNames().value(i, ""));
	}
	infoBlocks.resize(ui->comboBox->count());
	infoBlocks[0].read("./GEN/P", OutputContext(err, warn, _log));
	infoBlocks[1].read("./TCH/E", OutputContext(err, warn, _log));
	infoBlocks[2].read("./INF/Q", OutputContext(err, warn, _log));
	infoBlocks[3].read("./TXT/W", OutputContext(err, warn, _log));
	infoBlocks[4].read("./TER/M", OutputContext(err, warn, _log));
	infoBlocks[5].read("./INF/T", OutputContext(err, warn, _log));
//	infoBlocks[6].read("./TCH/A", OutputContext(err, warn, _log));
	infoBlocks[7].read("./GEN/B", OutputContext(err, warn, _log));
	ui->helpBrowser->setNames((QVector<QString>*)&s.symbolNames());
	ui->priemWidget->setNames((QVector<QString>*)&s.symbolNames());
	ui->textBrowser->setVisible(false);
	//ui->helpBrowser->setStringFilter(new HelpFilter);
	ui->helpDock->setVisible(false);
	hl = 0;
	setupHighlighter();
	ui->trLog->setVisible(false);
	ui->priemWidget->setVisible(false);
	readSettings();
//	connect(ui->listWidget, SIGNAL(doubleClicked(QModelIndex)), ui->listWidget, SLOT(edit(QModelIndex)));
}
#include <QTableWidget>
void MainWindow::resetSymbolList() {
	QStringList sl = s.symbolNames().toList();
	sl.removeAll(QString());
	int row = ui->listWidget->currentRow();
	if (row < 0)row = 0;
	ui->listWidget->clear();
	ui->listWidget->addItems(sl);
	for(int i = sl.size(); i--;){
		QListWidgetItem *item = ui->listWidget->item(i);
		if (item) {
			item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			item->setData(Qt::UserRole, s.nameToSymbol(item->text()));
		}
	}
	ui->listWidget->setCurrentRow(row);
	QCompleter *completer = new QCompleter(sl, ui->findedit);
	ui->findedit->setCompleter(completer);
	showProgram();
}

void MainWindow::updateTranslationTable(){
	QVector<QPair<QString,QString> > tb = s.getTranstationTable("rus", "eng");
//	ui->translation->clearContents();
	ui->translation->setRowCount(0);
	leftSet.clear(); rightSet.clear();
	currTr.clear();
	int i=0;
	bool se = ui->translation->isSortingEnabled();
	if(se)ui->translation->setSortingEnabled(false);
	for(i=0; i<tb.size(); i++){
		QPair<QString,QString>& p = tb[i];
//		qDebug()<<
		insertTranslationRow(p.first, p.second, initialNames.value(p.first, -1));
/*		ui->translation->insertRow(i);
		ui->translation->setItem(i, 0, new QTableWidgetItem(p.first));
		ui->translation->setItem(i, 1, new QTableWidgetItem(p.second));
		currTr[p.first] = p.second;
		int num = initialNames.value(p.first, -1);
		if(num >= 0)
			ui->translation->setItem(i,2,new QTableWidgetItem(QString::number(num)));
		leftSet[p.first] = rightSet[p.second] = num;*/
	}
	if (se)ui->translation->setSortingEnabled(true);
//	ui->translation->insertRow(i);
}
void MainWindow::insertTranslationRow(const QString &l, const QString &r, int num) {
	if(!transliterated.contains(l) || r != transliterate(l)){
		bool se = ui->translation->isSortingEnabled();
		if(se)ui->translation->setSortingEnabled(false);
		int i = ui->translation->rowCount();
		ui->translation->insertRow(ui->translation->rowCount());
		QTableWidgetItem *I = new QTableWidgetItem(l);

		I->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		ui->translation->setItem(i, 0, I);
		ui->translation->setItem(i, 1, new QTableWidgetItem(r));
		I = new QTableWidgetItem;//(num>=0 ? QString::number(num) : QString());
		if(num>0)I->setData(Qt::DisplayRole, num);
		I->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		ui->translation->setItem(i, 2, I);
		if (se)ui->translation->setSortingEnabled(true);
//		qDebug()<<"added ("<<l<<r<<")";
	}
	leftSet[l] = rightSet[r] = num;
	currTr[l] = r;
}

void MainWindow::onTrTextChanged(QTableWidgetItem *I){
	if(!I)return;
	int row = I->row();
	if(row<0||row>=ui->translation->rowCount()||I->column()!=1)return;
	QTableWidgetItem *f = ui->translation->item(row,0);
	if(I->text().isEmpty()){
		rightSet.remove(I->text());
		if(f){
			currTr.remove(f->text());
			leftSet.remove(f->text());
		}
		ui->translation->removeRow(row);
	}else{
		if(!f||f->text().isEmpty())return;
		QString &po = currTr[f->text()];
		int &n = rightSet[I->text()], &o = rightSet[po];
		n = o;
		if(&n!=&o)rightSet.remove(po);
		po = I->text();
	}
}

void MainWindow::addTranslation(){
	int num = initialNames.value(ui->leftEdit->text(),-1);
	if(num<0 || ui->rightEdit->text().isEmpty())return;
	if(ui->rightEdit->text().isEmpty())return;
	QList<QTableWidgetItem*> l = ui->translation->findItems(ui->leftEdit->text(),Qt::MatchExactly);
	int row = ui->translation->rowCount();
	ui->translation->setSortingEnabled(false);
	if (l.size() >= 1) {
		row = l[0]->row();
		ui->translation->item(row, 0)->setText(ui->leftEdit->text());
		ui->translation->item(row, 1)->setText(ui->rightEdit->text());
		ui->translation->item(row, 2)->setData(Qt::DisplayRole, num);// Text(QString::number(num));
	} else {
		if(rightSet.contains(ui->rightEdit->text()))return;
		insertTranslationRow(ui->leftEdit->text(), ui->rightEdit->text(), num);
/*		ui->translation->insertRow(row);
		ui->translation->setItem(row, 0, new QTableWidgetItem(ui->leftEdit->text()));
		ui->translation->setItem(row, 1, new QTableWidgetItem(ui->rightEdit->text()));
		ui->translation->setItem(row, 2, new QTableWidgetItem(QString::number(num)));*/
	}
	ui->translation->setSortingEnabled(true);
	leftSet[ui->leftEdit->text()] = rightSet[ui->leftEdit->text()] = num;
	currTr[ui->leftEdit->text()] = ui->rightEdit->text();
	ui->leftEdit->clear(); ui->rightEdit->clear(); ui->addTranslation->setEnabled(false);
	ui->leftEdit->setFocus();
}

void MainWindow::checkTranslationEnabled(){
	if(sender()==ui->leftEdit && ui->rightEdit->selectedText()==ui->rightEdit->text()){
		if(currTr.contains(ui->leftEdit->text())){
			ui->rightEdit->setText(currTr[ui->leftEdit->text()]);
			ui->rightEdit->selectAll();
		}else ui->rightEdit->clear();
	}
	ui->addTranslation->setEnabled(
				!ui->leftEdit->text().isEmpty() &&
				!ui->rightEdit->text().isEmpty() &&
				initialNames.contains(ui->leftEdit->text()) &&
				!initialNames.contains(ui->rightEdit->text())
				);
}

void MainWindow::retranslate(){
	QMultiMap<QString,QString> str;
	for(QMap<QString, QString>::Iterator it = currTr.begin(); it!=currTr.end(); ++it)
		str.insertMulti(it.value(), it.key());
	QList<QString> r = str.uniqueKeys();
	QString err;
	foreach(const QString& s, r){
		if(str.count(s)>1){
			err+="collision : translation "+s+"occurs multiple times\n";
			QList<QString> range = str.values(s);
			foreach(const QString &x, range){
				err += "    "+x+"\n";
			}
		}
	}
	if(!err.isEmpty()){
		ui->trLog->show();
		ui->trLog->setTextColor(Qt::red);
		ui->trLog->setText(err);
	}else{
		if(!s.setTranslations(currTr, "rus", "eng")){
			ui->trLog->show();
			ui->trLog->setTextColor(Qt::red);
			ui->trLog->setText("failed to retranslate symbols");
		}else ui->trLog->hide();
		s.translateSymbols("eng");
		showProgram();
	}
	resetSymbolList();
	setupHighlighter();
}

MainWindow::~MainWindow(){
	delete ui;
}

static bool removePath(const QString &path)
{
    bool result = true;
    QFileInfo info(path);
    if (info.isDir()) {
        QDir dir(path);
        foreach (const QString &entry, dir.entryList(QDir::AllDirs | QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot)) {
            result &= removePath(dir.absoluteFilePath(entry));
        }
        if (!info.dir().rmdir(info.fileName()))
            return false;
    } else {
        result = QFile::remove(path);
    }
    return result;
}


void MainWindow::savePriem()
{
	removePath("./Priem/");
    /*if(removePath("./Priem/"))
        return;*/
	QFile file("text.txt");
	file.open(QIODevice::WriteOnly|QIODevice::Text);
	QTextStream stream(&file);
	LosInfoNode *n = infoBlocks[0].node(0);
    qDebug()<<currSymbol;
	int nNodes = ui->priemWidget->getNames()->count();
	//QVector<QString> *qst = ui->priemWidget->getNames();
    //if(!n)
    //    return;
	QMap<QString, QByteArray> texts;
    for(int i = 1; i <=nNodes; i++)
    //for(int i = 186; i <= 186; i++)
    {
        n = infoBlocks[0].node(i);
		//ui->priemWidget->setNode(n);
		ui->priemWidget->writePriem(i, n, texts);
    }
	for(auto& it = texts.begin(); it!=texts.end(); ++it){
		QString fn = it.key();
		QString path = fn.left(fn.lastIndexOf('/'));
		QDir().mkpath(path);
		QFile f(fn);
		f.open(QFile::WriteOnly);
		f.write(it.value());
		f.close();
		stream<<fn<<endl;
	}
	file.close();
}

void MainWindow::loadRules() {
	removePath("./Priem/");
	/*if(removePath("./Priem/"))
	return;*/
	QFile file("text.txt");
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream stream(&file);
	LosInfoNode *n = infoBlocks[0].node(0);
	//qDebug() << currSymbol;
	int nNodes = ui->priemWidget->getNames()->count();
	//QVector<QString> *qst = ui->priemWidget->getNames();
	//if(!n)
	//    return;
	QMap<QString, QByteArray> texts;
	rules.clear();
	auto t = QDateTime::currentDateTime();
	for (int i = 1; i < nNodes; i++) {
		n = infoBlocks[0].node(i);
		ui->priemWidget->writePriem(i, n, rules, { (*ui->priemWidget->getNames())[i] });
		auto t1 = QDateTime::currentDateTime();
		if (t.msecsTo(t1) > 10) {
			ui->statusBar->showMessage(QString("loading rules : %1 loaded").arg(rules.size()));
			t = t1;
			QApplication::processEvents();
		}
	}
	ui->statusBar->showMessage(QString("done : %1 rules loaded").arg(rules.size()));
}

void MainWindow::translitAll(){
	for(QMap<QString,int>::iterator it = initialNames.begin(); it!=initialNames.end(); ++it){
		const QString &str = it.key();
		if(!str.isEmpty() && !currTr.contains(str)){
			QString translit = transliterate(str);
			if(rightSet.contains(translit))
				ui->trLog->append("cannot transliterate symbol "+str+" : translation '"+translit+"' already exists");
			else{
				leftSet[str] = rightSet[translit] = *it;
				currTr[str] = translit;
				transliterated << str;
//				qDebug()<<translit;
			}
		}
	}
	s.setTranslations(currTr, "rus", "eng", false);
	qDebug()<<"translations were set";
	updateTranslationTable();
	qDebug()<<"translation table updated";
	retranslate();
	qDebug()<<"retranslated";
}


void MainWindow::readDir(){
	QString d = QFileDialog::getExistingDirectory();
	if(d.isEmpty())return;
	FileOut err("errors.log"), warn("warnings.log"), _log("output.log");
	if(const char *str = s.readPrograms(d, OutputContext(err,warn,_log))){
		ui->statusBar->showMessage(str);
	}
}

void MainWindow::showProgram(QListWidgetItem *i, QListWidgetItem *, bool push){
	if(!i){i = ui->listWidget->currentItem(); push = false;}
	if(!i)return;
	QString str;
	if(fb.c)fb.st[fb.c-1].pos = ui->decoded->textCursor().position();
	uint n = currSymbol = s.nameToSymbol(i->text());
	if(s.programToHString(n, str))//ui->textBrowser->setText(str);
		ui->textBrowser->setHtml(str);
	else return;
	str="/// program for symbol "+i->text()+" (Symbol "+QString::number(n)+")\n";
	//const LosProgramFragment *f;
	ui->statusBar->clearMessage();
	showCurrentInfo();
	if(progtree.decodeProgram(s.program(n))){
		NameInfo info; info.format = NameInfo::SymbolicUnicode;
		info.format = ui->forSave->isChecked() ? NameInfo::ForCopy : NameInfo::PlainText;
		info.operatorNames = info.symbolNames = &s.symbolNames();
		info.rules = &rules;
	//	str.reserve(500000);
		ProgramHeader::Call c;
		c.tp = ProgramHeader::Program;
		for(QMap<ProgramHeader::Call, LosProgramNode*>::const_iterator it = progtree.program().calls.begin(); it!=progtree.program().calls.end(); ++it){
			QString res;
			progtree.print(it.key(), n, res, info);
			str+=res;
		}
		/*if(!progtree.print(c, n, str, info)){
			c.tp = ProgramHeader::NoType;
			if(!progtree.print(c, n, str, info)){
				c = progtree.program().calls.begin().key();
				progtree.print(c, n, str, info);
			}
		}*/
		//str.remove(QChar(0));
		ui->decoded->setPlainText(str);
		ui->statusBar->showMessage("program length = "+QString::number(str.length())+" symbols");
	//	QTextCharFormat fmt; fmt.
//		ui->decoded->textCursor().setCharFormat();
	}else{
		ui->decoded->setPlainText("cannot recognize fragment");
	}
	if(push)fb.push(i->text(), 0);
}

void MainWindow::showPos(QListWidgetItem *i, QListWidgetItem *){
	if(i==0)return;
	bool ok;
	int sn = i->data(Qt::UserRole).toInt(&ok);
	if(!ok)return;
	QString s0 = i->data(Qt::UserRole+1).toString();
	if(s0.isEmpty())return;
	int f = i->data(Qt::UserRole+2).toInt(&ok);
	if(!ok)return;
	if(s.symbolNames()[sn].isEmpty())return;
	QList<QListWidgetItem *> l = ui->listWidget->findItems(s.symbolNames()[sn], Qt::MatchExactly);
	if(l.size()>1)return;
	if(!l.size())return;
	ui->listWidget->setCurrentItem(l[0]);
	ui->progsearch->setText(s0);
	ui->progsearch->setSearchFlags((Qt::MatchFlag)f);
	this->findFirstProg(f);
}

void MainWindow::updateFragments() {
	if (trees.size())return;
	trees.resize(s.symbolNames().size());
	auto t = QDateTime::currentDateTime();
	int j = 0;
	for (int i = 1; i < s.symbolNames().size(); i++) {
		if (trees[i].decodeProgram(s.program(i))) {
			j++;
			NameInfo info;
			//info.format = NameInfo::SymbolicUnicode;
			info.format = NameInfo::ForCopy;
			info.operatorNames = info.symbolNames = &s.symbolNames();
			info.rules = &rules;

			trees[i].updateHeaders(i, info);
			info.format = NameInfo::PlainText;
			//	str.reserve(500000);
			ProgramHeader::Call c;
			c.tp = ProgramHeader::Program;
			for (auto it = trees[i].program().calls.begin(); it != trees[i].program().calls.end(); ++it) {
				it.value()->updateParent();
				it.value()->createRuleIndex(info, ruleNodes);
			}
		}
		auto t1 = QDateTime::currentDateTime();
		if (t.msecsTo(t1) > 10) {
			ui->statusBar->showMessage(QString("decode programs : %1 processed").arg(j));
			QApplication::processEvents();
			t = t1;
		}
	}
	ui->statusBar->showMessage(QString("done : %1 programs decoded").arg(j));
}
void MainWindow::updateRuleIndex() {
	updateFragments();
}

void MainWindow::saveHtml(){
	if(currSymbol && !s.symbolNames()[currSymbol].isEmpty()){
		QByteArray ss = ui->decoded->toHtml().toLocal8Bit();
		QFile f(s.symbolNames()[currSymbol]+".html");
		f.open(QFile::WriteOnly);
		f.write(ss);
		f.close();
	}
}

void MainWindow::print(){
	QPrinter p;
	p.setOutputFileName(s.symbolNames()[currSymbol]+".pdf");
	ui->decoded->print(&p);
}

void MainWindow::renameSymbol(QListWidgetItem *it){
	if(!it)return;
	bool ok = true;
	uint snum = it->data(Qt::UserRole).toUInt(&ok), sn1;
	if(!ok || snum>=(uint)s.symbolNames().size())return;
	QString t = it->text();
	uint i, l = t.size();
	for(i=0; i<l; i++)
		if(!t[i].isSpace())break;
	t.remove(0,i);
	l = t.size();
	for(i=l; i--;)
		if(!t[i].isSpace())break;
	t.chop(l-i-1);
	l=t.size();
	if(l>24||!l || !t[0].isLetter()) {
		it->setText(s.symbolNames()[snum]);
		return;
	}
	for(i=0; i<l; i++) {
		if(!t[i].isLetterOrNumber()){
			ui->statusBar->showMessage("name of symbol can only contain letters or digits");
			it->setText(s.symbolNames()[snum]);
			return;
		}
	}
	if(((sn1=s.nameToSymbol(t))!=0)) {
		if(sn1!=snum){
			it->setText(s.symbolNames()[snum]);
			ui->statusBar->showMessage("symbol with this name already exists");
			return;
		}
		it->setText(t);
	} else {
		it->setText(t);
		s.renameSymbol(snum, t);
		showProgram(it,it);
	}
}

void MainWindow::saveNames(){
	FileOut err("errors.log"), warn("warnings.log"), _log("output.log");
	if(!s.saveNames("./INF", OutputContext(err,warn,_log))){
		ui->statusBar->showMessage("cannot save names");
	}
}
#include <fstream>
void MainWindow::saveNameMap(){
	std::fstream f("symbol_name_map.txt", std::ios::out);
	const QVector<QString> &sn = s.symbolNames();
	for(int i = 0; i<sn.size(); i++){
		if(sn[i].isEmpty())continue;
		f<<i<<"\t"<<sn[i].toStdString()<<std::endl;
	}
	f.close();
}

bool containSymbol(const QString &fn, const QString& sym){
	QFile f(fn);
	if(!f.open(QFile::ReadOnly))return false;
	QByteArray str = f.readLine();
	QByteArray s = sym.toLocal8Bit();
	return str.contains(s);
}

void MainWindow::updateAllProgrammes(){
	QString dir = QFileDialog::getExistingDirectory();
	if(dir.isEmpty())return;
	QDir d(dir);
	d.mkdir("solve");
	d.mkdir("core");
	d.mkdir("info");
	d.mkdir("default");
	if(dir.endsWith('/'))dir.chop(1);
	const QVector<QString> &sn = s.symbolNames();
	int i, n, r;
	QString str = "";
	for(i = n = 0; i<sn.size(); i++){
		if(!sn[i].isEmpty()){
			if(!(n%6))str+="symbol";
			str+=" "+sn[i];
			n++;
			if(!(n%6))str+=";\n";
			else str+=",";
		}
	}
	if(n%6)str+=";\n";
	if(!str.isEmpty()){
		QFile f(dir+"/symbols.lsh");
		if(f.open(QFile::WriteOnly)){
			QByteArray ba = str.toLocal8Bit();
			f.write(ba);
		}
	}
	ui->findRes->clear();
	if(!n)return;
	QProgressBar* p = new QProgressBar;
	ui->statusBar->addPermanentWidget(p);
	QDateTime t = QDateTime::currentDateTime(), t0;
	for(i = r = 0; i<sn.size(); i++){
		if(sn[i].isEmpty())continue;
		QString fn0;
		for(int k=0; k<sn[i].length(); k++)
			fn0.append(sn[i][k].toLower());

		if(progtree.decodeProgram(s.program(i))){
			NameInfo info; info.format = NameInfo::ForCopy;
			info.operatorNames = info.symbolNames = &s.symbolNames();
			info.rules = &rules;
			str.clear();
		//	ProgramHeader::Call c;
		//	c.tp = ProgramHeader::Program;
			QString func, infocall, solve;
			for(QMap<ProgramHeader::Call, LosProgramNode*>::const_iterator it = progtree.program().calls.begin(); it!=progtree.program().calls.end(); ++it){
				QString res;
				if(!progtree.print(it.key(), i, res, info))continue;
				res = "symbol "+sn[i]+";\n"+res;
				res.remove(QChar(0));
				if(it.key().tp == ProgramHeader::Program ||
						(it.key().tp == ProgramHeader::CallIndex && it.key().symbol == 260)||
						(it.key().tp == ProgramHeader::Symbol && it.key().symbol == 32)){
					func+=res;
				}
				else if(it.key().tp == ProgramHeader::Symbol && it.key().symbol == 11)
					solve+=res;
				else if(it.key().tp == ProgramHeader::CallIndex)
					infocall+=res;
				else str+=res;
			}
			if(!str.isEmpty()){
				QString fn = dir+"/default/"+fn0+".los";
				if(!containSymbol(fn, sn[i])){
					QFile f(fn);
					if(f.open(QFile::Append)){
						QByteArray ba = str.toLocal8Bit();
						f.write(ba);
					}
				}
			}
			if(!func.isEmpty()){
				QString fn = dir+"/core/"+fn0+".los";
				if(!containSymbol(fn, sn[i])){
					QFile f(fn);
					if(f.open(QFile::Append)){
						QByteArray ba = func.toLocal8Bit();
						f.write(ba);
					}
				}
			}
			if(!solve.isEmpty()){
				QString fn = dir+"/solve/"+fn0+".los";
				if(!containSymbol(fn, sn[i])){
					QFile f(fn);
					if(f.open(QFile::Append)){
						QByteArray ba = solve.toLocal8Bit();
						f.write(ba);
					}
				}
			}
			if(!infocall.isEmpty()){
				QString fn = dir+"/info/"+fn0+".los";
				if(!containSymbol(fn, sn[i])){
					QFile f(fn);
					if(f.open(QFile::Append)){
						QByteArray ba = infocall.toLocal8Bit();
						f.write(ba);
					}
				}
			}
			//fns << fn;
		}
		r++;
		t0 = QDateTime::currentDateTime();
		if(t.msecsTo(t0)>10){
			p->setValue(int(r*100./n));
			QApplication::processEvents();
			t = QDateTime::currentDateTime();
		}
	}
	//	searching = false;
	delete p;
}

void MainWindow::saveAllProgrammes(){
	QString dir = QFileDialog::getExistingDirectory();
	if(dir.isEmpty())return;
	QDir d(dir);
	d.mkdir("solve");
	d.mkdir("core");
	d.mkdir("info");
	d.mkdir("default");
	if(dir.endsWith('/'))dir.chop(1);
	const QVector<QString> &sn = s.symbolNames();
	int i, n, r;
	QString str = "";
	for(i = n = 0; i<sn.size(); i++){
		if(!sn[i].isEmpty()){
			if(!(n%6))str+="symbol";
			str+=" "+sn[i];
			n++;
			if(!(n%6))str+=";\n";
			else str+=",";
		}
	}
	if(n%6)str+=";\n";
	if(!str.isEmpty()){
		QFile f(dir+"/symbols.lsh");
		if(f.open(QFile::WriteOnly)){
			QByteArray ba = str.toLocal8Bit();
			f.write(ba);
		}
	}
	ui->findRes->clear();
	if(!n)return;
	QProgressBar* p = new QProgressBar;
	ui->statusBar->addPermanentWidget(p);
	QDateTime t = QDateTime::currentDateTime(), t0;
	QSet<QString> fns;
	for(i = r = 0; i<sn.size(); i++){
		if(sn[i].isEmpty())continue;
		QString fn;
		for(int k=0; k<sn[i].length(); k++)
			fn.append(sn[i][k].toLower());

		if(progtree.decodeProgram(s.program(i))){
			NameInfo info; info.format = NameInfo::ForCopy;
			info.operatorNames = info.symbolNames = &s.symbolNames();
			str.clear();
		//	ProgramHeader::Call c;
		//	c.tp = ProgramHeader::Program;
			QString func, infocall, solve;
			for(QMap<ProgramHeader::Call, LosProgramNode*>::const_iterator it = progtree.program().calls.begin(); it!=progtree.program().calls.end(); ++it){
				QString res;
				if(!progtree.print(it.key(), i, res, info))continue;
				res = "symbol "+sn[i]+";\n"+res;
				res.remove(QChar(0));
				if(it.key().tp == ProgramHeader::Program ||
						(it.key().tp == ProgramHeader::CallIndex && it.key().symbol == 260)||
						(it.key().tp == ProgramHeader::Symbol && it.key().symbol == 32)){
					func+=res;
				}
				else if(it.key().tp == ProgramHeader::Symbol && it.key().symbol == 11)
					solve+=res;
				else if(it.key().tp == ProgramHeader::CallIndex)
					infocall+=res;
				else str+=res;
			}
			if(!str.isEmpty()){
				fn = dir+"/default/"+fn+".los";
				QFile f(fn);
				if(f.open(fns.contains(fn) ? QFile::Append : QFile::WriteOnly)){
					QByteArray ba = str.toLocal8Bit();
					f.write(ba);
				}
			}
			if(!func.isEmpty()){
				fn = dir+"/core/"+fn+".los";
				QFile f(fn);
				if(f.open(fns.contains(fn) ? QFile::Append : QFile::WriteOnly)){
					QByteArray ba = func.toLocal8Bit();
					f.write(ba);
				}
			}
			if(!solve.isEmpty()){
				fn = dir+"/solve/"+fn+".los";
				QFile f(fn);
				if(f.open(fns.contains(fn) ? QFile::Append : QFile::WriteOnly)){
					QByteArray ba = solve.toLocal8Bit();
					f.write(ba);
				}
			}
			if(!infocall.isEmpty()){
				fn = dir+"/info/"+fn+".los";
				QFile f(fn);
				if(f.open(fns.contains(fn) ? QFile::Append : QFile::WriteOnly)){
					QByteArray ba = infocall.toLocal8Bit();
					f.write(ba);
				}
			}
			fns << fn;
		}
		r++;
		t0 = QDateTime::currentDateTime();
		if(t.msecsTo(t0)>10){
			p->setValue(int(r*100./n));
			QApplication::processEvents();
			t = QDateTime::currentDateTime();
		}
	}
	//	searching = false;
	delete p;
}

bool operator<(const QVector<int>&x, const QVector<int>&y){
	if(x.size()!=y.size())return x.size()<y.size();
	for(int i=0; i<x.size(); i++)
		if(x[i]!=y[i])return x[i]<y[i];
	return false;
}
const int dlinaS = 589;
void MainWindow::saveAllProblems(QString dirname){
	if(dirname.isEmpty()) dirname = QFileDialog::getExistingDirectory();
	if(dirname.isEmpty())return;
	QProgressBar* p = new QProgressBar;
	QLabel *label = new QLabel;
	ui->statusBar->addPermanentWidget(label);
	ui->statusBar->addPermanentWidget(p);
	QDateTime t = QDateTime::currentDateTime(), t0;
	int r = 0;
	LosInfoBlock &prob = infoBlocks[1];
	QVector<LosInfoNode*>& N = prob.getNodes();
	QMap<QVector<int>, QString> files;
	int n = N.size();
	label->setText("processing problems ");
	foreach(LosInfoNode *pn, N){
		LosInfoList *l = dynamic_cast<LosInfoList*>(pn);
		r++;
		if (!l)continue;
		QVector<LosInfoList*> temp, t2; temp << l;
		int depth = 1;
		foreach(const LosInfoList::Label &lb, l->labels){
			if(lb.term.isSymbol(dlinaS) && lb.refs.size()){
				LosLogicTerminal *t = dynamic_cast<LosLogicTerminal*>(lb.refs[0]);
				if(t && t->terms.size() && t->terms[0].isSymbol()){
					depth = t->terms[0].data[0].val - 260;
					if(depth>9||depth<=0)continue;
					break;
				}
			}
		}
		while(depth--){
			t2.clear();
			foreach(LosInfoList *t, temp){
				foreach(const LosInfoList::Label &lb, t->labels){
					if(lb.term.isSymbol() && size_t(lb.term.data[0].val-260)<=9 && lb.refs.size()){
						LosInfoList *ll = dynamic_cast<LosInfoList*>(lb.refs[0]);
						if(ll)t2<<ll;
					}
				}
			}
			temp.swap(t2);
		}
		foreach(LosInfoList *t, temp){
			QVector<int> a;
			QString str = convertProblemNode(t, s.symbolNames(), a);
			files[a] += str;
		}
		t0 = QDateTime::currentDateTime();
		if(t.msecsTo(t0)>10){
			p->setValue(int(r*50./n));
			QApplication::processEvents();
			t = QDateTime::currentDateTime();
		}
	}
	QDir dir(dirname);
	dir.mkdir("problems");
	dir.cd("problems");
	n = files.size();
	r=0;
	label->setText("save problems ");
	for(QMap<QVector<int>, QString>::Iterator it = files.begin(); it!=files.end(); ++it){
		QDir d(dir);
		QString path;
		for(int i=0; i < it.key().size()-1; i++)
			path+=QString::number(it.key()[i])+"/";
		d.mkpath(path);
		QString filename = it.key().empty() ? QString("other.pr") : QString::number(it.key().last())+".pr";
		d.cd(path);
		QFile f(d.path()+'/'+filename);
		if(!f.open(QFile::WriteOnly))continue;
		f.write(it->toLocal8Bit());
		f.close();
		r++;
		t0 = QDateTime::currentDateTime();
		if(t.msecsTo(t0)>10){
			p->setValue(50+int(r*50./n));
			QApplication::processEvents();
			t = QDateTime::currentDateTime();
		}
	}
	delete label;
	delete p;
}

void MainWindow::onHRef(const QUrl &u){
	QString str = u.toString();//ui->textBrowser->textCursor().selectedText();
	if(s.symbolNames().contains(str)){
		QList<QListWidgetItem*> l = ui->listWidget->findItems(str, Qt::MatchExactly);
		if(l.isEmpty())return;
		if(l.size()>=2)qDebug()<<"more than one equal strings "<<str;
		ui->listWidget->setCurrentItem(l.first());
	}
}
void MainWindow::onTextDblClk(){
	QTextCursor cc = ui->decoded->textCursor();
	//cc.movePosition(QTextCursor::StartOfWord);
	cc.select(QTextCursor::WordUnderCursor);
	QString str = cc.selectedText();
	if(s.symbolNames().contains(str)){
		QList<QListWidgetItem*> l = ui->listWidget->findItems(str, Qt::MatchExactly);
		if(l.isEmpty())return;
		if(l.size()>=2)qDebug()<<"more than one equal strings "<<str;
		ui->listWidget->setCurrentItem(l.first());
	}else cc.clearSelection();
}
void MainWindow::onHelp(QTextEdit *snd){
//	qDebug()<<"1";
	if (!snd)snd = ui->decoded;
	QString str;
	QTextCursor cc = snd->textCursor();
	if (!cc.selectedText().isEmpty())
		str = snd->textCursor().selectedText();
	else {
		if (snd == ui->ruleBrowser)return;
		cc.select(QTextCursor::WordUnderCursor);
		str = cc.selectedText();
	}
	int i = s.symbolNames().indexOf(str);
	//qDebug()<<i;
	if(i>=0){
		ui->helpBrowser->setNode(help.node(i));
		ui->helpDock->show();
		ui->decoded->setTextCursor(cc);
	}else cc.clearSelection();
}

void MainWindow::showCurrentInfo(){
	if(ui->showInfo->isChecked() && currSymbol>0){
		uint n = s.nameToSymbol(ui->listWidget->currentItem()->text());
		ui->helpBrowser->setNode(help.node(n));
		ui->helpDock->show();
	}
	uint n = s.nameToSymbol(ui->listWidget->currentItem()->text());
	int i = ui->comboBox->currentIndex();
	if(i>=0 && i<infoBlocks.size())
		ui->priemWidget->setNode(infoBlocks[i].node(n));
}

void MainWindow::findRules() {
	RuleFilter f;
	QMap<QString, int> nm;
	auto nnm = s.symbolNames();
	if (rules.empty())loadRules();
	updateRuleIndex();
	for (int i = 1; i < nnm.size(); i++)
		nm[nnm[i]] = i;
	for (auto it = initialNames.begin(); it != initialNames.end();++it)
		nm[it.key()] = it.value();
	QString r = f.read(nm, ui->lineRule->text());
	ui->ruleBrowser->clear();
	QTextCharFormat fmt;
	if (!r.isEmpty()) {
		fmt.setForeground(Qt::red);
		ui->ruleBrowser->setCurrentCharFormat(fmt);
		ui->ruleBrowser->append(r);
		return;
		//ui->statusBar->showMessage(r); return;
	}
	QVector<decltype(rules.begin())> resKeys;
	for (auto it = rules.begin(); it != rules.end(); ++it) {
		if (f(it.value())) {
			resKeys.append(it);
			if (ui->ruleCount->value() == resKeys.size())
				break;
		}
	}
	ui->ruleBrowser->setCurrentCharFormat(fmt);
	int i = 0;
	QString str;
	for (auto it : resKeys) {
		i++;
		//ui->ruleBrowser->append("<hr> </hr><br>");
		QString link;
		if (ruleNodes.contains(it.key())) {
			link = it.key().join('/');// QString::fromLocal8Bit("<a href=\"%1\" style=color:blue >�������� �������� �� ����</a>").arg(it.key().join('/'));
			//ui->ruleBrowser->append(link);
		}
		it.value().writeHTML(i, str, &s.symbolNames(), link);
	}
	ui->ruleBrowser->append(str);
	QTextCursor cc = ui->ruleBrowser->textCursor();
	cc.setPosition(0);
	ui->ruleBrowser->setTextCursor(cc);
	//ui->ruleBrowser->
}

void MainWindow::onRuleURL(const QUrl &url) {
	QStringList k = url.toString().split('/', QString::SkipEmptyParts);
	QString str;
	auto ptr = ruleNodes.value(k, {});
	if (ptr.empty()) {
		ui->statusBar->showMessage("cannot find node for "+url.toString());
		return;
	}

	NameInfo info;
	info.format = NameInfo::PlainText;
	info.operatorNames = info.symbolNames = &s.symbolNames();
	info.rules = &rules;
	for (auto p : ptr) {
		p->printUpper(str, info);
	}
	ui->decoded->setText(str);
}

void MainWindow::startSearch(QString s0, Qt::MatchFlags f){
	if(!stop_search){ //if startSearch called when previous search in progress
		restart_search = true;
		return;
	}
	//	searching = true;
	do{
		restart_search = false;
		stop_search = true;
		if(s0.isEmpty())s0 = ui->findedit->text();
		//while(s0.length()&&s0[0].isSpace())s0.remove(0,1);
		//while(s0.length()&&s0[s0.length()-1].isSpace())s0.chop(1);
		if(s0.isEmpty()){
			ui->statusBar->showMessage("find : empty string");
			return;
		}
		f = this->search_flags;// ui->progsearch->flags();
		QRegExp re(s0);
		if(f & Qt::MatchRegExp){
			if(!re.isValid()){
				ui->statusBar->showMessage("error: " + re.errorString());
				return;
			}
		}
		stop_search = false;
		const QVector<QString> &sn = s.symbolNames();
		int i, n, r, pos;
		for(i = n = 0; i<sn.size(); i++){
			if(!sn[i].isEmpty())n++;
		}
		ui->findRes->clear();
		if(!n)return;
		QString str;
		QProgressBar* p = new QProgressBar;
		ui->statusBar->addPermanentWidget(p);
		QDateTime t = QDateTime::currentDateTime(), t0;
		for(i = r = 0; i<sn.size(); i++){
			if(sn[i].isEmpty())continue;
			if(progtree.decodeFragment(s.program(i))){
				NameInfo info; info.format = NameInfo::SymbolicUnicode;
				info.operatorNames = info.symbolNames = &s.symbolNames();
				str.clear();
				progtree.print(str, info);
				int k=0;
				for(pos = 0; pos>=0; pos++){
					if(f&Qt::MatchRegExp)pos = str.indexOf(re, pos);
					else {
						if(f&Qt::MatchCaseSensitive)
							pos = str.indexOf(s0, pos, Qt::CaseSensitive);
						else
							pos = str.indexOf(s0, pos, Qt::CaseInsensitive);
					}
					if(pos<0)break;
					if(f & Qt::MatchFixedString){
						if(pos && str[pos-1].category()==str[pos].category() && str[pos].isLetterOrNumber())
							continue;
						if(pos+s0.length() < str.length()&&
								str[pos+s0.length()].category() == str[pos+s0.length()-1].category() &&
								str[pos+s0.length()].isLetterOrNumber())
							continue;
					}
					k++;
				}
				if(k){
					QListWidgetItem *item = new QListWidgetItem(sn[i]+" ("+ QString::number(k) + (k> 1 ? " occurences)" : " occurence)"), ui->findRes);
					item->setData(Qt::UserRole, i);
					item->setData(Qt::UserRole+1, s0);
					item->setData(Qt::UserRole+2, int(f));
					ui->findRes->addItem(item);
				}
			}
			r++;
			t0 = QDateTime::currentDateTime();
			if(t.msecsTo(t0)>10){
				p->setValue(int(r*100./n));
				QApplication::processEvents();
				t = QDateTime::currentDateTime();
			}
			if(stop_search || restart_search)break;
		}
		//	searching = false;
		delete p;
		s0 = QString();
	}while(!stop_search && restart_search);
	stop_search = true;
}

void MainWindow::stopSearch(){
	stop_search = true;
}

struct LICmp{
	bool operator()(QListWidgetItem *x, QListWidgetItem* y)const{
		return x->listWidget()->row(x)<y->listWidget()->row(y);
	}
};
bool MainWindow::find(){
	ui->statusBar->clearMessage();
	{
		if(!found.isEmpty())return true;
		//if(!ui->decoded==QAppli)
		QString t = ui->findedit->text();
		switch(search_flags^(search_flags&Qt::MatchCaseSensitive)){
		case Qt::MatchExactly:
		case Qt::MatchFixedString:
		case Qt::MatchRegExp:
		case Qt::MatchEndsWith:
			//t+=QChar(0);
		default:break;
		}
		if(t.isEmpty())return false;
		found = ui->listWidget->findItems(t, search_flags);
		qSort(found.begin(),found.end(),LICmp());
		return !found.isEmpty();
	}
}

void MainWindow::findFirst(){
	if(this->sender()==ui->progsearch){
		findFirstProg();
		//if(!progfound.isEmpty())return true;
	} else {
		if(!find()){
			ui->statusBar->showMessage(ui->findedit->text().isEmpty() ? "empty search request" : "no logic symbols found");
			return;
		}
		ui->listWidget->setCurrentItem(found.first());
	}
}

void MainWindow::findFirstProg(int mf, bool fromStart, bool backw){
	QTextDocument::FindFlags f;
	if(mf==-1)mf = ui->progsearch->flags();
	int f0 = mf & Qt::MatchCaseSensitive;
	if(f0)f |= QTextDocument::FindCaseSensitively;
	if((mf^f0) == (int)Qt::MatchFixedString)
		f|=QTextDocument::FindWholeWords;
	if(backw)f|=QTextDocument::FindBackward;
	if(fromStart)ui->decoded->textCursor().setPosition(0);//document()->cursorPositionChanged();
	ui->decoded->find(ui->progsearch->text(), f);
}

void MainWindow::findNext(){
	if(this->sender()==ui->progsearch){
		findFirstProg(-1, false, false);
	}else{
		if(!ui->listWidget->currentItem())return findFirst();
		if(!find())return;
		QList<QListWidgetItem*>::Iterator it = qUpperBound(found.begin(),found.end(),ui->listWidget->currentItem(), LICmp());
		if(it!=found.end() && (*it == ui->listWidget->currentItem()))++it;
		if(it==found.end()){
			it = found.begin();
			ui->statusBar->showMessage("there are no more occurences; searching from start");
		}
		ui->listWidget->setCurrentItem(*it);
	}
}
void MainWindow::findPrevious(){
	if(sender()==ui->progsearch){
		//if(!progfound.isEmpty())return true;
		findFirstProg(-1, false, true);
	}else{
		if(!ui->listWidget->currentItem())return findFirst();
		if(!find())return;
		QList<QListWidgetItem*>::Iterator it = qLowerBound(found.begin(),found.end(),ui->listWidget->currentItem(), LICmp());
		if(*it == ui->listWidget->currentItem()){
			if(it==found.begin()){
				it = found.end();
				ui->statusBar->showMessage("there are no previous occurences; searching from end");
			}
			--it;
		}
		ui->listWidget->setCurrentItem(*it);
	}
}

void MainWindow::clearSelection(){
	if(sender()==ui->progsearch)progfound.clear();
	else found.clear();
}

void MainWindow::setSearchCaseSensitive(bool cs){
	if(cs == ((search_flags & Qt::MatchCaseSensitive)!=0))return;
	found.clear();
	search_flags^=Qt::MatchCaseSensitive;
	//qDebug()<<search_flags;
}
void MainWindow::setSearchContains(){
	//if(search_flags &0xF == Qt::MatchContains)return;
	found.clear();
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchContains;
	//qDebug()<<search_flags;
}
void MainWindow::setSearchWhole(){
	//if(search_flags &0xF == Qt::MatchFixedString)return;
	found.clear();
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchFixedString;
	//qDebug()<<search_flags;
}
void MainWindow::setSearchRegExp(){
	//if(search_flags &0xF == Qt::MatchRegExp)return;
	found.clear();
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchRegExp;
	//qDebug()<<search_flags;
}
void MainWindow::setSearchPrefix(){
	//if(search_flags &0xF == Qt::MatchStartsWith)return;
	found.clear();
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchStartsWith;
	//qDebug()<<search_flags;
}
void MainWindow::setSearchSuffix(){
	//if(search_flags &0xF == Qt::MatchEndsWith)return;
	found.clear();
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchEndsWith;
	//qDebug()<<search_flags;
}
bool hasP(QWidget *w, QWidget *p){
	while(w){
		if(w==p)return true;
		w = w->parentWidget();
	}
	return false;
}
void MainWindow::back(){
	if(hasP(QApplication::focusWidget(), ui->helpDock)){
		ui->helpBrowser->backward();
		return;
	}
	if(fb.c<=1)return;
	fb.c--;
	if(fb.st[fb.c-1].sym != fb.st[fb.c].sym){
		if(s.symbolNames().contains(fb.st[fb.c-1].sym)){
			QList<QListWidgetItem*> l = ui->listWidget->findItems(fb.st[fb.c-1].sym, Qt::MatchExactly);
			if(l.isEmpty())return;
	//		if(l.size()>=2)qDebug()<<"more than one equal strings "<<str;
			ui->listWidget->blockSignals(true);
			ui->listWidget->setCurrentItem(l.first());
			ui->listWidget->blockSignals(false);
			fb.c++;
			showProgram(l.first(), 0, false);
			fb.c--;
		}else {fb.c++; return;}
	}
	QTextCursor cc = ui->decoded->textCursor();
	cc.setPosition(fb.st[fb.c-1].pos);
	ui->decoded->setTextCursor(cc);
}
void MainWindow::forward(){
	if(hasP(QApplication::focusWidget(), ui->helpDock)){
		ui->helpBrowser->forward();
		return;
	}
	if(fb.c>=fb.st.size())return;
	if(fb.st[fb.c-1].sym != fb.st[fb.c].sym){
		if(s.symbolNames().contains(fb.st[fb.c].sym)){
			QList<QListWidgetItem*> l = ui->listWidget->findItems(fb.st[fb.c].sym, Qt::MatchExactly);
			if(l.isEmpty())return;
	//		if(l.size()>=2)qDebug()<<"more than one equal strings "<<str;
			ui->listWidget->blockSignals(true);
			ui->listWidget->setCurrentItem(l.first());
			ui->listWidget->blockSignals(false);
			showProgram(l.first(), 0, false);
			fb.c++;
		}else return;
	}
	QTextCursor cc = ui->decoded->textCursor();
	cc.setPosition(fb.st[fb.c-1].pos);
	ui->decoded->setTextCursor(cc);
}

void MainWindow::closeEvent(QCloseEvent *){
	stopSearch();
	foreach(const QString &str, transliterated){
		if(currTr[str]==transliterate(str))
			currTr.remove(str);
	}
	s.setTranslations(currTr, "rus", "eng");
	s.saveTranslations("eng.tr");
	writeSettings();
/*	if(s.namesModified()){
		switch(QMessageBox::warning(0, "", "Do you want to save changes of symbol names",
							 QMessageBox::Yes, QMessageBox::No, QMessageBox::Cancel)){
		case QMessageBox::Yes: saveNames(); break;
		case QMessageBox::No: break;
		default:
			e->ignore();
		}
	}*/
}

enum LosItems{
	LosKeyWord,
	LosInternalSymbol,
	LosSymbol,
	LosAuxSym,
	LosRecBr,
	LosEnumerator
};

bool splitInfo(LosInfoNode *nn, QStringList &res){
	LosDataTerminal *n = dynamic_cast<LosDataTerminal*>(nn);
	if(!n)return false;
	QString str = n->data.toString();
	QString sn;
	int i, j;
	for(i=str.indexOf(">")+1; i<str.size(); ++i){
		if(str[i].isLetterOrNumber())break;
	}
	for(j=i; j<str.size(); ++j){
		if(!str[j].isLetterOrNumber())break;
	}
	sn = str.mid(i, j-i);
	res<<sn;
	i = str.indexOf("<hr>");
	if(i>=0)str.remove(0, i+4);
	int pi=0;
	for(i=0; i<str.length(); i++){
		i = str.indexOf("<",i);
		if(i<0){
			res << str.mid(pi, str.length()-pi);
			break;
		}
		QStringRef sr(&str, i, str.length()-i);
		if(sr.startsWith("<ol>")){
			i = sr.indexOf("</ol>", i+4);
			if(i<0)i=str.length()-2;
		}else if(sr.startsWith("<li>")){
			if(!pi){pi=i+4;continue;}
			QStringRef item(&str, pi, i-pi);
			for(j = 0; j<item.size(); j++){
				if(!item.at(j).isLetter()){
					res.append(item.toString());
					break;
				}
			}
			pi = i+4;
		}
	}
	return true;
}

bool programInfo(QStringList &l, QString &simple,
			 QMap<QString, QString> &calls,
			 QString &callIndex){
	calls.clear();
	simple.clear();
	callIndex.clear();
	if(l.size()<2)return false;
	const QString &s0 = l[0];
	QString callind_pref = QString::fromLocal8Bit("&#8212; ������ ���������");
	for(int i = 1; i < l.size(); i++){
		QString &s = l[i];
		int j;
		for(j=0; j<s.length(); j++){
			if(!s[j].isSpace())break;
		}
		if(!s.midRef(j).startsWith(s0))continue;
		j+=s0.length();
		for(; j<s.length(); j++)
			if(!s[j].isSpace())break;
		if(j == s.length())continue;
		if(s[j] == '.'){
			if(simple.isEmpty())simple = "<h4>"+s0+"</h4><hr>";
			else simple+="<br><br>";
			simple += s.midRef(j+1);
			continue;
		}else if(s[j] == '('){
			for(j++; j<s.length(); j++){
				if(!s[j].isSpace())break;
			}
			int k = j;
			for(;j<s.length(); j++){
				if(!s[j].isLetterOrNumber())break;
			}
			QString sm = s.mid(k,j-k);
			int h = s.indexOf(')', j);
			if(h<0)continue;
			h++;
			for(j=h; j<s.length(); j++)
				if(!s[j].isSpace())break;
			QStringRef mr = s.midRef(j);
			if(mr.startsWith("&#8212;")) j += 7;
			else if(mr.startsWith('.')) j++;
			QString *ps = 0;
			if(sm.length() && sm[0].isLetterOrNumber() && !QRegExp("\\w[0-9]").exactMatch(sm)){
				ps = &calls[sm];
			}else ps = &calls[QString()];
			if(ps->isEmpty()){
				*ps = "<h4>" + s.left(h) + "</h4><hr>";
				//else *ps += "<br><br>";
				*ps += s.midRef(j);
			}
	//		(*ps)[0] = (*ps)[0].toUpper();
		}else if(s.midRef(j).startsWith(callind_pref)){
			if(callIndex.isEmpty())callIndex = "<h4>" + s0 + "</h4><hr>";
			//else callIndex += "<br><br>";
			QStringRef mr = s.midRef(j+8);
//			mr[0] = mr[0].toUpper();
			callIndex += mr;
		}
	}
	if(simple.isEmpty() && l.size()==2){
		simple = "<h4>"+s0+"</h4><hr>"+l[1];
	}
	return true;
}

QString convertToTooltip(LosInfoNode *nn, const QString &sn, const QString &nm){
	LosDataTerminal *n = dynamic_cast<LosDataTerminal*>(nn);
	if(n){
		QString str = n->data.toString();
		int pos = str.indexOf("<li>");
		if(pos<0)pos = str.indexOf("<hr>");
		if(pos>=0){
			int p1 = pos;
			for(;;){
				p1 = str.indexOf("<li>", pos+4);
				if(p1<0 || p1>pos+4)break;
				pos = p1;
			}
			if(p1<0) {
				p1 = str.indexOf("</ul>", pos+4);
			}
			if(p1<0){
				str = str.mid(pos);
			}
			else str = str.mid(pos, p1-pos);
			str[1] = 'p'; str[2] = '>'; str[3] = ' ';
			str.append("</p>");
			int p = 4;
			for(;p<str.length(); p++)
				if(str[p].isLetterOrNumber())break;
			if(!nm.isEmpty()){
				str.replace(0,4,"<h4>"+nm+"("+sn+")</h4><hr>");
			}else{
				if(QStringRef(&str, p, sn.size())==sn){
					int p2 = str.indexOf('.'), l=1;
					int p3 = str.indexOf("&#8212;");
					if(p3>0 && p3<p2){p2=p3; l=7;}
					if(p2-p-sn.size() < 50 && p2>0){
						str.replace(p2,l,"</h4><hr>");
						str.replace(0,4,"<h4>");
					}else str.append("</p>");
				}else{
					str.replace(0, 4, "<h4>"+sn+"</h4><hr>");
				}
			}
		}
		return str;
	}else return QString();
}

void MainWindow::setupHighlighter(){
	if (hl)delete hl;
	hl = new LosHighlighter(ui->decoded);
	const QVector<QString> & sn = s.symbolNames();
	int i, l =sn.size();
	for(i=0; i<l; i++){
		if(sn[i].isEmpty())continue;
		if(sn[i]==",")continue;
		if(i<=281)hl->setWordType(sn[i], LosInternalSymbol);
		else hl->setWordType(sn[i], LosSymbol);
		QStringList l;
		if(!splitInfo(help.node(i), l))continue;
		QMap<QString,QString> calls;
		QString simple, callIndex;
		if(!programInfo(l, simple, calls, callIndex))continue;
		hl->setToolTips(sn[i], simple, calls, callIndex);
		//QList<QListWidgetItem*> I = ui->listWidget->findItems(sn[i],Qt::MatchExactly);
		/*if(!I.isEmpty()){
			I[0]->setToolTip(simple);
		}*/
	}
	QStringList sl;
	QStringList aux, rec;
	sl<<"if"<<"else"<<"then"<<"return"<<"where"<<"result"<<QString(Unicode::Forall)<<QString(Unicode::Exists)<<
		QString(Unicode::LogicAnd)<<QString(Unicode::LogicOr)<<
		QString(Unicode::NotSign)<<"&"<<"|"<<"!"<<"forall"<<"exists"<<"break"<<"continue"<<
		"solve"<<"operator"<<"function"<<"default"<<"info"<<"in"<<"from"<<"foreach"<<"Exists"<<
		"branch"<<"if_false";
	aux<<QString(Unicode::NotEqual)<<QString::fromLocal8Bit("����")<<
		 QString::fromLocal8Bit("��")<<QString::fromLocal8Bit("for")<<
		 QString(Unicode::NotElementOf)<<
		 QString(Unicode::DoesntExist)<<":"<<"?"<<"["<<"]"<<
		 QString(Unicode::NotElementOf);
	rec<<"[<"<<">]";
	QStringList sym;
	sym << QString(Unicode::EqualByDef)<<QString(Unicode::LeftArrowWithTail)<<
			QString(Unicode::DotPlus)<<QString(Unicode::DotMinus)<<
			QString(Unicode::DotLessEqual)<<QString(Unicode::DotSubset)<<
			QString(Unicode::DotLess)<<QString(Unicode::DotIntersection)<<
			QString(Unicode::ElementOf)<<
			"="<<"::"<<"*"<<"+"<<"-"<<"/"<<"<"<<">"<<"<="<<">=";
	QStringList enumer;
	if(sn.size()>=300){
		sl<<sn[241]<<sn[242]<<sn[17]<<sn[18]<<sn[19]<<sn[20]<<sn[27]<<
			sn[29]<<sn[5]<<sn[6]<<sn[7]<<sn[8]<<sn[9]<<
			sn[10]<<sn[2]<<sn[22]<<sn[56];
		aux<<sn[281];
		enumer << sn[16] << sn[15] << sn[14] << "countof";
	}
	foreach(const QString &s, sl)
		hl->setWordType(s, LosKeyWord);
	foreach(const QString &s, aux)
		hl->setWordType(s, LosAuxSym);
	foreach(const QString &s, rec)
		hl->setWordType(s, LosRecBr);
	foreach(const QString &s, sym)
		hl->setWordType(s, LosInternalSymbol);
	foreach(const QString &s, enumer)
		hl->setWordType(s, LosEnumerator);
/*	QFontMetrics m(ui->decoded->font());
	QRadialGradient grad(0.5,0.5,0.5,0.2,0.2);//m.height());
//	grad.setSpread(QGradient::ReflectSpread);
	grad.setColorAt(0, QColor(0,200,100));
	grad.setColorAt(0.5, QColor(0,0,200));
	grad.setColorAt(0.2, QColor(0,0,100));
	grad.setColorAt(0.35, QColor(0,0,255));
	grad.setColorAt(0.5, QColor(0,50,255));
	grad.setColorAt(0.65, QColor(0,0,255));
	grad.setColorAt(0.8, QColor(0,0,100));
	grad.setColorAt(1, Qt::black);
	grad.setCoordinateMode(QGradient::ObjectBoundingMode);// */
/*	QBrush wood(QPixmap("images/wood1.png"));
	QTextCharFormat * ci = hl->namedCharFormat("comment");
	if(ci)ci->setBackground(wood); // */
	hl->charFormat(LosEnumerator).setForeground(QColor(130,0,0));
	hl->charFormat(LosAuxSym).setForeground(QColor(0,100,0));
	hl->charFormat(LosKeyWord).setForeground(QColor(0,0,255));
	hl->charFormat(LosInternalSymbol).setForeground(QColor(100,0,150));
	hl->charFormat(LosSymbol).setForeground(QColor(0,100,150));
	hl->charFormat(LosRecBr).setForeground(QColor(0,100,0));
	hl->charFormat(LosRecBr).setFontWeight(QFont::Bold);
}
