#include "losinfoblock.h"
#include <QFile>

LosInfoBlock::LosInfoBlock()
{
}

bool LosDataTerminal::toHtml(QString &res, const QVector<QString> &) const{
	switch(data.type()){
	case QVariant::String: res = data.toString();
		break;
	case QVariant::ByteArray: res = QString::fromLocal8Bit(data.toByteArray().constData());
		break;
	case QVariant::Image:
	default: return false;
	}
	return true;
}

bool LosDataTerminal::toString(QString &res, const QVector<QString> &, const char *) const{
	switch (data.type()){
	case QVariant::String: res = data.toString();
		break;
	case QVariant::ByteArray: res = QString::fromLocal8Bit(data.toByteArray().constData());
		break;
	case QVariant::Image:
	default: return false;
	}
	return true;
}



bool LosLogicTerminal::toHtml(QString &res, const QVector<QString> &sn) const{
	if(terms.size()==1){
		terms[0].toString(res, sn);
		return true;
	}
	res+="<ol>\n";
	foreach(const LosTerm& t, terms){
		QString str;
		t.toString(str, sn);
		res += "<li>"+str+"</li>";
	}
	res+="</ol>\n";
	return true;
}

bool LosLogicTerminal::toString(QString &res, const QVector<QString> &sn, const char *sep) const{
	foreach(const LosTerm& t, terms){
		QString str;
		t.toString(str, sn, sep);
		res += str;
		if (str != "") res += ";";
	}
	return true;
}


union _InfoData{
	struct{
		uint used_refs    : 12;
		uint unused        : 1;
		uint is_term_logic : 1;
		uint is_invalid    : 1;
		uint is_terminal   : 1;
	};
	quint16 mask;
};

LosInfoNode *LosInfoNode::read(ByteStream &bs, QMap<int, LosInfoNode *> &n){
	if(n.contains(bs.ptr))return n[bs.ptr];
	_InfoData sh;
	int len, p0 = bs.ptr;
	for(;;){
		len = bs.read2bytes();
		//bs.read3bytes();
		sh.mask = bs.read2bytes();
		if(!sh.is_invalid)break;
		bs.ptr = bs.read3bytes();
		//invalid object: <3 zero bytes>, A(2 bytes), <pointer to vaild object: 3 bytes>
		if(bs.read_err)return 0;
	}
	LosInfoNode *res = 0;
	if(!sh.is_terminal)res = read_list(sh.used_refs, p0, bs, n);
	else {
		bs.len = bs.ptr-4+len;
		if(sh.is_term_logic)res = read_log_term(bs);
		else res = read_text_term(bs);
		if (res) res->_used++;
	}
	if (res) {
		n[p0] = res;
	}
	return res;
}
union _InfoTermSymbol{
	uchar mask;
	struct{
		uint sym_high  : 5;
		uint left_par  : 1;
		uint right_par : 1;
		uint is_var    : 1;
	};
};
bool LosTerm::match_m(const LosTerm&t, QMap<int, LosTerm> &m)const {
	if (t.data[0].isSymbol() && t.data[0].val < 0) {
		if (t.data.size() == 1)return true;
		else if (data.size() <= 1)return false;
		else if (t.data.size() == 3)return true;
	} else if ((t.data.size() == 1) != (data.size() == 1))return false;
	else if (!t.data[0].isSymbol()) {
		auto it = m.find(t.data[0].val);
		if (it != m.end())return *this == it.value();
		m[t.data[0].val] = *this;
		return true;
	} else if (t.data[0].cmp != data[0].cmp)return false;
	else if (t.data.size() == 3)return true;
	//else if (!t.data[0].isSymbol())return true;
	auto args = subterms();
	auto targs = t.subterms();
	if (args.size() != targs.size())return false;
	for (int i = 0; i < args.size(); i++)
		if (!args[i].match_m(targs[i], m))return false;
	return true;
}
static QSet<int> filterIgnore = {192/*���*/, 961/*��������*/};
bool TermCond::operator()(const LosTerm &t)const {
	if (exact)return t.match(pattern);
	if (root)return t.data[0].isSymbol() && pattern.data[0].val == t.data[0].val;
	if (subterm) {
		if (pattern == t)return true;
		for (auto &s : t.subterms())
			if ((*this)(s))return true;
		return false;
	}
	if (t.match(pattern))return true;
	if (t.data.size() <= 3)return false;
	if (filterIgnore.contains(t.data[0].val))return false;
	for (auto &s : t.subterms())
		if ((*this)(s))return true;
	return false;
}

bool readTerm(ByteStream &bs, LosTerm &res){
	res.data.clear();
	int pth = 0;
	do{
		_InfoTermSymbol s;
		quint32 sym = bs.read2bytes();
		s.mask = bs.readbyte();
		sym += s.sym_high << 16;
		if(sym)res.data<<LosTerm::Item(s.is_var ? LosTerm::Item::Var :
													 LosTerm::Item::Sym, sym);
		if(s.left_par){
			res.data<<LosTerm::Item::LeftP;
			pth++;
		}else if(s.right_par){
			res.data<<LosTerm::Item::RightP;
			pth--;
		}
	}while(pth && !bs.read_err);
	return !bs.read_err;
}

LosInfoNode *LosInfoNode::read_list(int used_refs, int p0, ByteStream &bs, QMap<int, LosInfoNode *> &n){
	//QVector<LosInfoList::Label> labels;
	LosInfoList* res = new LosInfoList;
	n[p0] = res;
	res->_used++;
	int sz0 = bs.len;
	for(int i=0, j=0; i<used_refs; j++){
		bs.len = sz0;
		int sz = bs.read2bytes();
		i+=2+sz;
		int p0 = bs.ptr+sz;
		bs.len = p0;
		if(bs.read_err || p0>sz0) return 0;
		res->labels.push_back(LosInfoList::Label());
		LosInfoList::Label& l = res->labels.back();
		if(!readTerm(bs, l.term))return 0;
		for(;;){
			quint32 ptr = bs.read3bytes();
			if(bs.read_err){bs.start(bs.len); break;}
			ByteStream bs1(bs); bs1.len = sz0; bs1.start(ptr);
			LosInfoNode *nn = read(bs1, n);
			if (!nn) {
				return 0;
			}
			nn->_used++;
			l.refs << nn;
		}
	}
	//LosInfoList *res = new LosInfoList;
	//res->labels.swap(labels);
	return res;
}

LosInfoNode *LosInfoNode::read_text_term(ByteStream &bs){
	if(bs.len<bs.ptr)return 0;
	LosDataTerminal *t = new LosDataTerminal;
	QString s;
	convertRusToUnicode((char*)(bs.d+bs.ptr), s, bs.len-bs.ptr);
	t->data = s;
	return t;
}

LosInfoNode *LosInfoNode::read_log_term(ByteStream &bs){
	QVector<LosTerm> terms;
	while(bs.ptr<bs.len){
		terms.push_back(LosTerm());
		if(!readTerm(bs, terms.back()))return 0;
	}
	LosLogicTerminal *res = new LosLogicTerminal;
	res->terms.swap(terms);
	return res;
}

bool LosInfoBlock::read(const QString &path, OutputContext c){
	clear();
	if(loadLosIndex(path+"000.los", catalog, c))return false;
	QMap<uint, QVector<uint> > mp;
	uint i, n = catalog.size();
	for(i=0; i<n; i++){
		if(!catalog[i].valid)continue;
		mp[catalog[i].num]<<i;
	}
	nodes.resize(catalog.size());
	nodes.fill(0);
	int ok = 0, failed = 0;
	for(QMap<uint, QVector<uint> >::Iterator it = mp.begin(); it!=mp.end(); ++it){
		QString fname = path+number7(it.key())+".los";
		QFile f(fname);
		if(!f.open(QFile::ReadOnly))c.errors()<<"error: cannot open file L"<<number7(it.key())<<".los\n";
		QByteArray ba = f.readAll();
		f.close();
		QMap<int, LosInfoNode *> nd;
		foreach(uint j, it.value()){
			ByteStream bs; bs.d = (uchar*)ba.data(); bs.len = ba.size(); bs.ptr = catalog[j].pos;
			if(!(nodes[j] = LosInfoNode::read(bs, nd))){
				c.log()<<"cannot load information for symbol "<<j<<"\n";
				failed++;
			}else{
				nodes[j]->_used++;
				ok++;
			}
		}
		foreach(LosInfoNode* nn, nd) {
			nn->del();
		}
	}
	c.log()<<"information for "<<ok<<" symbols loaded successful, "<<failed<<" failed\n";
	return true;

}
