#ifndef LOSHIGHLIGHTER_H
#define LOSHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QHash>
#include <QTimer>
#include <QTextCursor>
#include <QTextEdit>

class LosHlFilter : public QObject{
	Q_OBJECT
	QTextEdit *br;
	QTextCursor c;
	QPoint tpos;
	QString tt;
	QTimer tm;
	int ttdelay;
private slots:
	void onTimer();

protected:
	void showT(QPoint pos, const QString &t){
		tt = t;
		tpos = pos;
		if(!t.isEmpty())
			tm.start(ttdelay);
	}
	void hideT();
	bool eventFilter(QObject *, QEvent *e);
public:
	LosHlFilter(QTextEdit *b);
};

class LosHighlighter : public QSyntaxHighlighter{
	Q_OBJECT
	struct _tooltipinfo{
		QString simple, callIndex;
		QMap<QString,QString> calls;
	};
	QHash<QString, int> spec;
	QHash<QString, int> kw;
	QTextCharFormat def_cf;
	QHash<int, QTextCharFormat> formats;
	QTextCharFormat number, numbers, numberd, par, index, comment, info_comment, call_ind;
	QMap<QString, QTextCharFormat*> fixed_formats;
	QHash<QString, _tooltipinfo> tooltips;
public:
	explicit LosHighlighter(QTextEdit *parent = 0);
	~LosHighlighter();
	void highlightBlock(const QString &text);
	void setWordType(const QString &word, int t);
	void setCharFormat(int type, const QTextCharFormat& f);
	void setToolTip(const QString &s, const QString &t);
	void setToolTips(const QString &s, const QString &simple,
					 const QMap<QString,QString>& calls, const QString &ind);
	bool setNamedCharFormat(const char* name, const QTextCharFormat& f);
	QTextCharFormat & charFormat(int type);
	QTextCharFormat *namedCharFormat(const QString &nm);
signals:
	
public slots:
	
};

#endif // LOSHIGHLIGHTER_H
