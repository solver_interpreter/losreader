#include "searchlinewidget.h"
#include "ui_searchlinewidget.h"

SearchLineWidget::SearchLineWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::SearchLineWidget)
{
	ui->setupUi(this);
	search_flags = Qt::MatchExactly;
	QAction *a = 0;//new QAction("case sensitive",ui->findopt);
	ui->findopt->addAction(ui->casesensitive);	ui->casesensitive->setCheckable(true);
	connect(ui->casesensitive, SIGNAL(triggered(bool)), this, SLOT(setSearchCaseSensitive(bool)));

	//search_flags |= Qt::

	QActionGroup *ag = new QActionGroup(this);
	ag->addAction(ui->contains); ag->addAction(ui->regexp);
	ag->addAction(ui->prefix); ag->addAction(ui->suffix);
	ag->addAction(ui->whole);
	ui->findopt->addAction(a = new QAction("",ui->findopt));     a->setSeparator(true);
	ui->findopt->addAction(ui->contains);
	connect(ui->contains,SIGNAL(triggered()),this, SLOT(setSearchContains()));
	ui->findopt->addAction(ui->whole);
	connect(ui->whole,SIGNAL(triggered()),this, SLOT(setSearchWhole()));
	ui->findopt->addAction(ui->regexp);
	connect(ui->regexp,SIGNAL(triggered()),this, SLOT(setSearchRegExp()));
	ui->findopt->addAction(ui->prefix);
	connect(ui->prefix,SIGNAL(triggered()),this, SLOT(setSearchPrefix()));
	ui->findopt->addAction(ui->suffix);
	connect(ui->suffix,SIGNAL(triggered()),this, SLOT(setSearchSuffix()));

	connect(ui->findedit,SIGNAL(textEdited(QString)),this,SIGNAL(searchStringChanged(QString)));
	connect(ui->findedit,SIGNAL(returnPressed()),this,SIGNAL(entered()));
	connect(ui->find,SIGNAL(pressed()),this,SIGNAL(findPressed()));
	connect(ui->findNext,SIGNAL(pressed()),this,SIGNAL(nextPressed()));
	connect(ui->findPrev,SIGNAL(pressed()),this,SIGNAL(prevPressed()));
	setSearchFlags(Qt::MatchExactly);
}

SearchLineWidget::~SearchLineWidget()
{
	delete ui;
}

QString SearchLineWidget::text() const{
	return ui->findedit->text();
}

QLineEdit *SearchLineWidget::edit(){
	return ui->findedit;
}

void SearchLineWidget::setText(const QString &s){
	ui->findedit->setText(s);
}
void SearchLineWidget::setSearchCaseSensitive(bool cs){
	if(cs == ((search_flags & Qt::MatchCaseSensitive)!=0))return;
	search_flags^=Qt::MatchCaseSensitive;
	emit flagsChanged(search_flags);
	//qDebug()<<search_flags;
}
void SearchLineWidget::setSearchContains(){
	//if(search_flags &0xF == Qt::MatchContains)return;
	Qt::MatchFlags f0 = search_flags;
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchContains;
	if(f0!=search_flags)emit flagsChanged(search_flags);
	//qDebug()<<search_flags;
}
void SearchLineWidget::setSearchWhole(){
	//if(search_flags &0xF == Qt::MatchFixedString)return;
	Qt::MatchFlags f0 = search_flags;
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchFixedString;
	if(f0!=search_flags)emit flagsChanged(search_flags);
	//qDebug()<<search_flags;
}
void SearchLineWidget::setSearchRegExp(){
	//if(search_flags &0xF == Qt::MatchRegExp)return;
	Qt::MatchFlags f0 = search_flags;
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchRegExp;
	if(f0!=search_flags)emit flagsChanged(search_flags);
	//qDebug()<<search_flags;
}
void SearchLineWidget::setSearchPrefix(){
	//if(search_flags &0xF == Qt::MatchStartsWith)return;
	Qt::MatchFlags f0 = search_flags;
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchStartsWith;
	if(f0!=search_flags)emit flagsChanged(search_flags);
	//qDebug()<<search_flags;
}
void SearchLineWidget::setSearchSuffix(){
	//if(search_flags &0xF == Qt::MatchEndsWith)return;
	Qt::MatchFlags f0 = search_flags;
	search_flags=(search_flags&Qt::MatchCaseSensitive)|Qt::MatchEndsWith;
	if(f0!=search_flags)emit flagsChanged(search_flags);
	//qDebug()<<search_flags;
}

void SearchLineWidget::setSearchFlags(Qt::MatchFlags f){
	if(f==search_flags)return;
	search_flags = f;
	ui->casesensitive->setChecked((f&Qt::MatchCaseSensitive)!=0);
	switch(search_flags^(search_flags&Qt::MatchCaseSensitive)){
	case Qt::MatchContains:ui->contains->setChecked(true); break;
	case Qt::MatchFixedString:ui->whole->setChecked(true); break;
	case Qt::MatchEndsWith:ui->suffix->setChecked(true); break;
	case Qt::MatchRegExp: ui->regexp->setChecked(true); break;
	case Qt::MatchStartsWith: ui->prefix->setChecked(true); break;
	}
	emit flagsChanged(f);
}
